var Order = {
	init: function() {
		var sCorePath = $('#js_core_path').val();
		$("#js_select_province").select2({
			placeholder: "Chọn",
		});
		$("#js_select_district").select2({
			placeholder: "Chọn",
		});
		$("#js_select_ward").select2({
			placeholder: "Chọn"
		});
		$('#js_select_province').change(function() {
			var sProvinceId = $('#js_select_province').val();
			$.ajax({
				url : sCorePath + '/district/' + sProvinceId,
				type : 'GET',
				success:function(data){
					$("#js_select_ward").empty();
					$("#js_select_district").empty();
					$("#js_select_district").select2({
						placeholder: "Chọn",
						data: JSON.parse(data)
					})
				},
			});
			if (parseInt(sProvinceId) == 79) {
				$('#QUANGAY24H, #GIAONHANH6H').show();
				$('#lien_tinh, #tiet_kiem').hide();
			} else {
				$('#QUANGAY24H, #GIAONHANH6H').hide();
				$('#lien_tinh, #tiet_kiem').show();
			}
		});
		$('#js_select_district').change(function() {
			var sDistrictId = $('#js_select_district').val();
			$.ajax({
				url : sCorePath + '/ward/' + sDistrictId,
				type : 'GET',
				success:function(data){
					$("#js_select_ward").empty();
					$("#js_select_ward").select2({
						placeholder: "Chọn",
						data: JSON.parse(data)
					});
					$('#district_name').val($('#select2-js_select_district-container').text());
				},
			});
		});
	    $('#js_order_date_picker, .search-order-date').datepicker({
			format: 'dd-mm-yyyy',
		});
	    $("#js_receiver_purchase_fee").change(function() {
	    	if ($('#js_postage').prop('checked')) {

	    		$('#js_result_postage').text('Có');
	    	} else {
	    		$('#js_result_postage').text('Không');
	    	}
	    });
		$("#js_select_customer").select2({
			placeholder: "Chọn"
		});
	},
	checkSizeIsInteger: function (ele) {
		if (parseInt($(ele).val()) < 1) {
			$(ele).val('');
		}
	}
};
jQuery(document).ready(function() {
	Order.init();
});