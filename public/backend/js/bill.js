
var Bill = {
	init: function() {
		$("#js_select_charge").select2({
			placeholder: "Chọn người phụ trách"
		});
		$('#js_select_dc').select2({
			placeholder: "Chọn chi nhánh phụ trách"
		});
		var a = moment().subtract(29, "days"),
		t = moment();
		$(".m_daterangepicker_bill_1").daterangepicker({
			buttonClasses: "m-btn btn btn-sm",
			applyClass: "btn-primary btn-sm",
			cancelClass: "btn-secondary btn-sm",
			startDate: a,
			autoUpdateInput:false,
			"showDropdowns": true,
			locale: {
				customRangeLabel: "Tùy chỉnh",
				applyLabel: "Áp dụng",
				cancelLabel: "Hủy bỏ",
				daysOfWeek: [
				"CN",
				"T2",
				"T3",
				"T4",
				"T5",
				"T6",
				"T7"
				],
				"monthNames": [
				"Tháng 1",
				"Tháng 2",
				"Tháng 3",
				"Tháng 4",
				"Tháng 5",
				"Tháng 6",
				"Tháng 7",
				"Tháng 8",
				"Tháng 9",
				"Tháng 10",
				"Tháng 11",
				"Tháng 12"
				],
				"firstDay": 1
			},
			alwaysShowCalendars: true,
			endDate: t,
			ranges: {
				'Hôm nay': [moment(), moment()],
				'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
				"7 ngày gần đây": [moment().subtract(6, "days"), moment()],
				"30 ngày gần đây": [moment().subtract(29, "days"), moment()],
				"Tháng này": [moment().startOf("month"), moment().endOf("month")],
				"Tháng cuối": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
			}
		}, function(a, t, n) {
			$(".m_daterangepicker_bill_1").val(a.format("MM/DD/YYYY") + " - " + t.format("MM/DD/YYYY"))
		});

		$(".m_daterangepicker_bill").daterangepicker({
			buttonClasses: "m-btn btn btn-sm",
			applyClass: "btn-primary btn-sm",
			cancelClass: "btn-secondary btn-sm",
			startDate: a,
			autoUpdateInput:false,
			"showDropdowns": true,
			locale: {
				customRangeLabel: "Tùy chỉnh",
				applyLabel: "Áp dụng",
				cancelLabel: "Hủy bỏ",
				daysOfWeek: [
				"CN",
				"T2",
				"T3",
				"T4",
				"T5",
				"T6",
				"T7"
				],
				"monthNames": [
				"Tháng 1",
				"Tháng 2",
				"Tháng 3",
				"Tháng 4",
				"Tháng 5",
				"Tháng 6",
				"Tháng 7",
				"Tháng 8",
				"Tháng 9",
				"Tháng 10",
				"Tháng 11",
				"Tháng 12"
				],
				"firstDay": 1
			},
			alwaysShowCalendars: true,
			endDate: t,
			ranges: {
				'Hôm nay': [moment(), moment()],
				'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
				"7 ngày gần đây": [moment().subtract(6, "days"), moment()],
				"30 ngày gần đây": [moment().subtract(29, "days"), moment()],
				"Tháng này": [moment().startOf("month"), moment().endOf("month")],
				"Tháng cuối": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
			}
		}, function(a, t, n) {
			$(".m_daterangepicker_bill").val(a.format("MM/DD/YYYY") + " - " + t.format("MM/DD/YYYY"))
		});
	},
};
$(document).ready(function() {
	Bill.init();
});