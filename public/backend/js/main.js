var deletePages = function(id) {
	var data = {
		'_token' : $('meta[name=csrf-token]').attr('content'),
		'id' : id
	};
	if(confirm('Bạn có muốn xóa row này không ? ')){
		$.ajax({
			url : '/fs/cpanel/pages/delete/'+id,
			data : data,
			type : 'POST',
			success:function(data){
				if(data.code == '201'){
					$('.page-item-'+id).remove();
					toastr.success(data.message);
				}else if(data.code == '403'){
					toastr.error(data.message);
				}
			},
		});
	}
}
var editMenu = function($id) {
	var menu_parent = $('input[name="parent_menu"]').val();
	var data = {
		id : $id,
		token : $('meta[name=csrf-token]').attr('content')
	};
	$.ajax({
		url : '/fs/cpanel/menu/edit/'+$id,
		data : data,
		type : 'GET',
		success:function(data){
			$('.frm-menu button[type="submit"]').html('Cập nhật');
			$('.input-name input').val(data.name);
			$('.frm-menu form').attr('action','/fs/cpanel/menu/update/'+$id);
			if(data.parent == menu_parent){
				$('.input-name select').val(menu_parent).trigger('change');
			}else{
				$('.input-name select').val(data.parent).trigger('change');
			}
		},
	});
}

var deleteMenu = function($id){
	var data = {
		id : $id,
		_token : jQuery('meta[name=csrf-token]').attr('content')
	};

	if(confirm('Bạn muốn xóa menu này không ?')){
		jQuery.ajax({
			url : '/fs/cpanel/menu/delete',
			data : data,
			type : 'POST',
			success:function(data){
				if(data.code == '201'){
					jQuery('.wrapper-pages .item-'+$id).fadeOut();
					toastr.success(data.message);
				}else if(data.code == '403'){
					toastr.error(data.message);
				}
				
			},
		});
	}
}





