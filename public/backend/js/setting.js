$(document).ready(function() {
	setting.initCareer();
});

var setting = {
	initCareer: function (argument) {
		var expireDate = $('#js_career_expire_date').val();
		$('#js_career_date_picker').datepicker({
			format: 'dd-mm-yyyy',
		});		
		if (expireDate.length > 0) {
			$('#js_career_date_picker').datepicker('update', expireDate);
		} else {
			$('#js_career_date_picker').datepicker('update', new Date());
		}
	}
};

$('#js_setting_all').click(function(e) {
	$(".js-setting-checkbox").prop('checked', $(this).prop('checked'));
	if ($(this).prop('checked') === true) {

		$('.js-setting-input').val(1);
	} else {
		$('.js-setting-input').val(0);
	}
});
$('.js-setting-checkbox').change(function(e) {
	if ($(this).prop('checked') === true) {

		$(this).parent().find('.js-setting-input').val(1);
	} else {
		$(this).parent().find('.js-setting-input').val(0);
	}
});