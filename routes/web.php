<?php

Route::group(['prefix' => '/'], function () {
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	Route::get('/', [
		'as' => 'index',
		'uses' => 'Fontend\HomeController@index',
	]);

	Route::get('/gioi-thieu', [
		'as' => 'gioi-thieu',
		'uses' => 'Fontend\PagesController@Intro',
	]);

	Route::get('/lien-he', [
		'as' => 'lien-he',
		'uses' => 'Fontend\PagesController@Contact',
	]);

	Route::get('/tin-tuc', [
		'as' => 'tin-tuc',
		'uses' => 'Fontend\PagesController@News',
	]);
	Route::get('/tin-tuc/{id}', [
		'as' => 'tin-tuc-chi-tiet',
		'uses' => 'Fontend\PagesController@ShowNews',
	]);

	Route::get('/tuyen-dung', [
		'as' => 'tuyen-dung',
		'uses' => 'Fontend\PagesController@Careers',
	]);
	Route::get('/tuyen-dung/{id}', [
		'as' => 'tuyen-dung-chi-tiet',
		'uses' => 'Fontend\PagesController@ShowCareers',
	]);

	Route::get('/giao-hang-nhanh', [
		'as' => 'giao-hang-nhanh',
		'uses' => 'Fontend\PagesController@Fastship',
	]);

	Route::get('/giao-hang-thu-tien-cod', [
		'as' => 'giao-hang-thu-tien-cod',
		'uses' => 'Fontend\PagesController@CashReceiptsCod',
	]);

	Route::get('/giao-hang-tiet-kiem', [
		'as' => 'giao-hang-tiet-kiem',
		'uses' => 'Fontend\PagesController@DeliveryOfSavings',
	]);

	Route::get('/giao-hang-trong-ngay', [
		'as' => 'giao-hang-trong-ngay',
		'uses' => 'Fontend\PagesController@DeliveryOfDay',
	]);

	Route::get('/giao-hang-dia-phuong', [
		'as' => 'giao-hang-dia-phuong',
		'uses' => 'Fontend\PagesController@DeliveryOfRegion',
	]);

	Route::get('/van-tai-bac-nam', [
		'as' => 'van-tai-bac-nam',
		'uses' => 'Fontend\PagesController@NorthSouthTransportation',
	]);

	Route::get('/luu-kho', [
		'as' => 'luu-kho',
		'uses' => 'Fontend\PagesController@Storage',
	]);
	Route::get('/Account/Login', [
		'as' => 'login',
		'uses' => 'Fontend\AccountController@Index',
	]);
	Route::post('/Account/Login', [
		'as' => 'account-login-post',
		'uses' => 'Fontend\AccountController@Login',
	]);
	Route::get('/Account/Logout', [
		'as' => 'account-logout',
		'uses' => 'Fontend\AccountController@Logout',
	]);
	/* front end order */
	Route::get('/fs/order', [
		'as' => 'fs-order-index',
		'uses' => 'Fontend\OrderController@index',
	]);
	Route::get('/fs/order/add', [
		'as' => 'fs-order-add',
		'uses' => 'Fontend\OrderController@add',
	]);
	Route::post('/fs/order/store', [
		'as' => 'fs-order-store',
		'uses' => 'Fontend\OrderController@store',
	]);
	Route::get('/fs/order/destroy/{id}', [
		'as' => 'fs-order-destroy',
		'uses' => 'Fontend\OrderController@destroy',
	]);
	Route::get('/fs/order/edit/{id}', [
		'as' => 'fs-order-edit',
		'uses' => 'Fontend\OrderController@edit',
	]);
	Route::post('/fs/order/update/{id}', [
		'as' => 'fs-order-update',
		'uses' => 'Fontend\OrderController@update',
	]);
	Route::post('/fs/order/import', [
		'as' => 'fs-order-import',
		'uses' => 'Fontend\OrderController@import',
    ]);
    Route::get('/fs/order', [
		'as' => 'fs-order-index',
		'uses' => 'Fontend\OrderController@index',
	]);
    Route::get('/fs/order/search', 'Fontend\OrderController@indexSearch')->name('fs-order-search');
    // end frontend order

	/* get province, district, ward*/
	Route::get('/province', [
		'uses' => 'Fontend\HelperController@getProvince',
	]);
	Route::get('/district/{id}', [
		'uses' => 'Fontend\HelperController@getDistrict',
	]);
	Route::get('/ward/{id}', [
		'uses' => 'Fontend\HelperController@getWard',
	]);
    // end get province

    /* ware house */
    Route::get('/fs/warehouse', 'Fontend\WarehouseController@index')->name('warehouse-index');
    Route::get('/fs/warehouse/add', 'Fontend\WarehouseController@add')->name('warehouse-add');
    Route::get('/fs/warehouse/edit/{id}', 'Fontend\WarehouseController@edit')->name('warehouse-edit');
    Route::post('/fs/warehouse/store', 'Fontend\WarehouseController@store')->name('warehouse-store');
    Route::post('/fs/warehouse/update/{id}', 'Fontend\WarehouseController@update')->name('warehouse-update');
    Route::get('/fs/warehouse/destroy/{id}', 'Fontend\WarehouseController@destroy')->name('warehouse-destroy');
    // end warehouse
});

// Backend
Route::group(['prefix' => '/fs/cpanel/'], function () {
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    
    // home
	Route::get('/dashboard', [
		'as' => 'dashboard-index',
		'uses' => 'Backend\SettingsController@index',
    ]);
    
	// Pages
	Route::get('/pages', [
		'as' => 'pages-index',
		'uses' => 'Backend\PagesController@index',
	]);
	Route::get('/pages/add', [
		'as' => 'pages-get-add',
		'uses' => 'Backend\PagesController@add',
	]);
	Route::post('/pages/add', [
		'as' => 'pages-post-add',
		'uses' => 'Backend\PagesController@postAdd',
	]);
	Route::get('/pages/edit/{id}', [
		'as' => 'pages-get-edit',
		'uses' => 'Backend\PagesController@edit',
	]);
	Route::post('/pages/edit/{id}', [
		'as' => 'pages-post-edit',
		'uses' => 'Backend\PagesController@update',
	]);
	Route::post('/pages/delete/{id}', [
		'as' => 'pages-get-delete',
		'uses' => 'Backend\PagesController@delete',
	]);
	// End Pages

	// Menu
	Route::get('/menu', [
		'as' => 'menu-index',
		'uses' => 'Backend\MenuController@index',
	]);
	Route::post('/menu/add', [
		'as' => 'menu-add',
		'uses' => 'Backend\MenuController@create',
	]);
	Route::get('/menu/edit/{term_id}', [
		'as' => 'menu-edit',
		'uses' => 'Backend\MenuController@edit',
	]);
	Route::post('/menu/update/{term_id}', [
		'as' => 'menu-update',
		'uses' => 'Backend\MenuController@update',
	]);
	Route::post('/menu/delete', [
		'as' => 'menu-delete',
		'uses' => 'Backend\MenuController@delete',
	]);

	Route::get('/delivery', [
		'as' => 'delivery-index',
		'uses' => 'Backend\PagesController@create',
	]);
	Route::get('/email', [
		'as' => 'email-index',
		'uses' => 'Backend\PagesController@create',
	]);
	Route::get('/customer', [
		'as' => 'page-add',
		'uses' => 'Backend\PagesController@create',
	]);
	Route::get('/finance', [
		'as' => 'finance-index',
		'uses' => 'Backend\PagesController@create',
	]);

	//User
	Route::get('/users', [
		'as' => 'users-index',
		'uses' => 'Backend\UserController@index',
	]);

	Route::get('/users/edit', [
		'as' => 'users-edit',
		'uses' => 'Backend\UserController@edit',
	]);

	Route::get('/users/create', [
		'as' => 'users-add',
		'uses' => 'Backend\UserController@create',
	]);
	Route::post('/users/create', [
		'as' => 'users-add-post',
		'uses' => 'Backend\UserController@store',
	]);
	Route::post('/users/update/{id}', [
		'as' => 'users-update',
		'uses' => 'Backend\UserController@update',
	]);
	Route::get('/users/delete', [
		'as' => 'users-delete',
		'uses' => 'Backend\UserController@delete',
	]);
	Route::get('/users/lock', [
		'as' => 'users-lock',
		'uses' => 'Backend\UserController@lock',
	]);
	Route::post('/users/pw-generate', [
		'as' => 'pw-generate',
		'uses' => 'Backend\UserController@generate',
	]);
	// End user

	// Customer
	Route::get('/customer', [
		'as' => 'customer-index',
		'uses' => 'Backend\CustomerController@index',
	]);
	Route::get('/customer/add', [
		'as' => 'customer-add',
		'uses' => 'Backend\CustomerController@add',
	]);
	Route::post('/customer/add', [
		'as' => 'customer-add-post',
		'uses' => 'Backend\CustomerController@postAdd',
	]);
	Route::get('/customer/edit', [
		'as' => 'customer-edit',
		'uses' => 'Backend\CustomerController@edit',
	]);
	Route::post('/customer/update', [
		'as' => 'customer-update',
		'uses' => 'Backend\CustomerController@update',
	]);
	Route::get('/customer/delete', [
		'as' => 'customer-delete',
		'uses' => 'Backend\CustomerController@delete',
	]);
	Route::post('/customer/generate', [
		'as' => 'customer-pw-generate',
		'uses' => 'Backend\CustomerController@generate',
	]);
	Route::get('/customer/apply-price-default', [
		'as' => 'customer-apply-price-default',
		'uses' => 'Backend\CustomerController@priceDefault',
	]);

	// End Customer

	Route::get('/statistical', [
		'as' => 'statistical-index',
		'uses' => 'Backend\PagesController@create',
	]);
	Route::get('/news', [
		'as' => 'news-index',
		'uses' => 'Backend\PagesController@create',
	]);
	Route::get('/partner', [
		'as' => 'partner-index',
		'uses' => 'Backend\PagesController@create',
	]);
	Route::get('/branch', [
		'as' => 'branch-index',
		'uses' => 'Backend\BranchController@index',
	]);
	Route::get('/branch/add', [
		'as' => 'branch-add',
		'uses' => 'Backend\BranchController@add',
	]);
	Route::post('/branch/add', [
		'as' => 'branch-add-post',
		'uses' => 'Backend\BranchController@postAdd',
	]);
	Route::get('/branch/edit', [
		'as' => 'branch-edit',
		'uses' => 'Backend\BranchController@edit',
	]);

	Route::post('/branch/update', [
		'as' => 'branch-update',
		'uses' => 'Backend\BranchController@update',
	]);

	Route::get('/branch/delete', [
		'as' => 'branch-delete',
		'uses' => 'Backend\BranchController@delete',
	]);
	/* settings */
	Route::get('/settings', [
		'as' => 'settings-index',
		'uses' => 'Backend\SettingsController@index',
	]);
	Route::post('/settings-update', [
		'as' => 'settings-update',
		'uses' => 'Backend\SettingsController@update',
	]);

	/* slides */
	Route::get('/slides', [
		'as' => 'slides-index',
		'uses' => 'Backend\SlidesController@index',
	]);
	Route::get('/slides/add', [
		'as' => 'slides-add',
		'uses' => 'Backend\SlidesController@create',
	]);
	Route::post('/slides/store', [
		'as' => 'slides-store',
		'uses' => 'Backend\SlidesController@store',
	]);
	Route::get('/slides/destroy/{id}', [
		'as' => 'slides-destroy',
		'uses' => 'Backend\SlidesController@destroy',
	]);
	Route::put('/slides/update/{id}', [
		'as' => 'slides-update',
		'uses' => 'Backend\SlidesController@update',
	]);
	Route::get('/slides/edit/{id}', [
		'as' => 'slides-edit',
		'uses' => 'Backend\SlidesController@edit',
	]);

	/* careers */
	Route::get('/careers/', [
		'as' => 'careers-index',
		'uses' => 'Backend\CareersController@index',
	]);
	Route::get('/careers/add', [
		'as' => 'careers-add',
		'uses' => 'Backend\CareersController@add',
	]);
	Route::post('/careers/store', [
		'as' => 'careers-store',
		'uses' => 'Backend\CareersController@store',
	]);
	Route::get('/careers/destroy/{id}', [
		'as' => 'careers-destroy',
		'uses' => 'Backend\CareersController@destroy',
	]);
	Route::post('/careers/update/{id}', [
		'as' => 'careers-update',
		'uses' => 'Backend\CareersController@update',
	]);
	Route::get('/careers/edit/{id}', [
		'as' => 'careers-edit',
		'uses' => 'Backend\CareersController@edit',
	]);

	/* news */
	Route::get('/news/', [
		'as' => 'news-index',
		'uses' => 'Backend\NewsController@index',
	]);
	Route::get('/news/add', [
		'as' => 'news-add',
		'uses' => 'Backend\NewsController@add',
	]);
	Route::post('/news/store', [
		'as' => 'news-store',
		'uses' => 'Backend\NewsController@store',
	]);
	Route::get('/news/destroy/{id}', [
		'as' => 'news-destroy',
		'uses' => 'Backend\NewsController@destroy',
	]);
	Route::post('/news/update/{id}', [
		'as' => 'news-update',
		'uses' => 'Backend\NewsController@update',
	]);
	Route::get('/news/edit/{id}', [
		'as' => 'news-edit',
		'uses' => 'Backend\NewsController@edit',
	]);

	/*Pill*/
	Route::group(['prefix' => '/Bill'], function () {
		Route::get('/', [
			'as' => 'bill-index',
			'uses' => 'Backend\BillController@index',
		]);
		
		Route::get('/PickupGoodFromCustomer', [
			'as' => 'bill-pickup-good-from-customer',
			'uses' => 'Backend\BillController@PickupGoodFromCustomer',
		]);

		Route::post('/responsive-data',[
			'as' => 'bill-server-side',
			'uses' => 'Backend\BillController@responseData',
		]);

		Route::get('/bill-detail', [
			'as' => 'bill-detail',
			'uses' => 'Backend\BillController@AjaxDeployOrders',
		]);

		Route::get('/print-bill',[
			'as' => 'print-bill',
			'uses' => 'Backend\BillController@PrintBill',
		]);

		Route::get('/print-bill-recevied',[
			'as' => 'print-bill-recevied',
			'uses' => 'Backend\BillController@PrintBillRecevied',
		]);

		

		Route::get('/update-receive-goods-success', [
			'as' => 'update-receive-goods-success',
			'uses' => 'Backend\BillController@UpdateReciveGoodsSuccess',
		]);

		Route::get('/PickupGoodFromOrderRecover', [
			'as' => 'bill-pickup-good-from-order-recover',
			'uses' => 'Backend\BillController@PickupGoodFromOrderRecover',
		]);

		Route::get('/ReceiveGoodsFromPickupBill', [
			'as' => 'bill-receive-goods-from-pickup-bill',
			'uses' => 'Backend\BillController@BillReceiveGoodsFromPickupBill',
		]);

		Route::get('/ReceiveGoodsFromPickupBills', [
			'as' => 'bill-receive-goods-from-pickup-bills',
			'uses' => 'Backend\BillController@BillReceiveGoodsFromPickupBills',
		]);

		Route::get('/LoadingToWarehouseFromCustomer', [
			'as' => 'loading-to-warehouse-from-customer',
			'uses' => 'Backend\BillController@LoadingToWarehouseFromCustomer',
		]);

		Route::get('/TransferToDCOther', [
			'as' => 'bill-transfer-to-dc-other',
			'uses' => 'Backend\BillController@BillTransferToDCOther',
		]);

		Route::get('/LoadingToWareHouseFromDCOther', [
			'as' => 'bill-loading-to-warehouse-from-dc-other',
			'uses' => 'Backend\BillController@BillLoadingToWareHouseFromDCOther',
		]);

		Route::get('/DeployDelivery', [
			'as' => 'bill-deploy-delivery',
			'uses' => 'Backend\BillController@BillDeployDelivery',
		]);

		Route::get('/Delivery', [
			'as' => 'bill-delivery',
			'uses' => 'Backend\BillController@BillDelivery',
		]);

		Route::get('/ReceiveGoodsFromDeliveryBill', [
			'as' => 'bill-receive-goods-from-delivery-bill',
			'uses' => 'Backend\BillController@BillReceiveGoodsFromDeliveryBill',
		]);

		Route::get('/ReceiveTakeMoneyFromDeliveryBill', [
			'as' => 'bill-receive-take-money-from-delivery-bill',
			'uses' => 'Backend\BillController@BillReceiveTakeMoneyFromDeliveryBill',
		]);

		Route::get('/DeliveryOutOfServe', [
			'as' => 'bill-delivery-out-of-serve',
			'uses' => 'Backend\BillController@BillDeliveryOutOfServe',
		]);

		Route::get('/ReceiveGoodsOutOfServe', [
			'as' => 'bill-receive-goods-out-of-serve',
			'uses' => 'Backend\BillController@BillReceiveGoodsOutOfServe',
		]);

		Route::get('/ReceiveTakeMoneyOutOfServe', [
			'as' => 'bill-receive-take-money-out-of-serve',
			'uses' => 'Backend\BillController@BillReceiveTakeMoneyOutOfServe',
		]);

		Route::get('/DeployToCustomer', [
			'as' => 'bill-deploy-to-customer',
			'uses' => 'Backend\BillController@BillDeployToCustomer',
		]);

		Route::get('/ReturnGoodsForCustomer', [
			'as' => 'bill-return-goods-for-customer',
			'uses' => 'Backend\BillController@BillReturnGoodsForCustomer',
		]);

		Route::get('/ReceiveGoodsFromReturnBill', [
			'as' => 'bill-receive-goods-from-return-bill',
			'uses' => 'Backend\BillController@BillReceiveGoodsFromReturnBill',
		]);

		Route::get('/CheckWareHouse', [
			'as' => 'bill-check-warehouse',
			'uses' => 'Backend\BillController@BillCheckWareHouse',
		]);



	});

	/*End*/

	Route::get('/pickup', [
		'as' => 'pickup-index',
		'uses' => 'Backend\PickupController@index',
	]);

	Route::get('/OrderProcess/Deploy', [
		'as' => 'delivery-index',
		'uses' => 'Backend\DeliveryController@index',
	]);

	Route::post('/CreateOrderProcess', [
		'as' => 'create-order-process',
		'uses' => 'Backend\DeliveryController@CreateOrderProcess',
	]);
	
	Route::post('/CreateDeployReturn', [
		'as' => 'create-deploy-return',
		'uses' => 'Backend\DeployReturnController@CreateDeployReturn',
	]);


	Route::get('/OrderProcess/DeployReturn', [
		'as' => 'return-index',
		'uses' => 'Backend\DeployReturnController@index',
	]);

	Route::get('/OrderProcess/DeployPickup', [
		'as' => 'deloy-pickup-index',
		'uses' => 'Backend\DeployPickupController@index',
	]);
	

	Route::post('/OrderProcess/CreatePillPickup', [
		'as' => 'create-pill-pickup',
		'uses' => 'Backend\DeployPickupController@CreatePillPickup',
	]);

	Route::post('/GetData/Pickup', [
		'as' => 'get-data-deploy-pickup',
		'uses' => 'Backend\DeployPickupController@getDataRowsTable',
	]);

	Route::post('/GetWareHouse', [
		'as' => 'get-warehouse',
		'uses' => 'Backend\DeployPickupController@getWareHouse',
	]);

	Route::get('/OrderOutOfServe', [
		'as' => 'order-out-of-serve-index',
		'uses' => 'Backend\OrderOutOfServeController@index',
	]);

	Route::get('/UnloadingFromCustomer', [
		'as' => 'unloading-from-customer-index',
		'uses' => 'Backend\UnloadingFromCustomerController@index',
	]);
	Route::post('/createPill', [
		'as' => 'pill-add',
		'uses' => 'Backend\UnloadingFromCustomerController@createPill',
	]);

	/* Shipped to another DC */
	Route::get('/UnloadingFromDc', [
		'as' => 'unloading-from-dc-index',
		'uses' => 'Backend\UnloadingFromDcController@index',
	]);


	/*End*/

	/* user group setting */
	Route::get('/group', [
		'as' => 'group-index',
		'uses' => 'Backend\GroupController@index',
	]);
	Route::get('/group/add', [
		'as' => 'group-add',
		'uses' => 'Backend\GroupController@add',
	]);
	Route::post('/group/store', [
		'as' => 'group-store',
		'uses' => 'Backend\GroupController@store',
	]);
	Route::get('/group/destroy/{id}', [
		'as' => 'group-destroy',
		'uses' => 'Backend\GroupController@destroy',
	]);
	Route::post('/group/update/{id}', [
		'as' => 'group-update',
		'uses' => 'Backend\GroupController@update',
	]);
	Route::get('/group/edit/{id}', [
		'as' => 'group-edit',
		'uses' => 'Backend\GroupController@edit',
	]);
	Route::get('/group/setting/{id}', [
		'as' => 'group-setting',
		'uses' => 'Backend\GroupController@setting',
	]);
	Route::post('/group/setting/update/{id}', [
		'as' => 'group-setting-update',
		'uses' => 'Backend\GroupController@updateSetting',
    ]);   
	/* end user group setting */
	
    /* order */
    Route::get('/order', 'Backend\OrderController@index')->name('order-index');
    Route::get('/order/add', 'Backend\OrderController@add')->name('order-add');
    Route::post('/order/store', 'Backend\OrderController@store')->name('order-store');
    Route::get('/order/edit/{id}', 'Backend\OrderController@edit')->name('order-edit');
    Route::post('/order/update/{id}', 'Backend\OrderController@update')->name('order-update');
    Route::get('/order/destroy/{id}', 'Backend\OrderController@destroy')->name('order-destroy');
    Route::post('/order/import', 'Backend\OrderController@import')->name('order-import');
    Route::get('/order/export', 'Backend\OrderController@export')->name('order-export');
    Route::get('/order/label', 'Backend\OrderController@label')->name('order-label');
    Route::get('/order/print-label', 'Backend\OrderController@printlabel')->name('order-print-label');
    Route::get('/order/show', 'Backend\OrderController@show')->name('order-show');
    Route::get('/order/print/{id}', 'Backend\OrderController@print')->name('order-print');
    Route::get('/order/setting', 'Backend\OrderController@setting')->name('order-setting');
    Route::post('/order/setting-store', 'Backend\OrderController@storeService')->name('order-setting-store');
    Route::get('/order/search', 'Backend\OrderController@search')->name('order-search');
    Route::get('/order/setting-destroy/{id}', 'Backend\OrderController@destroyService')->name('order-setting-destroy');
    Route::get('/Order/OrderOutOfServe', 'Backend\OrderOutOfServeController@index')->name('order-out-of-serve');
    Route::get('/OrderProcess/DeliveryOutOfServe', 'Backend\OrderOutOfServeController@DeliveryOutOfServe')->name('delivery-out-of-serve');
    Route::get('/order/update-outside', 'Backend\OrderController@orderUpdateOutside')->name('order-update-outside');
    Route::get('/order/update-status', 'Backend\OrderController@orderUpdateStatus')->name('order-update-status');
    Route::get('/order/update-return', 'Backend\OrderController@orderUpdateReturn')->name('order-update-return');
    
	/* statistic */
    Route::get('/statistic/in-dc', 'Backend\StatisticController@inDc')->name('statistic-inDc');
    Route::get('/statistic/create-new', 'Backend\StatisticController@createNew')->name('statistic-createNew');
    Route::get('/statistic/receive-new', 'Backend\StatisticController@receiveNew')->name('statistic-receiveNew');
    Route::get('/statistic/inventory-in-dc', 'Backend\StatisticController@inventoryInDc')->name('statistic-inventoryInDc');
    Route::get('/statistic/by-customer', 'Backend\StatisticController@byCustomer')->name('statistic-byCustomer');
    Route::get('/statistic/lead-time-success', 'Backend\StatisticController@leadTimeSuccess')->name('statistic-leadTimeSuccess');
    Route::get('/statistic/lead-time-start', 'Backend\StatisticController@leadTimeStart')->name('statistic-leadTimeStart');

    // finance
    Route::get('/finance', 'Backend\FinanceController@index')->name('finance-index');
    Route::get('/finance/order-salary', 'Backend\FinanceController@orderSalary')->name('finance-order-salary');
    Route::get('/finance/transfer-to-accountant', 'Backend\FinanceController@transferToAccountant')->name('finance-transfer-to-accountant');

    Route::get('/finance/index-search', 'Backend\FinanceController@indexSearch')->name('finance-index-search');
    Route::get('/finance/order-salary-search', 'Backend\FinanceController@orderSalarySearch')->name('finance-order-salary-search');
    Route::get('/finance/transfer-to-accountant-search', 'Backend\FinanceController@transferToAccountantSearch')->name('finance-transfer-to-accountant-search');

	Route::get('/finance/detail/{id}', 'Backend\FinanceController@detail')->name('finance-detail');
	Route::get('/finance/updateStatusFinance/{id}', 'Backend\FinanceController@updateStatusFinance')->name('finance-updateStatusFinance');
	

	// Settings order
    Route::get('/settings-area', 'Backend\AreaController@index')->name('settings-area');

    Route::post('/express-delivery', 'Backend\AreaController@postExpressDelivery')->name('post-express-delivery');
    Route::get('/express-delivery-edit', 'Backend\AreaController@getExpressDelivery')->name('get-express-delivery');
    Route::post('/express-delivery-update', 'Backend\AreaController@updateExpressDelivery')->name('update-express-delivery');
    Route::get('/express-delivery-delete', 'Backend\AreaController@deleteExpressDelivery')->name('delete-express-delivery');

    Route::post('/express-delivery-addon', 'Backend\AreaController@postExpressDeliveryAddon')->name('post-express-delivery-addon');
    Route::get('/express-delivery-addon', 'Backend\AreaController@getExpressDeliveryAddon')->name('get-express-delivery-addon');
    Route::post('/express-delivery-addon-update', 'Backend\AreaController@updateExpressDeliveryAddon')->name('update-express-delivery-addon');
    Route::get('/express-delivery-addon-delete', 'Backend\AreaController@deleteExpressDeliveryAddon')->name('delete-express-delivery-addon');

    Route::post('/express-delivery-interdepartmental-area', 'Backend\AreaController@postExpressDeliveryInterprovincial')->name('post-express-delivery-interprovincial');

    
    Route::get('/get-express-delivery-interdepartmental', 'Backend\AreaController@getExpressDeliveryInterprovincial')->name('get-express-delivery-interdepartmental');
    Route::post('/update-express-delivery-interdepartmental', 'Backend\AreaController@updateExpressDeliveryInterprovincial')->name('update-express-delivery-interdepartmental');
    Route::get('/delete-express-delivery-interdepartmental', 'Backend\AreaController@deleteExpressDeliveryInterprovincial')->name('delete-express-delivery-interdepartmental-area');
    Route::post('/express-delivery-interprovincial', 'Backend\AreaController@postExpressDeliveryInterprovincial')->name('post-express-delivery-interprovincial');


    Route::post('/express-delivery-interprovincial-addon', 'Backend\AreaController@postExpressDeliveryInterprovincialAddon')->name('post-express-delivery-interprovincial-addon');
    Route::post('/update-express-delivery-interprovincial-addon', 'Backend\AreaController@updateExpressDeliveryInterprovincialAddon')->name('update-express-delivery-interprovincial-addon');
    Route::get('/delete-express-delivery-interprovincial-addon', 'Backend\AreaController@deleteExpressDeliveryInterprovincialAddon')->name('delete-express-delivery-interprovincial-addon');    
    Route::get('/get-express-delivery-interprovincial-addon', 'Backend\AreaController@getExpressDeliveryInterprovincialAddon')->name('get-express-delivery-interprovincial-addon');

    Route::post('/delivery-of-savings', 'Backend\AreaController@postDeliveryOfSavings')->name('post-delivery-of-saving');
    Route::get('/get-delivery-of-savings-settings', 'Backend\AreaController@getDeliveryOfSavings')->name('get-delivery-of-savings-settings');

    Route::post('/update-delivery-of-savings-settings', 'Backend\AreaController@updateDeliveryOfSavings')->name('update-delivery-of-savings-settings');

    Route::get('/delete-delivery-of-savings-settings', 'Backend\AreaController@deleteDeliveryOfSavings')->name('delete-delivery-of-savings-settings');

});
