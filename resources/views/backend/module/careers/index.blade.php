@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					Quản lý tuyển dụng
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-primary" href="{{ route('careers-add') }}">
							Thêm mới
						</a>
					</h3>
				</div>	
			</div>
		</div>
		<div class="m-portlet__body">
			@if (!empty($aCareers))
			<table class="table table-striped">
				<thead>
					<tr>
						<th>STT</th>
						<th>#</th>
						<th>Tiêu đề</th>
						<th>Người tạo</th>
						<th>Trạng thái</th>
						<th>Ngày đăng</th>
						<th>Ngày hết hạn</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($aCareers as $iKey => $aCareer)
					<tr>
						<td>{{ $iKey + 1 }}</td>
						<td>{{ $aCareer->career_id }}</td>
						<td>{{ $aCareer->title }}</td>
						<td>@if(!empty($aCareer->author)){{ $aCareer->author }}@else
						Không có
						@endif</td>
						<td>
							@if ($aCareer->is_active)
							<span class="label label-success">Bật</span>
							@else
							<span class="label label-danger">Tắt</span>
							@endif
						</td>
						<td>{{ date("d-m-Y h:i:s", strtotime($aCareer->created_at)) }}</td>
						<td>{{ $aCareer->expire_date }}</td>
						<td>
							<a class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/careers/destroy', $aCareer->career_id) }}">
								<i class="fa fa-trash-o"></i>
							</a>
							<a class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/careers/edit', $aCareer->career_id) }}">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $aCareers->links('backend.layouts.pagination') }}
			@else
			<div class="alert alert-danger">
				Không tìm thấy mục tuyển dụng nào
			</div>
			@endif
		</div>
	</div>
</div>
@endsection
