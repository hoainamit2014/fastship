@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					@if( isset($aCareer->career_id) )
						Sửa tin tuyển dụng
					@else
						Thêm tin tuyển dụng
					@endif
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-secondary" href="{{ route('careers-index') }}">
							<span>
								<i class="la la-angle-left"></i>
								<span>Trở về</span>
							</span>
						</a>
					</h3>
				</div>	
			</div>
		</div>
		<div class="m-portlet__body">
			@if( isset($aCareer->career_id) )
			<form action="{{ url('/fs/cpanel/careers/update/'.$aCareer->career_id)}}" method="POST">
			@else
			<form action="{{ route('careers-store') }}" method="POST">
			@endif
				{{ csrf_field() }}
				<div class="form-group">
					<label>Tiêu đề</label>
					<input name="title" type="text" class="form-control" value="@if(isset($aCareer->title)){{ $aCareer->title }}@endif">
				</div>
				<div class="form-group">
					<label>Nội dung</label>
					<textarea name="content" class="form-control" rows="2">@if(isset($aCareer->content)){{ $aCareer->content }}@endif</textarea>
				</div>
				<div class="form-group">
					<label>Từ khóa SEO</label>
					<input name="keyword" class="form-control" value="@if(isset($aCareer->keyword)){{ $aCareer->keyword }}@endif" type="text">
				</div>
				<div class="form-group">
					<label>Mô tả trang</label>
					<textarea name="description" id="description" class="form-control" rows="2">@if(isset($aCareer->description)){{ $aCareer->description }}@endif</textarea>
				</div>
				<div class="form-group">
					<label>Ngày hết hạn</label>
					<div class="form-group">
		                <div class="input-group date date-picker" id="js_career_date_picker">
						    <span class="input-group-addon">
	                            <button class="btn default" type="button">
	                                <i class="fa fa-calendar"></i>
	                            </button>
	                        </span>
		                    <input name="expire_date" class="form-control" type="text">
						</div>
	                    <input id="js_career_expire_date" value="@if(isset($aCareer->expire_date)){{ $aCareer->expire_date }}@endif" class="form-control" type="hidden">
		            </div>
				</div>
				<div class="form-group">
					<label>Trạng thái</label>
					<select name="is_active" class="form-control">
						<option value="1" @if(isset($aCareer->is_active) && $aCareer->is_active)selected="selected"@endif>Bật</option>
						<option value="0" @if(isset($aCareer->is_active) && !$aCareer->is_active)selected="selected"@endif>Tắt</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary">
				@if (isset($aCareer->career_id))
					Cập nhật
				@else
					Lưu
				@endif
				</button>
			</form>
		</div>
	</div>
</div>
@endsection

