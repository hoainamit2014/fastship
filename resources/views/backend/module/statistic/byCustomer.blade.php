@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Thống kê đơn hàng nhận mới theo đối tác') }}
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<form class="m-form m-form--fit" action="{{ route('statistic-byCustomer') }}" method="get">
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<label>Thời gian:</label>
							<div class="input-daterange input-group" id="js_order_date_picker">
								<input type="text" class="form-control"  name="search[from_date]" placeholder="Từ" value="@if(isset($aSearch['from_date'])){{ $aSearch['from_date'] }}@endif"/>
								<div class="input-group-append">
									<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
								</div>
								<input type="text" class="form-control"  name="search[to_date]" placeholder="đến" value="@if(isset($aSearch['to_date'])){{ $aSearch['to_date'] }}@endif"/>
							</div>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Khách hàng:</label>
							<select class="form-control" name="search[customer_id]">
								<option value="">Chọn:</option>
								@foreach ($aCustomers as $aCustomer)
								<option value="{{ $aCustomer->id }}" @if(isset($aSearch['customer_id']) && $aCustomer->id == $aSearch['customer_id'])selected="selected"@endif>{{ $aCustomer->customer_code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>DC:</label>
							<select class="form-control" name="search[branch_id]">
								<option value="">Chọn:</option>
								@foreach ($aBranchs as $aBranch)
								<option value="{{ $aBranch['id'] }}" @if(isset($aSearch['branch_id']) && $aBranch['id'] == $aSearch['branch_id'])selected="selected"@endif>{{ $aBranch['name'] }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-2">
						<button class="btn btn-primary m-btn m-btn--icon" type="submit" style="margin-top: 34px">
							<span>
								<i class="la la-search"></i>
								<span>Tìm kiếm</span>
							</span>
						</button>
					</div>
				</div>
			</form>
			<table class="table table-bordered table-hover statistics-table">
				<thead>
					<tr>
						<th>Ngày nhận</th>
						@foreach ($aSelectCustomers as $aRow)
						<th>{{ $aRow['customer_code'] }}</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					@foreach ($aRows as $iKey => $aRow)
					<tr>
						<td>{{ $aRow[0] }}</td>
						@for ($i = 1; $i <= count($aSelectCustomers); $i++)
						<td>{{ $aRow[$i] }}</td>
						@endfor
					</tr>
					@endforeach
					<tr>
						<td>Tổng</td>
						@for ($i = 0; $i < count($aSelectCustomers); $i++)
						<td>{{ $aSum[$i] }}</td>
						@endfor
					</tr>
					<tr>
						<td>Trung bình</td>
						@for ($i = 0; $i < count($aSelectCustomers); $i++)
						<td>{{ $aAverage[$i] }}</td>
						@endfor
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection