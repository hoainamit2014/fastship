@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Thống kê đơn hàng tồn tại DC') }}
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<form class="m-form m-form--fit" action="{{ route('statistic-inventoryInDc') }}" method="get">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label>Khách hàng:</label>
							<select class="form-control" name="search[customer_id]">
								<option value="">Chọn:</option>
								@foreach ($aCustomers as $aCustomer)
								<option value="{{ $aCustomer->id }}" @if(isset($aSearch['customer_id']) && $aCustomer->id == $aSearch['customer_id'])selected="selected"@endif>{{ $aCustomer->customer_code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>DC:</label>
							<select class="form-control" name="search[branch_id]">
								<option value="">Chọn:</option>
								@foreach ($aBranchs as $aBranch)
								<option value="{{ $aBranch['id'] }}" @if(isset($aSearch['branch_id']) && $aBranch['id'] == $aSearch['branch_id'])selected="selected"@endif>{{ $aBranch['name'] }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<button class="btn btn-primary m-btn m-btn--icon" type="submit" style="margin-top: 34px">
							<span>
								<i class="la la-search"></i>
								<span>Tìm kiếm</span>
							</span>
						</button>
					</div>
				</div>
			</form>
			<table class="table table-bordered table-hover statistics-table">
				<thead>
					<tr>
						@foreach ($aRows as $iKey => $aRow)
						<th>{{ $aRow['name'] }}</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					<tr>
						@foreach ($aRows as $iKey => $aRow)
						<td>{{ $aRow['count'] }}</td>
						@endforeach
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection