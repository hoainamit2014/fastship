<!DOCTYPE html>
<html>
<head>
	<title>In phiếu pickup - Vinelshipping</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/fontawesome/css/font-awesome.min.css') }}">
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/bootstrap.min.js') }}"></script>
</head>
<body>
	<style type="text/css">
		.item__col{
			display: flex;
			align-items: center;
		}
		@page {
		  size: A2;
		  margin: 0;
		}
		body{
			padding: 30px;
		}
	</style>
	<div class="item__col">
		<div class="left" style="width: 33%;">
			<strong>CÔNG TY TNHH VINACAPITAL VIỆT NAM</strong>
			<p>Số ĐKKD: 0123456789</p>
			<p><strong>Đối tác:</strong></p>
			<p><strong>Kho xuất:</strong></p>
		</div>
		<div class="center" style="width: 33%">
			<h3 class="text-center">BIÊN BẢN BÀN <br> GIAO HÀNG HÓA</h3>
		</div>
		<div class="right" style="width: 33%;text-align: center;">
			<?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($data['bill']['bill_code'], "C128", 1, 50) . '" alt="barcode"/>'; ?>
		</div>
	</div>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>STT</th>
				<th>Mã HT</th>
				<th>Mã ĐT</th>
				<th>Tỉnh</th>
				<th>Quận</th>
				<th>Phường</th>
				<th style="width: 300px">Địa chỉ</th>
				<th>Tên KH</th>
				<th>SĐT</th>
				<th>Sản phẩm</th>
				<th>KM</th>
				<th>D+</th>
				<th>COD</th>
				<th>Ghi chú</th>
			</tr>
		</thead>
		<tbody>
			@if($data['orders'])
				@foreach($data['orders'] as $k => $aVal)
					<tr>
						<td>{{ $k + 1 }}</td>
						<td>{{ $aVal['order_code'] }}</td>
						<td>{{ $aVal['customer_order_code'] }}</td>
						<td>{{ Helpers::getProvinceName($aVal['province_id']) }}</td>
						<td>{{ Helpers::getDistrictName($aVal['district_id']) }}</td>
						<td>
							@if($aVal['ward_id'])
								{{ Helpers::getDistrictName($aVal['ward_id']) }}
							@endif
						</td>
						<td >@if($aVal['address'])
								{{ $aVal['address'] }}
							@endif</td>
						<td>
							{{ $aVal['receiver_name'] }}							
						</td>
						<td>
							{{ $aVal['phone'] }}
						</td>
						<td>
							{{ $aVal['product_name'] }}
						</td>
						<td>{{ $aVal['gift'] }}</td>
						<td>1</td>
						<td>{{ number_format($aVal['cod'],0,',','.') }} đ</td>
						<td>{{ $aVal['note'] }}</td>
					</tr>
				@endforeach
			@endif
		</tbody>
	</table>
	<script type="text/javascript">
		window.print();
	</script>
</body>
</html>

