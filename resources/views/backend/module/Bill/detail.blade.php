<div class="m-grid__item m-grid__item--fluid m-wrapper" style="background-color: #ffffff;">
	<div class="header_detail">
		<span class="text-uppercase">Chi tiết {{ $aTypeName }}</span>: <strong>{{ $_GET['billCode'] }}</strong>
		<div class="pull-right btn___group">
			<a href="{{ route('print-bill') }}?billCode={{ $_GET['billCode'] }}" class="btn btn-success">In phiếu</a>
			<a href="{{ route('update-receive-goods-success') }}?billCode={{ $_GET['billCode'] }}" class="btn btn-success">Nhận hàng</a>
			<button class="btn btn-success">Xuất ra excel</button> 
			<a href="{{ route('print-bill-recevied') }}?billCode={{ $_GET['billCode'] }}" class="btn btn-success">In phiếu đã nhận hàng</a>
			<a href="{{ route($routeCurrent) }}" class="btn btn-danger">
				<i class="fa fa-long-arrow-left"></i> Danh sách phiếu
			</a>
		</div>
	</div>
	<div class="info__bill">
		<table class="table">
			<tr>
				<td>Số đơn: <strong class="totalOder"></strong></td>
				<td>Số đơn thành công: <strong class="orderSuccess"></strong></td>
				<td>Trạng thái pickup: <strong class="status_static">Đang đi lấy hàng</strong></td>
				<td>Trạng thái: <strong class="static__bill"></strong></td>
				<td>Phụ trách: <strong class="charge"></strong></td>
			</tr>
		</table>
	</div>
	<div class="item__table">
		<div class="table-responsive">
			<table class="table table-bordered table-hover tbl_orders" >
				<thead>
					<tr style="white-space: nowrap;" class="text-center">
						<th>#</th>
						<th>STT</th>
						<th>Mã</th>
						<th>Mã đối tác</th>
						<th>Lý do</th>
						<th>Trạng thái</th>
						<th>Tên người gửi</th>
						<th>Số điện thoại</th>
						<th>Sản phẩm</th>
						<th>KM1</th>
						<th>COD</th>
						<th>Địa chỉ</th>
						<th>Trạng thái vật lý</th>
						<th>D+ pickup</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var tbl_order;
	$(document).ready(function() {
		loadAjax();
		tbl_order = $('.tbl_orders').DataTable({
			"searching": false,
			"bLengthChange": false,
			"ordering": false
		});
	});
	function loadAjax()
	{
		
		var data = {
			'type' : <?php echo $_GET['type']; ?>,
			'billCode' : "{{ !empty($_GET['billCode']) ? $_GET['billCode'] : '' }}",
			'_token' : $('meta[name="csrf-token"]').attr('content')
		};
		$.ajax({
			type : 'GET',
			url : '{{ route("bill-detail") }}',
			data : data ,
			success:function(res){
				var res = $.parseJSON(res);
				$('.bill_number').html(res.data.length);
				$('.bill_number').html(res.bill_status);
				$('.charge').html(res.charge);
				$('.author').html(res.author);
				$('.totalOder').html(res.total);
				$('.orderSuccess').html(res.orderSuccess);
				$('.static__bill').html(res.status_pickup);
				$('.charge').html(res.charge);
				
				
				var OrderArray = [];
				$.each( res.data, function(i, obj) {
					console.log(obj);
					var aStatus = {
						0 : {
							'title' : 'Tạo mới'
						},
						1 : {
							'title' : '<span class="m-badge m-badge--warning m-badge--wide text__white">Đang xử lý</span>'
						},
						2 : {
							'title' : '<span class="m-badge m-badge--success m-badge--wide text__white">Đã xử lý</span>'
						},
					};

					var reason = '<i class="la la-pencil"></i>';
					if(obj.date_received != null && obj.date_received != ''){
						var dateCurrent = new Date().toLocaleDateString();
						var date_recevied = obj.date_received.split(' ');

						dateCurrent = dateCurrent.split('/');
						date_recevied = date_recevied[0];
						date_recevied = date_recevied.split('-');
						console.log(date_recevied);
						console.log(dateCurrent);
						dateCurrent = new Date(dateCurrent[2], dateCurrent[0], dateCurrent[1]);
						date_recevied = new Date(date_recevied[0], date_recevied[1], date_recevied[2]);
						dateCurrent_unixtime = parseInt(dateCurrent.getTime() / 1000);
						date_recevied_unixtime = parseInt(date_recevied.getTime() / 1000);

						var timeDifference = dateCurrent_unixtime - date_recevied_unixtime;
						var timeDifferenceInHours = timeDifference / 60 / 60;

						var timeDifferenceInDays = timeDifferenceInHours  / 24;
					}
					

					OrderArray[i] = [
						'<a href="#" class="btn btn-success btn-sm"><i class="la la-map-marker"></i></a>',
						i + 1,
						obj.order_code,
						obj.customer_code,
						reason,
						obj.status,
						obj.receiver_name,
						obj.phone,
						obj.product_name,
						obj.gift_1,
						obj.cod,
						obj.address,
						obj.physical_status,
						date_recevied ? timeDifferenceInDays : 0,
					];
				});
				tbl_order.rows.add(OrderArray).draw();
			}
		});
	}
</script>