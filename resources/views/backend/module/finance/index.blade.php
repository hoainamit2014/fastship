@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ __('Danh sách phiếu chuyển tiền cho kế toán') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Thời gian tạo:</label>
                            <input type='text' class="form-control m_daterangepicker_bill" name="created_date"/>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Thời gian hoàn thành:</label>
                            <input type='text' class="form-control m_daterangepicker_bill_1" name="complete_date"/>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Người phụ trách:</label>
                            <select class="form-control" name="dm_id">
                                <option value="">Chọn:</option>
                                @foreach ($aUsers as $aUser)
                                <option value="{{ $aUser->id }}">{{ $aUser->user_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Trạng thái phiếu:</label>
                            <select class="form-control" name="finance_status">
                                <option value="">Chọn:</option>
                                <option value="0">Chưa xử lý</option>
                                <option value="1">Đã xử lý</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div style="margin-bottom: 10px">
                    <button class="btn btn-primary m-btn m-btn--icon" type="submit" id="m_search">
                        <span>
                            <i class="la la-search"></i>
                            <span>Tìm kiếm</span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="js_finance_listing">
                    <thead>
                        <tr>
                            <th>Mã phiếu</th>
                            <th>Số đơn</th>
                            <th>Loại phiếu</th>
                            <th>Phụ trách</th>
                            <th>Trạng thái</th>
                            <th>Người tạo</th>
                            <th>Ngày tạo</th>
                            <th>Ngày hoàn thành</th>
                            <th>Quản lý</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    var Finnace = {
        init: function() {
            var a = moment().subtract(29, "days"),
            t = moment();
            $(".m_daterangepicker_bill").daterangepicker({
                buttonClasses: "m-btn btn btn-sm",
                applyClass: "btn-primary btn-sm",
                cancelClass: "btn-secondary btn-sm",
                startDate: a,
                autoUpdateInput:false,
                "showDropdowns": true,
                locale: {
                    customRangeLabel: "Tùy chỉnh",
                    applyLabel: "Áp dụng",
                    cancelLabel: "Hủy bỏ",
                    daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                    ],
                    "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                    ],
                    "firstDay": 1
                },
                alwaysShowCalendars: true,
                endDate: t,
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "7 ngày gần đây": [moment().subtract(6, "days"), moment()],
                    "30 ngày gần đây": [moment().subtract(29, "days"), moment()],
                    "Tháng này": [moment().startOf("month"), moment().endOf("month")],
                    "Tháng cuối": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                }
            }, function(a, t, n) {
                $(".m_daterangepicker_bill").val(a.format("MM/DD/YYYY") + " - " + t.format("MM/DD/YYYY"))
            });
            $(".m_daterangepicker_bill_1").daterangepicker({
                buttonClasses: "m-btn btn btn-sm",
                applyClass: "btn-primary btn-sm",
                cancelClass: "btn-secondary btn-sm",
                startDate: a,
                autoUpdateInput:false,
                "showDropdowns": true,
                locale: {
                    customRangeLabel: "Tùy chỉnh",
                    applyLabel: "Áp dụng",
                    cancelLabel: "Hủy bỏ",
                    daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                    ],
                    "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                    ],
                    "firstDay": 1
                },
                alwaysShowCalendars: true,
                endDate: t,
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "7 ngày gần đây": [moment().subtract(6, "days"), moment()],
                    "30 ngày gần đây": [moment().subtract(29, "days"), moment()],
                    "Tháng này": [moment().startOf("month"), moment().endOf("month")],
                    "Tháng cuối": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                }
            }, function(a, t, n) {
                $(".m_daterangepicker_bill_1").val(a.format("MM/DD/YYYY") + " - " + t.format("MM/DD/YYYY"))
            });
        },
    };
    $(document).ready(function() {
        Finnace.init();
        var table = $('#js_finance_listing').DataTable({
            bProcessing: true,
            language: {
                "sProcessing":   "Đang xử lý...",
                "sLengthMenu":   "Xem _MENU_ mục",
                "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
                "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            },
            ajax: {
                url: "{{ route('finance-index-search') }}",
                type: "GET",
                data: function(data){
                    data.customsearch = {
                        'dm_id' : $('select[name="dm_id"]').val(),
                        'finance_status' : $('select[name="finance_status"]').val(),
                        'created_date' : $('input[name="created_date"]').val(),
                        'complete_date' : $('input[name="complete_date"]').val(),
                    };
                }
            },
            bPaginate:true,
            serverSide: true,
            sPaginationType:"full_numbers",
            searching: false,
            lengthMenu: [20, 50, 100, 150],
            pageLength: 20,
            aoColumns: [
                { mData: 'finance_id' },
                { mData: 'count' },
                { mData: 'finance_type' },
                { mData: 'accountant_name' },
                { mData: null,  mRender: function (data, type, full) {
                        if (full.status == 1) {
                            return `<span class="m-badge  m-badge--success m-badge--wide">Đã xử lý</span>`;
                        } else {
                            return `<span class="m-badge  m-badge--warning m-badge--wide">Chưa xử lý</span>`;
                        }
                }},
                { mData: 'creater_name' },
                { mData: 'created_date' },
                { mData: 'updated_date' },
                { mData: null , mRender: function (data, type, full) {
                        return `<a class="btn btn-info" href="{{url('fs/cpanel/finance/detail/${full.finance_id}')}}">
                                <i class="la la-pencil"></i> Xem
                            </a>`;
                }},
            ],
            columnDefs: [ {
                    targets: [0, 1, 2, 3, 4, 5, 6, 7, 8], // column or columns numbers
                    orderable: false,  // set orderable for selected columns
                }
            ]
        });
        $("#m_search").on("click", function(t) {
            table.ajax.reload();
            return false;
        });
    });
</script>
@endsection