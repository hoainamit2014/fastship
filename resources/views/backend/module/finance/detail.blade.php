@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ __('Chi tiết phiếu chuyển tiền') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div style="padding-bottom: 10px">
                <div id="status_success" @if ($aFinance->status) style="display: block" @else style="display: none" @endif>
                    <span class="m-badge  m-badge--success m-badge--wide">Đã xử lý</span>
                </div>
                <div id="status_not_success" @if (!$aFinance->status) style="display: block" @else style="display: none" @endif>
                    <button class="btn btn-primary" onclick="updateStatusFinance();"><i class="la la-check"></i> Xử lý</button>
                    <input type="hidden" value="{{ url('fs/cpanel/finance/updateStatusFinance', $aFinance->finance_id) }}" id="finance_updateStatusFinance">
                </div>
            </div>
            <div class="m-form m-form--fit" style="border: 1px solid #e3e3e3; background-color: #f5f5f5; padding: 10px">
                <div class="row">
                    <div class="col-lg-3">
                        Số đơn: <b>{{ $aFinance->count }}</b>
                    </div>
                    <div class="col-lg-3">
                        Phụ trách: <b>{{ $aFinance->creater_name }}</b>
                    </div>
                    <div class="col-lg-3">
                        Người nhận: <b>{{ $aFinance->accountant_name }}</b>
                    </div>
                    <div class="col-lg-3">
                        Tổng tiền: <b>{{ number_format($aFinance->total_money) }}</b>
                    </div>
                </div>
            </div><br>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded">
                <table class="m-datatable__table table table-bordered" style="overflow-x: auto;">
                    <thead class="m-datatable__head">
                        <tr class="m-datatable__row">
                            <th class="m-datatable__cell"><span style="width: 50px;font-size: 12px">STT</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">Mã</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">Mã DT</span></th>
                            <th class="m-datatable__cell"><span style="width: 400px;font-size: 12px">Địa chỉ</span></th>
                            <th class="m-datatable__cell"><span style="width: 200px;font-size: 12px">Tên KH</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">SĐT</span></th>
                            <th class="m-datatable__cell"><span style="width: 400px;font-size: 12px">Sản phẩm</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">KM</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">COD</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">Phí VC</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">Tổng thu</span></th>
                            <th class="m-datatable__cell"><span style="width: 100px;font-size: 12px">Trạng thái</span></th>
                        </tr>
                    </thead>
                    <tbody class="m-datatable__body">
                        @foreach ($aOrders as $iKey => $aOrder)
                        <tr class="m-datatable__row">
                            <td class="m-datatable__cell"><span style="width: 50px;font-size: 12px">{{ $iKey + 1 }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ $aOrder->order_code }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ $aOrder->customer_order_code }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 400px;font-size: 12px">{{ $aOrder->address }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 200px;font-size: 12px">{{ $aOrder->receiver_name }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ $aOrder->phone }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 400px;font-size: 12px">{{ $aOrder->product_name }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ $aOrder->gift }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ number_format($aOrder->cod) }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ number_format($aOrder->transport_fee) }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ number_format($aOrder->cod + $aOrder->transport_fee) }}</span></td>
                            <td class="m-datatable__cell"><span style="width: 100px;font-size: 12px">{{ $aOrder->status_name }}</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('public/backend/js/finance.js') }}"></script>
@endsection