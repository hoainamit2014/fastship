@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ __('Danh sách đơn hàng tính lương') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Từ khóa:</label>
                            <input type='text' class="form-control" name="key_word"/>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Loại danh sách:</label>
                            <select class="form-control" name="flow_order">
                                <option value="">Chọn:</option>
                                <option value="di_giao">Đơn đi giao</option>
                                <option value="tra_ve">Đơn trả về</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Loại đơn hàng:</label>
                            <select class="form-control" name="order_type">
                                <option value="">Chọn:</option>
                                <option value="1">Đơn giao hàng</option>
                                <option value="2">Đơn đổi hàng</option>
                                <option value="3">Đơn thu hồi</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>DM phụ trách:</label>
                            <select class="form-control" name="dm_id">
                                <option value="">Chọn:</option>
                                @foreach ($aUsers as $aUser)
                                <option value="{{ $aUser->id }}">{{ $aUser->user_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Ngày giao thành công:</label>
                            <input type='text' class="form-control m_daterangepicker_bill_1" name="complete_date"/>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <button class="btn btn-primary m-btn m-btn--icon" type="submit" id="m_search" style="margin-top: 34px">
                            <span>
                                <i class="la la-search"></i>
                                <span>Tìm kiếm</span>
                            </span>
                        </button>
                    </div>
                </div>
                <div style="border: 1px solid #e3e3e3; background-color: #f5f5f5; padding: 10px">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            Số đơn: <b id="order_amount"></b>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            Số tiền: <b id="money_amount"></b>
                        </div>
                    </div>
                </div><br>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="js_finance_listing" style="font-size: 12px">
                    <thead>
                        <tr>
                            <th></th>
                            <th>STT &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Mã hệ thống &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Mã đối tác &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>DM phụ trách &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Hệ số DC &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Hệ số khách hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Hệ số phường xã &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Hệ số cân nặng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Lương khoán &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Tiền lương &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Trạng thái &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Trạng thái vật lý &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Ngày giao thành công &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>DC phát &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    var Finnace = {
        init: function() {
            var a = moment().subtract(29, "days"),
            t = moment();
            $(".m_daterangepicker_bill_1").daterangepicker({
                buttonClasses: "m-btn btn btn-sm",
                applyClass: "btn-primary btn-sm",
                cancelClass: "btn-secondary btn-sm",
                startDate: a,
                autoUpdateInput:false,
                "showDropdowns": true,
                locale: {
                    customRangeLabel: "Tùy chỉnh",
                    applyLabel: "Áp dụng",
                    cancelLabel: "Hủy bỏ",
                    daysOfWeek: [
                    "CN",
                    "T2",
                    "T3",
                    "T4",
                    "T5",
                    "T6",
                    "T7"
                    ],
                    "monthNames": [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                    ],
                    "firstDay": 1
                },
                alwaysShowCalendars: true,
                endDate: t,
                ranges: {
                    'Hôm nay': [moment(), moment()],
                    'Hôm qua': [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "7 ngày gần đây": [moment().subtract(6, "days"), moment()],
                    "30 ngày gần đây": [moment().subtract(29, "days"), moment()],
                    "Tháng này": [moment().startOf("month"), moment().endOf("month")],
                    "Tháng cuối": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                }
            }, function(a, t, n) {
                $(".m_daterangepicker_bill_1").val(a.format("DD/MM/YYYY") + " - " + t.format("DD/MM/YYYY"))
            });
        },
    };
    $(document).ready(function() {
        Finnace.init();
        var table = $('#js_finance_listing').DataTable({
            bProcessing: true,
            language: {
                "sProcessing":   "Đang xử lý...",
                "sLengthMenu":   "Xem _MENU_ mục",
                "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
                "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            },
            ajax: {
                url: "{{ route('finance-order-salary-search') }}",
                type: "GET",
                data: function(data){
                    data.customsearch = {
                        'key_word' : $('input[name="key_word"]').val(),
                        'flow_order' : $('select[name="flow_order"]').val(),
                        'order_type' : $('select[name="order_type"]').val(),
                        'dm_id' : $('select[name="dm_id"]').val(),
                        'complete_date' : $('input[name="complete_date"]').val(),
                    };
                },
                complete: function(data) {
                    $("#order_amount").text(data.responseJSON.iTotalRecords);
                    $("#money_amount").text(data.responseJSON.iTotalSalary);
                }
            },
            bPaginate:true,
            serverSide: true,
            sPaginationType:"full_numbers",
            searching: false,
            lengthMenu: [20, 50, 100, 150],
            pageLength: 20,
            aoColumns: [
                { mData: null , mRender: function (data, type, full) {
                        return `<a href="{{url('fs/cpanel/order/show/')}}/?order_code=${full.order_code}">
                                <i class="la la-eye"></i>
                            </a>`;
                }},
                { mData: 'stt' },
                { mData: 'order_code' },
                { mData: 'customer_order_code' },
                { mData: 'dm_name' },
                { mData: 'dc_number' },
                { mData: 'customer_number' },
                { mData: 'district_number' },
                { mData: 'weight_number' },
                { mData: 'transport_fee' },
                { mData: 'salary' },
                { mData: 'status_name' },
                { mData: 'physical_status_name' },
                { mData: 'delivery_success_date' },
                { mData: 'dc_broadcasting_actual' },
            ],
            columnDefs: [ {
                    targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], // column or columns numbers
                    orderable: false,  // set orderable for selected columns
                }
            ]
        });
        $("#m_search").on("click", function(t) {
            table.ajax.reload();
            return false;
        });
    });
</script>
@endsection