@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ __('Chuyển tiền') }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-form m-form--fit">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Chọn kế toán nhận tiền:</label>
                                <select class="form-control" name="dm_id">
                                    <option value="">Chọn:</option>
                                    @foreach ($aUsers as $aUser)
                                    <option value="{{ $aUser->id }}">{{ $aUser->user_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <button class="btn btn-success m-btn m-btn--icon" type="submit" style="margin-top: 34px">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Tạo phiếu chuyển tiền</span>
                                </span>
                            </button>
                            <input type="hidden" value="1" name="submit">
                        </div>
                    </div>
                </form>
                <div style="border: 1px solid #e3e3e3; background-color: #f5f5f5; padding: 10px">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            Số đơn: <b>{{ number_format($aData->count_order) }}</b>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            Số tiền: <b>{{ number_format($aData->count_transport_fee + $aData->count_cod) }}</b>
                        </div>
                    </div>
                </div><br>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover" style="font-size: 12px">
                    <thead>
                        <tr style="white-space: nowrap;" class="text-center">
                            <th></th>
                            <th>STT</th>
                            <th>Mã</th>
                            <th>Mã đối tác</th>
                            <th>Tỉnh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Quận</th>
                            <th>Địa chỉ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>COD</th>
                            <th>Phí VC</th>
                            <th>Tổng thu</th>
                            <th>Trạng thái&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $( document ).ready(function() {
        var table = $('#js_finance_listing').DataTable({
            bProcessing: true,
            language: {
                "sProcessing":   "Đang xử lý...",
                "sLengthMenu":   "Xem _MENU_ mục",
                "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
                "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            },
            ajax: {
                url: "{{ route('finance-transfer-to-accountant-search') }}",
                type: "GET",
                data: function(data){
                }
            },
            bPaginate:true,
            serverSide: true,
            sPaginationType:"full_numbers",
            searching: false,
            lengthMenu: [20, 50, 100, 150],
            pageLength: 20,
            aoColumns: [
                { mData: null , mRender: function (data, type, full) {
                        return `<a href="{{url('fs/cpanel/order/show/')}}/?order_code=${full.order_code}">
                                <i class="la la-eye"></i>
                            </a>`;
                }},
                { mData: 'stt' },
                { mData: 'order_code' },
                { mData: 'customer_order_code' },
                { mData: 'province_name' },
                { mData: 'district_name' },
                { mData: 'address' },
                { mData: 'cod' },
                { mData: 'transport_fee' },
                { mData: 'sum' },
                { mData: 'status_name' },
            ],
            columnDefs: [ {
                    targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], // column or columns numbers
                    orderable: false,  // set orderable for selected columns
                }
            ]
        });
    });
</script>
@endsection