@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
						Quản lý nhóm người dùng
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-primary" href="{{ route('group-add') }}">
							Thêm mới
						</a>
					</h3>
				</div>	
			</div>
		</div>
		<div class="m-portlet__body">
			@if (!empty($aRows))
			<table class="table table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th>Tiêu đề</th>
						<th>Trạng thái</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($aRows as $iKey => $aRow)
					<tr>
						<td>{{ $iKey + 1 }}</td>
						<td>{{ $aRow->title }}</td>
						<td>
							@if ($aRow->is_active)
							<span class="label label-success">Bật</span>
							@else
							<span class="label label-danger">Tắt</span>
							@endif
						</td>
						<td>
							<a class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/group/edit', $aRow->user_group_id) }}" title="Sửa">
								<i class="fa fa-edit"></i>
							</a>
							<a class="btn btn-brand m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/group/setting', $aRow->user_group_id) }}"  title="Cài đặt">
								<i class="fa fa-cog"></i>
							</a>
							@if ($aRow->user_group_id > 2)
							<a class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/group/destroy', $aRow->user_group_id) }}" title="Xóa">
								<i class="fa fa-trash-o"></i>
							</a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
			<div class="alert alert-danger">
				Không tìm thấy nhóm người dùng nào
			</div>
			@endif
		</div>
	</div>
</div>
@endsection
