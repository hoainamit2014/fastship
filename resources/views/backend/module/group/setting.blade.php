@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					@if(isset($aRow->title)){{ $aRow->title }}@endif
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-secondary" href="{{ route('group-index') }}">
							<span>
								<i class="la la-angle-left"></i>
								<span>Trở về</span>
							</span>
						</a>
					</h3>
				</div>	
			</div>
		</div>

		<div class="m-portlet__body">
			@if (!empty($aRow))
			<form id="form-setting" action="{{ url('fs/cpanel/group/setting/update/'.$aRow->user_group_id) }}" method="post">
				{{ csrf_field() }}
				<div class="checkbox">
					<label>
						<input type="checkbox" value="" id="js_setting_all">
						Chọn tất cả
					</label>
				</div>
				@foreach ($aSettings as $aSetting)
				<div class="checkbox">
					<label>
						@if (isset($aCustoms[$aSetting->setting_id]))
							@if ($aCustoms[$aSetting->setting_id] == 1) 
							<input type="checkbox" class="js-setting-checkbox" checked="checked">
							@else
							<input type="checkbox" class="js-setting-checkbox">
							@endif
						@elseif ($aRow->user_group_id == 1)
							<input type="checkbox" class="js-setting-checkbox" @if($aSetting->default_admin == '1')checked="checked"@endif>
						@elseif ($aRow->user_group_id == 2)
							<input type="checkbox" class="js-setting-checkbox" @if($aSetting->default_user == '1')checked="checked"@endif>
						@else
							<input type="checkbox" class="js-setting-checkbox">
						@endif
						<input type="hidden" class="js-setting-input" name="val[{{ $aSetting->setting_id }}]">
						{{ $aSetting->title }}
					</label>
				</div>
				@endforeach
				<button type="submit" class="btn btn-primary">Cập nhật</button>
			</form>
			@else
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
				Không tìm thấy nhóm người dùng này
			</div>
			@endif
		</div>
	</div>
</div>
@endsection
