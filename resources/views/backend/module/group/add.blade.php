@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					@if( isset($aRow->user_group_id) )
						Sửa nhóm người dùng
					@else
						Thêm nhóm người dùng
					@endif
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-secondary" href="{{ route('group-index') }}">
							<span>
								<i class="la la-angle-left"></i>
								<span>Trở về</span>
							</span>
						</a>
					</h3>
				</div>	
			</div>
		</div>

		<div class="m-portlet__body">
			@if( isset($aRow->user_group_id) )
			<form action="{{ url('/fs/cpanel/group/update/'.$aRow->user_group_id)}}" method="POST">
			@else
			<form action="{{ route('group-store') }}" method="POST">
			@endif
				{{ csrf_field() }}
				<div class="form-group">
					<label>Tên nhóm</label>
					<input name="title" type="text" class="form-control" value="@if(isset($aRow->title)){{ $aRow->title }}@endif">
				</div>
				<div class="form-group">
					<label>Trạng thái</label>
					<select name="is_active" class="form-control">
						<option value="1" @if(isset($aRow->is_active) && $aRow->is_active)selected="selected"@endif>Bật</option>
						<option value="0" @if(isset($aRow->is_active) && !$aRow->is_active)selected="selected"@endif>Tắt</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary">
				@if (isset($aRow->user_group_id))
					Cập nhật
				@else
					Lưu
				@endif
				</button>
			</form>
		</div>
	</div>
</div>
@endsection

