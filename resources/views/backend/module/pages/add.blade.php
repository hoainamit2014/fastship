@extends('backend.app')
@section('title', 'Page Title')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		@if( isset($pages->edit) )
		<form class="frm-edit m-form m-form--fit m-form--label-align-right" action="{{ url('/fs/cpanel/pages/edit/'.$pages->id)}}" method="POST" style="width: 100%">
			@else
			<form class="frm-add" method="POST" class="m-form m-form--fit m-form--label-align-right " style="width: 100%">
				@endif
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="content-box-large">
					<div class="form-group m-form__group">
						<span>Tiêu đề:</span>

						<input type="text" class="form-control" name="name" value="{{ isset($pages->post_name) ? $pages->post_name : '' }}" placeholder="Nhập tiêu đề">
					</div>
					<div class="form-group m-form__group">
						<span>Nội dung:</span>
						<textarea class="form-control" id="content" name="content" placeholder="Nhập nội dung" rows="4" cols="50">{{ isset($pages->post_content) ? $pages->post_content : '' }}</textarea>
					</div>
					<div class="form-group m-form__group">
						<span>Từ khóa:</span>
						<input type="text" name="keyword" placeholder="Nhập từ khóa SEO" value="{{ isset($pages->post_keyword )? $pages->post_keyword : '' }}" class="form-control">
					</div>
					<div class="form-group m-form__group">
						<span>Mô tả trang:</span>
						<textarea rows="4" cols="50" name="excerpt" placeholder="Nhập mô tả trang" class="form-control">{{ isset($pages->post_excerpt) ? $pages->post_excerpt : '' }}</textarea>
					</div>
					<div class="form-group m-form__group">
						<span>Trang cha: </span>
						<select class="form-control" name="parent">
							<option value="">(no parent)</option>
							@if( isset($pages->edit) )
							<?php getPagesOptionsRecursive($parent, 0, '', $pages->post_parent);?>
							@else
							<?php getPagesOptionsRecursive($pages);?>
							@endif
						</select>
					</div>
					<div class="form-group m-form__group">
						<span>Trạng thái: </span>
						<div class="status-box">
							@if( isset($pages->edit) )
							<span><input type="radio"  name="status" value="1" {{ ($pages->post_status == 1) ? 'checked' : '' }}> Bật</span>
							<span><input type="radio" name="status" value="0" {{ ($pages->post_status == 0) ? 'checked' : '' }}> Tắt </span>
							@else
							<span><input type="radio"  name="status" value="1" checked> Bật</span>
							<span><input type="radio" name="status" value="0"> Tắt </span>
							@endif
						</div>
					</div>
					<div class="form-group m-form__group">
						<button type="submit" class="btn btn-success btn-add text-right" >
							@if( isset($pages->edit) )
							Cập nhật
							@else
							Thêm mới
							@endif
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection