@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<p>
		<a href="#" class="btn btn-success btn-sm m-btn m-btn--custom m-btn--icon">
			<span>
				<i class="la la-arrow-left"></i>
				<span>Danh sách phiếu
			</span>
		</a>
	</p>
	<div class="clearfix"></div>
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
						{{ __('Tạo phiếu nhận hàng vào kho từ DC') }}
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body form-group m-form m-form__group row ">
			<form class="row" method="POST" action="{{ route('pill-add') }}">
				{{ csrf_field() }}
				<div class="col-lg-3 col-sm-12 col-form-label col-xs-12"> {{ __('Chọn khách hàng cần nhập kho') }}</div>
				<div class="col-lg-7 col-sm-12 col-xs-12">
					<select class="form-control m-select2" id="m_select2_1" name="dc">
						<option>Khách hàng</option>
						@if(isset($customer))
							@foreach($customer as $k => $aVal)
								<option value="{{ $aVal['id'] }}">{{ $aVal['name'] }} ({{ $aVal['username'].' - '. $aVal['email'] }})</option>
							@endforeach
						@endif
					</select>
					<div class="form-control-feedback">{{ __('Để trống nếu bạn muốn unloading từ nhiều khách hàng') }}</div>
				</div>
				<div class="clearfix"></div>
				<div class="text-center" style="margin: 0 auto">
					<button type="submit" class="btn btn-sm btn-success m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15 text-white"><i class="la la-check"></i> {{ __('Tạo phiếu nhập kho') }}</button>
					<a href="" class="btn btn-sm text-white btn-danger m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15"><i class="la la-arrow-left"></i> {{ __('Danh sách đơn hàng') }}</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
