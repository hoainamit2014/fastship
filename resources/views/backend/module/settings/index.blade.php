 @extends('backend.app')
 @section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					Cài đặt
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<form class="form-group" action="{{ route('settings-update') }}" method="post" enctype='multipart/form-data'>
 			{{ csrf_field() }}
 			<table class="table">
 				<tbody>
 					<tr>
 						<td width="15%">Tên Website:</td>
 						<td><input type="text" name="website_name" class="form-control" value="{{ $aWebsiteName->option_value }}"></td>
 					</tr>
 					<tr>
 						<td>Logo Website:</td>
 						<td><input type="file" accept="image/*" name="image" class="form-control"></td>
 					</tr>
 					@if(!empty($aLogoWebsite->option_value))
 					<tr>
 						<td></td>
 						<td>
 							<img src="{{ $aLogoWebsite->option_value }}" width="200" alt="">
 						</td>
 					</tr>
 					@endif
 					<tr>
 						<td> Địa chỉ:</td>
 						<td><textarea class="form-control" name="address">{{ $aAddress->option_value }}</textarea></td>
 					</tr>
 					<tr>
 						<td>Email:</td>
 						<td><input type="text" name="email" class="form-control" value="{{ $aEmail->option_value }}"></td>
 					</tr>
 					<tr>
 						<td>Số điện thoại:</td>
 						<td><input type="text" name="phone" class="form-control" value="{{ $aPhone->option_value }}"></td>
 					</tr>
 					<tr>
 						<td>Facebook:</td>
 						<td><input type="text" name="facebook" class="form-control" value="{{ $aFacebook->option_value }}"></td>
 					</tr>
 					<tr>
 						<td>Instagram:</td>
 						<td><input type="text" name="instagram" class="form-control" value="{{ $aInstagram->option_value }}"></td>
 					</tr>
 					<tr>
 						<td>Twitter:</td>
 						<td><input type="text" name="twitter" class="form-control" value="{{ $aTwitter->option_value }}"></td>
 					</tr>
 					<tr>
 						<td>Linkedin:</td>
 						<td><input type="text" name="linkedin" class="form-control" value="{{ $aLinkedin->option_value }}"></td>
 					</tr>
 					<tr>
 						<td></td>
 						<td><button type="submit" class="btn btn-primary"> Cập nhật </button></td>
 					</tr>
 				</tbody>
 			</table>
 		</form>
		</div>
	</div>
</div>
 @endsection
