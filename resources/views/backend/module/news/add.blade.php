@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					@if( isset($aNew->new_id) )
						Sửa tin tức
					@else
						Thêm tin tức
					@endif
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-secondary" href="{{ route('news-index') }}">
							<span>
								<i class="la la-angle-left"></i>
								<span>Trở về</span>
							</span>
						</a>
					</h3>
				</div>	
			</div>
		</div>

		<div class="m-portlet__body">
			@if( isset($aNew->new_id) )
			<form action="{{ url('/fs/cpanel/news/update/'.$aNew->new_id)}}" method="POST">
			@else
			<form action="{{ route('news-store') }}" method="POST">
			@endif
				{{ csrf_field() }}
				<div class="form-group">
					<label>Tiêu đề</label>
					<input name="title" type="text" class="form-control" value="@if(isset($aNew->title)){{ $aNew->title }}@endif">
				</div>
				<div class="form-group">
					<label>Nội dung</label>
					<textarea name="content" class="form-control" rows="2">@if(isset($aNew->content)){{ $aNew->content }}@endif</textarea>
				</div>
				<div class="form-group">
					<label>Từ khóa SEO</label>
					<input name="keyword" class="form-control" value="@if(isset($aNew->keyword)){{ $aNew->keyword }}@endif" type="text">
				</div>
				<div class="form-group">
					<label>Mô tả trang</label>
					<textarea name="description" class="form-control" rows="2">@if(isset($aNew->description)){{ $aNew->description }}@endif</textarea>
				</div>
				<div class="form-group">
					<label>Trạng thái</label>
					<select name="is_active" class="form-control">
						<option value="1" @if(isset($aNew->is_active) && $aNew->is_active)selected="selected"@endif>Bật</option>
						<option value="0" @if(isset($aNew->is_active) && !$aNew->is_active)selected="selected"@endif>Tắt</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary">
				@if (isset($aNew->new_id))
					Cập nhật
				@else
					Lưu
				@endif
				</button>
			</form>
		</div>
	</div>
</div>
@endsection

