@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					Quản lý tin tức
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-primary" href="{{ route('news-add') }}">
							Thêm mới
						</a>
					</h3>
				</div>	
			</div>
		</div>
		<div class="m-portlet__body">
			@if (!empty($aNews))
			<table class="table table-striped">
				<thead>
					<tr>
						<th>STT</th>
						<th>#</th>
						<th>Tiêu đề</th>
						<th>Người tạo</th>
						<th>Trạng thái</th>
						<th>Ngày đăng</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($aNews as $iKey => $aNew)
					<tr>
						<td>{{ $iKey + 1 }}</td>
						<td>{{ $aNew->new_id }}</td>
						<td>{{ $aNew->title }}</td>
						<td>@if(!empty($aNew->user_name)){{ $aNew->user_name }}@else
						Không có
						@endif</td>
						<td>
							@if ($aNew->is_active)
							<span class="label label-success">Bật</span>
							@else
							<span class="label label-danger">Tắt</span>
							@endif
						</td>
						<td>{{ date("d-m-Y h:i:s", strtotime($aNew->created_at)) }}</td>
						<td>
							<a class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/news/destroy', $aNew->new_id) }}">
								<i class="fa fa-trash-o"></i>
							</a>
							<a class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/news/edit', $aNew->new_id) }}">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $aNews->links('backend.layouts.pagination') }}
			@else
			<div class="alert alert-danger">
				Không tìm thấy mục tin tức nào
			</div>
			@endif
		</div>
	</div>
</div>
@endsection
