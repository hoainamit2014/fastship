@extends('backend.app')
@section('title', 'Page Title')
@section('content')

<div class="col-md-10">
	<div class="manage-menus">
		<form method="GET">
			<span>Chọn menu để sửa :</span>
			<input type="hidden" name="action" value="edit">
			<select name="menu" id="select-menu-to-edit">
				@if(isset($data['navs_menu']) && !empty($data['navs_menu']) )
					@foreach($data['navs_menu'] as $key => $val)
						<option
						@if(isset($_GET['menu']) && $_GET['menu'] == $val['term_id'])
							selected="selected"
						@endif
						value="{{ $val['term_id'] }}">{{ $val['name']. (!empty($val['display_position']) ? ('('.$val['display_position'].')') : '') }}</option>
					@endforeach
				@else
					<option>Chọn menu</option>
				@endif
			</select>
			<span class="submit-btn">
				<input type="submit" class="button" value="Chọn">
			</span>

			<span class="add-new-menu-action pull-right">
				<?php if (isset($_GET['action']) && $_GET['action'] != 'add'): ?>
					<a href="/fs/cpanel/menu?module=add-menu&action=add" class="btn btn-outline-success btn-sm m-btn m-btn--custom"><i class="la la-plus"></i> Thêm mới </a>
				<?php else: ?>
					<a href="/fs/cpanel/menu" class="btn btn-outline-success btn-sm m-btn m-btn--custom"><i class="la la-arrow-left"></i> Quay lại </a>
				<?php endif;?>
			</span>
			<span class="submit-btn">
				<a href="/fs/cpanel/menu?module=delete-menu&action=delete&menu={{ (isset($_GET['menu']) && !empty($_GET['menu'])) ? intval($_GET['menu']) : '' }}" class="button" value="">Xóa</a>
			</span>
		</form>
	</div>
	<div class="manage-management">
		@if(isset($_GET['action']) && $_GET['action'] == 'add')
		<div class="content-box-large col-sm-5">
			<form class="frm-add-menu" method="POST" action="{{ url('/fs/cpanel/menu/add') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<span>Tên menu :</span>
				<input type="text" name="name" placeholder="Nhập tên menu" class="form-control">
				<button type="submit" class="btn btn-success btn-submit-menu m-btn--wide">Thêm</button>
			</form>
		</div>
		@else
		<div class="wrapper-pages col-md-12">

			<div class="row">
				<div class="col-sm-3">
					<div class="row">
					<div class="content-box-large frm-menu">
						<h3>Thêm danh mục</h3>
						<form method="POST" action="{{ url('/fs/cpanel/menu/add') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="sub" value="1">
							<input type="hidden" name="parent_menu" value="{{ (isset($_GET['menu']) && !empty($_GET['menu'])) ? intval($_GET['menu']) : '' }}">
							<div class="input-name">
								<span>Tên danh mục:</span>
								<input type="text" required placeholder="Nhập tên menu" class="form-control" name="name">
							</div>
							<div class="input-name">
								<span>Danh mục cha:</span>
								<select class="form-control" name="parent">
									<option value="{{ (isset($_GET['menu']) && !empty($_GET['menu'])) ? intval($_GET['menu']) : '' }}">Chọn danh mục</option>
									@if( isset($data['menu_list']) && !empty($data['menu_list']))
										{{ Helpers::getMenuOptionRecursive($data['menu_list'],$_GET['menu']) }}
									@endif
								</select>
							</div>
							<button type="submit" class="btn btn-primary btn-sm m-btn m-btn--custom btn-add-menu">Thêm</button>
						</form>
					</div>
					</div>
				</div>
				<div class="col-sm-9">

					<div class="clearfix"></div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#ID</th>
								<th>Menu</th>
								<th class="text-center">Danh mục cha</th>
								<th class="text-center">Trạng thái </th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@if($data['menu_list'])
								{{ Helpers::getMenuTableRecursive($data['menu_list'],$_GET['menu']) }}
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>
@endsection