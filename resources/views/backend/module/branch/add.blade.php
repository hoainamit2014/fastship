@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    @if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <p>
                <a href="{{ route('branch-index') }}"> <i class="la la-arrow-left"></i> {{  __('Quay lại')  }} </a></p>
                <div class="m-portlet m-portlet--tab">
                    <form method="POST" action="{{ isset($aEdit) ? route('branch-update',['id' => intval($_GET['id'])]) : route('branch-add-post') }}" class="m-form m-form--fit m-form--label-align-right">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        {{ __('THÊM CHI NHÁNH') }}
                                    </h3>
                                </div>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn m-btn--pill m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn-save-users"><i class="la la-save"></i>
                                    @if(!isset($aEdit))
                                    {{ __('Lưu lại') }}
                                    @else
                                    {{ __('Cập nhật') }}
                                    @endif
                                </button>
                            </div>
                        </div>

                        <div class="m-portlet__body row">
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label>{{  __('Chọn Thành Phố') }} </label>
                                    <select class="form-control m-input m-input--square" name="province" onchange="return selectProvince(this)">
                                        <option value="">{{ __('Chọn chi nhánh') }}</option>
                                        @if($province)
                                            @foreach($province as $k => $v)
                                            <option value="{{ $v['province_id'] }}"
                                            >{{ $v['name'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{ __('Chọn quận / huyện') }}</label>
                                    <select class="m-loader m-loader--primary form-control m-input m-input--square" name="district">
                                        <option value="">
                                            {{ __('Chọn quận / huyện') }}
                                        </option>
                                        @if(isset($data['district']))
                                            @foreach($data['district'] as $k => $v)
                                            <option value="{{ $v['district_id'] }}">{{ $v }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{  __('Địa chỉ') }} </label>
                                    <input type="text" name="address" class="form-control m-input m-input--square" value="{{ old('address') }}" placeholder="{{ __('Nhập địa chỉ') }}">
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{  __('Thứ tự') }} </label>
                                    <input type="text" name="sort" class="form-control m-input m-input--square" value="{{ old('sort') }}" placeholder="{{ __('Nhập số thứ tự') }}">
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{  __('Thuộc DC cha') }} </label>
                                    <select class="form-control m-input m-input--square" name="parent_id">
                                        <option value="">Không</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label>{{  __('Khu vực phụ trách') }} </label>
                                    <select class="form-control m-input m-input--square" name="area_responsible">
                                        <option value="">Không</option>
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{  __('Hệ số') }} </label>
                                    <input type="text" name="coefficient" class="form-control m-input m-input--square" value="{{ old('brand_name')}}" placeholder="{{ __('Hệ số') }}">
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{  __('Triển khai đơn loading') }} </label>
                                    <select class="form-control m-input m-input--square" name="deploy_order_loading">
                                        <option value="0">{{ __('Không') }}</option>
                                        <option value="1">{{ __('Có') }}</option>
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{  __('Trung tâm') }} </label>
                                    <select class="form-control m-input m-input--square" name="center_branch">
                                        <option value="0">{{ __('Không') }}</option>
                                        <option value="1">{{ __('Có') }}</option>
                                    </select>
                                </div>
                                <div class="form-group m-form__group">
                                    <label>{{  __('Giao hàng của địa chỉ khác') }} </label>
                                    <select class="form-control m-input m-input--square" name="delivery_of_other_address">
                                        <option value="0">{{ __('Không') }}</option>
                                        <option value="1">{{ __('Có') }}</option>
                                    </select>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var BootstrapDatetimepickers = {
        init: function() {
            $(".datetimepicker").datetimepicker({
                format: "yyyy/mm/dd",
                todayHighlight: !0,
                autoclose: !0,
                startView: 2,
                minView: 2,
                forceParse: 0,
                pickerPosition: "bottom-left"
            })
        }
    };
    jQuery(document).ready(function() {
        BootstrapDatetimepickers.init()
    });

    var selectProvince = function($this){
        var id = $($this).val();

        $.ajax({
            type : 'GET',
            url : '{{ route("branch-add") }}?_id='+ id,
            data : {
                id : id,
                _token : $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function( xhr ) {
                $('select[name="district"]').empty();
                $('select[name="district"]').attr('disabled',true);
                $('select[name="district"]').append('<option value="">Chọn quận / huyện</option>');
            },
            success : function(response){
                var data = $.parseJSON(response);
                if(data['district'] !== ''){
                    $.each(data['district'],function(i,val){
                        $('select[name="district"]').append('<option value="'+ val['district_id'] +'">'+ val['name'] +'</option>');
                    });
                    $('select[name="district"]').attr('disabled',false);
                }
            }
        });
        return false;
    }

</script>
@endsection
