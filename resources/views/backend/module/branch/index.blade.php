@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	@if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Quản lý người dùng') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<a href="{{ route('branch-add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
					<span>
						<i class="la la-plus"></i>
						<span>{{ __('Thêm mới') }}</span>
					</span>
				</a>
			</div>
		</div>
		<div class="m-portlet__body m-wrapper-user">
			<!--begin: Datatable -->
            <div class="m-form m-form--label-align-right m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-3">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input m-input--solid" placeholder="{{ __('Tìm kiếm') }}" id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
			<div class="m_datatable"></div>
			<!--end: Datatable -->
		</div>
	</div>
</div>
<div class="modal fade" id="resetpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" class="m-form frm_resetPW">
                    <label>Mật khẩu:</label>
                    <input type="hidden" name="random_user_id" value="">
                    <div class="m-input-icon m-input-icon--right">
                        <input type="text" class="form-control m-input generate_passwd" disabled>
                        <span class="m-input-icon__icon m-input-icon__icon--right" onclick="return generate()"><span><i class="la la-lock"></i></span></span>

                    </div>
                    <span class="m-form__help">{{ __('Vui lòng lưu lại mật khẩu khi khôi phục') }}</span><br>
                    <button type="button" class="btn btn-primary btn-sm m--margin-top-10" onclick="return confirmReset()">{{ __('Xác nhận mật khẩu') }}</button>
                    <button type="button" class="btn btn-danger close-frm btn-sm m--margin-top-10" data-dismiss="modal">{{ __('Đóng') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	var DatatableDataLocalDemo = {
    init: function() {
        var e, a, i;
        e = JSON.parse('{!! $aReturn !!}');

        a = $(".m_datatable").mDatatable({
            data: {
                type: "local",
                source: e,
                pageSize: 10
            },
            layout: {
                theme: "default",
                class: "",
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch")
            },
            columns: [
            {
                field: "id",
                width: 40,
                title: "#",
                class:'text-left',
                responsive: {
                    visible: "lg"
                }
            },
            {
                field: "province_id",
                width: 150,
                title: "Tên DC",
                class:'text-left',
                responsive: {
                    visible: "lg"
                }
            },
            {
                field: "coefficient",
                width: 50,
                title: "Hệ số",
                class:'text-center',
                responsive: {
                    visible: "lg"
                }
            },
            {
                field: "address",
                width: 300,
                title: "Địa chỉ",
                class:'text-left',
                responsive: {
                    visible: "lg"
                }
            },

            {
                field: "center",
                width: 100,
                title: "Trung tâm",
                class:'text-left',
                responsive: {
                    visible: "lg"
                },
                template: function(e, a, i) {
                    var event = {
                        0: {
                            title: "Không có",
                        },
                        1: {
                            title: "có",
                        }
                    };
                    return event[e.center].title;
                }
            },
            {
                field: "other_dc_interface",
                width: 200,
                title: "Giao hàng của DC khác",
                class:'text-center',
                responsive: {
                    visible: "lg"
                },
                template: function(e, a, i) {
                    var event = {
                        0: {
                            title: "Không có",
                        },
                        1: {
                            title: "có",
                        }
                    };
                    return event[e.other_dc_interface].title;
                }

            },
            {
                field: "Actions",
                width: 110,
                title: "Quản lý",
                sortable: !1,
                overflow: "visible",
                template: function(e, a, i) {
                    var lock = {
                        1: {
                            title: "Khóa",
                        },
                        2: {
                            title: "Mở khóa",
                        }
                    };
                    return '<a href="{{ route("branch-edit") }}?id='+e.id+'"><i class="la la-edit"></i></a> <a href="{{ route("branch-delete") }}?id='+e.id+'" class="delete_Row"><i class="la la-close"></i></a>'
                }
            }]
        });

        i = a.getDataSourceQuery();

        $("#m_form_status").on("change", function() {
            a.search($(this).val(), "profile_status")
        }).val(void 0 !== i.Status ? i.Status : "");

        $('#generalSearchs').on( 'keyup', function () {
            a.search( this.value ).draw();
        } );

        $("#m_form_insurrance").on("change", function() {
            a.search($(this).val(), "insurrance")
        }).val(void 0 !== i.Type ? i.Type : "");

    }
};
jQuery(document).ready(function() {
    DatatableDataLocalDemo.init();
    $('.delete_Row').on('click',function(e){
        if(confirm('Bạn có muốn xóa chi nhánh này không ')){
            return true;
        }else{
            e.preventDefault();
        }
    });
});

</script>
@endsection
