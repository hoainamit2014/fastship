@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
						Quản lý trình chiếu
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-primary" href="{{ route('slides-add') }}">
							Thêm mới
						</a>
					</h3>
				</div>	
			</div>
		</div>
		<div class="m-portlet__body">
			@if (!empty($aSlides))
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Tiêu đề chính</th>
							<th>Tiêu đề con</th>
							<th>Đường link</th>
							<th>Hình ảnh</th>
							<th>Trạng thái</th>
							<th style="width: 130px"></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($aSlides as $iKey => $aSlide)
						<tr>
							<td>{{ $iKey + 1}}</td>
							<td>{{ $aSlide->title }}</td>
							<td>{{ $aSlide->subtitle }}</td>
							<td>{{ $aSlide->link }}</td>
							<td>
								@if(!empty($aSlide->image_path))
								<img src="{{ $aSlide->image_path }}" alt="image_path" width="200">
								@endif
							</td>
							<td>
								@if($aSlide->is_active)
								<span class="label label-success">Bật</span>
								@else
								<span class="label label-danger">Tắt</span>
								@endif
							</td>
							<td>
								<a class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/slides/destroy', $aSlide->slide_id) }}">
									<i class="fa fa-trash-o"></i>
								</a>
								<a class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only" href="{{ url('fs/cpanel/slides/edit', $aSlide->slide_id) }}">
									<i class="fa fa-edit"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@else
			<div class="alert alert-danger">
				Không tìm thấy mục trình chiếu nào
			</div>
			@endif
		</div>
	</div>
</div>
@endsection