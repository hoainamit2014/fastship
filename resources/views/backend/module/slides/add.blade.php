@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title pull-left">
					<h3 class="m-portlet__head-text">
					@if (isset($aSlide->slide_id))
						Sửa trình chiếu
					@else
						Thêm trình chiếu
					@endif
					</h3>
				</div>
				<div class="m-portlet__head-title pull-right">
					<h3 class="m-portlet__head-text">
						<a class="btn btn-secondary" href="{{ route('slides-index') }}">
							<span>
								<i class="la la-angle-left"></i>
								<span>Trở về</span>
							</span>
						</a>
					</h3>
				</div>	
			</div>
		</div>

		<div class="m-portlet__body">
			<form action="{{ route('slides-store') }}" method="POST" enctype='multipart/form-data'>
				{{ csrf_field() }}
				<div class="form-group">
					<label>Tựa đề chính</label>
					<textarea name="title" class="form-control" rows="2">@if(!empty($aSlide->title)){{ $aSlide->title }}@endif</textarea>
				</div>
				<div class="form-group">
					<label>Tựa đề con</label>
					<textarea name="subtitle" class="form-control" rows="2">@if(!empty($aSlide->subtitle)){{ $aSlide->subtitle }}@endif</textarea>
				</div>
				<div class="form-group">
					<label>Đường link</label>
					<input type="text" class="form-control" name="link" 
						@if (!empty($aSlide->title))
						value="{{ $aSlide->link }}"
						@endif
					>
				</div>
				<div class="form-group">
					<label>Hình ảnh</label>
					<input type="file" class="form-control" accept="image/*" name="image">
					@if (isset($aSlide->image_path))
					<input type="hidden" class="form-control" name="image_path" value="{{ $aSlide->image_path }}">
					<img src="{{ $aSlide->image_path }}" alt="image_path" width="200">
					@endif
				</div>
				@if (isset($aSlide->slide_id))
				<div class="form-group">
					<label>Trạng thái</label>
					<select name="is_active" class="form-control">
						<option value="1" @if($aSlide->is_active)selected="selected"@endif>Bật</option>
						<option value="0" @if(!$aSlide->is_active)selected="selected"@endif>Tắt</option>
					</select>
				</div>
				<input type="hidden" class="form-control" name="slide_id" value="{{ $aSlide->slide_id }}">
				@else
				<div class="form-group">
					<label>Trạng thái</label>
					<select name="is_active" class="form-control">
						<option value="1">Bật</option>
						<option value="0">Tắt</option>
					</select>
				</div>
				@endif
			
				<button type="submit" class="btn btn-primary">
				@if (isset($aSlide->slide_id))
					Cập nhật
				@else
					Lưu
				@endif
				</button>
			</form>
		</div>
	</div>
</div>
@endsection