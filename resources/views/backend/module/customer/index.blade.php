@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	@if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
       <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
         <h3 class="m-portlet__head-text">
          {{ __('Quản lý khách hàng') }}
      </h3>
  </div>
</div>
<div class="m-portlet__head-tools">
    <a href="{{ route('customer-add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
     <span>
      <i class="la la-plus"></i>
      <span>{{ __('Thêm mới') }}</span>
  </span>
</a>
</div>
</div>
<div class="m-portlet__body m-wrapper-user">
   <!--begin: Datatable -->
   <div class="m-form m-form--label-align-right m--margin-bottom-30">
    <div class="row align-items-center">
        <div class="col-xl-12 order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">
                <div class="col-md-3">
                    <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input m-input--solid" placeholder="{{ __('Tìm kiếm') }}" id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                            <span><i class="la la-search"></i></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="m_datatable"></div>
<!--end: Datatable -->
</div>
</div>
</div>

<div class="modal fade" id="resetpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" class="m-form frm_resetPW">
                    <label>Mật khẩu:</label>
                    <input type="hidden" name="random_user_id" value="">
                    <div class="m-input-icon m-input-icon--right">
                        <input type="text" class="form-control m-input generate_passwd" disabled>
                        <span class="m-input-icon__icon m-input-icon__icon--right" onclick="return generate()"><span><i class="la la-lock"></i></span></span>

                    </div>
                    <span class="m-form__help">{{ __('Vui lòng lưu lại mật khẩu khi khôi phục') }}</span><br>
                    <button type="button" class="btn btn-primary btn-sm m--margin-top-10" onclick="return confirmReset()">{{ __('Xác nhận mật khẩu') }}</button>
                    <button type="button" class="btn btn-danger close-frm btn-sm m--margin-top-10" data-dismiss="modal">{{ __('Đóng') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var DatatableDataLocalDemo = {
        init: function() {
            var e, a, i;
            e = JSON.parse('{!! $aReturn !!}');

            a = $(".m_datatable").mDatatable({
                data: {
                    type: "local",
                    source: e,
                    pageSize: 10
                },
                layout: {
                    theme: "default",
                    class: "",
                    scroll: !1,
                    footer: !1
                },
                sortable: !0,
                pagination: !0,
                search: {
                    input: $("#generalSearch")
                },
                columns: [
                {
                    field: "id",
                    title: "#",
                    width: 50,
                    sortable: !1,
                    textAlign: "center",
                },
                {
                    field: "user_name",
                    width: 100,
                    title: "Tên đăng nhập",
                    class:'text-center',
                    responsive: {
                        visible: "lg"
                    }
                },
                {
                    field: "full_name",
                    width: 100,
                    title: "Họ và tên",
                    class:'text-center',
                    responsive: {
                        visible: "lg"
                    }
                },
                {
                    field: "email",
                    width: 200,
                    title: "Email",
                    class:'text-center',
                    responsive: {
                        visible: "lg"
                    }
                },
                {
                    field: "phone",
                    width: 100,
                    title: "Số điện thoại",
                    class:'text-center',
                    responsive: {
                        visible: "lg"
                    }
                },
                {
                    field: "customer_code",
                    width: 150,
                    title: "Mã Khách Hàng",
                    class:'text-center',
                    responsive: {
                        visible: "lg"
                    }
                },
                {
                    field: "status",
                    title: "Trạng Thái",
                    width: 100,
                    template: function(e) {
                        var a = {
                            1: {
                                title: "Hoạt động",
                                class: "m-badge--success"
                            },
                            0: {
                                title: "Đang bị khóa",
                                class: " m-badge--danger"
                            }
                        };
                        if(e.status == 1 || e.status == 0)
                        {
                            return '<span class="m-badge ' + a[e.status].class + ' m-badge--wide">' + a[e.status].title + "</span>"
                        }else{
                            return '';
                        }
                    }
                },
                {
                    field: "Actions",
                    width: 80,
                    title: "Quản lý",
                    sortable: !1,
                    overflow: "visible",
                    template: function(e, a, i) {
                        var price = {
                            1 : {
                                title : 'Bỏ cài đặt giá mặc định'
                            },
                            0 : {
                                title : 'Cài cài đặt giá mặc định'
                            },
                        }
                        var apply_fee_default = (e.apply_default_fee == 1) ? 0 : 1;

                        return '<div class="dropdown"><a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false"> <i class="la la-ellipsis-h"></i> </a><div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"><a class="dropdown-item" href="{{ route("customer-apply-price-default") }}?id='+e.id+'&apply_fee_default='+apply_fee_default+'" onclick="return applyPriceDefault('+e.apply_default_fee+')"><i class="la la-edit"></i> '+price[e.apply_default_fee].title+'</a> <a class="dropdown-item" href="{{ route("customer-edit") }}?id='+e.id+'"> <i class="la la-print"></i> {{ __("Sửa") }} </a><a class="dropdown-item resetPasswd" onclick="return setID('+e.id+')" href="#" data-toggle="modal" data-target="#resetpassword"><i class="la la-print"></i> {{ __("Reset mật khẩu") }}</a> <a class="dropdown-item delete-customer" href="{{ route("customer-delete") }}?id='+e.id+'"><i class="la la-print"></i> Xóa</a> </div></div>';
                    }
                }
                ]
            });

i = a.getDataSourceQuery();
}
};
$(document).ready(function() {
    DatatableDataLocalDemo.init();
    $('.delete-customer').on('click',function(){
        if(confirm('Bạn có đồng ý xóa dữ liệu này không ?')){
            return true;
        }else{
            return false;
        }
    });
});
var randomPassword = function(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

var setID = function($id){ $('input[name="random_user_id"]').val($id); }
var generate = function() { $('input.generate_passwd').val(randomPassword(20)); }
var confirmReset = function(){
    var data = {
        _token : $('meta[name="csrf-token"]').attr('content'),
        id : $('input[name="random_user_id"]').val(),
        password : $('input.generate_passwd').val(),
    };
    $.ajax({
        type : 'POST',
        data : data,
        url : '{{ route("customer-pw-generate") }}',
        success:function(data){
            if(data.status = 201){
                toastr.success(data.message);
                setTimeout(function(){
                 window.location.reload();
             },1000);

            }
        }
    })
}
var applyPriceDefault = function($id)
{
    var message = ($id == 1) ? 'bỏ áp dụng' : 'áp dụng';
    if(confirm('Bạn có muốn '+message+' bảng giá mặc định cho tài khoản này không ? ')){
        return true;
    } else {
        return false;
    }
}
</script>
@endsection
