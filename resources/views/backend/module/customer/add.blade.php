@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    @if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <p>
                <a href="{{ route('customer-index') }}"> <i class="la la-arrow-left"></i> {{  __('Quay lại')  }} </a></p>
                <div class="m-portlet m-portlet--tab">
                    <form method="POST" action="{{ isset($aEdit) ? route('customer-update',['id' => intval($_GET['id'])]) : route('customer-add-post') }}" class="m-form m-form--fit m-form--label-align-right">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        {{ __('THÊM KHÁCH HÀNG MỚI') }}
                                    </h3>
                                </div>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn m-btn--pill m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn-save-users"><i class="la la-save"></i>
                                    @if(!isset($aEdit))
                                    {{ __('Lưu lại') }}
                                    @else
                                    {{ __('Cập nhật') }}
                                    @endif
                                </button>
                            </div>
                        </div>

                        <div class="m-portlet__body row">
                            <div class="col-md-6">
                               <div class="form-group m-form__group">
                                <label>{{  __('Tên đăng nhập') }} </label>
                                <input type="text" name="username" class="form-control m-input m-input--square" value="{{ old('username') }}{{ !isset($aEdit->user_name) ? '' : $aEdit->user_name }}" placeholder="{{ __('Nhập tên đăng nhập') }}">
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{  __('Họ và tên') }}</label>
                                <input type="text" value="{{ old('fullname') }}{{ !isset($aEdit->full_name) ? '' : $aEdit->full_name }}" name="fullname" class="form-control m-input m-input--square" placeholder="{{ __('Nhập họ và tên') }}">
                            </div>
                            @if(!isset($aEdit))
                            <div class="form-group m-form__group">
                                <label>{{  __('Mật khẩu') }}</label>
                                <input type="password" value="" name="password" class="form-control m-input m-input--square" placeholder="{{ __('Nhập mật khẩu') }}">
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{  __('Nhập lại mật khẩu') }}</label>
                                <input type="password" value="" class="form-control m-input m-input--square" name="confirm_password" placeholder="{{ __('Nhập lại mật khẩu') }} ">
                            </div>
                            @endif
                            <div class="form-group m-form__group">
                                <label>{{  __('Địa chỉ') }}</label>
                                <textarea class="form-control m-input m-input--square" placeholder="{{  __('Nhập địa chỉ') }}" name="address">{{ old('address') }}{{ !isset($aEdit->address) ? '' : $aEdit->address }}</textarea>
                            </div>


                        </div>
                        <div class="col-md-6">

                            <div class="form-group m-form__group">
                                <label>{{  __('Email') }}</label>
                                <input type="email" value="{{ old('email') }}{{ !isset($aEdit->email) ? '' : $aEdit->email }}" name="email" class="form-control m-input m-input--square" placeholder="Nhập địa chỉ email">
                            </div>

                            <div class="form-group m-form__group">
                                <label>{{ __('Số điện thoại liên hệ') }}</label>
                                <input type="text" name="phone" value="{{ old('phone') }}{{ !isset($aEdit->phone) ? '' : $aEdit->phone }}" class="form-control m-input m-input--square"  placeholder="{{ __('Nhập số điện thoại') }}">
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ __('Mã khách hàng') }}</label>
                                <input type="text" name="customer_code" value="{{ old('phone') }}{{ !isset($aEdit->phone) ? '' : $aEdit->phone }}" class="form-control m-input m-input--square"  placeholder="{{ __('Nhập mã khách hàng') }}">
                            </div>
                            <div class="form-group m-form__group">
                                <label>{{ __('Áp dụng bảng giá mặc định ') }}</label>
                                <br>

                                <span class="m-switch m-switch--icon-check">
                                    <label>
                                        <input type="checkbox" name="apply_fee_default" value="1" >
                                        <span></span>
                                    </label>
                                </span>
                            </div>

                            <div class="form-group m-form__group">
                                <label>{{ __('Trạng thái') }}</label>
                                <select class="form-control m-input m-input--square" name="status">
                                    <option value="1" {{ !isset($aEdit->insurrance) ? '' : (($aEdit->insurrance) == 1 ? 'selected' : '')  }}>{{ __('Hoạt động') }}</option>
                                    <option value="0">{{ __('Bị khóa') }}</option>
                                </select>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

@endsection
