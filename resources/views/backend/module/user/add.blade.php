@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    @if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <p>
            <a href="{{ route('users-index') }}"> <i class="la la-arrow-left"></i> {{  __('Quay lại')  }} </a></p>
            <div class="m-portlet m-portlet--tab">
                <form method="POST" action="{{ isset($aEdit) ? route('users-update',['id' => intval($_GET['id'])]) : route('users-add-post') }}" class="m-form m-form--fit m-form--label-align-right">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                {{ __('THÊM TÀI KHOẢN MỚI') }}
                            </h3>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn m-btn--pill m-btn m-btn--gradient-from-success m-btn--gradient-to-accent btn-save-users"><i class="la la-save"></i>
                            @if(!isset($aEdit))
                                {{ __('Lưu lại') }}
                            @else
                                {{ __('Cập nhật') }}
                            @endif
                        </button>
                   </div>
               </div>

                 <div class="m-portlet__body row">
                    <div class="col-md-6">
                       <div class="form-group m-form__group">
                        <label>{{  __('Tên đăng nhập') }} </label>
                        <input type="text" name="username" class="form-control m-input m-input--square" value="{{ old('username') }}{{ !isset($aEdit->user_name) ? '' : $aEdit->user_name }}" placeholder="{{ __('Nhập tên đăng nhập') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{  __('Họ và tên') }}</label>
                        <input type="text" value="{{ old('fullname') }}{{ !isset($aEdit->full_name) ? '' : $aEdit->full_name }}" name="fullname" class="form-control m-input m-input--square" placeholder="{{ __('Nhập họ và tên') }}">
                    </div>
                    @if(!isset($aEdit))
                    <div class="form-group m-form__group">
                        <label>{{  __('Mật khẩu') }}</label>
                        <input type="password" value="" name="password" class="form-control m-input m-input--square" placeholder="{{ __('Nhập mật khẩu') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{  __('Nhập lại mật khẩu') }}</label>
                        <input type="password" value="" class="form-control m-input m-input--square" name="confirm_password" placeholder="{{ __('Nhập lại mật khẩu') }} ">
                    </div>
                    @endif
                    <div class="form-group m-form__group">
                        <label>{{  __('Địa chỉ') }}</label>
                        <textarea class="form-control m-input m-input--square" placeholder="{{  __('Nhập địa chỉ') }}" name="address">{{ old('address') }}{{ !isset($aEdit->address) ? '' : $aEdit->address }}</textarea>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{  __('Email') }}</label>
                        <input type="email" value="{{ old('email') }}{{ !isset($aEdit->email) ? '' : $aEdit->email }}" name="email" class="form-control m-input m-input--square" placeholder="Nhập địa chỉ email">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{  __('Năm sinh') }}</label>
                        <div class="input-group date" >
                            <input type="text" name="birthday" class="form-control m-input datetimepicker" value="{{ old('birthday') }}{{ !isset($aEdit->birthday) ? '' : $aEdit->birthday }}" placeholder="{{ __('Chọn ngày hết hạn') }}"  />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label> {{ __('Giới tính') }}</label>
                        <select class="form-control m-input m-input--square" name="sex">
                            <option value="">{{ __('Chọn giới tính') }}</option>
                            <option value="1" {{ (old('sex') == 1) ? 'selected' : '' }} {{ !isset($aEdit->sex) ? '' : (($aEdit->sex) == 1 ? 'selected' : '')  }}>{{ __('Nam') }}</option>
                            <option value="2" {{ (old('sex') == 2) ? 'selected' : '' }} {{ !isset($aEdit->sex) ? '' : (($aEdit->sex) == 2 ? 'selected' : '')  }}>{{ __('Nữ') }}</option>
                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Số điện thoại liên hệ') }}</label>
                        <input type="text" name="phone" value="{{ old('phone') }}{{ !isset($aEdit->phone) ? '' : $aEdit->phone }}" class="form-control m-input m-input--square"  placeholder="{{ __('Nhập số điện thoại') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Chức vụ') }}</label>
                        <select class="form-control" name="position">
                            <option value="AK" selected="selected">Alaska</option>
                            <option value="HI">Hawaii</option>

                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Phòng ban') }}</label>
                        <input type="text" value="{{ old('department') }}{{ !isset($aEdit->department) ? '' : $aEdit->department }}"  name="department" class="form-control m-input m-input--square" placeholder="{{ __('Nhập phòng ban') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Thuộc') }}</label>
                        <select class="form-control m-select2" id="m_select2_1" name="brand">
                            <option value="AK" selected="selected">Alaska</option>
                            <option value="HI">Hawaii</option>
                        </select>
                    </div>
                     <div class="form-group m-form__group">
                        <label>
                            {{ __('Ngày bắt đầu làm việc') }}
                        </label>
                        <div class="input-group date" >
                            <input type="text" value="{{ old('start_work_date') }}{{ !isset($aEdit->start_work_date) ? '' : $aEdit->start_work_date }}" name="start_work_date" class="form-control m-input  datetimepicker" placeholder="{{ __('Ngày bắt đầu làm việc') }}"  />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group m-form__group">
                        <label>
                            {{ __('Ngày hết hạn hợp đồng') }}
                        </label>
                        <div class="input-group date" >
                            <input type="text" value="{{ old('exprire_date') }}{{ !isset($aEdit->contract_expiry_date) ? '' : $aEdit->contract_expiry_date }}" name="exprire_date" class="form-control m-input  datetimepicker" placeholder="{{ __('Chọn ngày hết hạn') }}"  />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Người bảo lãnh') }}</label>
                        <textarea type="text" class="form-control" name="sponsor" placeholder="{{ __('Nhập người bảo lãnh') }}">{{ old('sponsor') }}{{ !isset($aEdit->sponsor) ? '' : $aEdit->sponsor }}</textarea>
                        <span class="m-form__help">{{ __('Xin vui lòng nhập theo mẫu sau: Tên người bảo lãnh - Quan hệ - Số điện thoại.') }}</span>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Số tài khoản ngân hàng') }}</label>
                        <input type="text" name="account_number" value="{{ old('account_number') }}{{ !isset($aEdit->account_number) ? '' : $aEdit->account_number }}" class="form-control m-input m-input--square" placeholder="{{  __('Nhập số tài khoản ngân hàng')  }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Tên chủ thẻ') }}</label>
                        <input type="text" name="owner" value="{{ old('owner') }}{{ !isset($aEdit->account_name) ? '' : $aEdit->account_name }}" class="form-control m-input m-input--square" placeholder="{{ __('Nhập tên chủ thẻ') }} ">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Chi nhánh ngân hàng') }}</label>
                        <input type="text" name="bank_branch" value="{{ old('bank_branch') }}{{ !isset($aEdit->bank_branch) ? '' : $aEdit->bank_branch }}" class="form-control m-input m-input--square" placeholder="{{ __('Nhập chi nhánh ngân hàng ') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Tên ngân hàng') }}</label>
                        <input type="text" name="bank_name" value="{{ old('bank_name') }}{{ !isset($aEdit->bank_name) ? '' : $aEdit->bank_name }}" class="form-control m-input m-input--square" placeholder="{{ __('Nhập tên ngân hàng') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Số CMTND') }}</label>
                        <input type="text" name="identity_card" value="{{ old('identity_card') }}{{ !isset($aEdit->identity_card) ? '' : $aEdit->identity_card }}" class="form-control m-input m-input--square" placeholder="{{ __('Nhập số CMTND') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Ngày cấp') }}</label>
                        <div class="input-group date" >
                            <input type="text" name="release_date" value="{{ old('release_date') }}{{ !isset($aEdit->release_date) ? '' : $aEdit->release_date }}" class="form-control m-input  datetimepicker" placeholder="{{ __('Chọn ngày hết hạn') }}"  />
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Nơi cấp') }}</label>
                        <input type="text" name="register_place" value="{{ old('register_place') }}{{ !isset($aEdit->register_place) ? '' : $aEdit->register_place }}" class="form-control m-input m-input--square" placeholder="{{ __('Nơi cấp CMND') }}">
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Bảo hiểm') }}</label>
                        <select class="form-control m-input m-input--square" name="insurrance">
                            <option value="1" {{ !isset($aEdit->insurrance) ? '' : (($aEdit->insurrance) == 1 ? 'selected' : '')  }}>{{ __('Đã có') }}</option>
                            <option value="0" {{ !isset($aEdit->insurrance) ? 'selected' : (($aEdit->insurrance) == 0 ? 'selected' : '')  }} >{{ __('Chưa có') }}</option>
                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Chức vụ') }}</label>
                        <select class="form-control m-input m-input--square" name="user_group_id">
                                @if(isset($uGroup))
                                    @foreach($uGroup as $k => $aVal)
                                        <option value="{{ $aVal['user_group_id'] }}" {{ !isset($aEdit->user_group_id) ? '' : (($aEdit->user_group_id) == $aVal['user_group_id'] ? 'selected' : '')  }}>{{ $aVal['title'] }}</option>
                                    @endforeach
                                @endif
                        </select>
                    </div>
                    <div class="form-group m-form__group">
                        <label>{{ __('Trạng thái') }}</label>
                        <select class="form-control m-input m-input--square" name="status">
                            <option value="1" {{ !isset($aEdit->profile_status) ? '' : (($aEdit->profile_status) == 1 ? 'selected' : '')  }}>{{ __('Hoạt động') }}</option>
                            <option value="2" {{ !isset($aEdit->profile_status) ? 'selected' : (($aEdit->profile_status) == 2 ? 'selected' : '')  }}>{{ __('Khóa') }}</option>
                        </select>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
</div>
<script type="text/javascript">
    var BootstrapDatetimepickers = {
        init: function() {
            $(".datetimepicker").datetimepicker({
                format: "yyyy/mm/dd",
                todayHighlight: !0,
                autoclose: !0,
                startView: 2,
                minView: 2,
                forceParse: 0,
                pickerPosition: "bottom-left"
            })
        }
    };
    jQuery(document).ready(function() {
        BootstrapDatetimepickers.init()
    });
</script>
@endsection
