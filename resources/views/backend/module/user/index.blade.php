@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	@if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Quản lý người dùng') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<a href="{{ route('users-add') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
					<span>
						<i class="la la-plus"></i>
						<span>{{ __('Thêm mới') }}</span>
					</span>
				</a>
			</div>
		</div>
		<div class="m-portlet__body m-wrapper-user">
			<!--begin: Datatable -->
            <div class="m-form m-form--label-align-right m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">
                        <div class="col-md-3">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-form__label">
                                    <label class="m-label m-label--single" style="min-width: 80px;">{{ __('Tình trạng:') }}</label>
                                </div>
                                <div class="m-form__control">
                                    <select class="form-control" id="m_form_status">
                                        <option value="">{{ __('Chọn tất cả') }}</option>
                                        <option value="1">{{ __('Hoạt động') }}</option>
                                        <option value="2">{{ __('Khóa') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-md-none m--margin-bottom-10"></div>
                        </div>

                        <div class="col-md-3">
                            <div class="m-form__group m-form__group--inline">
                                <div class="m-form__label">
                                    <label class="m-label m-label--single" style="min-width: 70px;">{{ __('Bảo hiểm:') }}</label>
                                </div>
                                <div class="m-form__control">
                                    <select class="form-control" id="m_form_insurrance">
                                        <option value="">{{ __('Chọn tất cả') }}</option>
                                        <option value="1">{{ __('Đã có') }}</option>
                                        <option value="2">{{ __('Không có') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-md-none m--margin-bottom-10"></div>
                        </div>
                        <div class="col-md-3">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input m-input--solid" placeholder="{{ __('Tìm kiếm') }}" id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
			<div class="m_datatable"></div>
			<!--end: Datatable -->
		</div>
	</div>
</div>
<div class="modal fade" id="resetpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form method="POST" class="m-form frm_resetPW">
                    <label>Mật khẩu:</label>
                    <input type="hidden" name="random_user_id" value="">
                    <div class="m-input-icon m-input-icon--right">
                        <input type="text" class="form-control m-input generate_passwd" disabled>
                        <span class="m-input-icon__icon m-input-icon__icon--right" onclick="return generate()"><span><i class="la la-lock"></i></span></span>

                    </div>
                    <span class="m-form__help">{{ __('Vui lòng lưu lại mật khẩu khi khôi phục') }}</span><br>
                    <button type="button" class="btn btn-primary btn-sm m--margin-top-10" onclick="return confirmReset()">{{ __('Xác nhận mật khẩu') }}</button>
                    <button type="button" class="btn btn-danger close-frm btn-sm m--margin-top-10" data-dismiss="modal">{{ __('Đóng') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	var DatatableDataLocalDemo = {
    init: function() {
        var e, a, i;
        e = JSON.parse('{!! $aReturn !!}');

        a = $(".m_datatable").mDatatable({
            data: {
                type: "local",
                source: e,
                pageSize: 10
            },
            layout: {
                theme: "default",
                class: "",
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch")
            },
            columns: [
            {
                field: "id",
                title: "#",
                width: 50,
                sortable: !1,
                textAlign: "center",
                selector: {
                    class: "m-checkbox--solid m-checkbox--brand"
                }
            }, {
                field: "user_name",
                width: 100,
                title: "Tên đăng nhập",
                class:'text-left',
                responsive: {
                    visible: "lg"
                }
            },
            {
                field: "ShipCountry",
                title: "Thông tin người dùng",
                responsive: {
                    visible: "lg"
                },
                width: 600,
                class:'text-left',
                template: function(e) {
                    var sex = {
                        1 : {
                            title: "Nam",
                        },
                        2 : {
                            title: "Nữ",
                        }
                    };

                    var insurrance = {
                        0 : {
                            title: "Không có",
                        },
                        1 : {
                            title: "Đã có",
                        }
                    };

                    return '<div class="row"><div class="col-md-6"><p><strong>Họ tên:</strong> '+ e.full_name +'</p> <p><strong>Email: </strong>'+e.email+'</p> <p><strong>Điện thoại:</strong> '+e.phone+'</p> <p><strong>Thuộc DC:</strong> '+e.brand+'</p> <p><strong>Vị trí:</strong>  '+e.position+'</p> <p><strong>Phòng ban:</strong> '+e.department+'</p> <p><strong>Năm sinh:</strong>  '+e.birthday+'</p>  <p><strong>Giới tính:</strong> '+sex[e.sex].title+'</p> <p><strong>Địa chỉ: </strong> '+e.address+'</p> <p><strong>Ngày bắt đầu làm việc:</strong> '+e.start_work_date+'</p> </div><div class="col-md-6"><p><strong>Ngày hết hạn hợp đồng:</strong> '+e.contract_expiry_date+'</p> <p><strong>Người bảo lãnh: </strong>'+e.sponsor+'</p> <p><strong>Số CMTND:</strong> '+e.identity_card+'</p> <p><strong>Ngày cấp:</strong> '+e.release_date+'</p> <p><strong>Nơi cấp:</strong> '+e.register_place+'</p> <p><strong>Số tài khoản:</strong>  '+e.account_number+'</p> <p><strong>Chủ thẻ: </strong>'+e.account_name+'</p> <p><strong>Chi nhánh:</strong> '+e.bank_branch+'</p> <p><strong>Ngân hàng: </strong> '+e.bank_name+'</p> <p><strong>Bảo hiểm:</strong> '+insurrance[e.insurrance].title+'</p></div></div>';
                }
            },{
                field: "profile_status",
                title: "Trạng thái",
                width: 150,
                template: function(e) {
                    var a = {
                        1: {
                            title: "Hoạt động",
                            class: "m-badge--success"
                        },
                        2: {
                            title: "Đang bị khóa",
                            class: " m-badge--danger"
                        }
                    };
                    return '<span class="m-badge ' + a[e.profile_status].class + ' m-badge--wide">' + a[e.profile_status].title + "</span>"
                }
            }, {
                field: "Actions",
                width: 110,
                title: "Quản lý",
                sortable: !1,
                overflow: "visible",
                template: function(e, a, i) {
                    var lock = {
                        1: {
                            title: "Khóa",
                        },
                        2: {
                            title: "Mở khóa",
                        }
                    };
                    return '\t\t\t\t\t\t<div class="dropdown ' + (i.getPageSize() - a <= 4 ? "dropup" : "") + '">\t\t\t\t\t\t\t<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"><i class="la la-ellipsis-h"></i> </a>\t\t\t\t\t\t\t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t\t<a class="dropdown-item" href="{{ route("users-edit") }}?id='+e.id+'"><i class="la la-edit"></i> Sửa</a><a href="{{ route("users-delete") }}?id='+e.id+'" class="dropdown-item"><i class="la la-close"></i> Xóa</a>\t\t\t\t\t\t\t<a class="dropdown-item" href="{{ route("users-lock") }}?id='+e.id+'"><i class="la la-leaf"></i> '+lock[e.profile_status].title+'</a>\t\t\t\t\t\t\t<a class="dropdown-item resetPasswd" onclick="return setID('+e.id+')" data-toggle="modal" data-target="#resetpassword"><i class="la la-refresh"></i> Reset mật khẩu</a></div>\t\t\t\t\t\t</div>\t\t\t\t\t\t'
                }
            }]
        });

        i = a.getDataSourceQuery();

        $("#m_form_status").on("change", function() {
            a.search($(this).val(), "profile_status")
        }).val(void 0 !== i.Status ? i.Status : "");


        $("#m_form_insurrance").on("change", function() {
            a.search($(this).val(), "insurrance")
        }).val(void 0 !== i.Type ? i.Type : "");

    }
};
jQuery(document).ready(function() {
    DatatableDataLocalDemo.init()
});

var randomPassword = function(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

var setID = function($id){ $('input[name="random_user_id"]').val($id); }
var generate = function() { $('input.generate_passwd').val(randomPassword(20)); }
var confirmReset = function(){
    var data = {
        _token : $('meta[name="csrf-token"]').attr('content'),
        id : $('input[name="random_user_id"]').val(),
        password : $('input.generate_passwd').val(),
    };
    $.ajax({
        type : 'POST',
        data : data,
        url : '{{ route("pw-generate") }}',
        success:function(data){
            if(data.status = 201){
                toastr.success(data.message);
                setTimeout(function(){
                    $("#resetpassword .close-frm").click();
                    $('.modal-backdrop').remove();
                },1000);
            }
        }
    })
}

</script>
@endsection
