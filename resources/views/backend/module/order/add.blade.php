@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    @if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ !isset($aOrder) ? __('Thêm đơn hàng') : __('Sửa đơn hàng')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <a href="{{ route('order-index') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-list"></i>
                        <span>{{ __('Danh sách') }}</span>
                    </span>
                </a>
            </div>
        </div>
        <div class="m-portlet__body">
            @if(isset($aOrder))
            <form action="{{ url('/fs/cpanel/order/update/'.$aOrder->order_id) }}" method="post">
            @else
            <form action="{{ route('order-store') }}" method="post">
            @endif
                <div class="row">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ url('') }}" id="js_core_path">

                    <div class="col-md-12">
                        <b>{{ __('Thông tin người nhận') }}</b>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Họ và tên') }}</label>
                            <input type="text" name="receiver_name" class="form-control m-input" id="js_receiver_name" value="{{ old('receiver_name') }}@if(isset($aOrder)){{ $aOrder->receiver_name }}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>{{ __('Số điện thoại') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                <input type="text" name="phone" class="form-control" id="js_phone" value="{{ old('phone') }}@if(isset($aOrder)){{ $aOrder->phone }}@endif">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Địa chỉ') }}</label>
                            <input type="text" name="address" class="form-control m-input" id="js_address" value="{{ old('address') }}@if(isset($aOrder)){{ $aOrder->address }}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Tỉnh / Thành phố') }}</label>
                            <select name="province_id" id="js_select_province" class="form-control m-input">
                                @if (isset($aProvinces))
                                <option value="">Chọn:</option>
                                @foreach ($aProvinces as $aProvince)
                                <option value="{{ $aProvince->id }}" @if(isset($aOrder) && $aProvince->id == $aOrder->province_id)selected="selected"@endif>{{ $aProvince->text }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Quận / huyện') }}</label>
                            <select name="district_id" class="form-control m-input" id="js_select_district">
                                @if (isset($aDistricts))
                                @foreach ($aDistricts as $aDistrict)
                                <option value="{{ $aDistrict->id }}" @if($aDistrict->id == $aOrder->district_id)selected="selected"@endif>{{ $aDistrict->text }}</option>
                                @endforeach
                                @else
                                <option value="">Chọn:</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Phường / Xã') }}</label>
                            <select name="ward_id" class="form-control m-input" id="js_select_ward">
                                @if (isset($aWards))
                                @foreach ($aWards as $aWard)
                                <option value="{{ $aWard->id }}" @if($aWard->id == $aOrder->ward_id)selected="selected"@endif>{{ $aWard->text }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <br>
                        <b>{{ __('Thông tin hàng hóa') }}</b>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Tên sản phẩm') }}</label>
                            <input type="text" name="product_name" class="form-control" id="js_product_name" value="{{ old('product_name') }}@if(isset($aOrder)){{ $aOrder->product_name }}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>{{ __('Thu hộ (COD)') }} (đ)</label>
                            <input type="number" name="cod" class="form-control m-input" id="js_cod" value="{{ old('cod') }}@if(isset($aOrder)){{ $aOrder->cod }}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>{{ __('Dịch vụ') }}</label>
                            <select name="service_id" class="form-control m-input" id="js_package">
                                <option value="">Chọn:</option>
                                <option value="QUANGAY24H" id="QUANGAY24H">Qua ngày (24h)</option>
                                <option value="GIAONHANH6H" id="GIAONHANH6H">Giao nhanh (6h)</option>
                                <option value="lien_tinh" id="lien_tinh">Chuyển phát nhanh liên tỉnh</option>
                                <option value="tiet_kiem" id="tiet_kiem">Chuyển phát tiết kiệm</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>{{ __('Trọng lượng') }} (gram)</label>
                            <input type="number" name="weight" class="form-control m-input" id="js_weight" value="{{ old('weight') }}@if(isset($aOrder)){{ $aOrder->weight }}@endif">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group m-form__group">
                            <label>{{ __('Ghi chú') }}</label>
                            <input class="form-control m-input" name="note" id="js_note" value="{{ old('note') }}@if(isset($aOrder)){{ $aOrder->note }}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="m-form__group form-group">
                            <label>{{ __('Dài (mm)') }}</label>
                            <input type="number" name="length" class="form-control m-input" value="{{ old('length') }}@if(isset($aOrder)){{ $aOrder->length }}@endif" onchange="Order.checkSizeIsInteger(this)">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="m-form__group form-group">
                            <label>{{ __('Rộng (mm)') }}</label>
                            <input type="number" name="width" class="form-control m-input" value="{{ old('width') }}@if(isset($aOrder)){{ $aOrder->width }}@endif" onchange="Order.checkSizeIsInteger(this)">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="m-form__group form-group">
                            <label>{{ __('Cao (mm)') }}</label>
                            <input type="number" name="height" class="form-control m-input" value="{{ old('height') }}@if(isset($aOrder)){{ $aOrder->height }}@endif" onchange="Order.checkSizeIsInteger(this)">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>{{ __('Phí giao hàng') }} (đ)</label>
                            <input type="number" @if(!isset($aOrder))disabled="disabled"@endif name="transport_fee" class="form-control m-input" id="js_transport_fee" value="{{ old('transport_fee') }}@if(isset($aOrder)){{ $aOrder->transport_fee }}@endif">
                        </div>
                    </div>
                    @if(isset($aOrder))
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>{{ __('Trạng thái') }}</label>
                            <select name="status" class="form-control m-input">
                                @foreach ($aStatus as $aValue)
                                <option value="{{ $aValue->id }}" @if((isset($aOrder) && $aValue->id == $aOrder->status) || old('status') == $aValue->id)selected="selected"@endif>
                                    {{ $aValue->text }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>{{ __('Trạng thái vật lý') }}</label>
                            <select name="physical_status" class="form-control m-input">
                                @foreach ($aPhysicalStatus as $aValue)
                                <option value="{{ $aValue->id }}" @if((isset($aOrder) && $aValue->id == $aOrder->status) || old('status') == $aValue->id)selected="selected"@endif>
                                    {{ $aValue->text }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Đối tác') }}</label>
                            <select name="customer_code" class="form-control m-input" id="js_select_customer">
                                <option value="">Chọn:</option>
                                @foreach ($aCustomers as $aCustomer)
                                <option value="{{ $aCustomer->customer_code }}" @if((isset($aOrder) && $aCustomer->customer_code == $aOrder->customer_code) || old('customer_code') == $aCustomer->customer_code )selected="selected"@endif>
                                    {{ $aCustomer->customer_code }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="m-form__group form-group">
                            <div class="m-checkbox-list">
                                <label class="m-checkbox">
                                    <input type="checkbox" name="receiver_purchase_fee" id="js_receiver_purchase_fee" @if((isset($aOrder) && $aOrder->receiver_purchase_fee == '1') || old('receiver_purchase_fee'))checked="checked"@endif> {{ __('Người nhận thanh toán cước') }}
                                    <span style="margin-top: 4px"></span>
                                </label>
                                <span class="m-form__help" style="font-size: .85rem;">{{ __('Mặc định người gửi thanh toán cước') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary m-btn m-btn--icon" type="submit">
                            <span>
                                <i class="la la-save"></i>
                                <span>{{ __('Lưu') }}</span>
                            </span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
