@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('In nhãn đơn hàng') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
                <a href="{{ route('order-index') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-list"></i>
                        <span>{{ __('Danh sách') }}</span>
                    </span>
                </a>
            </div>
		</div>
		<div class="m-portlet m-portlet--tabs">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_tabs_receiver_info" role="tab" aria-selected="true">
                                In nhãn
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_product_info" role="tab" aria-selected="false">
                                In mã vạch
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">                   
                <div class="tab-content">
                    <div class="tab-pane active show" id="m_tabs_receiver_info" role="tabpanel">
                        <form class="m-form m-form--fit" action="{{ route('order-label') }}" method="get">
							<div class="row m--margin-bottom-20">
								<div class="col-md-12 m--margin-bottom-10-tablet-and-mobile">
									<label>Danh sách mã đơn hàng:</label>
									<textarea class="form-control m-input" name="order_code" rows="3"></textarea>
									<i>Ví dụ: VL130720181,VL130720182,VL130720183</i>
								</div>
							</div>
							<button name="print_label" value="1" class="btn btn-primary m-btn m-btn--icon" type="submit">
								<span>
									<i class="la la-print"></i>
									<span>In nhãn</span>
								</span>
							</button>
						</form>
                    </div>
                    <div class="tab-pane" id="m_tabs_product_info" role="tabpanel">
                        <form class="m-form m-form--fit" action="{{ route('order-label') }}" method="get">
							<div class="row m--margin-bottom-20">
								<div class="col-md-12 m--margin-bottom-10-tablet-and-mobile">
									<label>Danh sách mã đơn hàng:</label>
									<textarea class="form-control m-input" name="order_code" rows="3"></textarea>
									<i>Ví dụ: VL130720181,VL130720182,VL130720183</i>
								</div>
							</div>
							<button name="print_barcode" value="1" class="btn btn-warning m-btn m-btn--icon" type="submit">
								<span>
									<i class="la la-print"></i>
									<span>In Mã vạch</span>
								</span>
							</button>
						</form>
                    </div>
                </div>      
            </div>
        </div>
	</div>
</div>
@endsection