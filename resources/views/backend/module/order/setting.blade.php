@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ !isset($aOrder) ? __('Thêm đơn hàng') : __('Sửa đơn hàng')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#m_modal_4">
                    <span>
                        <i class="la la-plus"></i>
                        <span>{{ __('Tạo gói dịch vụ') }}</span>
                    </span>
                </a>
            </div>
        </div>
        <div class="m-portlet__body">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th><b>STT</b></th>
                        <th><b>Tên dịch vụ</b></th>
                        <th><b>Trọng lượng quy đổi (kg)</b></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($aRows as $iKey => $aRow)
                    <tr>
                        <td>{{ $iKey + 1 }}</td>
                        <td>{{ $aRow->name }}</td>
                        <td>{{ $aRow->number }}</td>
                        <td>
                            <a href="{{ url('fs/cpanel/order/setting-destroy', $aRow->service_id) }}">
                                <i class="la la-remove"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tạo mới gói dịch vụ</h5>
                <a  class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -2px">
                    <span aria-hidden="true">×</span>
                </a>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('order-setting-store') }}">
                    {{ csrf_field() }}
                    <div class="form-group m-form__group">
                        <label>Tên dịch vụ</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group m-form__group">
                        <label>Trọng lượng quy đổi chia cho</label>
                        <input type="number" name="number" class="form-control">
                        <i>Ví dụ: 2000</i>
                    </div>
                    <div class="form-group m-form__group">
                        <button type="submit" class="btn btn-primary m-btn m-btn--icon">
                            <span>
                                <i class="la la-save"></i>
                                <span>{{ __('Lưu') }}</span>
                            </span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection