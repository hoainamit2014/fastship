@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Tra cứu đơn hàng') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
                <a href="{{ route('order-index') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-list"></i>
                        <span>{{ __('Danh sách') }}</span>
                    </span>
                </a>
            </div>
		</div>
		<div class="m-portlet__body">
			<form class="m-form m-form--fit" action="{{ route('order-show') }}" method="get">
				<div class="row m--margin-bottom-20">
					<div class="col-md-12 m--margin-bottom-10-tablet-and-mobile">
						<label>Nhập mã đơn hàng:</label>
						<input class="form-control m-input" name="order_code" value="{{ isset($_GET['order_code']) ? $_GET['order_code'] : '' }}">
					</div>
				</div>
				<button class="btn btn-primary m-btn m-btn--icon" type="submit">
					<span>
						<i class="la la-search"></i>
						<span>Tra cứu</span>
					</span>
				</button>
				@if (isset($aRow))
				<a class
				="btn btn-warning m-btn m-btn--icon" href="{{ url('fs/cpanel/order/edit', $aRow->order_id) }}">
					<span>
						<i class="la la-edit"></i>
						<span>Sửa</span>
					</span>
				</a>
				@endif
			</form><br>
			@if (isset($aRow))
			<div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_tabs_receiver_info" role="tab" aria-selected="true">
                                    Thông tin người nhận
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_product_info" role="tab" aria-selected="false">
                                    Thông tin người hàng hóa
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_order_info" role="tab" aria-selected="false">
                                    Hành trình đơn hàng
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">                   
                    <div class="tab-content">
                        <div class="tab-pane active show" id="m_tabs_receiver_info" role="tabpanel">
                            <table class="table table-bordered table-hover">
							    <tbody>
									<tr>
										<th style="width: 30%;"><b>Họ và tên</b></th>
										<td>{{ $aRow->receiver_name }}</td>
									</tr>
									<tr>
										<th><b>Số điện thoại</b></th>
										<td>{{ $aRow->phone }}</td>
									</tr>
									<tr>
										<th><b>Địa chỉ</b></th>
										<td>{{ $aRow->address }}</td>
									</tr>
									<tr>
										<th><b>Tỉnh / thành phố</b></th>
										<td>{{ $aRow->province_name }}</td>
									</tr>
									<tr>
										<th><b>Quận / huyện</b></th>
										<td>{{ $aRow->district_name }}</td>
									</tr>
									<tr>
										<th><b>Phường / xã</b></th>
										<td>{{ $aRow->ward_name }}</td>
									</tr>
							    </tbody>
							</table>
                        </div>
                        <div class="tab-pane" id="m_tabs_product_info" role="tabpanel">
                            <table class="table table-bordered table-hover">
							    <tbody>
							    	<tr>
										<th><b>Đối tác</b></th>
										<td>{{ $aRow->customer_code }}</td>
									</tr>
									<tr>
										<th style="width: 30%;"><b>Tên sản phẩm</b></th>
										<td>{{ $aRow->product_name }}</td>
									</tr>
									<tr>
										<th><b>Dịch vụ</b></th>
										<td>{{ $aRow->service_name }}</td>
									</tr>
										<th><b>Trọng lượng</b></th>
										<td>{{ number_format($aRow->weight) }} kg</td>
									</tr>
									<tr>
										<th><b>Chiều dài</b></th>
										<td>{{ number_format($aRow->length) }} mm</td>
									</tr>
									<tr>
										<th><b>Chiều rộng</b></th>
										<td>{{ number_format($aRow->width) }} mm</td>
									</tr>
									<tr>
										<th><b>Cân cao</b></th>
										<td>{{ number_format($aRow->height) }} mm</td>
									</tr>
									<tr>
										<th><b>Tiền thu hộ</b></th>
										<td>{{ number_format($aRow->cod) }} đ</td>
									</tr>
									<tr>
										<th><b>Phí giao hàng</b></th>
										<td>{{ number_format($aRow->transport_fee) }} đ</td>
									</tr>
									<tr>
										<th><b>Trạng thái</b></th>
										<td>{{ $aRow->status_name }}</td>
									</tr>
									<tr>
										<th><b>Trạng thái vật lý</b></th>
										<td>{{ $aRow->physical_status_name }}</td>
									</tr>
									<tr>
										<th><b>Ghi chú</b></th>
										<td>{{ $aRow->note }}</td>
									</tr>
									<tr>
										<th><b>Người nhận thanh toán cước</b></th>
										<td>{{ $aRow->receiver_purchase_fee ? 'Có' : 'Không' }}</td>
									</tr>
							    </tbody>
							</table>
                        </div>
                        <div class="tab-pane" id="m_tabs_order_info" role="tabpanel">
							<div class="m-timeline-3">
								<div class="m-timeline-3__items">
									<div class="m-timeline-3__item m-timeline-3__item--info">
										<div class="m-timeline-3__item-desc">
											<span class="m-timeline-3__item-text">
											Tạo đơn hàng thành công
											</span><br>
											<span class="m-timeline-3__item-user-name">
											<a href="javascript:void(0);" class="m-link m-link--metal m-timeline-3__item-link">
											Tạo bởi {{ $aRow->user_name }}
											</a>
											</span>		 
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>      
                </div>
            </div>
			@endif
		</div>
	</div>
</div>
@endsection