@extends('backend.app')
@section('content')

<div class="m-content" style="width: 100%">

    <div class="express_delivery">
        <h4>GIAO NHANH NỘI THÀNH</h4>
        <div class="row">
            <div class="col-xl-12">
                <div class="">
                    <div class="m-portlet">
                        <div class="m-portlet__body">
                            <div class="services">
                                <div class="head__caption">
                                    <h4>Gói dịch vụ</h4>
                                    <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#package_services">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Thêm mới</span>
                                        </span>
                                    </a>
                                </div>

                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Gói dịch vụ </th>
                                            <th>Mã dịch vụ </th>
                                            <th>Khối lượng </th>
                                            <th>Nội thành</th>
                                            <th>Ngoại thành</th>
                                            <th>Ngoại thành 2</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($aExpressDelivery))
                                        @foreach($aExpressDelivery as $k => $aVal)
                                        <tr class="text-center">
                                            <td>{{ $aVal['package_name'] }}</td>
                                            <td>{{ $aVal['package_code'] }}</td>

                                            <td>{{ $aVal['kg'].'kg' }}</td>
                                            <td>{{ !empty($aVal['internal']) ? number_format($aVal['internal'] ,0, ',','.') : '' }}</td>
                                            <td>{{ !empty($aVal['external']) ? number_format($aVal['external'] ,0, ',','.') : '' }}</td>
                                            <td>{{ !empty($aVal['external2']) ? number_format($aVal['external2'] ,0, '.','.') : '' }}</td>
                                            <td class="text-center">
                                                <a href="" data-toggle="modal" data-target="#package_services" onclick="return editServices({{ $aVal['id'] }})"><i class="la la-edit"></i></a>  <a onclick="return deleteServices()" href="{{ route('delete-express-delivery',[ 'id' => $aVal['id'] ]) }}"> <i class="la la-remove"></i> </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        <tr class="text-center">
                                            <td colspan="3">
                                                Mỗi 0.5kg tiếp theo
                                            </td>
                                            <td>2.000</td>
                                            <td>2.500</td>
                                            <td>3.000</td>
                                            <td></td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td colspan="7">Chưa có gói dịch vụ nào</td>
                                        </tr>
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="">
                    <div class="m-portlet">
                        <div class="m-portlet__body">
                            <div class="services">
                                <div class="head__caption">
                                    <h4>Dịch vụ cộng thêm</h4>
                                    <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#package_services_addon">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Thêm mới</span>
                                        </span>
                                    </a>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Tên dịch vụ</th>
                                            <th>Mã dịch vụ</th>
                                            <th colspan="2">Gói cước dịch vụ</th>
                                            <th width="20%">Ghi chú</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        @if(!empty($aExpressDeliveryAddon))
                                        @foreach($aExpressDeliveryAddon as $k => $aVal)
                                        <tr class="text-center">
                                            <td>{{ $aVal['services_name'] }}</td>
                                            <td>{{ $aVal['services_code'] }}</td>
                                            <?php $price = unserialize($aVal['services_price']);?>
                                            @if($price['condition'] == 0 && $price['services_price'] == 0)
                                            <td colspan="2">{{ !empty($price['services_price']) ? $price['services_price'] : 'Miễn phí' }}</td>
                                            @elseif($price['condition'] == 0 && !empty($price['services_price']))
                                            <td colspan="2">{{ !empty($price['services_price']) ? number_format($price['services_price'],0,',','.') .' đ' : 'Miễn phí' }}</td>
                                            @else
                                            <td>
                                                <?php $prices = explode('_',$price['condition']); ?>
                                                @if(count($prices)  < 2)
                                                < {{ number_format($prices[0] , 0 , ',','.').' đ' }}
                                                @elseif(count($prices)  >= 2)
                                                @if($prices[0] && empty($prices[1]))
                                                > {{ number_format($prices[0] , 0 , ',','.').' đ' }}
                                                @else
                                                {{ number_format($prices[0] , 0 , ',','.').' đ' }}
                                                @endif

                                                @if($prices[1])
                                                - {{ number_format($prices[1] , 0 , ',','.').' đ' }}
                                                @endif

                                                @endif
                                            </td>
                                            <td>

                                                @if(empty($price['services_price']) && $price['percent'] )
                                                {{ $price['percent'].'% giá trị khai báo' }}
                                                @else
                                                Miễn phí
                                                @endif

                                            </td>
                                            @endif
                                            <td width="20%">{{ $aVal['note'] }}</td>
                                            <td class="text-center"><a href="#" data-toggle="modal" data-target="#package_services_addon" onclick="return editServicesAddon({{ $aVal['id'] }})"><i class="la la-edit"></i></a>  <a href="{{ route('delete-express-delivery-addon',['id' => $aVal['id']]) }}" onclick="return deleteServicesAddon({{ $aVal['id'] }})"> <i class="la la-remove"></i> </a></td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="express_delivery_interdepartmental">
        <h4>CHUYỂN PHÁT NHANH LIÊN TỈNH</h4>
        <div class="row">
            <div class="col-xl-12">
                <div class="">
                    <div class="m-portlet">
                        <div class="m-portlet__body">
                            <div class="services">
                                <div class="head__caption">
                                    <h4>Khu vực</h4>
                                    <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#express_delivery_interdepartmental_area">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Thêm mới</span>
                                        </span>
                                    </a>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="text-center">
                                            <th>KHU VỰC </th>
                                            <th colspan="2">NỘI VÙNG </th>
                                            <th colspan="2">CÁCH VÙNG </th>
                                            <th colspan="2">ĐẶC BIỆT</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr class="text-center">
                                            <td>TRỌNG LƯỢNG<br> (gram)</td>
                                            <td>Trung tâm <br> (1-2 ngày)</td>
                                            <td>Huyện xã <br> (2-3 ngày)</td>
                                            <td>Trung tâm <br> (2-3 ngày)</td>
                                            <td>Huyện xã <br> (3-4 ngày)</td>
                                            <td>Trung tâm <br> (1 ngày)</td>
                                            <td>Huyện xã <br> (1.5 ngày)</td>
                                            <td class="text-center"></td>
                                        </tr>
                                        @if($ExpressDeliveryInterprovincial)
                                            @foreach($ExpressDeliveryInterprovincial as $k => $aVal)
                                            <tr class="text-center">
                                                <td>
                                                    <?php
                                                        $str = '';
                                                        $explode = explode('_', $aVal['kg']);
                                                        if($explode[0] == 0 ){
                                                            $str .= 'Từ ';
                                                        }else{
                                                            $str .= 'Trên ';
                                                        }
                                                        echo $str . number_format($explode[0],0,',','.') . ' - ' . number_format($explode[1],0,',','.');
                                                    ?>
                                                </td>
                                                <?php 
                                                    $internal = unserialize($aVal['internal_area']);
                                                    $external = unserialize($aVal['external_area']);
                                                    $special = unserialize($aVal['special_area']);
                                                ?>
                                                <td><?php  echo number_format($internal['center'],0,',','.'); ?> </td>
                                                <td><?php  echo number_format($internal['ward'],0,',','.'); ?> </td>
                                                <td><?php  echo number_format($external['center'],0,',','.'); ?> </td>
                                                <td><?php  echo number_format($external['ward'],0,',','.'); ?> </td>
                                                <td><?php  echo number_format($special['center'],0,',','.'); ?></td>
                                                <td><?php  echo number_format($special['ward'],0,',','.'); ?></td>
                                                <td class="text-center"><a onclick="return editExpressDeliveryInterdepartmental({{ $aVal['id'] }})"  data-toggle="modal" data-target="#express_delivery_interdepartmental_area"><i class="la la-edit"></i></a>  <a href="{{ route('delete-express-delivery-interdepartmental-area',['id' => $aVal['id'] ] ) }}" onclick="return deleteExpressDeliveryInterdepartmental()"> <i class="la la-remove"></i> </a></td>
                                            </tr>
                                            @endforeach
                                        @endif

                                        <tr class="text-center">
                                            <td>500 gram tiếp theo</td>
                                            <td>25.000</td>
                                            <td>35.000</td>
                                            <td>35.000</td>
                                            <td>45.000</td>
                                            <td>30.000</td>
                                            <td>40.000</td>
                                            <td class="text-center"><a href=""><i class="la la-edit"></i></a>  <a href=""> <i class="la la-remove"></i> </a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="">
                    <div class="m-portlet">
                        <div class="m-portlet__body">
                            <div class="services">
                                <div class="head__caption">
                                    <h4>Dịch vụ cộng thêm</h4>

                                    <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#express_delivery_services_addon">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span>Thêm mới</span>
                                        </span>
                                    </a>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Tên dịch vụ </th>
                                            <th>Mã dịch vụ</th>
                                            <th colspan="2">Gói cước dịch vụ</th>
                                            <th>Ghi chú</th>

                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($ExpressDeliveryInterprovincialAddon)
                                            @foreach($ExpressDeliveryInterprovincialAddon as $k => $aVal)
                                                <tr class="text-center" style="vertical-align: middle;">
                                                    <td style="vertical-align: middle;">{{ $aVal['services_name'] }}</td>
                                                    <td style="vertical-align: middle;">{{ $aVal['services_code'] }}</td>
                                                    <?php 
                                                        $sArray = unserialize($aVal['price']);
                                                        $condition = (count(explode('_', $sArray['condition'])) > 1) ? explode('_', $sArray['condition']) : '';
                                                    ?>
                                                    
                                                    <?php 
                                                        if(empty($sArray['condition']) && empty($sArray['services_price']) && !empty($sArray['percent_declaration_value']) ){
                                                            echo '<td colspan="2" style="vertical-align: middle;">';
                                                                echo $sArray['percent_declaration_value'].'% giá trị khai báo';
                                                            echo '</td>';
                                                        }elseif(empty($sArray['condition']) && empty($sArray['services_price']) && empty($sArray['percent_declaration_value']) && !empty($sArray['postage'])){
                                                            echo '<td colspan="2" style="vertical-align: middle;">';
                                                                echo $sArray['postage'].'% cước chiều đi';
                                                            echo '</td>';
                                                        }else{
                                                            $strPrice = '';
                                                            if(empty($condition[0])){
                                                                $strPrice .= 'Đến '.number_format($condition[1] ,0, ',','.') . 'đ';
                                                            }
                                                            elseif(!empty($condition[0]) && empty($condition[1])){
                                                                $strPrice .= 'Trên '.number_format($condition[0] ,0, ',','.') . 'đ';
                                                            }
                                                            elseif(!empty($condition[0]) && !empty($condition[1])) {
                                                                $strPrice .= 'Trên '.number_format($condition[0] ,0, ',','.').'đ ' . ' - '.number_format($condition[1] ,0, ',','.').'đ';
                                                            }

                                                            $strServicesPrice = '';
                                                            if(!empty($sArray['services_price']) && empty($sArray['percent_declaration_value'])){
                                                                $strServicesPrice = number_format($sArray['services_price'],0,',','.').' đ';
                                                            }else{
                                                                $strServicesPrice = $sArray['percent_declaration_value'].'%';
                                                            }
                                                            echo '<td style="vertical-align: middle;">';
                                                                echo $strPrice;
                                                            echo '</td>';
                                                            echo '<td style="vertical-align: middle;">';
                                                                 echo $strServicesPrice;
                                                            echo '</td>';
                                                        }
                                                    ?>
                                                    
                                                    <td width="20%" style="vertical-align: middle;">{{ $sArray['note'] }}</td>
                                                    <td class="text-center" style="vertical-align: middle;"><a onclick="return editExpressDeliveryInterprovincialAddon({{ $aVal['id'] }} )" data-toggle="modal" data-target="#express_delivery_services_addon"><i class="la la-edit"></i></a>  <a href="{{ route('delete-express-delivery-interprovincial-addon') }}" onclick="return deleteExpressDeliveryInterprovincialAddon()"> <i class="la la-remove"></i> </a></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="delivery_of_savings">
        <h4>CHUYỂN PHÁT TIẾT KIỆM</h4>
        <div class="row">
            <div class="col-xl-12">
                <div class="">
                    <div class="m-portlet">
                        <div class="m-portlet__body">
                            <div class="services">

                             <div class="head__caption">
                                <h4>Khu vực</h4>

                                <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#delivery_of_savings">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>Thêm mới</span>
                                    </span>
                                </a>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="text-center">
                                        <th>KHU VỰC </th>
                                        <th colspan="2">NỘI VÙNG </th>
                                        <th colspan="2">CÁCH VÙNG </th>
                                        <th colspan="2">ĐẶC BIỆT</th>
                                        <th></th>
                                    </tr>
                                    <tr class="text-center">
                                        <th>TRỌNG LƯỢNG (kg)</th>
                                        <th>Trung tâm</th>
                                        <th>Huyện xã</th>
                                        <th>Trung tâm</th>
                                        <th>Huyện xã</th>
                                        <th>Trung tâm</th>
                                        <th>Huyện xã</th>
                                        <th class="text-center"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($aDeliveryOfSavingsSettings)
                                        @foreach($aDeliveryOfSavingsSettings as $k => $aVal)

                                        <tr class="text-center">
                                            <td><?php 
                                            $explodeKG = explode('_', $aVal['kg']); 
                                            if($explodeKG[0] > 0){
                                                echo 'Trên ' .$explodeKG[0].' - '.$explodeKG[1];
                                            }else{
                                                echo 'Từ ' .$explodeKG[0].' - '.$explodeKG[1];
                                            }
                                            ?></td>
                                            <td>
                                                <?php $internalArea = unserialize($aVal['internal_area']);?>
                                                {{ number_format($internalArea['center'],0,',','.') }}
                                            </td>
                                            <td>
                                                {{ number_format($internalArea['ward'],0,',','.') }}
                                            </td>
                                            <td>
                                                <?php $externalArea = unserialize($aVal['external_area']);?>
                                                {{ number_format($externalArea['ward'],0,',','.') }}
                                            </td>
                                            <td>
                                                {{ number_format($externalArea['ward'],0,',','.') }}
                                            </td>
                                            <td>
                                                 <?php $special = unserialize($aVal['special_area']);?>
                                                 {{ number_format($special['center'],0,',','.') }}
                                            </td>
                                            <td>
                                                {{ number_format($special['ward'],0,',','.') }}
                                            </td>
                                            <td class="text-center"><a><i class="la la-edit" onclick="return editDeliveryOfSavingsSettings({{ $aVal['id'] }})" data-toggle="modal" data-target="#delivery_of_savings"></i></a>  <a href="{{ route('delete-delivery-of-savings-settings',['id'=> $aVal['id']]) }}" onclick="return deleteDeliveryOfSavingsSettings()"> <i class="la la-remove"></i> </a></td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal__wrapper">
    <!--  Gói dịch vụ  -->
    <div id="package_services" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="POST" action="{{ route('post-express-delivery') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-header">
                        <h4 class="modal-title"> {{ __('THÊM GÓI DỊCH VỤ') }}</h4>
                    </div>
                    <input type="hidden" name="id_services">
                    <div class="modal-body">
                        <p>
                            <span>Gói dịch vụ:</span>
                            <input type="text" class="form-control" onkeyup="return parseCODE(this,'#package_services input[name=services_code]')" name="services_name" required placeholder="Gói dịch vụ">
                        </p>
                        <p>
                            <span>Mã dịch vụ:</span>
                            <input type="text" class="form-control" readonly name="services_code" placeholder="Mã dịch vụ">
                        </p>
                        <p>
                            <span>Khối lượng:</span>
                            <input type="number" required class="form-control" name="kg" placeholder="Khối lượng">
                            
                        </p>
                        <p>
                            <span>Giá nội thành:</span>
                            <input type="number" class="form-control" name="internal_price" placeholder="Giá nội thành">
                        </p>

                        <p>
                            <span>Giá ngoại thành:</span>
                            <input type="number" class="form-control" name="external_price" placeholder="Giá ngoại thành">
                        </p>
                        <p>
                            <span>Giá ngoại thành 2:</span>
                            <input type="number" class="form-control" name="external_price_2" placeholder="Giá ngoại thành 2">
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Lưu lại</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- // -->

    <!-- Gói dịch vụ cộng thêm -->
    <div id="package_services_addon" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="POST" action="{{ route('post-express-delivery-addon') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_services_addon" value="">

                    <div class="modal-header">
                        <h4 class="modal-title"> {{ __('DỊCH VỤ CỘNG THÊM') }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <span>Tên dịch vụ:</span>
                            <input type="text" class="form-control" name="services_name" onkeyup="return parseCODE(this,'#package_services_addon input[name=services_code]')" placeholder="Tên dịch vụ">
                        </p>
                        <p>
                            <span>CODE:</span>
                            <input type="text" readonly class="form-control" name="services_code" placeholder="CODE">
                        </p>
                        <p>
                            <div class="m-form__group form-group">
                                <label for="">Điều kiện (nếu có)</label>
                                <div class="m-radio-inline">
                                    <label class="m-radio">
                                        <input type="radio" checked name="condition" value="0"> Không
                                        <span></span>
                                    </label>
                                    <label class="m-radio">
                                        <input type="radio" name="condition" value="3000000"> Dưới 3tr
                                        <span></span>
                                    </label>
                                    <label class="m-radio">
                                        <input type="radio" name="condition" value="3000000_30000000"> Trên 3 triệu đến 30 triệu
                                        <span></span>
                                    </label>
                                    <label class="m-radio">
                                        <input type="radio" name="condition" value="30000000_"> Trên 30 triệu
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                        </p>
                        <div>
                            <p><span>Giá cước dịch vụ:</span></p>
                            <p><input type="number" class="form-control" name="services_price" placeholder="Giá cước dịch vụ / đơn ">

                            </p>
                            hoặc
                            <p><input type="number" class="form-control" name="percent_declaration_value" step="any" placeholder="% Giá trị khai báo"></p>
                            <span class="m-form__help">Chọn 1 trong 2 ô trên (mặc định là giá cước dịch vụ)</span>
                            <strong>Miễn phí = 0</strong>
                        </div>
                        <p>
                            <span>Ghi chú:</span>
                            <textarea class="form-control" name="note" class="note"></textarea>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Lưu lại</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- // -->

    <!-- Khu vực chuyển phát nhanh liên tỉnh -->
    <div id="express_delivery_interdepartmental_area" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form method="POST" action="{{ route('post-express-delivery-interprovincial') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="">

                    <div class="modal-header">
                        <h4 class="modal-title"> {{ __('KHU VỰC') }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <span>Trọng lượng: </span>
                            <select class="form-control" name="kg">
                                <option value="0_500">Từ 0 - 500 (gram)</option>
                                <option value="500_1000">Từ 500 - 1000 (gram)</option>
                                <option value="1000_1500">Từ 1000 - 1500 (gram)</option>
                                <option value="1500_2000">Từ 1500 - 2000 (gram)</option>
                            </select>
                        </p>
                        <div>
                            <strong>NỘI VÙNG:</strong>
                            <div class="wrapper__modal">
                                <p>
                                    <span>Trung tâm (1-2 ngày):</span>
                                    <input type="number" name="internal_center_1_2" placeholder="Trung tâm (1-2 ngày)" class="form-control">
                                </p>
                                <p>
                                    <span>Huyện xã (2-3 ngày):</span>
                                    <input type="number" name="internal_ward_2_3" placeholder="Huyện xã (2-3 ngày)" class="form-control">
                                </p>

                            </div>
                        </div>
                        <div>
                            <strong>CÁCH VÙNG:</strong>
                            <div class="wrapper__modal">
                                <p>
                                    <span>Trung tâm (2-3 ngày):</span>
                                    <input type="number" name="external_center_2_3" placeholder="Trung tâm (2-3 ngày)" class="form-control">
                                </p>
                                <p>
                                    <span>Huyện xã (3-4 ngày):</span>
                                    <input type="number" name="external_ward_3_4" placeholder="Huyện xã (3-4 ngày)" class="form-control">
                                </p>
                            </div>
                        </div>
                        <div>
                            <strong>ĐẶC BIỆT:</strong>
                            <div class="wrapper__modal">
                                <p>
                                    <span>Trung tâm (1 ngày):</span>
                                    <input type="number" name="special_center_1" placeholder="Trung tâm (1 ngày)" class="form-control">
                                </p>

                                <p>
                                    <span>Huyện xã (1.5 ngày):</span>
                                    <input type="number" name="special_center_1_dot_5" placeholder="Trung tâm (1.5 ngày)" class="form-control">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Lưu lại</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- // -->

    <!-- Dịch vụ cộng thêm chuyển phát nhanh liên tỉnh -->
    <div id="express_delivery_services_addon" class="modal fade" role="dialog">
       <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST" action="{{ route('post-express-delivery-interprovincial-addon') }}">
                <input type="hidden"  name="_token" value="{{ csrf_token() }}">
                <input type="hidden"  name="id" value="">

                <div class="modal-header">
                    <h4 class="modal-title"> {{ __('DỊCH VỤ CỘNG THÊM') }}</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span>Tên dịch vụ:</span>
                        <input type="text" class="form-control" onkeyup="return parseCODE(this,'#express_delivery_services_addon input[name=services_code]')" name="services_name" placeholder="Tên dịch vụ">
                    </p>
                    <p>
                        <span>CODE:</span>
                        <input type="text" readonly class="form-control" name="services_code" placeholder="CODE">
                    </p>
                    <p>
                        <div class="m-form__group form-group">
                            <label for="">Điều kiện (nếu có)</label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="condition" value="_300000"> Dưới 300.000đ
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="condition" value="300000_600000"> Từ 300.000đ - 600.000đ
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="condition" value="600000_1000000"> Trên 600.000đ - 1 triệu
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="condition" value="1000000_"> Trên 1 triệu
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </p>

                    <div>
                        <p><span>Giá cước dịch vụ:</span></p>
                        <p>
                            <input type="number" class="form-control" name="services_price" placeholder="Giá cước dịch vụ / đơn ">
                        </p>
                        hoặc
                        <p><input type="number" class="form-control" step="any" name="percent_declaration_value" placeholder="% Giá trị khai báo"></p>
                        hoặc
                        <p><input type="number" class="form-control" name="postage" placeholder="% cước chiều đi"></p>

                        <span class="m-form__help">Chọn 1 trong 3 ô trên (mặc định là giá cước dịch vụ)</span>
                        <strong>Miễn phí = 0</strong>
                    </div>
                    <br>
                    <p>
                        <span><strong> Ghi chú:</strong></span>
                        <textarea class="form-control" name="note" placeholder="Ghi chú"></textarea>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu lại</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                </div>
            </form>
        </div>

    </div>
</div>
<!-- // -->


<!-- Chuyển phát tiết kiệm -->
<div id="delivery_of_savings" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST" action="{{ route('post-delivery-of-saving') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="">

                <div class="modal-header">
                    <h4 class="modal-title"> {{ __('KHU VỰC') }}</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <span>Trọng lượng:</span>
                        <select class="form-control" name="kg">
                            <option value="0_3">Từ 0 - 3kg</option>
                            <option value="3_30">Trên 3kg - 30kg</option>
                            <option value="30_200">Trên 30kg - 200kg</option>
                            <option value="200_500">Trên 200kg - 500kg</option>
                            <option value="500_">Trên 500kg</option>
                        </select>
                    </p>
                    <div>
                        <span>NỘI VÙNG:</span>
                        <div class="wrapper__modal">
                            <div class="item__modal" >
                                <p>
                                    <span>Trung tâm:</span>
                                    <input type="number" name="internal_center" placeholder="Trung tâm" class="form-control">
                                </p>
                                <p>
                                    <span>Huyện xã:</span>
                                    <input type="number" name="internal_ward" placeholder="Huyện xã" class="form-control">
                                </p>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div>
                        <span>CÁCH VÙNG:</span>
                        <div class="wrapper__modal">
                            <div class="item__modal" >
                               <p>
                                <span>Trung tâm:</span>
                                <input type="number" name="external_center" placeholder="Trung tâm" class="form-control">
                            </p>
                            <p>
                                <span>Huyện xã:</span>
                                <input type="number" name="external_ward" placeholder="Huyện xã" class="form-control">
                            </p>
                        </div>

                    </div>
                </div>
                <br>
                <div>
                    <span>ĐẶC BIỆT:</span>
                    <div class="wrapper__modal">
                        <div class="item__modal" >
                            <p>
                                <span>Trung tâm:</span>
                                <input type="number" name="special_center" placeholder="Trung tâm" class="form-control">
                            </p>
                            <p>
                                <span>Huyện xã:</span>
                                <input type="number" name="special_ward" placeholder="Huyện xã" class="form-control">
                            </p>
                        </div>

                    </div>
                </div>
            </div> 
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Lưu lại</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
            </div>
        </form>
    </div>

</div>
</div>
<!-- // -->

</div>

@stop

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.express_delivery [data-target]').click(function(){
            var action = "{{ route('post-express-delivery') }}";
            $('#package_services form').attr('action',action);
            $('#package_services h4.modal-title').html("{{ __('THÊM GÓI DỊCH VỤ') }}");
        });
    });
    function parseCODE($this,$class)
    {
        var slug = $($this).val();
        slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        slug = slug.replace(/đ/gi, 'd');
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        slug = slug.replace(/ /gi, "");

        slug = slug.replace(/\-\-\-\-\-/gi, '');
        slug = slug.replace(/\-\-\-\-/gi, '');
        slug = slug.replace(/\-\-\-/gi, '');
        slug = slug.replace(/\-\-/gi, '');
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        $($class).val(slug.toUpperCase());
    }

    var editServices = function($id){

        var data = {
            _token : "{{ csrf_token() }}",
            id : $id
        };
        $.ajax({
            type : 'GET',
            data : data,
            url :  "{{ route('get-express-delivery') }}",
            success:function(data){
                var action = "{{ route('update-express-delivery') }}";
                $('#package_services form').attr('action',action);
                var data = JSON.parse(data);
                $('#package_services h4.modal-title').html("{{ __('CHỈNH SỬA GÓI DỊCH VỤ') }}");
                
                $('#package_services input[name="id_services"]').val(data.id);
                $('#package_services input[name="services_name"]').val(data.package_name);
                $('#package_services input[name="services_code"]').val(data.package_code);
                $('#package_services input[name="kg"]').val(data.kg);
                $('#package_services input[name="internal_price"]').val(data.internal);
                $('#package_services input[name="external_price"]').val(data.external);
                $('#package_services input[name="external_price_2"]').val(data.external2);

            }
        })
    }

    var deleteServices = function($id){
        if(confirm('Bạn có muốn xóa dịch vụ này không?')){
            return true;
        }
        return false;
    }

    var editServicesAddon = function($id){
        var data = {
            _token : "{{ csrf_token() }}",
            id : $id
        };
        $.ajax({
            type : 'GET',
            data : data,
            url :  "{{ route('get-express-delivery-addon') }}",
            success:function(data){
                var action = "{{ route('update-express-delivery-addon') }}";
                $('#package_services_addon form').attr('action',action);
                
                $('#package_services_addon input[name="id_services_addon"]').val(data.id);
                $('#package_services_addon input[name="services_name"]').val(data.services_name);
                $('#package_services_addon input[name="services_code"]').val(data.services_code);
                $('#package_services_addon input[name="services_price"]').val(data.services_price);
                $('#package_services_addon input[name="percent_declaration_value"]').val(data.percent);
                $('#package_services_addon textarea[name="note"]').html(data.note);
                if(data.condition != 0)
                {
                    $('#package_services_addon input[name="condition"][value='+data.condition+']').attr('checked','checked');
                }else{
                    $('#package_services_addon input[name="condition"][value="0"]').attr('checked','checked');

                }

            }
        })
    }

    var deleteServicesAddon = function($id){
        if(confirm('Bạn có muốn xóa dịch vụ này không?')){
            return true;
        }
        return false;
    }

    var editExpressDeliveryInterdepartmental = function($id){
        var data = {
            _token : "{{ csrf_token() }}",
            id : $id
        };
        $.ajax({
            type : 'GET',
            data : data,
            url :  "{{ route('get-express-delivery-interdepartmental') }}",
            success:function(data){
                var data = $.parseJSON(data);
                var action = "{{ route('update-express-delivery-interdepartmental') }}";
                $('#express_delivery_interdepartmental_area form').attr('action',action);
                
                $('#express_delivery_interdepartmental_area input[name="id"]').val(data.id);
                $('#express_delivery_interdepartmental_area select option[value="'+data.kg+'"]').attr('selected','selected');
                $('#express_delivery_interdepartmental_area input[name="internal_center_1_2"]').val(data.internal_area.center);
                $('#express_delivery_interdepartmental_area input[name="internal_ward_2_3"]').val(data.internal_area.ward);


                $('#express_delivery_interdepartmental_area input[name="external_center_2_3"]').val(data.external_area.center);
                $('#express_delivery_interdepartmental_area input[name="external_ward_3_4"]').val(data.external_area.ward);

                $('#express_delivery_interdepartmental_area input[name="special_center_1"]').val(data.special_area.center);
                $('#express_delivery_interdepartmental_area input[name="special_center_1_dot_5"]').val(data.special_area.ward);

            }
        });
        return false;
    }

    var deleteExpressDeliveryInterdepartmental = function()
    {
        if(confirm('Bạn có muốn xóa ? ')){
            return true;
        }else{
            return false;
        }
        return false;
    }

     var editExpressDeliveryInterprovincialAddon = function($id){
        var data = {
            _token : "{{ csrf_token() }}",
            id : $id
        };
        $.ajax({
            type : 'GET',
            data : data,
            url :  "{{ route('get-express-delivery-interprovincial-addon') }}",
            success:function(data){
                var data = $.parseJSON(data);
                var action = "{{ route('update-express-delivery-interprovincial-addon') }}";
                $('#express_delivery_services_addon form').attr('action',action);
                
                $('#express_delivery_services_addon input[name="services_name"]').val(data.services_name);
                $('#express_delivery_services_addon input[name="services_code"]').val(data.services_code);
                if(data.price.condition != '' && data.price.condition != null){
                    $('#express_delivery_services_addon input[name="condition"][value="'+data.price.condition+'"]').attr('checked','checked');
                }else{
                    $('#express_delivery_services_addon input[name="condition"]').removeAttr('checked')
                }

                $('#express_delivery_services_addon input[name="id"]').val(data.id);
                $('#express_delivery_services_addon textarea[name="note"]').val(data.price.note);
                $('#express_delivery_services_addon input[name="services_price"]').val(data.price.services_price);
                $('#express_delivery_services_addon input[name="postage"]').val(data.price.postage);
                $('#express_delivery_services_addon input[name="percent_declaration_value"]').val(data.price.percent_declaration_value);

            }
        });
        return false;
    }

    var deleteExpressDeliveryInterprovincialAddon = function(){
        if(confirm('Bạn có muốn xóa ? ')){
            return true;
        }else{
            return false;
        }
        return false;
    }

    var editDeliveryOfSavingsSettings = function($id){
        var data = {
            _token : "{{ csrf_token() }}",
            id : $id
        };
        $.ajax({
            type : 'GET',
            data : data,
            url :  "{{ route('get-delivery-of-savings-settings') }}",
            success:function(data){
                var data = $.parseJSON(data);
                console.log(data);
                var action = "{{ route('update-delivery-of-savings-settings') }}";
                $('#delivery_of_savings form').attr('action',action);
                $('#delivery_of_savings select[name="kg"] option[value="'+data.kg+'"]').attr("selected","selected");
                $('#delivery_of_savings input[name="internal_center"]').val(data.internal_area.center);
                $('#delivery_of_savings input[name="internal_ward"]').val(data.internal_area.ward);

                $('#delivery_of_savings input[name="external_center"]').val(data.external_area.center);

                $('#delivery_of_savings input[name="external_ward"]').val(data.external_area.ward);

                $('#delivery_of_savings input[name="special_center"]').val(data.special_area.center);

                $('#delivery_of_savings input[name="special_ward"]').val(data.special_area.ward);

                $('#delivery_of_savings input[name="id"]').val(data.id);

            }
        });
        return false;
    }

    var deleteDeliveryOfSavingsSettings = function(){
        if(confirm('Bạn có muốn xóa ? ')){
            return true;
        }else{
            return false;
        }
        return false;
    }

</script>
@endsection

