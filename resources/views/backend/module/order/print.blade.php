<html>
<head>
	<title>In phiếu triển khai</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/fontawesome/css/font-awesome.min.css') }}">
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/bootstrap.min.js') }}"></script>
	<style>
		.print-title {
			text-align: center;
			padding: 20px 0;
		}
		table {
			font-size: 10px;
		}
		.padding-10 {
			padding: 20px 0;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<br>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<img src="{{ asset('public/fontend/logo.png') }}" alt="">
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
				<h1>PHIẾU GIAO HÀNG</h1>
				<div>(Kiêm hóa đơn vận chuyển)</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<table class="table table-bordered table-hover">
					<tbody>
						<tr>
							<th>Mã hệ thống</th>
							<td><?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($aRow->order_code, "C128", 1, 50) . '" alt="barcode"/>'; ?></td>
						</tr>
						<tr>
							<th>Mã đơn hàng</th>
							<td>{{ $aRow->order_code }}</td>
						</tr>
						<tr>
							<th>Mã DC phát</th>
							<td></td>
						</tr>
						<tr>
							<th>Loại dịch vụ</th>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-12 padding-10">
				<strong>Người gửi</strong>
			</div>
			<div class="col-md-6">
				Họ và tên: LAZADA
			</div>
			<div class="col-md-6">
				Điện thoại: 0080808080
			</div>
			<div class="col-md-12 padding-10">
				<strong>Người nhận</strong>
			</div>
			<div class="col-md-6">
				Họ và tên: {{ $aRow->receiver_name }}
			</div>
			<div class="col-md-6">
				Điện thoại: {{ $aRow->phone }}
			</div>
			<div class="col-md-12">
				Địa chỉ: {{ $aRow->address }}
			</div>
			<div class="col-md-4">
				Tỉnh / TP: {{ $aRow->province_name }}
			</div>
			<div class="col-md-4">
				Quận / Huyện: {{ $aRow->district_name }}
			</div>
			<div class="col-md-4">
				Phường / Xã: {{ $aRow->ward_name }}
			</div>
			<div class="col-md-12 padding-10">
				<strong>Thông tin sản phẩm</strong>
			</div>
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>STT</th>
						<th>Sản phẩm</th>
						<th>Số lượng</th>
						<th>Trọng lượng</th>
						<th>Thành tiền</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>{{ $aRow->product_name }}</td>
						<td>1</td>
						<td>{{ $aRow->weight }} gram</td>
						<td>{{ $aRow->cod }} đ</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
	    $(document).ready(function() {
	        window.onload = function () {
	            window.print();
	            setTimeout(function(){window.close();}, 1);
	        }
	    });

	</script>

</body>
</html>