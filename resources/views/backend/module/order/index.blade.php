@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Quản lý đơn hàng') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<a href="{{ route('order-add') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-plus"></i>
                        <span>{{ __('Tạo vận đơn') }}</span>
                    </span>
                </a>
                <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#m_modal_4">
                    <span>
                        <i class="la la-cloud-upload"></i>
                        <span>{{ __('Tạo nhiều vận đơn (Excel)') }}</span>
                    </span>
                </a>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="m-form m-form--fit">
				<input type="hidden" value="{{ url('') }}" id="js_core_path">
				<div class="row">
					<div class="col-lg-3">
						<div class="form-group">
							<label>Từ khóa:</label>
							<input type="text" class="form-control" name="product_name">
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Đối tác:</label>
							<select class="form-control" name="customer_code">
								<option value="">Chọn:</option>
								@foreach ($aCustomers as $aValue)
								<option value="{{ $aValue->customer_code }}">{{ $aValue->customer_code }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Trạng thái:</label>
							<select class="form-control" name="status">
								<option value="">Chọn:</option>
								@foreach ($aStatus as $aValue)
								<option value="{{ $aValue->id }}">{{ $aValue->text }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Trạng thái vật lý:</label>
							<select class="form-control" name="physical_status">
								<option value="">Chọn:</option>
								@foreach ($aPhysicalStatus as $aValue)
								<option value="{{ $aValue->id }}">{{ $aValue->text }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-lg-3">
						<div class="form-group">
							<label>Tỉnh / thành phố:</label>
							<select name="province_id" id="js_select_province" class="form-control">
								<option value=""></option>
								@foreach ($aProvinces as $aValue)
								<option value="{{ $aValue->id }}">{{ $aValue->text }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Quận / huyện:</label>
							<select name="district_id" class="form-control" id="js_select_district">
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Phường / xã:</label>
							<select name="ward_id" class="form-control" id="js_select_ward">
							</select>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="form-group">
							<label>Loại đơn hàng:</label>
							<select name="order_type" class="form-control">
								<option value="1">Đơn giao hàng</option>
								<option value="2">Đơn đổi hàng</option>
							</select>
						</div>
					</div>

					<!-- tiem kiem nang cao -->
					<div class="col-md-12">
						<div>Tìm kiếm nâng cao</div>
						<span class="m-switch m-switch--icon m-switch--brand" onclick="toggleSearch();">
							<label>
		                        <input type="checkbox" name="advanced_checkbox">
		                        <span></span>
	                        </label>
	                    </span>
					</div>

					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Ngày tạo:</label>
							<div class="input-daterange input-group" id="js_order_date_picker">
								<input type="text" class="form-control"  name="from_date" placeholder="Từ"/>
								<div class="input-group-append">
									<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
								</div>
								<input type="text" class="form-control"  name="to_date" placeholder="đến"/>
							</div>
						</div>
					</div>

					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Ngày bắt đầu giao:</label>
							<div class="input-group date">
								<input type="text" class="form-control search-order-date" name="delivery_date">
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Ngày nhận vào DC phát:</label>
							<div class="input-group date">
								<input type="text" class="form-control search-order-date" name="save_to_dc_date">
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Ngày giao thành công:</label>
							<div class="input-group date">
								<input type="text" class="form-control search-order-date" name="delivery_success_date">
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="la la-calendar-check-o"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>DC nhận hàng:</label>
							<select class="form-control" name="dc_receive">
								<option value="0">Chọn:</option>
								@foreach ($aDC as $aValue)
								<option value="{{ $aValue->id }}">{{ $aValue->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>DC hiện tại:</label>
							<select class="form-control" name="dc_current">
								<option value="0">Chọn:</option>
								@foreach ($aDC as $aValue)
								<option value="{{ $aValue->id }}">{{ $aValue->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>DC phát:</label>
							<select class="form-control" name="dc_delivery">
								<option value="0">Chọn:</option>
								@foreach ($aDC as $aValue)
								<option value="{{ $aValue->id }}">{{ $aValue->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>DC phát thành công:</label>
							<select class="form-control" name="dc_delivery_success">
								<option value="0">Chọn:</option>
								@foreach ($aDC as $aValue)
								<option value="{{ $aValue->id }}">{{ $aValue->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Luồng đơn hàng:</label>
							<select class="form-control" name="flow_order">
								<option value="0">Chọn:</option>
								<option value="di_giao">Đi giao</option>
								<option value="tra_Ve">Trả về</option>
							</select>
						</div>
					</div>

					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>D+ lớn hơn:</label>
							<input type="number" class="form-control" name="bigger_d">
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>D+ nhỏ hơn:</label>
							<input type="number" class="form-control" name="smaller_d">
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Ghi chú đơn hàng nhận mới:</label>
							<select class="form-control" name="note">
								<option value="0">Chọn:</option>
								<option value="1">Chưa ghi chú</option>
								<option value="2">Đã ghi chú</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Trạng thái cân nặng:</label>
							<select class="form-control" name="weight">
								<option value="0">Chọn:</option>
								<option value="1">Chưa có cân nặng</option>
								<option value="2">Đã có câng nặng</option>
							</select>
						</div>
					</div>

					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Trạng thái cước:</label>
							<select class="form-control" name="status_service">
								<option value="0">Chọn:</option>
								<option value="1">Đã có cước</option>
								<option value="2">Chưa có cước</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Thanh toán cước:</label>
							<select class="form-control" name="purchase_service">
								<option value="0">Chọn:</option>
								<option value="1">Đã thanh toán cước</option>
								<option value="2">Chưa thanh toán cước</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Trạng thái hẹn giao:</label>
							<select class="form-control" name="status_appointment">
								<option value="0">Chọn:</option>
								<option value="1">Không có hẹn giao</option>
								<option value="2">Có hẹn giao</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Đơn hàng nội thành:</label>
							<select class="form-control" name="urban_suburban">
								<option value="0">Chọn:</option>
								<option value="1">Nội thành</option>
								<option value="2">Ngoại thành</option>
							</select>
						</div>
					</div>

					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Khu vực:</label>
							<select class="form-control" name="north_south">
								<option value="0">Chọn:</option>
								<option value="1">Miền bắc</option>
								<option value="2">Miền Nam</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Đơn hàng ngoài vùng phủ:</label>
							<select class="form-control" name="inside_outside">
								<option value="0">Chọn:</option>
								<option value="inside">Ngoài vùng phủ</option>
								<option value="outside">Trong vùng phủ</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Loại hình dịch vụ:</label>
							<select class="form-control" name="service_id">
								<option value="">Chọn:</option>
                                <option value="GIAONHANH6H" id="GIAONHANH6H">Chuyển phát nhanh</option>
                                <option value="lien_tinh" id="lien_tinh">Chuyển phát nhanh liên tỉnh</option>
                                <option value="tiet_kiem" id="tiet_kiem">Chuyển phát tiết kiệm</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Đơn hàng và địa chỉ Pickup:</label>
							<select class="form-control" name="order_pickup_status_ward">
								<option value="0">Chọn:</option>
								<option value="1">Đơn không có phường / xã</option>
								<option value="2">Đơn đã có phường / xã</option>
							</select>
						</div>
					</div>
					<div class="col-lg-3 advanced-search" style="display:none">
						<div class="form-group">
							<label>Đơn hàng và địa chỉ phát:</label>
							<select class="form-control" name="order_delivery_status_ward">
								<option value="0">Chọn:</option>
								<option value="1">Đơn không có phường / xã</option>
								<option value="2">Đơn đã có phường / xã</option>
							</select>
						</div>
					</div>

					<div class="col-lg-3 advanced-search" style="display:none"><br>
						<div class="m-checkbox-list">
                            <label class="m-checkbox">
                                <input type="checkbox" name="order_difficult_delivery"> {{ __('Đơn hàng khó giao') }}
                                <span style="margin-top: 4px"></span>
                            </label>
                        </div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<button class="btn btn-primary m-btn m-btn--icon" id="m_search">
							<span>
								<i class="la la-search"></i>
								<span>Tìm kiếm</span>
							</span>
						</button>
						&nbsp;&nbsp;
						<a class="btn btn-danger m-btn m-btn--icon" href="{{ route('order-index') }}">
							<span>
								<i class="la la-close"></i>
								<span>Reset</span>
							</span>
						</a>
					</div>
				</div>
			</div><br>

			<div class="table-responsive">
				<table class="table table-bordered table-hover" id="js_order_listing" style="font-size: 12px">
					<thead>
						<tr style="white-space: nowrap;" class="text-center">
							<th></th>
							<th>Id</th>
							<th>Mã đơn hàng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Mã đối tác&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Tỉnh&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Quận&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Phường&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Địa chỉ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Tên khách hàng&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Số điện thoại</th>
							<th>Sản phẩm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>KM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>COD</th>
							<th>Cập nhật cuối&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Lý do cuối</th>
							<th>Trạng thái&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Trạng thái vật lý&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>D+</th>
							<th>Ghi chú&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Dc hiện tại</th>
							<th>Người tạo</th>
							<th>Số lần triển khai</th>
							<th>Số lần giao hàng</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tạo đơn hàng bằng file (Excel)</h5>
				<a  class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -2px">
					<span aria-hidden="true">×</span>
				</a>
			</div>
			<div class="modal-body">
				<form id="js_import_order" method="post" action="{{ route('order-import') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group m-form__group">
						<label for="exampleInputEmail1">Chọn tập tin</label>
						<div></div>
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
							<label class="custom-file-label">Choose file</label>
						</div>
					</div>
					<div class="form-group m-form__group">
						<button type="submit" class="btn btn-outline-success btn-sm m-btn m-btn--custom">
							<i class="la la-upload"></i>Upload
						</button>
					</div>

				</form>
			</div>
			<div class="modal-footer text-left">
				<a href="{{ asset('public/backend/donhang.xlsx') }}"><i class="la la-download"></i>Tải file mẫu</a>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$( document ).ready(function() {
		var count = 1;
		var table = $('#js_order_listing').DataTable({
			bProcessing: true,
			language: {
                "sProcessing":   "Đang xử lý...",
                "sLengthMenu":   "Xem _MENU_ mục",
                "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
                "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            },
			ajax: {
				url: "{{ route('order-search') }}",
				type: "GET",
				data: function(data){
					console.log(data);
					data.customsearch = {
						'product_name' : $('input[name="product_name"]').val(),
						'customer_code' : $('select[name="customer_code"]').val(),
						'status' : $('select[name="status"]').val(),
						'physical_status' : $('select[name="physical_status"]').val(),
						'province_id' : $('select[name="province_id"]').val(),
						'district_id' : $('select[name="district_id"]').val(),
						'ward_id' : $('select[name="ward_id"]').val(),
						'order_type' : $('select[name="order_type"]').val(),
						'from_date' : $('input[name="from_date"]').val(),
						'to_date' : $('input[name="to_date"]').val(),
						'delivery_date' : $('input[name="delivery_date"]').val(),
						'save_to_dc_date' : $('input[name="save_to_dc_date"]').val(),
						'delivery_success_date' : $('input[name="delivery_success_date"]').val(),
						'flow_order' : $('select[name="flow_order"]').val(),
						'bigger_d' : $('input[name="bigger_d"]').val(),
						'smaller_d' : $('input[name="smaller_d"]').val(),
						'note' : $('select[name="note"]').val(),
						'weight' : $('select[name="weight"]').val(),
						'status_service' : $('select[name="status_service"]').val(),
						'purchase_service' : $('select[name="purchase_service"]').val(),
						'status_appointment' : $('select[name="status_appointment"]').val(),
						'urban_suburban' : $('select[name="urban_suburban"]').val(),
						'north_south' : $('select[name="north_south"]').val(),
						'inside_outside' : $('select[name="inside_outside"]').val(),
						'service_id' : $('select[name="service_id"]').val(),
						'order_pickup_status_ward' : $('select[name="order_pickup_status_ward"]').val(),
						'order_delivery_status_ward' : $('select[name="order_delivery_status_ward"]').val(),
						'order_difficult_delivery' : $('input[name="order_difficult_delivery"]:checked').val() ? 1 : 0
					};
				}
			},
			bPaginate:true,
			serverSide: true,
			sPaginationType:"full_numbers",
			searching: false,
			lengthMenu: [20, 50, 100, 150],
			pageLength: 20,
			aoColumns: [
			{
				mData: null , mRender: function (data, type, full) {
					return '<div class="dropdown">'+
								'<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">'+
									'<i class="la la-ellipsis-h"></i>'+
								'</a>'+
								'<div class="dropdown-menu dropdown-menu-right">'+
									'<a href="{{url('fs/cpanel/order/show/')}}/?order_code='+full.order_code+'" class="dropdown-item">'+
										'<i class="la la-eye"></i> Xem'+
									'</a>'+
									'<a class="dropdown-item" href="{{url('fs/cpanel/order/edit/')}}/'+full.order_id+'">'+
										'<i class="la la-edit"></i> Sửa'+
									'</a>'+
									'<a href="{{url('fs/cpanel/order/destroy/')}}/'+full.order_id+'" class="dropdown-item">'+
										'<i class="la la-close"></i> Xóa'+
									'</a>'+
									'<a href="{{url('fs/cpanel/order/print/')}}/'+full.order_id+'" class="dropdown-item" target="_blank">'+
										'<i class="la la-print"></i> In'+
									'</a>'+
								'</div>'+
							'</div>';
				}
			},
			{ mData: 'order_id' } ,
			{ mData: 'order_code' },
			{ mData: 'customer_order_code' },
			{ mData: 'province_name' },
			{ mData: 'district_name' },
			{ mData: 'ward_name' },
			{ mData: 'address' },
			{ mData: 'receiver_name' },
			{ mData: 'phone' },
			{ mData: 'product_name' },
			{ mData: 'gift'},
			{ mData: 'cod'},
			{ mData: 'updated_at' },
			{ mData: 'reason'},
			{ mData: 'status_name' },
			{ mData: 'physical_status_name' },
			{ mData: 'd+'},
			{ mData: 'note' },
			{ mData: 'dc_current', mRender: function(){return '';}   },//cureent_dc
			{ mData: 'user_name' },
			{ mData: null, mRender: function(){return '';}   },//count_deploy
			{ mData: null, mRender: function(){return '';}   },//count_delivery
			]
		});
		$("#m_search").on("click", function(t) {
			table.ajax.reload();
			return false;
		});
	});
	toggleSearch = function (ele) {
		if ($('input[name=advanced_checkbox]:checked').length > 0) {
			$('.advanced-search').show();
		} else {
			$('.advanced-search').hide();
		}
	}
</script>
@endsection
