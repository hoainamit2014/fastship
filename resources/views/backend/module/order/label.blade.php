<html>
<head>
	<title>label</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/fontawesome/css/font-awesome.min.css') }}">
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/bootstrap.min.js') }}"></script>
	<style>
		.container-fluid {
			padding-top: 10px;
		}
		.table-bordered > tbody > tr > td {
			border-color: #333 !important;
		}
		.table-bordered {
			font-size: 12px;
		}
		.user-bottom-border {
			border-bottom: 1px solid #ccc;
		}
		.padding-10 {
			padding-top: 10px;
		}
		.div-barcode {
			border: 1px solid #000;
			padding: 10px 0;
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			@foreach ($aLabels as $aLabel)
			@if (isset($bLabel))
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td>
								<div>Trọng lượng</div>
								<div>{{ number_format($aLabel->weight) }} Kg</div>
							</td>
							<td>
								<div>DC phát</div>
							</td>
							<td>
								<div>Thu hộ (COD)</div>
								<div>{{ number_format($aLabel->cod) }} VNĐ</div>
							</td>
							<td>
								<div class="text-center">
									<?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($aLabel->order_code, "C128", 1, 50) . '" alt="barcode"/>'; ?>
								</div>
								<div class="text-center">{{ $aLabel->order_code }}</div>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<div>
									{{ $aLabel->product_name }}
								</div>
								<div class="user-bottom-border padding-10">Người gửi</div>
								<div class="padding-10">
									<i class="fa fa-user"> {{ $aLabel->user_name }} </i>
								</div>
								<div class="padding-10">
									<i class="fa fa-phone"> {{ $aLabel->phone }} </i>
								</div>
								<div class="padding-10">
									<i>Ghi chú: {{ $aLabel->note }}</i>
								</div>
							</td>
							<td>
								<div class="user-bottom-border padding-10">Người nhận</div>
								<div class="padding-10">
									<i class="fa fa-user"> {{ $aLabel->receiver_name }} </i>
								</div>
								<div class="padding-10">
									<i class="fa fa-map-marker">
										{{ $aLabel->address }}
									</i>
								</div>
								<div class="padding-10">
									<i class="fa fa-phone"> {{ $aLabel->phone }} </i>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			@else
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<div class="text-center div-barcode">
					<?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($aLabel->order_code, "C128", 1, 50) . '" alt="barcode"/>'; ?>
					<div class="text-center">{{ $aLabel->order_code }}</div>
				</div>
			</div>
			@endif
			@endforeach
		</div>
	</div>
</body>
</html>