@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Cập nhật đơn hàng trả về') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
                <a href="{{ route('order-index') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-list"></i>
                        <span>{{ __('Danh sách') }}</span>
                    </span>
                </a>
            </div>
		</div>
		<div class="m-portlet__body">
			<form class="m-form m-form--fit" action="{{ route('order-update-return') }}" method="get">
				<div class="row m--margin-bottom-20">
					<div class="col-md-12 m--margin-bottom-10-tablet-and-mobile">
						<label>Nhập các mã đơn hàng:</label>
						<textarea class="form-control m-input" name="list_order_id" rows="3"></textarea>
						<i>Ví dụ: VL130720181,VL130720182,VL130720183</i>
					</div>
				</div>
				<button class="btn btn-primary m-btn m-btn--icon" type="submit">
					<span>
						<i class="la la-check"></i>
						<span>Cập nhật</span>
					</span>
				</button>
			</form>
		</div>
	</div>
</div>
@endsection