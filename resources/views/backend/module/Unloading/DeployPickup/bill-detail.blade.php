<div class="m-portlet">
	<div class="m-portlet m-portlet--mobile">
		<div style="padding: 10px;">

			<div class="m-portlet__head-caption">
				<div class="row text-center">
					<div class="col-sm-3">
						Mã phiếu: {{ !empty($_GET['billCode']) ? $_GET['billCode'] : '' }}
					</div>
					<div class="col-sm-2">
						Số đơn: <span class="bill_number"></span>
					</div>
					<div class="col-sm-2">
						Trạng thái: <span class="bill_status"></span>Đang xử lý
					</div>
					<div class="col-sm-3">
						Phụ trách: <span class="charge"></span>
					</div>
					<div class="col-sm-2">
						Tạo bởi: <span class="author"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class=form-input-order>
		<div  class="col-sm-8" style="margin: 0 auto">
			<div class="form-group m-form__group">
				<div class="input-group">
					<input type="text" class="form-control m-input" placeholder="Nhập mã vận đơn cần thêm" aria-describedby="basic-addon2">
					<div class="input-group-append"><span class="input-group-text" id="basic-addon2">Thêm vào phiếu!</span></div>
					<div class="btn btn-success" style="margin-left: 15px;">In phiếu</div>
				</div>
				<i class="m-form__help">Để bắn barcode vui lòng đặt con trỏ chuột vào ô này</i>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-10">
				<i> Lưu ý : Đối với những đơn muốn cập nhật Fall hoặc Reject , bạn hãy cập nhật lý do trước khi cập nhật để hệ thống lưu lại trạng thái.</i>
			</div>
			<div class="col-sm-2 text-right">

			</div>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered table-hover tbl_orders" >
				<thead>
					<tr style="white-space: nowrap;" class="text-center">
						<th>#</th>
						<th>STT</th>
						<th>Mã</th>
						<th>Đối tác</th>
						<th>Mã đối tác</th>
						<th>Hẹn giao</th>
						<th>Lý do</th>
						<th>Trạng thái</th>
						<th>D+ tại DC</th>
						<th>Ghi chú</th>
						<th>Địa chỉ</th>
						<th>Tên khách hàng</th>
						<th>Số điện thoại 1</th>
						<th>Sản phẩm</th>
						<th>KM1</th>
						<th>KM2</th>
						<th>COD</th>
						<th>Cập nhật cuối</th>
						<th>Trạng thái vật lý</th>
						<th>Tỉnh</th>
						<th>Quận</th>
						<th>Phường</th>
						<th>Loại đơn hàng</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
	</div>
</div>


<script type="text/javascript">
	var tbl_order;
	$(document).ready(function() {
		loadAjax();
		tbl_order = $('.tbl_orders').DataTable({
			"searching": false,
			"bLengthChange": false,
			"ordering": false
		});

	});
	function loadAjax()
	{
		var data = {
			'type' : 8,
			'billCode' : "{{ !empty($_GET['billCode']) ? $_GET['billCode'] : '' }}",
			'_token' : $('meta[name="csrf-token"]').attr('content')
		};
		$.ajax({
			type : 'POST',
			url : '{{ route("ajax-deloy-pickup-index") }}',
			data : data ,
			success : function(res){
				$('.bill_number').html(res.data.length);
				$('.bill_number').html(res.bill_status);
				$('.charge').html(res.charge);
				$('.author').html(res.author);
				var OrderArray = [];
				$.each( res.data, function(i, obj) {
					var aStatus = {
						0 : {
							'title' : 'Tạo mới'
						},
						1 : {
							'title' : '<span class="m-badge m-badge--warning m-badge--wide text__white">Đang xử lý</span>'
						},
						2 : {
							'title' : '<span class="m-badge m-badge--success m-badge--wide text__white">Đã xử lý</span>'
						},
					};

					OrderArray[i] = [
						'<a href="#" class="btn btn-success btn-sm"><i class="la la-map-marker"></i></a>',
						i + 1,
						obj.order_code,
						obj.customer_code,
						obj.customer_code,
						obj.appointment_delivery,
						obj.reason,
						aStatus[obj.status].title,
						obj.warehouse_id,
						obj.note,
						obj.address,
						obj.receiver_name,
						obj.phone,
						obj.product_name,
						obj.gift_1,
						obj.gift_2,
						obj.cod,
						obj.updated_at,
						obj.physical_status,
						obj.province_name,
						obj.district_name,
						obj.ward_name,
						obj.order_type
					];
				});

				tbl_order.rows.add(OrderArray).draw();
			}
		});
	}
</script>