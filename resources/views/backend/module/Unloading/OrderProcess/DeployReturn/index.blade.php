@extends('backend.app')
@section('content')
<?php $helps = new Helpers();?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="menu-order-horizotal">
		<div class="m-tabs" data-tabs="true" data-tabs-contents="#m_sections">
			<ul class="m-nav m-nav--active-bg m-nav--active-bg-padding-lg m-nav--font-lg m-nav--font-bold m--margin-bottom-20 m--margin-top-10 m--margin-right-40" id="m_nav" role="tablist">
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 1) m-tabs__item--active @endif" href="{{ route('return-index') }}?active=1">
						<span class="m-nav__link-text">Triển khai trả về</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 2) m-tabs__item--active @endif" href="{{ route('return-index') }}?active=2">
						<span class="m-nav__link-text">Trả hàng về đối tác (Kho tổng)</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 3) m-tabs__item--active @endif" href="{{ route('return-index') }}?active=3">
						<span class="m-nav__link-title">
							<span class="m-nav__link-wrap">
								<span class="m-nav__link-text">Trả hàng về cho nhiều đối tác (DC)</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 4) m-tabs__item--active @endif" href="{{ route('return-index') }}?active=4">
						<span class="m-nav__link-title">
							<span class="m-nav__link-wrap">
								<span class="m-nav__link-text">Nhận hàng vào kho từ phiếu trả về</span>
							</span>
						</span>
					</a>

				</li>

			</ul>
		</div>
	</div>
	<div class="m-portlet m-portlet--space">
		@if(!isset($_GET['billCode']))
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">

					@if(isset($_GET['active']))
						@if($_GET['active'] == 1)
						<h3 class="m-portlet__head-text">
							Triển khai trả về
						</h3>
						@elseif($_GET['active'] == 2)
						<h3 class="m-portlet__head-text">
							Thông tin phiếu chuyển hàng về khách hàng
						</h3>
						@elseif($_GET['active'] == 3)
						<h3 class="m-portlet__head-text">
							Tạo phiếu trả về đối tác
						</h3>
						@elseif($_GET['active'] == 4)
						<h3 class="m-portlet__head-text">
							Tạo phiếu nhận hàng vào kho từ phiếu trả về
						</h3>
						@endif
					@endif
				</h3>
			</div>
		</div>
		@endif
	</div>
	<div class="m-portlet__body">
		@if(isset($_GET['active']) && $_GET['active'] == 1)
		<div class="row">

			<div class="col-xl-3 cus-list">
				<div class="m-portlet__head_ row">
					<span>Tỉnh - Quận - Phường</span>
				</div>
				<div class="m-wrapper-provider row">
					{{ $helps->getProvinceList($aTotalRows, 0) }}
				</div>
			</div>
			<div class="col-xl-7">
				<div class="m-portlet__head_">
					<i>Danh sách đơn đã chọn</i>
				</div>
				<div id="m_sections">
					@if(isset($_GET['active']) && $_GET['active'] == 1)
					<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_1">
						<div class="table-responsive">
							<table class="table table-bordered table-hover tbl-pickup" style="margin: 0px !important;">
								<thead>
									<tr style="white-space: nowrap;" class="text-center">
										<th>#</th>
										<th>STT</th>
										<th>Mã đối tác</th>
										<th>Lý do cuối</th>
										<th>Tỉnh</th>
										<th>Quận</th>
										<th>Phường</th>
										<th>Địa chỉ</th>
										<th>Tên khách hàng</th>
										<th>Số điện thoại</th>
										<th>Sản phẩm</th>
										<th>COD</th>
										<th>D+ tại DC</th>
									</tr>
								</thead>

							</table>
						</div>
					</div>
					@endif
				</div>
			</div>
			<div class="col-xl-2 cus-list">
				<form method="POST" action="{{ route('create-deploy-return') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="list[order]" value="">
					<div class="m-portlet__head_">
						<i>Thông tin phiếu</i>
					</div>
					<div class="m-portlet row">
						<div class="form-group">
							<label>Chọn DM</label>

							<select name="charge_dm" id="" class="form-control">
								<option value="">Chọn nhân viên</option>
								@if(isset($aUser) && !empty($aUser))
								@foreach($aUser as $k => $aVal)
								<option value="{{ $aVal['id'] }}">{{ $aVal['full_name'] }}</option>
								@endforeach
								@endif
							</select>
							<br>
							<label>Số đơn đã chọn: <span class="totalRecords">0</span></label>
							<button type="submit" class="btn btn-success btn-sm btn_create_pill_pickup">  Tạo phiếu </button>
						</div>
					</div>
				</form>
			</div>
		</div>
		@elseif(isset($_GET['active']) && $_GET['active'] == 2)
		<form class="row" method="POST" action="{{ route('pill-add') }}">
			{{ csrf_field() }}
			<div class="col-lg-3 col-sm-12 col-form-label col-xs-12 text-right"> {{ __('Chọn đối tác:') }}</div>
			<div class="col-lg-7 col-sm-12 col-xs-12">
				<select class="form-control" name="dc" onchange="return wareHouse(this)">
					<option>Chọn đối tác</option>
					@if(isset($customer))
						@foreach($customer as $k => $aVal)
							<option value="{{ $aVal['id'] }}">{{ $aVal['full_name'] }}</option>
						@endforeach
					@endif
				</select>
			</div>
			<br><br>
			<div class="clearfix"></div>
			<div class="text-center" style="margin: 0 auto">
				<button type="submit" class="btn btn-sm btn-success m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15 text-white"><i class="la la-check"></i> {{ __('Tạo phiếu trả về khách hàng') }}</button>
				<a href="" class="btn btn-sm text-white btn-danger m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15"><i class="la la-arrow-left"></i> {{ __('Danh sách đơn hàng') }}</a>
			</div>
		</form>
		@elseif(isset($_GET['active']) && $_GET['active'] == 3 )
		<form class="row" method="POST" action="{{ route('pill-add') }}">
			{{ csrf_field() }}
			<div class="col-lg-3 col-sm-12 col-form-label col-xs-12 text-right"> {{ __('Mã phiếu:') }}</div>
			<div class="col-lg-7 col-sm-12 col-xs-12">
				<input type="text" name="" placeholder="Nhập mã phiếu pickup cần nhập" class="form-control">
			</div>
			<div class="clearfix"></div>
			<div class="text-center" style="margin: 0 auto">
				<button type="submit" class="btn btn-sm btn-success m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15 text-white"><i class="la la-check"></i> {{ __('Tạo phiếu nhập kho') }}</button>
				<a href="" class="btn btn-sm text-white btn-danger m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15"><i class="la la-arrow-left"></i> {{ __('Danh sách đơn hàng') }}</a>
			</div>
		</form>
		@elseif(isset($_GET['active']) && $_GET['active'] == 4 )
		<form class="row" method="POST" action="{{ route('pill-add') }}">
			{{ csrf_field() }}
			<div class="col-lg-3 col-sm-12 col-form-label col-xs-12 text-right"> {{ __('Mã phiếu:') }}</div>
			<div class="col-lg-7 col-sm-12 col-xs-12">
				<input type="text" class="form-control" placeholder="Mã phiếu trả về cần nhập">
			</div>
			<div class="clearfix"></div>
			<div class="text-center" style="margin: 0 auto">
				<button type="submit" class="btn btn-sm btn-success m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15 text-white"><i class="la la-check"></i> {{ __('Tạo phiếu nhập kho') }}</button>
				<a href="" class="btn btn-sm text-white btn-danger m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15"><i class="la la-arrow-left"></i> {{ __('Danh sách đơn hàng') }}</a>
			</div>
		</form>
		@else
		@include('backend.module.Unloading.DeployPickup.bill-detail')
		@endif
	</div>
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	var table;
	$(document).ready(function() {
		table = $('.tbl-pickup').dataTable({
			"searching": false,
			"bLengthChange": false,
			"ordering": false
		});

		$('button.btn_create_pill_pickup').on('click',function(){
			if($('select[name="charge_dm"]').val() == undefined || $('select[name="charge_dm"]').val() == ''){
				alert('Vui lòng chọn DM ');
			}else{
				return true;
			}

			return false;
		});
	});
	var aItemAdd = new Array();
	var unsetItem = new Array();
	var aOrderListing = new Array();

	function arrayPush($this)
	{
		if($($this).hasClass('active'))
		{
			var removeIndex = aItemAdd.map(function(item) { return item.id; }).indexOf($($this).attr('data-id'));
			aItemAdd.splice(removeIndex, 1);
			if($( $this ).parent().find('ul li small').length > 0){

				$( $this ).parent().find('ul li small').each(function( i,e ) {
					removeIndex = aItemAdd.map(function(item) {
						return item.id;
					}).indexOf($(e).attr('data-id'));
					aItemAdd.splice(removeIndex, 1);
				});
			}
		}else{
			aItemAdd.push(
			{
				id : $($this).attr('data-id'),
				type : $($this).attr('data-type')
			}
			);
			if($( $this ).parent().find('ul li small').length > 0){
				$( $this ).parent().find('ul li small').each(function( i,e ) {
					aItemAdd.push(
					{
						id : $(e).attr('data-id'),
						type : $(e).attr('data-type')
					}
					);
				});
			}
		}
	}



	function addRowsToTable($this)
	{
		arrayPush($this);
		var aListing = removeDuplicates(aItemAdd, "id");
		var data = {
			'data' : aListing,
			'unsetItem' : unsetItem,
			'_token' : $('meta[name="csrf-token"]').attr('content')
		};
		$.ajax({
			type : 'POST',
			url : "{{ route('get-data-deploy-pickup') }}",
			data : data,
			beforeSend:function(){

				if($($this).hasClass('active')){
					$($this).text('Chọn');
					$($this).parent().find('ul li small').text('Chọn');
					$($this).parent().find('ul li small').removeClass('active');
					$($this).removeClass('active');
				}else{
					$($this).text('Bỏ Chọn');
					$($this).parent().find('ul li small').text('Bỏ Chọn');
					$($this).addClass('active');
					$($this).parent().find('ul li small').addClass('active');
				}
			},
			success : function(data){
				var aResponse = [];
				var aResponseObject = {};
				table.fnClearTable();

				if(aListing != ''){
					aOrderListing = [];
					$.each( $.parseJSON(data), function(i, obj) {
						aOrderListing.push(obj.order_code);
						$rows = [
							'<button class="btn btn-success btn-sm" data-value="'+obj.order_id+'" ">Bỏ chọn</button>',
							i + 1,
							obj.customer_code,
							obj.reason,
							obj.province_name,
							obj.district_name,
							obj.ward_name,
							obj.address,
							obj.receiver_name,
							obj.phone,
							obj.product_name,
							obj.cod,
							obj.save_to_dc_date
						];
						aResponse.push($rows);
					});
					$('.totalRecords').text(aOrderListing.length);
					$('input[name="list[order]"]').val(aOrderListing);
					table.fnAddData( aResponse );
				}else{
					$('.totalRecords').text(0);

				}
			}
		});
	}


	function removeDuplicates(originalArray, prop) {
		var newArray = [];
		var lookupObject  = {};

		for(var i in originalArray) {
			lookupObject[originalArray[i][prop]] = originalArray[i];
		}

		for(i in lookupObject) {
			newArray.push(lookupObject[i]);
		}
		return newArray;
	}

	function wareHouse($this)
	{
		var data = {
			'_id' : $($this).val(),
			'_token' : $('meta[name="csrf-token"]').attr('content')
		};

		$.ajax({
			type : 'POST',
			data : data , 
			url : '{{ route("get-warehouse") }}',
			success : function(data){
				$('.dc__of_user').empty();
				$('.dc__of_user').append('<option value="">Chọn kho hàng</option>');
				$.each(data,function(i,e){
					$('.dc__of_user').append('<option value="'+e.id+'">'+e.name+'</option>');
				});	
				
			} 
		});	
	}

</script>
@endsection
