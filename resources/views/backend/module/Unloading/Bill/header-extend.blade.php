<div class="menu-order-horizotal">
	<div class="m-tabs" data-tabs="true" data-tabs-contents="#m_sections">
		<ul class="m-nav m-nav--active-bg m-nav--active-bg-padding-lg m-nav--font-lg m-nav--font-bold m--margin-bottom-20 m--margin-top-10 m--margin-right-40" id="m_nav" role="tablist">
			<li class="m-nav__item">
				<a class="m-nav__link  m-tabs__item @if(\Route::current()->getName() == 'bill-index') m-tabs__item--active @endif" href="{{ route('bill-index') }}">
					<span class="m-nav__link-text">Danh sách phiếu</span>
				</a>
			</li>
			<li class="m-nav__item">
				<a class="m-nav__link collapsed" role="tab" id="m_nav_link_1" data-toggle="collapse" href="#m_nav_sub_1" aria-expanded="false">
					<span class="m-nav__link-title">
						<span class="m-nav__link-wrap">
							<span class="m-nav__link-text">Lấy hàng</span>
						</span>
					</span>
					<span class="m-nav__link-arrow"></span>
				</a>
				<ul class="m-nav__sub collapse menu-child-pages" id="m_nav_sub_1" role="tabpanel" aria-labelledby="m_nav_link_1" data-parent="#m_nav" style="">
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('bill-pickup') }}">
							<span class="m-nav__link-text">Phiếu pickup lấy hàng từ đối tác</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('collect-good-from-pickup-bill') }}">

							<span class="m-nav__link-text">Phiếu nhận hàng từ phiếu pickup</span>
						</a>
					</li>

					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('collect-good-from-pickup-bills') }}">

							<span class="m-nav__link-text">Phiếu nhận hàng từ nhiều phiếu pickup</span>
						</a>
					</li>

				</ul>
			</li>
			<li class="m-nav__item">
				<a class="m-nav__link collapsed" role="tab" id="m_nav_link_2" data-toggle="collapse" href="#m_nav_sub_2" aria-expanded="false">
					<span class="m-nav__link-title">
						<span class="m-nav__link-wrap">
							<span class="m-nav__link-text">Giao hàng</span>
						</span>
					</span>
					<span class="m-nav__link-arrow"></span>
				</a>
				<ul class="m-nav__sub collapse menu-child-pages" id="m_nav_sub_2" role="tabpanel" aria-labelledby="m_nav_link_2" data-parent="#m_nav" style="">
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-deploy') }}">
							<span class="m-nav__link-text">Phiếu triển khai giao hàng</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-delivery') }}">
							<span class="m-nav__link-text">Phiếu giao hàng</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-collect-good') }}">
							<span class="m-nav__link-text">Phiếu giao hàng từ phiếu nhận hàng</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-collect-cod') }}">
							<span class="m-nav__link-text">Phiếu nhận tiền từ phiếu giao hàng</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="m-nav__item">
				<a class="m-nav__link collapsed" role="tab" id="m_nav_link_3" data-toggle="collapse" href="#m_nav_sub_3" aria-expanded="true">
					<span class="m-nav__link-title">
						<span class="m-nav__link-wrap">
							<span class="m-nav__link-text">Trả về</span>
						</span>
					</span>
					<span class="m-nav__link-arrow"></span>
				</a>
				<ul class="m-nav__sub collapse menu-child-pages" id="m_nav_sub_3" role="tabpanel" aria-labelledby="m_nav_link_3" data-parent="#m_nav" style="">
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-deploy-return') }}">
							<span class="m-nav__link-text">Phiếu triển khai trả về đối tác</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-loading-to-customer') }}">
							<span class="m-nav__link-text">Phiếu chuyển hàng về đối tác</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item"  href="{{ route('collect-good-from-return-bill') }}">
							<span class="m-nav__link-text">Phiếu nhận hàng vào kho từ phiếu trả về</span>
						</a>
					</li>
				</ul>
			</li>

			<li class="m-nav__item">
				<a class="m-nav__link collapsed" role="tab" id="m_nav_link_4" data-toggle="collapse" href="#m_nav_sub_4" aria-expanded="false">
					<span class="m-nav__link-title">
						<span class="m-nav__link-wrap">
							<span class="m-nav__link-text">Giao hàng ngoại tuyến</span>
						</span>
					</span>
					<span class="m-nav__link-arrow"></span>
				</a>
				<ul class="m-nav__sub collapse menu-child-pages" id="m_nav_sub_4" role="tabpanel" aria-labelledby="m_nav_link_3" data-parent="#m_nav" style="">
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-delivery-out-of-serve') }}">
							<span class="m-nav__link-text">Phiếu giao hàng đơn ngoại tuyến</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-collect-good-out-of-serve') }}">
							<span class="m-nav__link-text">Phiếu nhận hàng đơn ngoại tuyến</span>
						</a>
					</li>
					<li class="m-nav__item">
						<a class="m-nav__link m-tabs__item" href="{{ route('pill-collect-cod-out-of-serve') }}">
							<span class="m-nav__link-text">Phiếu nhận tiền đơn ngoại tuyến</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="m-nav__item">
				<a class="m-nav__link  m-tabs__item @if(\Route::current()->getName() == 'unloading-from-partner-index') m-tabs__item--active @endif" href="{{ route('unloading-from-partner-index') }}">
					<span class="m-nav__link-text">Phiếu nhận hàng từ đối tác</span>
				</a>
			</li>
			<li class="m-nav__item">
				<a class="m-nav__link  m-tabs__item @if(\Route::current()->getName() == 'bill-loading-to-dc-index') m-tabs__item--active @endif" href="{{ route('bill-loading-to-dc-index') }}">
					<span class="m-nav__link-text">Phiếu chuyển hàng tới DC khác</span>
				</a>
			</li>
			<li class="m-nav__item">
				<a class="m-nav__link  m-tabs__item @if(\Route::current()->getName() == 'bill-unloading-from-dc-index') m-tabs__item--active @endif" href="{{ route('bill-unloading-from-dc-index') }}">
					<span class="m-nav__link-text">Phiếu nhận hàng từ DC khác</span>
				</a>
			</li>
			<li class="m-nav__item">
				<a class="m-nav__link  m-tabs__item @if(\Route::current()->getName() == 'bill-check-inventory-bill-index') m-tabs__item--active @endif" href="{{ route('bill-check-inventory-bill-index') }}">
					<span class="m-nav__link-text">Phiếu kiểm kho</span>
				</a>
			</li>

		</ul>
	</div>
</div>
