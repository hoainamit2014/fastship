@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	@include('backend.module.Unloading.Bill.header-extend')
	<div class="m-portlet m-portlet--space">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						QUẢN LÝ PHIẾU
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<form class="m-form m-form--fit m--margin-bottom-20" action="{{ route('order-index') }}" method="get" style="padding-left: 15px;padding-right: 15px">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
								<label>Từ khóa tìm kiếm:</label>
								<input type="text" class="form-control m-input" placeholder="Nhập từ khóa tìm kiếm" name="pill_code" >
							</div>

						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>DC phụ trách:</label>
								<select name="responsible_person_id" class="form-control m-input" data-col-index="2" id="js_select_dc">
									<option value="">Chọn chi nhánh phụ trách</option>
									@if(isset($aBranch) && count($aBranch) > 0)
									@foreach($aBranch as $k => $aVal)
									<option value="{{ $aVal['district_id'] }}">{{ Helpers::getDistrictName($aVal['district_id']) }}</option>
									@endforeach
									@endif
								</select>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Người phụ trách:</label>
								<select name="charge_people" class="form-control m-input" required data-col-index="3" id="js_select_charge">
									<option value="">Chọn người phụ trách</option>
									@if(isset($aCharge) && count($aCharge) > 0)
									@foreach($aCharge as $k => $aVal)
									<option value="{{ $aVal['id'] }}">{{ $aVal['full_name'] }}</option>
									@endforeach
									@endif
								</select>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="form-group">
								<label>Thời gian hoàn thành:</label>
								<input type='text' class="form-control m_daterangepicker_bill" name="complete_date" placeholder="Chọn thời gian hoàn thành" type="text"/>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Thời gian tạo:</label>
								<input type="text" class="form-control m_daterangepicker_bill_1" name="create_date" placeholder="Chọn thời tạo" data-col-index="5">
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Trạng thái phiếu:</label>
								<select class="form-control m-input" name="status" data-col-index="6">
									<option value="">Chọn trạng thái phiếu</option>
									<option value="1">Tạo mới </option>
									<option value="2">Đang xử lý</option>
									<option value="3">Đã xử lý</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Loại phiếu:</label>
								<select class="form-control m-input" name="pill_type_id" data-col-index="7">
									<option value="">Chọn loại phiếu</option>
									@if(isset($aFilType) && count($aFilType) > 0)
									@foreach($aFilType as $k => $aVal)
									<option value="{{ $aVal['id'] }}">{{ $aVal['name'] }}</option>
									@endforeach
									@endif

								</select>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
								<label>Nhận hàng:</label>
								<select class="form-control m-input" name="received_status" data-col-index="8">
									<option value="">Tất cả</option>
									<option value="1">Đã nhận hàng</option>
									<option value="2">Chưa nhận hàng</option>
								</select>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="m-checkbox-list">
								<label class="m-checkbox">
									<input type="checkbox" class="m-input" value="0" name="order_number" data-col-index="9"> {{ __('Phiếu 0 đơn') }}
									<span style="margin-top: 4px"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="row m--margin-top-20" >
						<div class="col-lg-12">
							<button class="btn btn-success m-btn m-btn--icon" type="submit" id="m_search">
								<span>
									<i class="la la-search"></i>
									<span>Tìm kiếm</span>
								</span>
							</button>
						</div>
					</div>
				</form>
				<div class="col-xl-12">
					<div id="m_sections">
						<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_1">
							<div class="table-responsive">
								<table class="table table-bordered table-hover" id="table-bill-listing">
									<thead>
										<tr style="white-space: nowrap;" class="text-center">
											<th>Id</th>
											<th>Mã phiếu</th>
											<th>Số đơn</th>
											<th>Loại phiếu</th>
											<th>Phụ trách</th>
											<th>Trạng thái</th>
											<th>Trạng thái nhận hàng</th>
											<th>Người tạo</th>
											<th>Người cập nhật</th>
											<th>Ngày tạo</th>
											<th>Ngày hoàn thành</th>
											<th>Quản lý</th>

										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$( document ).ready(function() {
		var sData = {
			_token : $('input[name="_token"]').val(),
			keyword : $('input[name="keyword"]').val(),
		}
		var table = $('#table-bill-listing').DataTable({
			bProcessing: true,
			language: {
				lengthMenu: " Hiển thị _MENU_ records",
				zeroRecords: "Không tìm thấy phiếu nào.",
			},
			ajax: {
				url: "{{ route('bill-server-side') }}",
				type: "POST",
				data: function(data){
					data.columns[1]['search']['value'] = $('input[name="pill_code"]').val();
					// 1 : Checked
					// 2 : Unchecked
					data.columns[2]['search']['value'] = $('input[name="order_number"]:checked').val() ? 1 : 2;
					data.columns[3]['search']['value'] = $('select[name="pill_type_id"]').val();
					data.columns[4]['search']['value'] = $('select[name="charge_people"]').val();
					data.columns[5]['search']['value'] = $('select[name="status"]').val();
					data.columns[6]['search']['value'] = $('select[name="received_status"]').val();
					data.columns[7]['search']['value'] = $('select[name="author"]').val();
					data.columns[9]['search']['value'] = $('input[name="create_date"]').val();
					data.columns[10]['search']['value'] = $('input[name="complete_date"]').val();
					data._token = $('input[name="_token"]').val();

				}
			},
			bPaginate:true,
			serverSide: true,
			sPaginationType:"full_numbers",
			searching: false,
			order: [[ 0, "desc" ]],
			// lengthMenu: [5, 10, 25, 50],
			pageLength: 10,
			aoColumns: [
			{ mData: 'id' } ,
			{
				mData: 'bill_code' , mRender:function(bill_code){
					return '<a href="{{ route("deloy-pickup-index") }}?billCode='+bill_code+'">'+ bill_code +'</a>';
				}
			},
			{ mData: 'order_number' },
			{ mData: 'bill_type_id' },
			{ mData: 'responsible_person_id' },
			{
				mData: 'status' , mRender: function ( status ) {
					var aStatus = {
						1 : {
							title : '<span class="m-badge m-badge--metal m-badge--wide">Tạo mới</span>'
						},
						2 : {
							title : '<span class="m-badge m-badge--warning m-badge--wide">Đang xử lý</span>'
						},
						3 : {
							title : '<span class="m-badge m-badge--success m-badge--wide">Đã xử lý</span>'
						}
					}
					return aStatus[status].title;
				}
			},
			{
				mData: 'received_status' , mRender: function ( received_status ) {
					var aReceivedStatus = {
						1 : {
							title : '<span class="m-badge m-badge--success m-badge--wide">Đã nhận hàng</span>'
						},
						2 : {
							title : '<span class="m-badge m-badge--warning m-badge--wide text-white">Chưa nhận hàng</span>'
						}
					};
					return (received_status !== '' && received_status !== null) ? aReceivedStatus[received_status].title : 'Chưa cập nhật';
				}
			},
			{ mData: 'author' },
			{ mData: 'author' },
			{ mData: 'create_date' },
			{ mData: 'complete_date' },
			{ mData: null,
				mRender: function ( data, type, full ) {
					return '<button class="btn btn-success btn-sm"> <i class="la la-pencil"></i> Xem</button>';
				}
			}
			]
		});
		$("#m_search").on("click", function(t) {
			table.ajax.reload();
			return false;
		});
	});
</script>
<script src="{{ asset('public/fontend/assets/demo/default/custom/crud/forms/widgets/bootstrap-daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/backend/js/bill.js') }}" type="text/javascript"></script>
@endsection
