@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="menu-order-horizotal">
		<div class="m-tabs" data-tabs="true" data-tabs-contents="#m_sections">
			<ul class="m-nav m-nav--active-bg m-nav--active-bg-padding-lg m-nav--font-lg m-nav--font-bold m--margin-bottom-20 m--margin-top-10 m--margin-right-40" id="m_nav" role="tablist">
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 1) m-tabs__item--active @endif" href="{{ route('order-out-of-serve') }}?active=1">
						<span class="m-nav__link-text">Danh sách đơn hàng ngoại tuyến</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 2) m-tabs__item--active @endif" href="{{ route('order-out-of-serve') }}?active=2">
						<span class="m-nav__link-text">Giao hàng đơn ngoại tuyến</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 3) m-tabs__item--active @endif" href="{{ route('order-out-of-serve') }}?active=3">
						<span class="m-nav__link-title">
							<span class="m-nav__link-wrap">
								<span class="m-nav__link-text">Cập nhật Complete từ file Excel</span>
							</span>
						</span>
					</a>

				</li>
				<li class="m-nav__item">
					<a class="m-nav__link  m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 4) m-tabs__item--active @endif" href="{{ route('order-out-of-serve') }}?active=4">
						<span class="m-nav__link-text">Nhận hàng từ DM</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link  m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 5) m-tabs__item--active @endif" href="{{ route('order-out-of-serve') }}?active=5">
						<span class="m-nav__link-text">Nhận tiền từ DM</span>
					</a>
				</li>

			</ul>
		</div>
	</div>
	<div class="m-portlet m-portlet--space">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						@if(isset($_GET['active']))
							@if($_GET['active'] == 1)
								ĐƠN HÀNG NGOẠI TUYẾN
							@elseif($_GET['active'] == 2)
								TẠO PHIẾU GIAO HÀNG
							@elseif($_GET['active'] == 3)
								CẬP NHẬT COMPLETE ĐƠN NGOẠI TUYẾN TỪ FILE EXCEL
							@elseif($_GET['active'] == 4)
								NHẬP HÀNG TỪ DM ĐƠN HÀNG NGOẠI TUYẾN
							@elseif($_GET['active'] == 5)
								NHẬN TIỀN TỪ DM ĐƠN HÀNG NGOẠI TUYẾN
							@endif
						@endif
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="row">

				<div class="col-xl-12">
					<div class="m-tabs-content" id="m_sections">
						@if(isset($_GET['active']) && $_GET['active'] == 1)
						<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_1">
							<div>
								<div class="m-accordion m-accordion--section m-accordion--padding-lg" id="m_section_1_content">
									<div class="m-accordion__item" >
										<div class="m-accordion__item-body collapse show" id="m_section_1_content_1_body" role="tabpanel" aria-labelledby="m_section_1_content_1_head" data-parent="#m_section_1_content" style="padding: 15px;">
											<form class="m-form m-form--fit m--margin-bottom-20" action="http://fastship.co/fs/cpanel/order" method="get">
												<input type="hidden" value="http://fastship.co" id="js_core_path">
												<div class="row">
													<div class="col-lg-3">
														<div class="form-group">
															<label>Từ khóa:</label>
															<input type="text" class="form-control" name="search[product_name]" value="">
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Đối tác:</label>
															<select class="form-control" name="search[customer_code]">
																<option value="">Chọn:</option>
																<option value="LAZADA">Trịnh Hoài Nam</option>
																<option value="LAZADA">Nguyễn Văn A</option>
																<option value="LAZADA">Nguyễn Văn B</option>
																<option value="LAZADA">Nguyễn Văn C</option>
																<option value="LAZADA">Nguyễn Văn D</option>
																<option value="LAZADA">Nguyễn Văn E</option>
																<option value="LAZADA">Nguyễn Văn F</option>
																<option value="LAZADA">Nguyễn Văn G</option>
																<option value="LAZADA">Nguyễn Văn H</option>
																<option value="LAZADA">Nguyễn Văn J</option>
																<option value="LAZADA">Nguyễn Văn K</option>
																<option value="LAZADA">Nguyễn Văn L</option>
																<option value="LAZADA">Nguyễn Văn Z</option>
																<option value="LAZADA">Nguyễn Văn X</option>
																<option value="LAZADA">Nguyễn Văn C</option>
																<option value="LAZADA">Nguyễn Văn V</option>
																<option value="LAZADA">Nguyễn Văn B</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Trạng thái:</label>
															<select class="form-control" name="search[status]">
																<option value="">Chọn:</option>
																<option value="1">Tạo mới (New)</option>
																<option value="2">Hủy (Cancel)</option>
																<option value="3">Lấy hàng không thành công (PickupFail)</option>
																<option value="4">Nhận mới (Received)</option>
																<option value="5">Giao không thành công (Fail)</option>
																<option value="6">Khách hàng từ chối nhận (Reject)</option>
																<option value="7">Thành công (Complete)</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Trạng thái vật lý:</label>
															<select class="form-control" name="search[physical_status]">
																<option value="">Chọn:</option>
																<option value="1">Chờ lấy hàng (WaitToPickup)</option>
																<option value="2">Đang đi lấy hàng (Picking)</option>
																<option value="3">Lấy hàng thành công (Picked)</option>
																<option value="4">Tồn kho (Unloading)</option>
																<option value="5">Đang vận chuyển (Loading)</option>
																<option value="6">Đang đi phát (Delivery)</option>
																<option value="7">Phát thành công (Successed)</option>
																<option value="8">Chuyển trả đối tác (LoadingToPartner)</option>
																<option value="9">Trả thành công (Returned)</option>
																<option value="10">Hủy giao hàng (Cancel)</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Tỉnh / thành phố:</label>
															<select name="search[province_id]" id="js_select_province" class="form-control">
																<option value=""></option>
																@foreach ($aProvinces as $aValue)
																<option value="{{ $aValue->id }}">{{ $aValue->text }}</option>
																@endforeach
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Quận / huyện:</label>
															<select name="search[district_id]" class="form-control" id="js_select_district">
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Phường / xã:</label>
															<select name="search[ward_id]" class="form-control" id="js_select_ward">
															</select>
														</div>
													</div>

													<div class="col-lg-3">
														<div class="form-group">
															<label>Ngày tạo:</label>
															<div class="input-daterange input-group" id="js_order_date_picker">
																<input type="text" class="form-control" name="search[from_date]" placeholder="Từ" value="">
																<div class="input-group-append">
																	<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
																</div>
																<input type="text" class="form-control" name="search[to_date]" placeholder="đến" value="">
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Ngày bắt đầu giao:</label>
															<div class="input-group date">
																<input type="text" class="form-control search-order-date">
																<div class="input-group-append">
																	<span class="input-group-text">
																		<i class="la la-calendar-check-o"></i>
																	</span>
																</div>
															</div>
														</div>
													</div>

													<div class="col-lg-3">
														<div class="form-group">
															<label>Ngày giao thành công:</label>
															<div class="input-group date">
																<input type="text" class="form-control search-order-date">
																<div class="input-group-append">
																	<span class="input-group-text">
																		<i class="la la-calendar-check-o"></i>
																	</span>
																</div>
															</div>
														</div>
													</div>

													<div class="col-lg-3">
														<div class="form-group">
															<label>Ngày hoàn thành:</label>
															<div class="input-group date">
																<input type="text" class="form-control search-order-date">
																<div class="input-group-append">
																	<span class="input-group-text">
																		<i class="la la-calendar-check-o"></i>
																	</span>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label>Ngày giao hàng gần nhất:</label>
															<div class="input-group date">
																<input type="text" class="form-control search-order-date">
																<div class="input-group-append">
																	<span class="input-group-text">
																		<i class="la la-calendar-check-o"></i>
																	</span>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-12">
														<button class="btn btn-success m-btn m-btn--icon" type="submit">
															<span>
																<i class="la la-search"></i>
																<span>Tìm kiếm</span>
															</span>
														</button>

													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="table-responsive tbl-track">
									<table class="table table-bordered table-hover tbl__order__outofservices">
										<thead>
											<tr style="white-space: nowrap;" class="text-center">
												<th>Track</th>
												<th>STT</th>
												<th>ID</th>
												<th>Mã</th>
												<th>Mã đối tác</th>
												<th>Tỉnh</th>
												<th>Quận</th>
												<th>Phường</th>
												<th>Địa chỉ</th>
												<th>Tên khách hàng</th>
												<th>Số điện thoại</th>
												<th>Sản phẩm</th>
												<th>KM1</th>
												<th>KM2</th>
												<th>COD</th>
												<th>Cập nhật cuối</th>
												<th>Lý do cuối</th>
												<th>Trạng thái</th>
												<th>Trạng thái vật lý</th>
												<th>D+</th>
												<th>Ghi chú</th>
												<th>Người tạo</th>
											</tr>
										</theady>
										<tbody>
											@if(!empty($aOrders))
											@foreach($aOrders as $k => $aVal)
											
											<tr style="white-space: nowrap;" class="text-center">
												<td>
													<button class="btn btn-success btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="la la-chain"></i>
													</button>
													<div class="dropdown-menu" x-placement="bottom-start">
														<a class="dropdown-item" href="#">Track</a>
													</div>
												</td>
												<td>{{ $k + 1  }}</td>
												<td>{{ $aVal['order_id'] }}</td>
												<td>{{ $aVal['order_code'] }}</td>
												<td>{{ $aVal['customer_code'] }}</td>
												<td>{{ $aVal['province_name'] }}</td>
												<td>{{ $aVal['district_name'] }}</td>
												<td>{{ $aVal['ward_name'] }}</td>
												<td>{{ $aVal['address'] }}</td>
												<td>{{ $aVal['receiver_name'] }}</td>
												<td>{{ $aVal['phone'] }}</td>
												<td>{{ $aVal['product_name'] }}</td>
												<td></td>
												<td></td>
												<td>
												<span class="">{{ number_format($aVal['cod'],0,',','.') }}</span> <span>đ</span>
												</td>
												<td>{{ $aVal['updated_at'] }}</td>
												<td>{{ $aVal['reason'] }}</td>
												<td>{{ $aVal['status'] }}</td>
												<td>{{ $aVal['physical_status'] }}</td>
												<td>{{ $aVal['save_to_dc_date'] }}</td>
												<td>{{ $aVal['note'] }}</td>
												<td>{{ $aVal['user_name'] }}</td>
											</tr>
											@endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
						@endif
						@if(isset($_GET['active']) && $_GET['active'] == 2)
						<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_2">
							<div class="m-accordion m-accordion--section m-accordion--padding-lg" id="m_section_2_content">
								<!--begin::Item-->
								<div class="m-accordion__item">
									<form class="row" method="POST" action="{{ route('pill-add') }}">
										{{ csrf_field() }}
										<div class="col-lg-3 col-sm-12 col-form-label col-xs-12"> {{ __('Chọn khách hàng cần nhập kho') }}</div>
										<div class="col-lg-7 col-sm-12 col-xs-12">
											<select class="form-control m-select2" id="m_select2_1" name="dc">
												<option>Khách hàng</option>
												@if(isset($customer))
												@foreach($customer as $k => $aVal)
												<option value="{{ $aVal['id'] }}">{{ $aVal['name'] }} ({{ $aVal['username'].' - '. $aVal['email'] }})</option>
												@endforeach
												@endif
											</select>
											<div class="form-control-feedback">{{ __('Để trống nếu bạn muốn unloading từ nhiều khách hàng') }}</div>
										</div>
										<div class="clearfix"></div>
										<div class="text-center" style="margin: 0 auto">
											<button type="submit" class="btn btn-sm btn-success m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15 text-white"><i class="la la-check"></i> {{ __('Tạo phiếu nhập kho') }}</button>
											<a href="" class="btn btn-sm text-white btn-danger m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15"><i class="la la-arrow-left"></i> {{ __('Danh sách đơn hàng') }}</a>
										</div>
									</form>
								</div>
								<!--end::Item-->
							</div>
						</div>
						@endif
						@if(isset($_GET['active']) && $_GET['active'] == 3)
						<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_3">

							<div class="m-accordion m-accordion--section m-accordion--padding-lg" id="m_section_2_content" style="border:1px solid #f0f0f0;padding: 15px;border-radius: 5px;">
								<!--begin::Item-->
								<div class="m-accordion__item">
									<div class="form-group m-form__group">
										<label for="exampleInputEmail1">Chọn file Excel</label>
										<div></div>
										<div class="custom-file">
											<input type="file" class="custom-file-input" id="customFile">
											<label class="custom-file-label" for="customFile">Chọn tập tin upload</label>
										</div>
									</div>
									<div class="row">
										<div class="col-xl-4">
											<a href="" class="btn btn-sm btn-primary"><i class="la la-download"></i> File mẫu</a>
										</div>
										<div class="col-xl-4 text-center">
											<button class="btn btn-sm btn-success"><i class="la la-upload"></i> Tải lên</button>
										</div>
										<div class="col-xl-4 text-right">
											<a href="" class="btn btn-sm btn-danger"> Danh sách đơn hàng</a>
										</div>
									</div>
								</div>

							</div>
						</div>
						@endif
						@if(isset($_GET['active']) && $_GET['active'] == 4)
						<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_4">

							<div class="m-accordion m-accordion--section m-accordion--padding-lg" id="m_section_2_content" style="border:1px solid #f0f0f0;padding: 15px;border-radius: 5px;">
								<!--begin::Item-->
								<div class="m-accordion__item">
									<form class="m-form m-form--fit m--margin-bottom-20" action="http://fastship.co/fs/cpanel/order/export" method="get">
										<div class="row m--margin-bottom-20">
											<div class="col-md-12 m--margin-bottom-10-tablet-and-mobile">
												<label>Danh sách mã đơn hàng:</label>
												<textarea class="form-control m-input" name="list_order_id" rows="3"></textarea>
											</div>
										</div>
										<button class="btn btn-primary m-btn m-btn--icon">
											Cập nhật
										</button>
										<button class="btn btn-danger m-btn m-btn--icon">
											Danh sách đơn hàng
										</button>
									</form>
								</div>
								<!--end::Item-->
							</div>
						</div>
						@endif
						@if(isset($_GET['active']) && $_GET['active'] == 5)
						<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_5">
							<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_4">
								<div class="m-accordion m-accordion--section m-accordion--padding-lg" id="m_section_2_content" style="border:1px solid #f0f0f0;padding: 15px;border-radius: 5px;">
									<!--begin::Item-->
									<div class="m-accordion__item">
										<form class="m-form m-form--fit m--margin-bottom-20" action="http://fastship.co/fs/cpanel/order/export" method="get">
											<div class="row m--margin-bottom-20">
												<div class="col-md-12 m--margin-bottom-10-tablet-and-mobile">
													<label>Danh sách mã đơn hàng:</label>
													<textarea class="form-control m-input" name="list_order_id" rows="3"></textarea>
												</div>
											</div>
											<button class="btn btn-primary m-btn m-btn--icon">
												Cập nhật
											</button>
											<button class="btn btn-danger m-btn m-btn--icon">
												Danh sách đơn hàng
											</button>
										</form>
									</div>
									<!--end::Item-->
								</div>
							</div>
						</div>
						@endif

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	jQuery(document).ready(function() {
		$('.tbl__order__outofservices').dataTable();
	});
</script>
@endsection


