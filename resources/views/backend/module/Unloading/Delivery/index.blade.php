@extends('backend.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="menu-order-horizotal">
		<div class="m-tabs" data-tabs="true" data-tabs-contents="#m_sections">
			<ul class="m-nav m-nav--active-bg m-nav--active-bg-padding-lg m-nav--font-lg m-nav--font-bold m--margin-bottom-20 m--margin-top-10 m--margin-right-40" id="m_nav" role="tablist">
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 1) m-tabs__item--active @endif" href="{{ route('delivery-index') }}?active=1">
						<span class="m-nav__link-text">Triển khai giao hàng</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 2) m-tabs__item--active @endif" href="{{ route('delivery-index') }}?active=2">
						<span class="m-nav__link-text">Giao hàng</span>
					</a>
				</li>
				<li class="m-nav__item">
					<a class="m-nav__link m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 3) m-tabs__item--active @endif" href="{{ route('delivery-index') }}?active=3">
						<span class="m-nav__link-title">
							<span class="m-nav__link-wrap">
								<span class="m-nav__link-text">Nhận hàng từ phiếu giao hàng</span>
							</span>
						</span>
					</a>

				</li>
				<li class="m-nav__item">
					<a class="m-nav__link  m-tabs__item @if(isset($_GET['active']) && $_GET['active'] == 4) m-tabs__item--active @endif" href="{{ route('delivery-index') }}?active=4">
						<span class="m-nav__link-text">Nhận tiền từ phiếu giao hàng</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet m-portlet--space">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						@if(isset($_GET['active']))
						@if($_GET['active'] == 1)
						TRIỂN KHAI PHÁT HÀNG
						@elseif($_GET['active'] == 2)
						TẠO PHIẾU GIAO HÀNG
						@elseif($_GET['active'] == 3)
						TẠO PHIẾU NHẬN HÀNG
						@elseif($_GET['active'] == 4)
						TẠO MÃ PHIẾU 2 CẦN NHẬN TIỀN
						@endif
						@endif
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">
			@if(isset($_GET['active']) && $_GET['active'] == 1)
			<div class="row">
				<div class="col-xl-2 cus-list">
					<div class="m-portlet__head_">
						<span>Tỉnh - Quận - Phường</span>
					</div>
					<div class="m-portlet">
						<div class="m-portlet__body_">
							<!--begin::Section-->
							<div class="m-accordion m-accordion--default customer-list" id="m_accordion_1" role="tablist">

								<ul>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>
									<li>
										<label>
											<i class="entypo entypo-user"></i>
											<span>LOTTE</span>
											<span> (104) </span>
											<button type="button" class="btn pull-right btn-success btn-sm">  Bỏ chọn
											</button>
										</label>
									</li>

								</ul>
							</div>
							<!--end::Section-->

						</div>
					</div>
				</div>
				<div class="col-xl-8">
					<div class="m-portlet__head_">
						<i>Danh sách đơn đã chọn</i>
					</div>
					<div class="m-portlet" id="m_sections">
						@if(isset($_GET['active']) && $_GET['active'] == 1)
						<div class="m-tabs-content__item m-tabs-content__item--active" id="m_section_1">
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
										<tr style="white-space: nowrap;" class="text-center">
											<th>#</th>
											<th>STT</th>
											<th>Mã hệ thống</th>
											<th>Mã</th>
											<th>Mã đối tác</th>
											<th>Đối tác</th>
											<th>Tỉnh</th>
											<th>Quận</th>
											<th>Phường</th>
											<th>Địa chỉ</th>
											<th>Tên người nhận</th>
											<th>Số điện thoại 1</th>
											<th>Sản phẩm</th>
											<th>COD</th>
											<th>Ghi chú</th>
										</tr>
									</thead>
									<tbody>
										@for($i = 1 ; $i < 29 ; $i++)
										<tr style="white-space: nowrap;" class="text-center">
											<td>
												<button class="btn btn-success btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="la la-chain"></i>
												</button>
												<div class="dropdown-menu" x-placement="bottom-start">
													<a class="dropdown-item" href="#">Track</a>

												</div>
											</td>
											<td>{{ $i }}</td>
											<td>1322543</td>
											<td>V1607182855WEAV</td>
											<td>3077861290</td>
											<td>Quảng Ninh</td>
											<td>Vân Đồn</td>
											<td>XÓM  2, Xã Minh Châu, Huyện Vân Đồn, Tỉnh Quảng Ninh</td>
											<td>TÔ CẠNH HẠT</td>
											<td>01654101450</td>
											<td> </td>
											<td>Bếp gas đôi hồng ngoại đầu đốt trắng Luckymax 7700 + bộ 3 nồi inox + chảo + 5 dao kéo [5p]</td>

											<td>
												<span class="">799.000</span> <span>đ</span>
											</td>
											<td>17/07/2018 13:54</td>
											<td>Đã nhận hàng</td>
											<td>Nhận hàng vào kho (Unloading)</td>
											<td>2</td>
											<td>ĐỀ NGHỊ QUÝ KHÁCH KIỂM TRA HÀNG KỸ TRƯỚC KHI THANH TOÁN 01654101450</td>
											<td>SCJ</td>
										</tr>
										@endfor
									</tbody>
								</table>
							</div>
						</div>
						@endif
					</div>
				</div>
				<div class="col-xl-2 cus-list">
					<div class="m-portlet__head_">
						<i>Thông tin phiếu</i>
					</div>
					<div class="m-portlet">
						<div class="form-group">
							<label>Chọn DM</label>
							<select name="search[province_id]" id="js_select_provinces" class="form-control">
								<option value=""></option>
							</select>
							<br>
							<label>Số đơn đã chọn:</label>
							<div>0</div>
							<br>
							<button type="button" class="btn btn-success btn-sm">  Tạo phiếu 1 </button>
						</div>

					</div>
				</div>
			</div>
			@elseif(isset($_GET['active']) && $_GET['active'] == 2)
			<form class="row" method="POST" action="{{ route('pill-add') }}">
				{{ csrf_field() }}
				<div class="col-lg-3 col-sm-12 col-form-label col-xs-12 text-right"> {{ __('Chọn DM:') }}</div>
				<div class="col-lg-7 col-sm-12 col-xs-12">
					<select class="form-control m-select2" id="m_select2_1" name="dc">
						<option>Chọn DM</option>
						@if(isset($customer))
						@foreach($customer as $k => $aVal)
						<option value="{{ $aVal['id'] }}">{{ $aVal['name'] }} ({{ $aVal['username'].' - '. $aVal['email'] }})</option>
						@endforeach
						@endif
					</select>
				</div>
				<div class="clearfix"></div>
				<div class="text-center" style="margin: 0 auto">
					<button type="submit" class="btn btn-sm btn-success m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15 text-white"><i class="la la-check"></i> {{ __('Tạo phiếu nhập kho') }}</button>
					<a href="" class="btn btn-sm text-white btn-danger m-btn m-btn--icon m-btn--wide m-btn--air m-btn--custom m--margin-top-15"><i class="la la-arrow-left"></i> {{ __('Danh sách đơn hàng') }}</a>
				</div>
			</form>
			@elseif(isset($_GET['active']) && $_GET['active'] == 3 )
			<form class="row" method="POST" action="{{ route('pill-add') }}">
				{{ csrf_field() }}
				<div class="col-sm-5">
					<div class="form-group m-form__group row">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Nhập mã phiếu nhận hàng">
							<div class="input-group-append">
								<button class="btn btn-success" type="button">Tạo phiếu nhận hàng</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			@elseif(isset($_GET['active']) && $_GET['active'] == 4 )
			<form class="row" method="POST" action="{{ route('pill-add') }}">
				{{ csrf_field() }}
				<div class="col-sm-5">
					<div class="form-group m-form__group row" >
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Nhập mã phiếu 2 cần nhận tiền">
							<div class="input-group-append">
								<button class="btn btn-success" type="button">Tải phiếu</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			@endif
		</div>
	</div>
</div>

@endsection
