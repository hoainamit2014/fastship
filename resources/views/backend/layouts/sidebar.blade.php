<?php $help = new Helpers();?>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{ route('settings-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-line-graph"></i>
					<span class="m-menu__link-text">
						Dashboard
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-users"></i>
					<span class="m-menu__link-text">Quản lý đơn hàng</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu " m-hidden-height="40" style="">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-add') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Tạo đơn hàng</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Danh sách</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-show') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Tra cứu</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-export') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Xuất excel</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-update-outside') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Cập nhật đơn hàng ngoại tuyến</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-update-status') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Cập nhật trạng thái</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-update-return') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Cập nhật trả về</span></a>
						</li>


						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-print-label') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">In nhãn</span></a>
						</li>
						<!-- <li class="m-menu__item " aria-haspopup="true"><a href="{{ route('order-setting') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Cài đặt</span></a>
						</li> -->
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('settings-area') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Cài đặt giá khu vực</span></a>
						</li>
					</ul>
				</div>
			</li>
			<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-list-3"></i>
					<span class="m-menu__link-text">Quản lý giao nhận</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu " m-hidden-height="200" style="display: none; overflow: hidden;">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
							<span class="m-menu__link">
								<span class="m-menu__link-text"></span>
							</span>
						</li>
						<li class="m-menu__item m-menu__item--submenu" >
							<a class="m-menu__link m-menu__toggle" data-toggle="modal" data-target="#bill_list">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text" >Danh sách phiếu</span>
							</a>
						</li>
						<li class="m-menu__item m-menu__item--submenu" data-toggle="modal" data-target="#create_bill_popup" >
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Tạo phiếu</span>
							</a>
						</li>
						<li class="m-menu__item m-menu__item--submenu" data-toggle="modal" data-target="#update_bill_popup">
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Cập nhật phiếu</span>
							</a>
						</li>
						<li class="m-menu__item m-menu__item--submenu" data-toggle="modal" data-target="#deploy_popup">
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Triển khai</span>
							</a>
						</li>
						<li class="m-menu__item m-menu__item--submenu" data-toggle="modal" data-target="#receive_popup">
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Nhận hàng</span>
							</a>
						</li>
						<li class="m-menu__item m-menu__item--submenu" data-toggle="modal" data-target="#money_receive_popup">
							<a href="#" class="m-menu__link m-menu__toggle">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">Nhận tiền</span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{ route('branch-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-diagram"></i>
					<span class="m-menu__link-text">
						Quản lý chi nhánh
					</span>
				</a>
			</li>
			<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-users"></i>
					<span class="m-menu__link-text">Quản lý tài khoản</span><i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu " m-hidden-height="40" style=""><span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('group-index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Quyền</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('users-index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Quản lý người dùng</span></a>
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('customer-index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Quản lý khách hàng</span></a>
						</li>
					</ul>
				</div>
			</li>

			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-diagram"></i>
					<span class="m-menu__link-text">Thống kê</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu " m-hidden-height="40" style="">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('statistic-inDc') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Thống kê D+ tại DC</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('statistic-createNew') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Thống kê đơn hàng tạo mới</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('statistic-receiveNew') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Thống kê đơn hàng nhận mới</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('statistic-inventoryInDc') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Thống kê đơn hàng tồn tại DC</span>
							</a>

						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('statistic-byCustomer') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Thống kê đơn hàng nhận mới theo đối tác</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('statistic-leadTimeSuccess') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Thống kê lead time đơn hàng giao thành công</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('statistic-leadTimeStart') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Thống kê lead time đơn hàng triển khai</span>
							</a>
						</li>
					</ul>
				</div>
			</li>

			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{ route('news-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-interface-6""></i>
					<span class="m-menu__link-text">
						Quản lý tin tức
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">Quản lý tài chính</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu " m-hidden-height="40" style="">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('finance-index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Danh sách phiếu chuyển tiền cho kế toán</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('finance-order-salary') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Danh sách đơn hàng tính lương</span>
							</a>
						</li>
						<li class="m-menu__item " aria-haspopup="true">
							<a href="{{ route('finance-transfer-to-accountant') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Chuyển tiền cho kế toán</span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{ route('slides-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-background"></i>
					<span class="m-menu__link-text">
						Quản lý trình chiếu
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a href="{{ route('pages-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-interface-10"></i>
					<span class="m-menu__link-text">
						Quản lý trang
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{ route('careers-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-suitcase"></i>
					<span class="m-menu__link-text">
						Tuyển dụng
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{ route('menu-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						Quản lý menu
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover" style="background-color:#E91E63">
				<a  href="//delim.co/" target="_blank" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-settings text-white"></i>
					<span class="m-menu__link-text text-white">
						Ghép chuỗi
					</span>
				</a>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a  href="{{ route('settings-index') }}" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-settings"></i>
					<span class="m-menu__link-text">
						Cài đặt
					</span>
				</a>
			</li>
		</ul>
	</div>
</div>

<div class="wrapper__popup">
	<div id="bill_list" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>CHỌN PHIẾU QUẢN LÝ:</h4>
					<form method="GET">
						<p>
							<select class="form-control" name="route">
								<option>Tất cả các phiếu</option>
								@if($help->getBillType())
									@foreach($help->getBillType() as $k => $aVal)
										<option value="{{ route($aVal->route) }}"><?php echo $aVal->name?></option>
									@endforeach
								@endif
							</select>
						</p>
						<button class="btn btn-success" onclick="return redirectRoute(this)" style="width: 100%">CHỌN PHIẾU</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="bill_list" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>CHỌN PHIẾU QUẢN LÝ:</h4>
					<p>
						<select class="form-control">
							<option>Tất cả các phiếu</option>
							<option>Phiếu pickup hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
						</select>
					</p>
					<button class="btn btn-success" style="width: 100%">CHỌN PHIẾU</button>

				</div>

			</div>

		</div>
	</div>
	<div id="update_bill_popup" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>TẠO PHIẾU:</h4>
					<p>
						<select class="form-control">
							<option>Tất cả các phiếu</option>
							<option>Phiếu pickup hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
						</select>
					</p>
					<button class="btn btn-success" style="width: 100%">CHỌN PHIẾU</button>

				</div>

			</div>

		</div>
	</div>
	<div id="create_bill_popup" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>CẬP NHẬT PHIẾU:</h4>
					<p>
						<select class="form-control">
							<option>Tất cả các phiếu</option>
							<option>Phiếu pickup hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
						</select>
					</p>
					<button class="btn btn-success" style="width: 100%">CHỌN PHIẾU</button>

				</div>

			</div>

		</div>
	</div>
	<div id="deploy_popup" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>TRIỂN KHAI:</h4>
					<p>
						<select class="form-control">
							<option>Tất cả các phiếu</option>
							<option>Phiếu pickup hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
						</select>
					</p>
					<button class="btn btn-success" style="width: 100%">CHỌN PHIẾU</button>

				</div>

			</div>

		</div>
	</div>
	<div id="receive_popup" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>NHẬN HÀNG:</h4>
					<p>
						<select class="form-control">
							<option>Tất cả các phiếu</option>
							<option>Phiếu pickup hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
						</select>
					</p>
					<button class="btn btn-success" style="width: 100%">CHỌN PHIẾU</button>

				</div>

			</div>

		</div>
	</div>

	<div id="money_receive_popup" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>NHẬN TIỀN:</h4>
					<p>
						<select class="form-control">
							<option>Tất cả các phiếu</option>
							<option>Phiếu pickup hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
							<option>Phiếu nhận hàng từ đối tác</option>
						</select>
					</p>
					<button class="btn btn-success" style="width: 100%">CHỌN PHIẾU</button>

				</div>

			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	var redirectRoute = function($this) {
		var url = $($this).parent().find('select[name="route"]').val();
		window.location.href = url;
		return false;
	}
</script>