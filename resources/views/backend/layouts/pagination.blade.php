@if ($paginator->hasPages())
<div class="m_datatable m-datatable m-datatable--default m-datatable--scroll m-datatable--loaded" id="m_datatable_latest_orders" style="position: static; zoom: 1;">
    <div class="m-datatable__pager m-datatable--paging-loaded clearfix">        
        <ul class="m-datatable__pager-nav">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--first m-datatable__pager-link--disabled" disabled="disabled">
                        <i class="la la-angle-double-left"></i>
                    </a>
                </li>
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--prev m-datatable__pager-link--disabled" disabled="disabled">
                        <i class="la la-angle-left"></i>
                    </a>
                </li>
            @else
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--first" href="{{ $paginator->url(1) }}">
                        <i class="la la-angle-double-left"></i>
                    </a>
                </li>
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--prev" href="{{ $paginator->previousPageUrl() }}">
                        <i class="la la-angle-left"></i>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li>
                        <a class="m-datatable__pager-link m-datatable__pager-link--more-next">
                            <i class="la la-ellipsis-h"></i>
                        </a>
                    </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li>
                                <a class="m-datatable__pager-link m-datatable__pager-link-number m-datatable__pager-link--active">{{ $page }}
                                </a>
                            </li>
                        @else
                            <li>
                                <a class="m-datatable__pager-link m-datatable__pager-link-number" href="{{ $url }}">
                                    {{ $page }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--next" href="{{ $paginator->nextPageUrl() }}">
                        <i class="la la-angle-right"></i>
                    </a>
                </li>
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--last" href="{{ $paginator->url($paginator->lastPage()) }}">
                        <i class="la la-angle-double-right"></i>
                    </a>
                </li>
            @else
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--next m-datatable__pager-link--disabled" disabled="disabled">
                        <i class="la la-angle-right"></i>
                    </a>
                </li>
                <li>
                    <a class="m-datatable__pager-link m-datatable__pager-link--last m-datatable__pager-link--disabled" disabled="disabled">
                        <i class="la la-angle-double-right"></i>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
@endif
