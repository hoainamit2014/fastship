</div>

	<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
		<i class="la la-arrow-up"></i>
	</div>
	<script src="{{ asset('public/fontend/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
	<script src="{{ asset('public/fontend/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
	<script src="{{ asset('public/fontend/assets/app/js/dashboard.js')}}" type="text/javascript"></script>
	<script src="{{ asset('public/fontend/assets/vendors/custom/wizard/wizard.js')}}" type="text/javascript"></script>

	<!--begin::Page Vendors -->
	<script src="{{ asset('public/fontend/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
	<!--end::Page Vendors -->

	<!--begin::Page Resources -->
	<script src="{{ asset('public/fontend/assets/demo/default/custom/crud/datatables/search-options/advanced-search.js')}}" type="text/javascript"></script>
	<script src="{{asset('public/backend/js/setting.js')}}"></script>
	<script src="{{asset('public/fontend/js/order.js')}}"></script>
	<script src="{{asset('public/fontend/assets/demo/default/custom/crud/forms/widgets/bootstrap-datetimepicker.js')}}"></script>
	<script src="{{asset('public/fontend/assets/demo/default/custom/crud/forms/widgets/select2.js')}}"></script>

	@yield('script')
</body>
</html>
