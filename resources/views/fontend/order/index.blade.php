@extends('fontend.layouts.order.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Quản lý đơn hàng') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<a href="{{ route('fs-order-add') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-plus"></i>
                        <span>{{ __('Tạo vận đơn') }}</span>
                    </span>
                </a>
                <a href="#" class="btn btn-success m-btn m-btn--icon" data-toggle="modal" data-target="#m_modal_4">
                    <span>
                        <i class="la la-cloud-upload"></i>
                        <span>{{ __('Tạo nhiều vận đơn (Excel)') }}</span>
                    </span>
                </a>
			</div>
		</div>
		<div class="m-portlet__body">
			<!--begin: Search Form -->

			<div>
				<input type="hidden" value="{{ url('') }}" id="js_core_path">
				<div class="row m--margin-bottom-20">
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Mã đơn hàng:</label>
						<input type="text" class="form-control" name="order_code">
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Tên sản phẩm:</label>
						<input type="text" class="form-control" name="product_name">
					</div>
					<div class="col-lg-6 m--margin-bottom-10-tablet-and-mobile">
						<label>Ngày tạo:</label>
						<div class="input-daterange input-group" id="js_order_date_picker">
							<input type="text" class="form-control"  name="from_date" placeholder="Từ"/>
							<div class="input-group-append">
								<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
							</div>
							<input type="text" class="form-control"  name="to_date" placeholder="đến"/>
						</div>
					</div>
				</div>

				<div class="row m--margin-bottom-20">
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Tỉnh/thành phố:</label>
						<select name="province_id" id="js_select_province" class="form-control">
							<option value=""></option>
							@foreach ($aProvinces as $aValue)
							<option value="{{ $aValue->id }}">{{ $aValue->text }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Quận/huyện:</label>
						<select name="district_id" class="form-control" id="js_select_district">
						</select>
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Phường/xã:</label>
						<select name="ward_id" class="form-control" id="js_select_ward">
						</select>
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Trạng thái:</label>
						<select class="form-control" name="status">
							<option value="">Chọn:</option>
							@foreach ($aStatus as $aValue)
							<option value="{{ $aValue->id }}">{{ $aValue->text }}</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<button class="btn btn-primary m-btn m-btn--icon" type="submit" id="m_search">
							<span>
								<i class="la la-search"></i>
								<span>Tìm kiếm</span>
							</span>
						</button>
						&nbsp;&nbsp;
						<a class="btn btn-danger m-btn m-btn--icon" href="{{ route('fs-order-index') }}">
							<span>
								<i class="la la-close"></i>
								<span>Reset</span>
							</span>
						</a>
					</div>
				</div>
			</div><br>

			<div class="table-responsive">
				<table class="table table-bordered table-hover" id="js_order_listing" style="font-size: 12px">
					<thead>
						<tr style="white-space: nowrap;" class="text-center">
							<th></th>
							<th>STT</th>
							<th>Thông tin đơn hàng &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Thông tin người nhận &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Thông tin sản phẩm &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>Thông tin khác &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tạo đơn hàng bằng file (Excel)</h5>
				<a  class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -2px">
					<span aria-hidden="true">×</span>
				</a>
			</div>
			<div class="modal-body">
				<form id="js_import_order" method="post" action="{{ route('fs-order-import') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group m-form__group">
						<label for="exampleInputEmail1">Chọn tập tin</label>
						<div></div>
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
							<label class="custom-file-label">Choose file</label>
						</div>
					</div>
					<div class="form-group m-form__group">
						<button type="submit" class="btn btn-outline-success btn-sm m-btn m-btn--custom">
							<i class="la la-upload"></i>Upload
						</button>
					</div>
					
				</form>
			</div>
			<div class="modal-footer text-left">
				<a href="{{ asset('public/fontend/donhang.xlsx') }}"><i class="la la-download"></i>Tải file mẫu</a>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$( document ).ready(function() {
		var count = 1;
		var table = $('#js_order_listing').DataTable({
			bProcessing: true,
			language: {
                "sProcessing":   "Đang xử lý...",
                "sLengthMenu":   "Xem _MENU_ mục",
                "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
                "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
                "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            },
			ajax: {
				url: "{{ route('fs-order-search') }}",
				type: "GET",
				data: function(data){
					console.log(data);
					data.customsearch = {
						'order_code' : $('input[name="order_code"]').val(),
						'product_name' : $('input[name="product_name"]').val(),
						'status' : $('select[name="status"]').val(),
						'province_id' : $('select[name="province_id"]').val(),
						'district_id' : $('select[name="district_id"]').val(),
						'ward_id' : $('select[name="ward_id"]').val(),
						'from_date' : $('input[name="from_date"]').val(),
						'to_date' : $('input[name="to_date"]').val(),
					};
				}
			},
			bPaginate:true,
			serverSide: true,
			sPaginationType:"full_numbers",
			searching: false,
			lengthMenu: [20, 50, 100, 150],
			pageLength: 20,
			aoColumns: [
			{ mData: null , mRender: function (data, type, full) {
					return '<div class="dropdown">'+
								'<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">'+
									'<i class="la la-ellipsis-h"></i>'+
								'</a>'+
								'<div class="dropdown-menu dropdown-menu-right">'+
									'<a href="{{url('fs/cpanel/order/show/')}}/?order_code='+full.order_code+'" class="dropdown-item">'+
										'<i class="la la-eye"></i> Xem'+
									'</a>'+
									'<a class="dropdown-item" href="{{url('fs/cpanel/order/edit/')}}/'+full.order_id+'">'+
										'<i class="la la-edit"></i> Sửa'+
									'</a>'+
									'<a href="{{url('fs/cpanel/order/destroy/')}}/'+full.order_id+'" class="dropdown-item">'+
										'<i class="la la-close"></i> Xóa'+
									'</a>'+
									'<a href="{{url('fs/cpanel/order/print/')}}/'+full.order_id+'" class="dropdown-item" target="_blank">'+
										'<i class="la la-print"></i> In'+
									'</a>'+
								'</div>'+
							'</div>';
				}
			},
			{ mData: 'STT' },
			{ mData: null, mRender: function(data, type, full){
					return `
						<div>
							<div><b>Mã hệ thống: </b>${full.order_code}</div>
							<div><b>Mã đối tác: </b>${full.customer_order_code ? full.customer_order_code : ''}</div>
							<div><b>Trạng thái: </b>${full.status_name}</div>
							<div><b>Dịch vụ: </b>${full.service_name}</div>
							<div><b>Chiều đơn hàng: </b>${full.flow_order}</div>
							<div><b>Loại đơn hàng: </b>${full.order_type_name}</div>
							<div><b>Ngày tạo: </b>${full.created_at_format}</div>
							<div><b>Ngày giao thành công: </b>${full.delivery_success_date_format}</div>
						</div>
					`;
				}
			},
			{ mData: null, mRender: function(data, type, full){
					return `
						<div>
							<div><b>Người nhận: </b>${full.receiver_name}</div>
							<div><b>Số điện thoại: </b>${full.phone}</div>
							<div><b>Địa chỉ: </b>${full.address}</div>
						</div>
					`;
				}
			},
			{ mData: null, mRender: function(data, type, full){
					return `
						<div>
							<div><b>Sản phẩm: </b>${full.product_name}</div>
							<div><b>Trọng lượng: </b>${full.weight}</div>
							<div><b>Dài: </b>${full.length}</div>
							<div><b>Rộng: </b>${full.width}</div>
							<div><b>Cao: </b>${full.height}</div>
						</div>
					`;
				}
			},
			{ mData: null, mRender: function(data, type, full){
					return `
						<div>
							<div><b>Ghi chú: </b>${full.note ? full.note : ''}</div>
							<div><b>Tiền thu hộ: </b>${full.cod}</div>
							<div><b>Phí vận chuyển: </b>${full.transport_fee}</div>
							<div><b>Trả phí vận chuyển: </b>${full.receiver_purchase_fee}</div>
						</div>
					`;
				}
			},
			],
			columnDefs: [ {
                    targets: [0, 1, 2, 3, 4, 5], // column or columns numbers
                    orderable: false,  // set orderable for selected columns
                }
            ]
		});
		$("#m_search").on("click", function(t) {
			table.ajax.reload();
			return false;
		});
	});
</script>
@endsection