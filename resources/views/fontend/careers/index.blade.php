@extends('fontend.app')
@section('title', 'Page Title')
@section('content')
@include('fontend.global.options')
@include('fontend.global.background')

<div class="container container-padding-20">
	<section class="careers-culture" style="padding: 30px 0;">
        <div class="section-title text-center">
            <h2 class="title-services-other title-2"> HÃY ĐẾN VÀ LÀM VIỆC CÙNG CHÚNG TÔI! </h2>
            <h4 class="subtitle-services-other subtitle-2">
                Hãy đăng ký ngay để trở thành một phần của đại gia đình VNCPOST
            </h4>
            <div class="spacer-30"> </div>
        </div>

        <div class="row culture">

            <div class="col-md-12">
                <h2 class="culture-title"> VĂN HÓA VNCPOST </h2>
                <h4 class="culture-subtitle">
                    Cống hiến, năng động, dân chủ, tư duy sáng tạo và ổn định
                </h4>
                <p>
                    Là một trong những công ty cung cấp dịch vụ giao hàng, hậu cần thương mại điện tử
                    hàng đầu Việt Nam hiện nay, chúng tôi luôn chào đón những thành viên muốn đóng góp
                    công sức, tài năng cho xã hội để từ đó tạo giá trị cho bản thân mỗi thành viên nói
                    riêng và cho công ty nói chung, giúp VNCPOST ngày càng trở nên lớn mạnh hơn và đem
                    lại nhiều lợi ích cho xã hội hơn.
                </p>

                <p>
                    Với phương châm <b>tận tâm, tận lực, tận cùng</b> VNCPOST hiện đang giúp hơn 1200 
                    cá nhân phát huy tối đa năng lực của mình, tạo môi trường tốt
                    nhất giúp ổn định cuộc sống cũng như định hình tương lai sự nghiệp
                    lâu dài cho mỗi thành viên - điều mà rất ít công ty có thể làm được.
                </p>

            </div>
        </div>
    </section>
    <h2 class="positions-title text-center"> Các vị trí đang tuyển </h2>
	<div class="row">
		@foreach ($aCareers as $aCareer)
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<div class="thumbnail">
				<div class="caption">
					<h3><a href="{{ url('tuyen-dung/' . $aCareer->career_id . '/' . $aCareer->alias) }}">{{ $aCareer->title }}</a></h3>
					<p class="format-text">
						{{ formatText($aCareer->content) }}
					</p>
					<p>
						<a href="{{ url('tuyen-dung/' . $aCareer->career_id . '/' . $aCareer->alias) }}" class="btn btn-primary">Xem và ứng tuyển</a>
					</p>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="text-center">
		{{ $aCareers->links() }}
	</div>
</div>

@endsection