@extends('fontend.app')
@section('title', 'Page Title')
@section('content')
@include('fontend.global.options')
@include('fontend.global.background')

<div class="container container-padding-20">
	@if (!empty($aCareer))
	<div class="news-title">
		<h2 class="text-center">{{ $aCareer->title }}</h2>
	</div>
	<div class="news-body">
		<p>
			{{ $aCareer->content }}
		</p>
		<p>
			<i>{{ $aCareer->description }}</i>
		</p>
	</div>
	<div class="news-footer">
		Ngày hết hạn: {{ $aCareer->expire_date }}
	</div>
	<div class="news-footer text-center">
		<button type="button" class="btn btn-lg btn-primary">Ứng tuyển vị trí này</button>
	</div>
	@else
	<div class="alert alert-danger">
		<strong>Không tìm thấy mục tuyển dụng</strong>
	</div>
	@endif
</div>

@endsection