@extends('fontend.layouts.order.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    @if ($errors->any())
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-exclamation m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ !isset($aWarehouse) ? __('Thêm kho hàng') : __('Sửa kho hàng')}}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <a href="{{ route('warehouse-index') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-list"></i>
                        <span>{{ __('Danh sách') }}</span>
                    </span>
                </a>
            </div>
        </div>
        <div class="m-portlet__body">
            @if(isset($aWarehouse))
            <form action="{{ url('/fs/warehouse/update/'.$aWarehouse->id) }}" method="post">
            @else
            <form action="{{ route('warehouse-store') }}" method="post">
            @endif
                <div class="row">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ url('') }}" id="js_core_path">
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Họ và tên') }}</label>
                            <input type="text" name="full_name" class="form-control m-input" id="js_receiver_name" value="{{ old('full_name') }}@if(isset($aWarehouse)){{ $aWarehouse->name }}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Số điện thoại') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                <input type="text" name="phone" class="form-control" id="js_phone" value="{{ old('phone') }}@if(isset($aWarehouse)){{ $aWarehouse->phone }}@endif">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Địa chỉ') }}</label>
                            <input type="text" name="address" class="form-control m-input" id="js_address" value="{{ old('address') }}@if(isset($aWarehouse)){{ $aWarehouse->address }}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Tỉnh / Thành phố') }}</label>
                            <select name="province_id" id="js_select_province" class="form-control m-input">
                                @if (isset($aProvinces))
                                <option value="">Chọn:</option>
                                @foreach ($aProvinces as $aProvince)
                                <option value="{{ $aProvince->id }}" @if(isset($aWarehouse) && $aProvince->id == $aWarehouse->province_id)selected="selected"@endif>{{ $aProvince->text }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Quận / huyện') }}</label>
                            <select name="district_id" class="form-control m-input" id="js_select_district">
                                @if (isset($aDistricts))
                                @foreach ($aDistricts as $aDistrict)
                                <option value="{{ $aDistrict->id }}" @if($aDistrict->id == $aWarehouse->district_id)selected="selected"@endif>{{ $aDistrict->text }}</option>
                                @endforeach
                                @else
                                <option value="">Chọn:</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group m-form__group">
                            <label>*{{ __('Phường / Xã') }}</label>
                            <select name="ward_id" class="form-control m-input" id="js_select_ward">
                                @if (isset($aWards))
                                @foreach ($aWards as $aWard)
                                <option value="{{ $aWard->id }}" @if($aWard->id == $aWarehouse->ward_id)selected="selected"@endif>{{ $aWard->text }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary m-btn m-btn--icon" type="submit">
                            <span>
                                <i class="la la-save"></i>
                                <span>{{ __('Lưu') }}</span>
                            </span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection