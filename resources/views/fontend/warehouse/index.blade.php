@extends('fontend.layouts.order.app')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						{{ __('Quản lý kho hàng') }}
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<a href="{{ route('warehouse-add') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="la la-plus"></i>
                        <span>{{ __('Tạo kho hàng') }}</span>
                    </span>
                </a>
			</div>
		</div>
		<div class="m-portlet__body">
			@if (!empty($aWarehouses))
			<div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="position: static; zoom: 1;">
				<table class="m-datatable__table table table-bordered" style="overflow-x: auto;">
					<thead class="m-datatable__head">
						<tr class="m-datatable__row" style="left: 0px;">
							<th class="m-datatable__cell">
								<span style="width: 50px;"></span>
							</th>
							<th class="m-datatable__cell">
								<span style="width: 100px;">Họ tên</span>
							</th>
							<th class="m-datatable__cell">
								<span style="width: 100px;">SĐT</span>
							</th>
							<th class="m-datatable__cell">
								<span style="width: 100px;">Địa chỉ</span>
							</th>
							<th class="m-datatable__cell">
								<span style="width: 100px;">Tỉnh / thành phố</span>
							</th>
							<th class="m-datatable__cell">
								<span style="width: 100px;">Quận / huyện</span>
							</th>
							<th class="m-datatable__cell">
								<span style="width: 100px;">Phường / xã</span>
							</th>
						</tr>
					</thead>
					<tbody class="m-datatable__body">
						@foreach ($aWarehouses as $iKey => $aWarehouse)
						<tr class="m-datatable__row">
							<td class="m-datatable__cell">
								<span style="overflow: visible; position: relative; width: 50px;">						
									<div class="dropdown">							
										<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="false">
											<i class="la la-ellipsis-h"></i> 
										</a>							
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="{{ url('fs/warehouse/edit', $aWarehouse->id) }}">
												<i class="la la-edit"></i> Sửa
											</a>
											<a href="{{ url('fs/warehouse/destroy', $aWarehouse->id) }}" class="dropdown-item">
												<i class="la la-close"></i> Xóa
											</a>
										</div>						
									</div>						
								</span>
							</td>
							<td class="m-datatable__cell"><span style="width: 100px;">{{ $aWarehouse->name }}</span></td>
							<td class="m-datatable__cell"><span style="width: 100px;">{{ $aWarehouse->phone }}</span></td>
							<td class="m-datatable__cell"><span style="width: 100px;">{{ $aWarehouse->address }}</span></td>
							<td class="m-datatable__cell"><span style="width: 100px;">{{ $aWarehouse->province_name }}</span></td>
							<td class="m-datatable__cell"><span style="width: 100px;">{{ $aWarehouse->district_name }}</span></td>
							<td class="m-datatable__cell"><span style="width: 100px;">{{ $aWarehouse->ward_name }}</span></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@else
			<div class="alert alert-danger">
				Không tìm thấy kho hàng nào
			</div>
			@endif
		</div>
	</div>
</div>
@endsection