@include('fontend.layouts.order.header')

<div class="m-content" style="width: 100%; padding: 30px 0px;">
	<div class="col-md-12">
		<a href="{{ route('ordre.index') }}" class="btn btn-success redirect-order m-btn m-btn--custom m-btn--icon">
			<span>
				<i class="la la-align-right"></i>
				<span>Danh sách vận đơn</span>&nbsp;&nbsp;
			</span>
		</a>
		<!--Begin::Main Portlet-->
		<div class="m-portlet m-portlet--full-height">
			
			<!--begin: Portlet Head-->
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Tạo vận đơn
						</h3>
					</div>
				</div>
			</div>
			<!--end: Portlet Head-->

			<!--begin: Portlet Body-->
			<div class="m-portlet__body m-portlet__body--no-padding">

				<!--begin: Form Wizard-->
				<div class="m-wizard m-wizard--3 m-wizard--success m-wizard--step-first" id="m_wizard">

					<!--begin: Message container -->
					<div class="m-portlet__padding-x">
						<!-- Here you can put a message or alert -->
					</div>
					<!--end: Message container -->

					<div class="row m-row--no-padding">
						<div class="col-xl-3 col-lg-12">
							<!--begin: Form Wizard Head -->
							<div class="m-wizard__head">

								<!--begin: Form Wizard Progress -->  		
								<div class="m-wizard__progress">	
									<div class="progress">		 
										<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 25%;"></div>						 	
									</div>			 
								</div> 
								<!--end: Form Wizard Progress --> 

								<!--begin: Form Wizard Nav -->
								<div class="m-wizard__nav">
									<div class="m-wizard__steps">
										<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
											<div class="m-wizard__step-info">
												<a href="#" class="m-wizard__step-number">							 
													<span><span>1</span></span>							 
												</a>
												<div class="m-wizard__step-line">
													<span></span>
												</div>
												<div class="m-wizard__step-label">
													Địa chỉ người nhận									
												</div>
											</div>
										</div>
										<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
											<div class="m-wizard__step-info">
												<a href="#" class="m-wizard__step-number">							 
													<span><span>2</span></span>							 
												</a>
												<div class="m-wizard__step-line">
													<span></span>
												</div>
												<div class="m-wizard__step-label">
													Kho và hàng hóa
												</div>
											</div>
										</div>
										<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
											<div class="m-wizard__step-info">
												<a href="#" class="m-wizard__step-number">							 
													<span><span>3</span></span>							 
												</a>
												<div class="m-wizard__step-line">
													<span></span>
												</div>
												<div class="m-wizard__step-label">
													Hoàn thành
												</div>
											</div>
										</div>
										
									</div>
								</div>	
								<!--end: Form Wizard Nav -->
							</div>
							<!--end: Form Wizard Head -->	
						</div>
						<div class="col-xl-9 col-lg-12">
							<!--begin: Form Wizard Form-->
							<div class="m-wizard__form">
							<!--
								1) Use m-form--label-align-left class to alight the form input lables to the right
								2) Use m-form--state class to highlight input control borders on form validation
							-->
							<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" novalidate="novalidate" action="{{ route('order.store') }}" method="post">
								<!--begin: Form Body -->
								<div class="m-portlet__body m-portlet__body--no-padding">
									<!--begin: Form Wizard Step 1-->
									<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
										<div class="m-form__section m-form__section--first">
											<div class="m-form__heading">
												<h3 class="m-form__heading-title">Thông tin người nhận</h3>
											</div>
											<div class="form-group m-form__group row">
												<label class="col-xl-3 col-lg-3 col-form-label">* Họ và tên</label>
												<div class="col-xl-9 col-lg-9">
													<input type="text" name="name" class="form-control m-input" placeholder="" value="Nick Stone">
													<span class="m-form__help">Vui lòng nhập họ và tên đầy đủ</span>
												</div>
											</div>
											
											<div class="form-group m-form__group row">
												<label class="col-xl-3 col-lg-3 col-form-label">* Số điện thoại</label>
												<div class="col-xl-9 col-lg-9">
													<div class="input-group">
														<div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
														<input type="text" name="phone" class="form-control" placeholder="Nhập số điện thoại" value="">
													</div>
												</div>
											</div>
										</div>
										<div class="m-separator m-separator--dashed m-separator--lg"></div>
										<div class="m-form__section">
											<div class="m-form__heading">
												<h3 class="m-form__heading-title">
													Địa chỉ người nhận 
													
												</h3>
											</div>
											<div class="form-group m-form__group row">
												<label class="col-xl-3 col-lg-3 col-form-label">* Địa chỉ:</label>
												<div class="col-xl-9 col-lg-9">
													<input type="text" name="address1" class="form-control m-input" placeholder="Nhập địa chỉ người nhận" value=" k djkash kdha h">
												</div>
											</div>
											<div class="form-group m-form__group row">
												<label class="col-xl-3 col-lg-3 col-form-label">* Tỉnh / Thành phố:</label>
												<div class="col-xl-9 col-lg-9">
													<select name="country" class="form-control m-input">
														<option value="">Select</option>
														
														<option value="ZW" selected="selected">Zimbabwe</option>
													</select>
												</div>
											</div>
											<div class="form-group m-form__group row">
												<label class="col-xl-3 col-lg-3 col-form-label">* Quận / huyện:</label>
												<div class="col-xl-9 col-lg-9">
													<select name="country" class="form-control m-input">
														<option value="">Select</option>
														<option value="ZW" selected="selected">Zimbabwe</option>
													</select>
												</div>
											</div>
											<div class="form-group m-form__group row">
												<label class="col-xl-3 col-lg-3 col-form-label">* Phường / Xã:</label>
												<div class="col-xl-9 col-lg-9">
													<select name="country" class="form-control m-input">
														<option value="">Select</option>
														<option value="ZW" selected="selected">Zimbabwe</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<!--end: Form Wizard Step 1-->
									<!--begin: Form Wizard Step 2-->
									<div class="m-wizard__form-step" id="m_wizard_form_step_2">
										<div class="m-form__section m-form__section--first">
											<div class="m-form__heading">
												<h3 class="m-form__heading-title">Thông tin hàng hóa</h3>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-12">
													<label class="form-control-label">* Tên sản phẩm:</label>
													<input type="text" name="" class="form-control" placeholder="Nhập tên sản phẩm" value="">
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-12">
													<label class="form-control-label">* Trọng lượng:</label>
													<input type="text" name="" class="form-control m-input" placeholder="Nhập trọng lượng" value="0">
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-12">
													<label class="form-control-label">* Thu hộ (COD):</label>
													<input type="text" name="" class="form-control m-input" placeholder="Số tiền thu hộ COD" value="">
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-12">
													<label class="form-control-label">* Ghi chú:</label>
													<textarea class="form-control m-input"  placeholder="Describe yourself here...">
													</textarea>
												</div>
											</div>
										</div>
										<div class="m-separator m-separator--dashed m-separator--lg"></div>
										<div class="m-form__section">
											<div class="m-form__heading">
												<h3 class="m-form__heading-title">Cước phí</h3>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-12">
													<label class="form-control-label">* Phí giao hàng:</label>
													<input type="text" name="" class="form-control m-input" placeholder="Số tiền thu hộ COD" value="">
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-12">
													<label class="form-control-label">* Thời gian:</label>
													<input type="text" name="" class="form-control m-input" placeholder="Số tiền thu hộ COD" value="">
												</div>
											</div>
											<div class="form-group m-form__group row">
												<div class="col-lg-12">
													<label class="form-control-label">* Gói cước áp dụng:</label>
													<input type="text" name="" class="form-control m-input" placeholder="Số tiền thu hộ COD" value="">
												</div>
											</div>
											<div class="m-form__group form-group">
												<div class="m-checkbox-list">
													<label class="m-checkbox">
														<input type="checkbox"> Người nhận thanh toán cước
														<span></span>
													</label>
													<span class="m-form__help">Mặc định người gửi thanh toán cước</span>
												</div>
											</div>
										</div>
									</div>
									<!--end: Form Wizard Step 2-->
									<!--begin: Form Wizard Step 3-->
									<div class="m-wizard__form-step" id="m_wizard_form_step_3">
										<div class="col-xl-12">
											<!--begin::Section-->                                            
											<div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
												<!--begin::Item-->              
												<div class="m-accordion__item active">
													<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
														<span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span>
														<span class="m-accordion__item-title">1. Thông tin người nhận</span>
														<span class="m-accordion__item-mode"></span>     
													</div>
													<div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1" style="">
														<!--begin::Content--> 	  
														<div class="tab-content  m--padding-30">
															<div class="m-form__section m-form__section--first">
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Họ tên:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">Nguyễn văn a</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Số điện thoại:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">09090909</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Địa chỉ:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">Hồ Chí Minh</span>
																	</div>
																</div>
															</div>
														</div>
														<!--end::Content--> 
													</div>
												</div>
												<!--end::Item--> 
												<!--begin::Item--> 
												<div class="m-accordion__item">
													<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_3_head" data-toggle="collapse" href="#m_accordion_1_item_3_body" aria-expanded="    false">
														<span class="m-accordion__item-icon"><i class="fa  flaticon-alert-2"></i></span>
														<span class="m-accordion__item-title">2. Thông tin sản phẩm</span>
														<span class="m-accordion__item-mode"></span>     
													</div>
													<div class="m-accordion__item-body collapse" id="m_accordion_1_item_3_body" role="tabpanel" aria-labelledby="m_accordion_1_item_3_head" data-parent="#m_accordion_1">
														<div class="tab-content  m--padding-30">
															<div class="m-form__section m-form__section--first">
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Tên sản phẩm:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">Giày dép</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Trọng lượng:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">4589</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Thu hộ:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">10</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Ghi chú:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">Ghi chú</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="m-accordion__item">
													<div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_4_head" data-toggle="collapse" href="#m_accordion_1_item_4_body" aria-expanded="    false">
														<span class="m-accordion__item-icon"><i class="fa  flaticon-alert-2"></i></span>
														<span class="m-accordion__item-title">3. Cước phí và thời gian</span>
														<span class="m-accordion__item-mode"></span>     
													</div>
													<div class="m-accordion__item-body collapse" id="m_accordion_1_item_4_body" role="tabpanel" aria-labelledby="m_accordion_1_item_3_head" data-parent="#m_accordion_1">
														<div class="tab-content  m--padding-30">
															<div class="m-form__section m-form__section--first">
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Phí giao hàng:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">10</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Thời gian:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">12/12/2019</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Gói cước áp dụng:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">10</span>
																	</div>
																</div>
																<div class="form-group m-form__group m-form__group--sm row">
																	<label class="col-xl-4 col-lg-4 col-form-label">Người nhận thanh toán cước:</label>
																	<div class="col-xl-8 col-lg-8">
																		<span class="m-form__control-static">Nguyễn văn a</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--end::Item-->
											</div>
											<!--end::Section-->	                
											<div class="m-separator m-separator--dashed m-separator--lg"></div>
											
										</div>
									</div>
									<!--end: Form Wizard Step 3-->
								
								</div>
								<!--end: Form Body -->
								<!--begin: Form Actions -->
								<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
									<div class="m-form__actions">
										<div class="row">
											<div class="col-lg-6 m--align-left">
												<a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
													<span>
														<i class="la la-arrow-left"></i>&nbsp;&nbsp;
														<span>Back</span>
													</span>
												</a>
											</div>
											<div class="col-lg-6 m--align-right">
												<a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
													<span>
														<i class="la la-check"></i>&nbsp;&nbsp;
														<span>Submit</span>
													</span>
												</a>
												<a href="#" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
													<span>
														<span>Save &amp; Continue</span>&nbsp;&nbsp;
														<i class="la la-arrow-right"></i>
													</span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!--end: Form Actions -->
							</form>
						</div>
						<!--end: Form Wizard Form-->

					</div>
				</div>
			</div>
			<!--end: Form Wizard-->

		</div>
	</div>
	<!--end: Portlet Body-->
</div>
<!--End::Main Portlet--> 


</div>
@include('fontend.layouts.order.footer')
