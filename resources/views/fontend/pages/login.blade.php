<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Radiant Admin Premium Bootstrap Admin Dashboard Template</title>
  <link rel="stylesheet" href="{{ asset('public/fontend/login.css') }}">
<style type="text/css">
</style>
<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
        <div class="row w-100">

          <div class="col-lg-3 mx-auto">
            <h3 class="text-center">ĐĂNG NHẬP</h3></br>
            @if($errors->has('errorlogin'))
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {{$errors->first('errorlogin')}}
            </div>
          @endif
            <div class="auto-form-wrapper">
              <form action="{{ route('account-login-post') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                  <label class="label">Tên đăng nhập:</label>
                  <input type="text" value="{{ old('username') }}" name="username" class="form-control" placeholder="Tên tài khoản">
                </div>
                <div class="form-group">
                  <label class="label">Mật khẩu</label>
                  <input type="password" name="password" class="form-control" placeholder="Mật khẩu">
                </div>
                <div class="form-group">
                  <button class="btn btn-primary submit-btn btn-block">Đăng nhập</button>
                </div>
                <div class="form-group d-flex justify-content-between">
                  <div class="form-check form-check-flat mt-0" style="font-size:0.8rem">
                    <input type="checkbox" name="remember" class="form-check-input">
                      Nhớ mật khẩu
                  </div>
                  <a href="#" class="text-small forgot-password text-black">Quên mật khẩu</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
