@include('fontend.layouts.order.header')
<div class="m-content" style="width: 100%">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						ĐƠN HÀNG
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
						<a href="{{ route('') }}" class="btn btn-success m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
							<span>
								<i class="la la-plus"></i>
								<span>Tạo vận đơn</span>
							</span>
						</a>
					</li>
					<li class="m-portlet__nav-item">
						<a href="#" class="btn btn-success m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air" data-toggle="modal" data-target="#m_modal_4">
							<span>
								<i class="la la-cloud-upload"></i>
								<span>Tạo nhiều vận đơn (Excel)</span>
							</span>
						</a>
					</li>

				</ul>
			</div>
		</div>
		<div class="m-portlet__body">
			<!--begin: Search Form -->

			<form class="m-form m-form--fit m--margin-bottom-20">
				<div class="row m--margin-bottom-20">
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>RecordID:</label>
						<input type="text" class="form-control m-input" placeholder="E.g: 4590" data-col-index="0">
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>OrderID:</label>
						<input type="text" class="form-control m-input" placeholder="E.g: 37000-300" data-col-index="1">
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Country:</label>
						<select class="form-control m-input" data-col-index="2">
							<option value="">Select</option>
						</select>
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Agent:</label>
						<input type="text" class="form-control m-input" placeholder="Agent ID or name" data-col-index="4">
					</div>
				</div>

				<div class="row m--margin-bottom-20">
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Ship Date:</label>
						<div class="input-daterange input-group" id="m_datepicker">
							<input type="text" class="form-control m-input" name="start" placeholder="From" data-col-index="5"/>
							<div class="input-group-append">
								<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
							</div>
							<input type="text" class="form-control m-input" name="end" placeholder="To" data-col-index="5"/>
						</div>
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Status:</label>
						<select class="form-control m-input" data-col-index="6">
							<option value="">Select</option>
						</select>
					</div>
					<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
						<label>Type:</label>
						<select class="form-control m-input" data-col-index="7">
							<option value="">Select</option>
						</select>
					</div>
				</div>

				<div class="m-separator m-separator--md m-separator--dashed"></div>

				<div class="row">
					<div class="col-lg-12">
						<button class="btn btn-secondary m-btn m-btn--icon" id="m_search">
							<span>
								<i class="la la-search"></i>
								<span>Tìm kiếm</span>
							</span>
						</button>
						&nbsp;&nbsp;
						<button class="btn btn-secondary m-btn m-btn--icon" id="m_reset">
							<span>
								<i class="la la-close"></i>
								<span>Reset</span>
							</span>
						</button>
					</div>
				</div>
			</form>

			<!--begin: Datatable -->
			<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
				<thead>
					<tr>
						<th><strong>Thông tin đơn hàng</strong></th>
						<th><strong>Người nhận</strong></th>
						<th><strong>Sản phẩm & Dịch vụ </strong></th>
						<th><strong>Phí</strong></th>
						<th><strong>Tiền thu hộ</strong></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th><strong>Thông tin đơn hàng</strong></th>
						<th><strong>Người nhận</strong></th>
						<th><strong>Sản phẩm & Dịch vụ </strong></th>
						<th><strong>Phí</strong></th>
						<th><strong>Tiền thu hộ</strong></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tạo đơn hàng bằng file (Excel)</h5>
				<a  class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -2px">
					<span aria-hidden="true">×</span>
				</a>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="recipient-name" class="form-control-label">Chọn kho hàng:</label>
						<select class="form-control m-input m-input--square" id="exampleSelect1">
							<option>Chọn kho hàng</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</select>
					</div>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Tên file</th>
								<th>Dung lượng file</th>
								<th>Xóa</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>donhang.xlsx</td>
								<td>73.81 KB</td>
								<td><a href=""><i class="la	la-close"></i></a></td>
							</tr>
						</tbody>
					</table>
					<div class="form-group m-form__group">
						<label for="exampleInputEmail1">Chọn tập tin</label>
						<div></div>
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="customFile">
							<label class="custom-file-label" for="customFile">Choose file</label>
						</div>
					</div>
					<div class="form-group m-form__group">
						<button class="btn btn-outline-success btn-sm m-btn m-btn--custom"><i class="la la-upload"></i>Upload</button>
					</div>
					
				</form>
			</div>
			<div class="modal-footer text-left">
				<a href="#"><i class="la la-download"></i>Tải file mẫu</a>
			</div>
		</div>
	</div>
</div>
@include('fontend.layouts.order.footer')