@extends('fontend.app')
@section('title', 'Page Title')
@section('content')
@include('fontend.global.options')
<div class="entry-content">
	{!! isset($data['content'][0]['post_content']) ? ($data['content'][0]['post_content']) : '' !!}
</div>
@endsection
