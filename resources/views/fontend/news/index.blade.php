@extends('fontend.app')
@section('title', 'Page Title')
@section('content')
@include('fontend.global.options')
@include('fontend.global.background')

<div class="container container-padding-20">
	<div class="row">
		@foreach ($aNews as $aNew)
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<div class="thumbnail">
				<div class="caption">
					<h3><a href="{{ url('tin-tuc/' . $aNew->new_id . '/' . $aNew->alias) }}">{{ $aNew->title }}</a></h3>
					<p class="format-text">
						{{ formatText($aNew->content) }}
					</p>
					<p>
						<a href="{{ url('tin-tuc/' . $aNew->new_id . '/' . $aNew->alias) }}" class="btn btn-primary">Xem thêm</a>
					</p>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="text-center">
		{{ $aNews->links() }}
	</div>
</div>

@endsection