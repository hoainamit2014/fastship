@extends('fontend.app')
@section('title', 'Page Title')
@section('content')
@include('fontend.global.options')
@include('fontend.global.background')

<div class="container container-padding-20">
	@if (!empty($aNew))
	<div class="news-title">
		<h2 class="text-center">{{ $aNew->title }}</h2>
	</div>
	<div class="news-body">
		<p>
			{{ $aNew->content }}
		</p>
		<p>
			<i>{{ $aNew->description }}</i>
		</p>
	</div>
	<div class="news-footer">
		Ngày tạo: {{ date("d-m-Y h:i:s", strtotime($aNew->created_at)) }}
	</div>
	@else
	<div class="alert alert-danger">
		<strong>Không tìm thấy mục tin tức</strong>
	</div>
	@endif
</div>

@endsection