<html>
<head>
	<title>App Name - @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/page.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/fontawesome/css/font-awesome.min.css') }}">
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/fontend/bootstrap/js/bootstrap.min.js') }}"></script>
	<!-- CAROUSEL -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/fontend/carousel/assets/owl.carousel.min.css') }}">
	<script type="text/javascript" src="{{ asset('public/fontend/carousel/owl.carousel.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/fontend/js/main.js') }}"></script>

</head>
<body>
	<header>
		<div class="container header-top">
			<div class="col-sm-3">
				<a href="{{ route('index') }}">
					<img src="https://vncpost.com/images/logo.png" class="img-responsive">
				</a>
			</div>
			<div class="col-sm-9">
				<div class="input-group search-input-group">
					<input type="text" class="form-control input-search" placeholder="Nhập mã đơn hàng để tra cứu lịch sử ..." >
				</div>
			</div>
		</div>
		<div class="main-menu">
			<div class="container-fluid">
				<nav class="navbar navbar-default">
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="collapse navbar-collapse" id="navbar-collapse-1">

								@if(!empty($data['header_menu']))
									<?php showCategories($data['header_menu'], 55, 0);?>
								@endif

							<div class="navbar-text navbar-right create_order"><a href="{{ route('fs-order-index') }}" class="navbar-link">ĐĂNG ĐƠN HÀNG</a></div>
						</div>
					</div>
				</nav>

			</div>
		</div>
	</header>