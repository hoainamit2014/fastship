<!DOCTYPE html>
<html lang="en" >
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8" />
  <title>
    Metronic | Dashboard
  </title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="description" content="Latest updates and statistic charts">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
  <link href="{{ asset('public/fontend/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/fontend/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
  <link rel="shortcut icon" href="{{ asset('public/fontend/assets/demo/default/media/img/logo/favicon.ico') }}" />
  <link href="{{ asset('public/fontend/assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />

  <link href="{{asset('public/backend/css/styles.css') }}" rel="stylesheet">
  <link href="{{asset('public/backend/css/main.css') }}" rel="stylesheet">
  <link href="{{asset('public/backend/css/setting.css') }}" rel="stylesheet">
  <script src="{{asset('public/backend/js/main.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
  <!-- begin:: Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    <header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
      <div class="m-container m-container--fluid m-container--full-height">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <!-- BEGIN: Brand -->
          <div class="m-stack__item m-brand  m-brand--skin-dark ">
            <div class="m-stack m-stack--ver m-stack--general">
              <div class="m-stack__item m-stack__item--middle m-brand__logo">
                <a href="{{ route('index') }}" class="m-brand__logo-wrapper">
                  <img alt="" src="{{asset('public/fontend/assets/demo/default/media/img/logo/logo_default_dark.png')}}"/>
                </a>
              </div>
              <div class="m-stack__item m-stack__item--middle m-brand__tools">
                <!-- BEGIN: Left Aside Minimize Toggle -->
                <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block
                ">
                <span></span>
              </a>
              <!-- END -->
              <!-- BEGIN: Responsive Aside Left Menu Toggler -->
              <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                <span></span>
              </a>

              <!-- END -->
              <!-- BEGIN: Topbar Toggler -->
              <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                <i class="flaticon-more"></i>
              </a>
              <!-- BEGIN: Topbar Toggler -->
            </div>
          </div>
        </div>
        <!-- END: Brand -->
        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
          <!-- BEGIN: Horizontal Menu -->
          <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
            <i class="la la-close"></i>
          </button>
          <?php $name = explode(' ', Auth::user()->full_name);?>
          <!-- BEGIN: Topbar -->
          <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
            <div class="m-stack__item m-topbar__nav-wrapper">
              <ul class="m-topbar__nav m-nav m-nav--inline">
                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click" aria-expanded="true">
                  <a href="#" class="m-nav__link m-dropdown__toggle">
                    <span class="m-topbar__userpic">
                      <span data-letters="{{ mb_substr($name[count($name) - 1], 0, 1) }}"></span>
                    </span>

                  </a>
                  <div class="m-dropdown__wrapper" style="z-index: 101;">
                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 12.5px;"></span>
                    <div class="m-dropdown__inner">
                      <div class="m-dropdown__header m--align-center" style="background: url({{asset('public/fontend/assets/app/media/img/misc/user_profile_bg.jpg')}}); background-size: cover;">
                        <div class="m-card-user m-card-user--skin-dark">
                          <div class="m-card-user__pic">

                            <span data-letter="{{ mb_substr($name[count($name) - 1], 0, 1) }}"></span>
                          </div>
                          <div class="m-card-user__details">
                            <span class="m-card-user__name m--font-weight-500">{{ Auth::user()->full_name }}</span>
                            <span class="text-white">{{ Auth::user()->email }}</span>
                          </div>
                        </div>
                      </div>
                      <div class="m-dropdown__body">
                        <div class="m-dropdown__content">
                          <ul class="m-nav m-nav--skin-light">
                            <li class="m-nav__item">
                              <a href="{{ route('account-logout') }}" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">{{ __('Đăng xuất') }}</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>

              </ul>
            </div>
          </div>
          <!-- END: Topbar -->

        </div>
      </div>
    </div>
  </header>
  <!-- END: Header -->

  <!-- begin::Body -->
  <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

    <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
      <i class="la la-close"></i>
    </button>
	@include('fontend.layouts.order.sidebar')