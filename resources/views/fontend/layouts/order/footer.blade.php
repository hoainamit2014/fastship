</div>
	<script src="{{ asset('public/fontend/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
	<script src="{{ asset('public/fontend/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
	<script src="{{ asset('public/fontend/assets/app/js/dashboard.js')}}" type="text/javascript"></script>
	<script src="{{ asset('public/fontend/js/order.js')}}" type="text/javascript"></script>
	<script src="{{ asset('public/fontend/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
	@yield('script')
</body>
</html>