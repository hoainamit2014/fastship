<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon la la-list"></i>
					<span class="m-menu__link-text">Quản lý đơn hàng</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
			    <div class="m-menu__submenu " m-hidden-height="40" style="">
			    	<span class="m-menu__arrow"></span>
			        <ul class="m-menu__subnav">
			        	<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('fs-order-add') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Tạo đơn hàng</span></a>
			            </li>
			            <li class="m-menu__item " aria-haspopup="true"><a href="{{ route('fs-order-index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Danh sách</span></a>
			            </li>
			        </ul>
			    </div>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon la la-shopping-cart"></i>
					<span class="m-menu__link-text">Điểm lấy hàng</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu " m-hidden-height="40" style="">
			    	<span class="m-menu__arrow"></span>
			        <ul class="m-menu__subnav">
			        	<li class="m-menu__item " aria-haspopup="true"><a href="{{ route('warehouse-add') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Tạo điểm lấy hàng</span></a>
			            </li>
			            <li class="m-menu__item " aria-haspopup="true"><a href="{{ route('warehouse-index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Danh sách</span></a>
			            </li>
			        </ul>
			    </div>
			</li>
		</ul>
	</div>
</div>
