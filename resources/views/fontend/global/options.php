<?php
function showCategories($categories,$parent = 0,$stt = 0){
	// BƯỚC 2.1: LẤY DANH SÁCH CATE CON
    $cate_child = array();
    $flag = '';
    foreach ($categories as $key => $val)
    {
        // Nếu là chuyên mục con thì hiển thị
        if ($val['parent'] == $parent)
        {
            $cate_child[] = $val;
            unset($categories[$key]);
        }
    }
     
    // BƯỚC 2.2: HIỂN THỊ DANH SÁCH CHUYÊN MỤC CON NẾU CÓ
    if ($cate_child)
    {
    	if($stt == 0)
    	{
    		echo '<ul class="nav navbar-nav">';
    	}
    	else
    	{
    		echo '<ul class="sub-menu">';
    	}
        
        foreach ($cate_child as $key => $val)
        {
        	echo '<li><a href="'.$val['slug'].'">'.$val['name'].'</a>';
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            showCategories($categories, $val['term_id'],$stt++);
            echo '</li>';
        }
        echo '</ul>';
    }
}

function formatText ($sText)
{
    if (strlen($sText) > 70) {
        echo substr($sText, 0, 70) . ' ...';
    } else {
        echo $sText;
    }
}