<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WareHouse extends Model
{
    protected $table    = 'fs_warehouse';
    public $timestamps  = false;
    protected $fillable = [
        'user_id', 'name', 'phone','address','province_id','district_id','ward_id'
    ];
}
