<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table    = 'fs_bill';
    public $timestamps  = false;
    protected $fillable = [
        'bill_code', 'order_number', 'pill_type', 'responsible_person_id', 'status', 'received_status', 'author', 'post_type', 'create_date', 'complete_date',
    ];
}
