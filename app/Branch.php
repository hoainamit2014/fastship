<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table    = 'fs_branch';
    public $timestamps  = false;
    protected $fillable = [
        'province_id', 'district_id', 'address', 'coefficient', 'sort', 'parent_id', 'area_responsible', 'deploy_order_loading', 'center_branch', 'delivery_of_other_address',
    ];
}
