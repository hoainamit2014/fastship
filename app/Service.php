<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'fs_order_service';
	public $timestamps = false;
    protected $fillable = [
        'service_id', 'name', 'number'
    ];
}
