<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryOfSavingsSettings extends Model {
	protected $table = 'fs_delivery_of_savings_settings';
	public $timestamps = false;
	protected $fillable = [
		'kg', 'internal_area', 'external_area', 'special_area'
	];
}
