<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroupSetting extends Model
{
    protected $table = 'fs_user_group_setting';
	public $timestamps = false;
    protected $fillable = [
        'name', 'title', 'module_id', 'default_admin', 'default_user'
    ];
}
