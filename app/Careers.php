<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Careers extends Model
{
    protected $table = 'fs_careers';
	public $timestamps = true;
    protected $fillable = [
        'title', 'alias', 'content', 'description', 'keyword', 'expire_date', 'user_id', 'is_active'
    ];
}
