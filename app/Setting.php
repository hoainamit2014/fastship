<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'fs_options';
	public $timestamps = false;
    protected $fillable = [
        'option_id', 'option_name', 'option_value'
    ];
}
