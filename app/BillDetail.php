<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillDetail extends Model {
	protected $table = 'fs_bill_detail';
	public $timestamps = false;
	protected $fillable = [
		'bill_code', 'order_id', 'create_date',
	];
}
