<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroupCustom extends Model
{
    protected $table = 'fs_user_group_custom';
	public $timestamps = false;
    protected $fillable = [
        'setting_id', 'user_group_id', 'value'
    ];
}
