<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {
	protected $table = 'fs_order';
	public $timestamps = true;
	protected $fillable = [
		'order_id', 'order_code', 'user_id', 'customer_code', 'customer_order_code', 'receiver_name', 'phone', 'address', 'province_id', 'district_id', 'ward_id', 'product_name', 'weight', 'length', 'width', 'height', 'cod', 'note', 'transport_fee', 'service_id', 'receiver_purchase_fee', 'status', 'physical_status', 'is_import', 'appointment_delivery', 'gift', 'reason','flow_order','order_type','inside_outside','order_difficult_delivery','delivery_date','save_to_dc_date','delivery_success_date','dc_current','dc_pickup_expected','dc_pickup_actual','dc_broadcasting_expected','dc_broadcasting_actual','dc_returns_expected','dc_returns_actual','date_lead_time','created_at','updated_at'
	];

	public function getProvinceName() {
		return $this->belongsTo('App\Province');
	}
}
