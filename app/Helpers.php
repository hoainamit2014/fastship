<?php

use App\Districts;
use App\Province;
use App\User;

class Helpers
{

    public static function getDistrictName($id)
    {
        $result = Districts::where('district_id', $id)->first();
        return $result->name;
    }

    public static function getProvinceName($id)
    {
        $result = Province::where('province_id', $id)->first();
        return $result->name;
    }

    public static function getPagesOptionsRecursive($pages, $parent_id = 0, $char = '', $selected = 0)
    {
        if ($selected != 0) {
            foreach ($pages as $key => $item) {
                if ($item['post_parent'] == $parent_id) {
                    if ($item['id'] == $selected) {
                        echo '<option selected="selected" value="' . $item['id'] . '">';
                    } else {
                        echo '<option  value="' . $item['id'] . '">';
                    }
                    echo $char . $item['post_name'];
                    echo '</option>';
                    unset($pages[$key]);
                    getPagesOptionsRecursive($pages, $item['id'], $char . '---', $selected);
                }
            }
        } else {
            foreach ($pages as $key => $item) {
                if ($item['post_parent'] == $parent_id) {
                    echo '<option value="' . $item['id'] . '">';
                    echo $char . $item['post_name'];
                    echo '</option>';
                    unset($pages[$key]);
                    getPagesOptionsRecursive($pages, $item['id'], $char . '---');
                }
            }
        }

    }

    public static function getPagesTableRecursive($pages, $parent_id = 0, $char = '')
    {
        if (!isset($_GET['search']) || empty($_GET['search'])) {
            foreach ($pages as $key => $val) {
                if ($val['post_parent'] == $parent_id) {
                    echo '<tr class="page-item-' . $val['id'] . '">';
                    echo '<td class="text-center ">#' . $val['id'] . '</td>';
                    echo '<td><a href="/fs/cpanel/pages/edit/' . $val['id'] . '">' . $char . $val['post_name'] . ' </a></td>';
                    echo '<td class="text-center">' . ($val['post_author'] == 1 ? 'admin' : '') . ' </td>';
                    echo '<td class="text-center">' . ($val['post_status'] ? 'Bật' : 'Tắt') . '</td>';
                    echo '<td class="text-center">' . date("d-m-Y h:i:s", strtotime($val['updated_at'])) . '</td>';
                    echo '<td class="text-center">';
                    echo '<div class="action-items">';
                    echo '<a href="/fs/cpanel/pages/edit/' . $val['id'] . '"><i class="la la-edit" aria-hidden="true"></i></a>';
                    echo '<a onclick="return deletePages(' . $val['id'] . ')"><i class="la la-close"></i>  </a>';
                    echo '</div>';
                    echo '</td>';
                    echo '</tr>';
                    unset($pages[$key]);
                    self::getPagesTableRecursive($pages, $val['id'], $char . '—');
                }
            }
        } else {
            foreach ($pages as $key => $val) {
                echo '<tr class="page-item-' . $val['id'] . '">';
                echo '<td class="text-center ">#' . $val['id'] . '</td>';
                echo '<td><a href="/fs/cpanel/pages/edit/' . $val['id'] . '">' . $char . $val['post_name'] . ' </a></td>';
                echo '<td class="text-center">' . ($val['post_author'] == 1 ? 'admin' : '') . ' </td>';
                echo '<td class="text-center">' . ($val['post_status'] ? 'Bật' : 'Tắt') . '</td>';
                echo '<td class="text-center">' . date("d-m-Y h:i:s", strtotime($val['updated_at'])) . '</td>';
                echo '<td class="text-center">';
                echo '<div class="action-items">';
                echo '<a href="/fs/cpanel/pages/edit/' . $val['id'] . '"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>';
                echo '<a onclick="return deletePages(' . $val['id'] . ')"><i class="glyphicon glyphicon-trash"></i>  </a>';
                echo '</div>';
                echo '</td>';
                echo '</tr>';
                unset($pages[$key]);
            }
        }
    }

    public static function getMenuTableRecursive($menu, $parent_id = 0, $char = '')
    {
        foreach ($menu as $key => $val) {

            if ($val['parent'] == $parent_id) {
                echo '<tr class="item-' . $val['term_id'] . '">';
                echo '<td>#' . $val['term_id'] . '</td>';
                echo '<td>' . $char . ' ' . $val["name"] . '</td>';
                echo '<td class="text-center">' . (($val["parent"] == $_GET['menu']) ? '-' : Helpers::getMenuName($val["parent"])) . '</td>';
                echo '<td class="text-center">' . (($val["status"] == 1) ? '<span class="m-badge  m-badge--success m-badge--wide">On</span>' : '<span class="m-badge  m-badge--danger m-badge--wide">Off</span>') . '</td>';
                echo '<td><div class="action-items"><a onclick="return editMenu(' . $val['term_id'] . ')"><i class="la la-edit" aria-hidden="true"></i></a><a onclick="return deleteMenu(' . $val['term_id'] . ')"><i class="la la-close"></i>  </a></div></td>';
                echo '</tr>';
                unset($menu[$key]);
                self::getMenuTableRecursive($menu, $val['term_id'], $char . '—');
            }
        }
    }

    public static function getMenuName($id)
    {
        $term = DB::table('fs_term')->where('term_id', $id)->get();
        return $term[0]->name;
    }

    public static function getMenuOptionRecursive($menu, $parent_id = 0, $char = '')
    {
        foreach ($menu as $key => $val) {

            if ($val['parent'] == $parent_id) {
                echo '<option value="' . $val['term_id'] . '">';
                echo $char . $val['name'];
                echo '</option>';
                unset($menu[$key]);
                self::getMenuOptionRecursive($menu, $val['term_id'], $char . '---');
            }
        }
    }

    public static function getFillTypeName($id)
    {
        $id = intval($id);
        $bill_type = DB::table('fs_bill_type')->where('id', $id)->first();
        return !empty($bill_type->name) ? $bill_type->name : 'Không hợp lệ';
    }

    public static function getAuthNameByID($id)
    {
        $aUser = User::select('full_name')->find($id);
        return $aUser;
    }

    public function getProvinceList($aProvince, $parent = 0, $class = '')
    {
        if (!empty($aProvince)) {
            $aProvinceListing = array();
            foreach ($aProvince as $k => $aVal) {
                if ($aVal['parent'] == $parent) {
                    $aProvinceListing[] = $aVal;
                    unset($aProvince[$k]);
                }
            }
            if ($aProvinceListing) {
                $i = 0;
                echo '<ul class="ul-provider"">';
                foreach ($aProvinceListing as $key => $aVal) {
                    echo '<li><a href="javascript:avoid(0)"><i class="flaticon flaticon-folder"></i> <span>' . $aVal['name'] . ' (' . $aVal['count'] . ') ';
                    echo '</a><small class="pull-right" data-id="' . $aVal['id'] . '" data-type="' . $aVal['type'] . '"  onclick="addRowsToTable(this)">Chọn</small>';
                    $this->getProvinceList($aProvince, $aVal['id'], $i++);
                    echo '</li>';
                }
                echo '</ul>';

            }
        }
    }

    public function getBillType()
    {
        $bill_type = DB::table('fs_bill_type')->get()->toArray();
        return $bill_type;

    }
}
