<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressDeliveryInterprovincial extends Model {
	protected $table = 'fs_express_delivery_interprovincial_area_settings';
	public $timestamps = false;
	protected $fillable = [
		'kg', 'internal_area', 'external_area', 'special_area'
	];
}
