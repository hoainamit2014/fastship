<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillType extends Model
{
    protected $table    = 'fs_bill_type';
    public $timestamps  = false;
    protected $fillable = [
        'id', 'name',
    ];
}
