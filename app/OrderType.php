<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderType extends Model
{
	protected $table = 'fs_order_type';
    protected $fillable = [
        'id' , 'type' , 'name' , 'description'
    ];
}
