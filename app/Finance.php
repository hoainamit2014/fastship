<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model {
	protected $table = 'fs_finance';
	public $timestamps = true;
	protected $fillable = [
		'finance_id', 'user_id, accountant_id, list_order, count, finance_type'
	];
}
