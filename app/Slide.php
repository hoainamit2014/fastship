<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'fs_slide';
	public $timestamps = false;
    protected $fillable = [
        'slide_id', 'title', 'subtitle', 'link', 'image_path', 'type', 'is_active'
    ];
}
