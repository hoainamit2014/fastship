<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table    = 'fs_user_group';
    public $timestamps  = false;
    protected $fillable = [
        'user_group_id', 'title', 'is_active',
    ];
}
