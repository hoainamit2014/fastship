<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'fs_store';
	public $timestamps = true;
    protected $fillable = [
        'branch_id', 'bill_code'
    ];
}
