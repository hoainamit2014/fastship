<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressDeliveryAddon extends Model {
	protected $table = 'fs_express_delivery_settings_addon';
	public $timestamps = false;
	protected $fillable = [
		'services_name', 'services_code', 'services_price', 'note'
	];
}
