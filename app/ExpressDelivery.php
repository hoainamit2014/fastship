<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressDelivery extends Model {
	protected $table = 'fs_express_delivery_settings';
	public $timestamps = false;
	protected $fillable = [
		'package_name', 'package_code', 'kg', 'internal', 'external','external2'
	];
}
