<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'fs_news';
	public $timestamps = true;
    protected $fillable = [
        'user_id', 'title', 'alias', 'content', 'description', 'keyword', 'expire_date', 'is_active'
    ];
}
