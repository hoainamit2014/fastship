<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'phone'         => 'required',
            'address'       => 'required',
            'province_id'   => 'required',
            'district_id'   => 'required',
            'ward_id'       => 'required',
        ];
    }

     public function messages()
    {
        return [
            'full_name.required'    => 'Họ tên không được để trống',
            'phone.required'            => 'Số điện thoại không được để trống',
            'address.required'          => 'Địa chỉ không được để trống',
            'province_id.required'      => 'Tỉnh/thành phố không được để trống',
            'district_id.required'      => 'Quận/huyện không được để trống',
            'ward_id.required'          => 'Phường/xã không được để trống',
        ];
    }
}
