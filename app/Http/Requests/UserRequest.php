<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'         => 'required|alpha|min:4|max:20',
            'email'            => 'required|email',
            'fullname'         => 'required',
            'password'         => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'address'          => 'required',
            'sex'              => 'required|numeric',
            'phone'            => 'required|numeric',
            'position'         => 'required',
            'department'       => 'required',
            'exprire_date'     => 'required',
            'account_number'   => 'numeric',
            'identity_card'    => 'required|numeric',
            'release_date'     => 'required',
            'register_place'   => 'required',

        ];
    }
}
