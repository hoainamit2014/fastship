<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'receiver_name'    => 'required',
            'address'    => 'required',
            'province_id'    => 'required',
            'district_id'    => 'required',
            'ward_id'    => 'required',
            'product_name'    => 'required',
            'customer_code'    => 'required',
            'weight'    => 'required',
            'length'    => 'required',
            'width'    => 'required',
            'height'    => 'required',
            'service_id'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'receiver_name.required'    => 'Họ tên không được để trống',
            'address.required'    => 'Địa chỉ không được để trống',
            'province_id.required'    => 'Tỉnh/thành phố không được để trống',
            'district_id.required'    => 'Quận/huyện không được để trống',
            'ward_id.required'    => 'Phường/xã không được để trống',
            'product_name.required'    => 'Tên sản phẩm không được để trống',
            'customer_code.required'    => 'Mã đối tác không được để trống',
            'weight.required'    => 'Khối lượng không được để trống',
            'length.required'    => 'Chiều dài không được để trống',
            'width.required'    => 'Chiều rộng không được để trống',
            'height.required'    => 'Chiều cao không được để trống',
            'service_id.required'    => 'Dịch vụ không được để trống',
        ];
    }
}
