<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'         => 'required|alpha|min:4|max:20',
            'fullname'         => 'required',
            'fullname'         => 'required',
            'password'         => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'address'          => 'required',
            'email'            => 'required|email',
            'phone'            => 'required|numeric',
            'customer_code'    => 'required',
        ];
    }
}
