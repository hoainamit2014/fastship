<?php

namespace App\Http\Controllers\Fontend;

use App\Warehouse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\WarehouseRequest;
use Illuminate\Support\Facades\Auth;

class WarehouseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aWarehouses = Warehouse::select(Db::raw('fs_warehouse.*, fs_province.name as province_name, fs_district.name as district_name, fs_ward.name as ward_name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_warehouse.province_id')
            ->join('fs_district', 'fs_district.district_id', '=', 'fs_warehouse.district_id')
            ->join('fs_ward', 'fs_ward.ward_id', '=', 'fs_warehouse.ward_id')->get();

        return view('fontend.warehouse.index', ['aWarehouses' => $aWarehouses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        return view('fontend.warehouse.add', ['aProvinces' => $aProvinces]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseRequest $request)
    {
        Warehouse::create([
            'user_id'       => Auth::id(),
            'name'         => $request->full_name,
            'phone'             => $request->phone,
            'address'           => $request->address,
            'province_id'       => $request->province_id,
            'district_id'       => $request->district_id,
            'ward_id'           => $request->ward_id,
        ]);
        return redirect()->route('warehouse-index')->with('success', 'Thêm kho hàng thành công');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aWarehouse = Warehouse::find($id);
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aDistricts = DB::table('fs_district')->where('province_id', '=', $aWarehouse->province_id)->get(['district_id as id', 'name as text']);
        $aWards     = DB::table('fs_ward')->where('district_id', '=', $aWarehouse->district_id)->get(['ward_id as id', 'name as text']);

        return view('fontend.warehouse.add', ['aWarehouse' => $aWarehouse, 'aProvinces' => $aProvinces, 'aDistricts' => $aDistricts, 'aWards' => $aWards]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aUpdate = [
            'name'         => $request->full_name,
            'phone'             => $request->phone,
            'address'           => $request->address,
            'province_id'       => $request->province_id,
            'district_id'       => $request->district_id,
            'ward_id'           => $request->ward_id,
        ];
        Warehouse::where('id', '=', $id)
            ->update($aUpdate);

        return redirect()->route('warehouse-index')->with('success', 'Sửa kho hàng thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Warehouse::find($id)->delete()) {
            $sMessage = 'Xóa kho hàng thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa kho hàng thành công';
            $sStatus  = 'error';
        }
        return redirect()->route('warehouse-index')->with($sStatus, $sMessage);
    }
}
