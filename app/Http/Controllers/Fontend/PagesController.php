<?php

namespace App\Http\Controllers\Fontend;

use Illuminate\Http\Request;
use App\Term;
use App\Pages;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\News;
use App\Careers;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->prefix = 'fs_';
        $this->_menu = Term::leftJoin($this->prefix.'term_taxonomy', $this->prefix.'term.term_id', '=', $this->prefix.'term_taxonomy.term_id')->where($this->prefix.'term_taxonomy.taxonomy','=','sub-menu')->get()->toArray();
    }
    public function Intro(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function Fastship(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function CashReceiptsCod(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function DeliveryOfSavings(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function DeliveryOfDay(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function DeliveryOfRegion(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function NorthSouthTransportation(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function Storage(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function News(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();

        $aNews = News::paginate(12);
        
        return view('fontend.news.index',['data' => $data, 'aNews' => $aNews, 'sPageName' => 'Tin tức']);
    }
    public function Contact(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();
        return view('fontend.pages.default',compact('data'));
    }
    public function Careers(Request $request)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();

        $aCareers = Careers::paginate(12);

        return view('fontend.careers.index', ['data' => $data, 'aCareers' => $aCareers, 'sPageName' => 'Tuyển dụng']);
    }
    public function ShowNews (Request $request, $id)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();

        $aNew = News::where('new_id', (int)$id)->first();

        return view('fontend.news.show', ['data' => $data, 'aNew' => $aNew, 'sPageName' => 'Tin tức']);
    }
    public function ShowCareers(Request $request, $id)
    {
        $data['header_menu'] = $this->_menu;
        
        $data['content'] = Pages::where('post_alias',$request->path())
        ->select('post_content')
        ->get()
        ->toArray();

        $aCareer = Careers::where('career_id', (int)$id)->first();

        return view('fontend.careers.show', ['data' => $data, 'aCareer' => $aCareer, 'sPageName' => 'Tuyển dụng']);
    }
}
