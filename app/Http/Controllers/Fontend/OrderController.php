<?php

namespace App\Http\Controllers\Fontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Service;
use App\WareHouse;
use Excel;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* get order status */
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aStatus    = DB::table('fs_order_status')->get(['status_id as id', 'name as text']);

        return view('fontend.order.index', compact('aProvinces', 'aStatus'));
    }

    public function indexSearch (Request $request)
    {
        $iDisplayStart  = $request->input('start');
        $iDisplayLength = $request->input('length');
        $iSearch        = $request->input('search');

        $sQuery = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_province.name as province_name, fs_district.name as district_name, fs_ward.name as ward_name, fs_order_service.name as service_name, fs_order_type.name as order_type_name'))
        ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
        ->leftJoin('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
        ->leftJoin('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
        ->leftJoin('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
        ->leftJoin('fs_order_type', 'fs_order_type.id', '=', 'fs_order.order_type')
        ->join('fs_order_service', 'fs_order_service.service_id', '=', 'fs_order.service_id');
        
        if ($aSearch = $request->input('customsearch')) {
            if (!empty($aSearch['order_code'])) {
                $sQuery->where(function ($query) use ($aSearch) {
                    $query->where('fs_order.order_code', '=', $aSearch['order_code'])
                          ->orWhere('fs_order.customer_order_code', '=', $aSearch['order_code']);
                });
            }
            if (!empty($aSearch['product_name'])) {
                $sQuery->where('fs_order.product_name', 'like', "%" . $aSearch['product_name'] . "%");
            }
            if (!empty($aSearch['from_date'])) {
                $aArray     = explode('-', $aSearch['from_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $sQuery->where('fs_order.created_at', '>=', $iTime);
            }
            if (!empty($aSearch['to_date'])) {
                $aArray     = explode('-', $aSearch['to_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $sQuery->where('fs_order.created_at', '<=', $iTime);
            }
            if (!empty($aSearch['province_id'])) {
                $sQuery->where('fs_order.province_id', '=', $aSearch['province_id']);
            }
            if (!empty($aSearch['district_id'])) {
                $sQuery->where('fs_order.district_id', '=', $aSearch['district_id']);
            }
            if (!empty($aSearch['ward_id'])) {
                $sQuery->where('fs_order.ward_id', '=', $aSearch['ward_id']);
            }
            if (!empty($aSearch['status'])) {
                $sQuery->where('fs_order.status', '=', $aSearch['status']);
            }
        }

        $aTotalRecord = $sQuery->count();
        $aRows        = $sQuery->offset($iDisplayStart)->limit($iDisplayLength)->get();

        foreach ($aRows as $iKey => $aValue) {
            $aRows[$iKey]['STT'] = $iKey + 1;
            $aRows[$iKey]['flow_order'] = $aValue['flow_order'] == 'di_giao' ? 'Đi giao' : 'Trả về';
            $aRows[$iKey]['created_at_format'] = date('d-m-Y H:s:i', strtotime($aValue['created_at']));
            if ($aValue['delivery_success_date']) {
                $aRows[$iKey]['delivery_success_date_format'] = date('d-m-Y', strtotime($aValue['delivery_success_date']));
            } else {
                $aRows[$iKey]['delivery_success_date_format'] = '';
            }
            $aRows[$iKey]['receiver_purchase_fee'] = $aValue['receiver_purchase_fee'] ? 'Người gửi thanh toán cước' : 'Người nhận thanh toán cước';
            $aRows[$iKey]['cod'] = number_format($aValue['cod'], 2);
            $aRows[$iKey]['transport_fee'] = number_format($aValue['transport_fee'], 2);
        }

        $results = array(
            "iTotalRecords"        => count($aRows),
            "iTotalDisplayRecords" => $aTotalRecord,
            "aaData"               => $aRows,
        );

        return json_encode($results);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aStatus    = DB::table('fs_order_status')->get(['status_id as id', 'name as text']);
        $aWarehouses    = Warehouse::where('user_id', Auth::id())->get();

        return view('fontend.order.add', ['aProvinces' => $aProvinces, 'aStatus' => $aStatus, 'aWarehouses' => $aWarehouses]);
    }

    public function distance ($lat1, $lon1, $lat2, $lon2)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
      
        return  $miles * 1.609344;
    }

    public function getWarehouse ($iUserid)
    {
        $aUser = WareHouse::where('user_id', '=', $iUserid)->first();
        return $aUser->address;
    }

    public function getCoordinate ($address)
    {
        $sGoogleApiKey = 'AIzaSyBVt1Fk48S7svJan6bjV_ztcXWbYCBWymQ';
        $sAddress = urlencode($address);
        $sAPi = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$sAddress.'&key='.$sGoogleApiKey;
        
        $sResult = file_get_contents($sAPi);dd($sResult);
        $sResult = json_decode($sResult, true);
        if ($sResult['status'] == 'OK' && $sResult['results'][0]) {
            $aData = array(
                'latitude' => $sResult['results'][0]['geometry']['location']['lat'],
                'longitude' => $sResult['results'][0]['geometry']['location']['lng'],
            );
            return $aData;
        }
        return false;
    }

    public function store(OrderRequest $request)
    {
        // tinh gia don hang
        $noithanh = [
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', 'Bình Thạnh', 'Gò Vấp', 'Phú Nhuận', 'Tân Bình', 'Tân Phú'
        ];
        $ngoaithanh = [
            '9', '12', 'Thủ Đức', 'Bình Tân'
        ];
        $ngoaithanh2 = [
            'Nhà Bè', 'Bình Chánh', 'Hóc Môn', 'Củ Chi'
        ];
        $sStartAddress = $this->getWarehouse(Auth::id());
        $sEndAddress = $request->address;

        if ($request->province_id == 79) { // chuyen phat TPHCM
            $request->service_id = 1;
            $iPriceCal = 0;
            if (in_array($request->district_name, $noithanh)) { // noi thanh
                switch ($request->district_name) {
                    case 'QUANGAY24H':
                    // tinh gia theo trong luong
                        $iWeightCal = $request->length * $request->width * $request->height / 5;
                        if ($iWeightCal <= 3000) {
                            $iPriceCal = 20000;
                        } else {
                            $iPriceCal = ($iWeightCal - 3000) * 2000 / 500;
                        }
                        break;
                    case 'QUANGAY6H':
                        // tinh gia theo so km
                        // lay toa do cua kho hang
                        $arrayStart = $this->getCoordinate($sStartAddress);
                        // lay toa do cua diem lay hang
                        $arrayEnd = $this->getCoordinate($sEndAddress);
                        // tinh khoang giua 2 toa do
                        if ($arrayStart && $arrayEnd) {
                            $sKm = $this->distance($arrayStart['latitude'], $arrayStart['longitude'], $arrayStart['latitude'], $arrayStart['longitude']);
                            if ($sKm <= 5) {
                                $iPriceCal = 25000;
                            } else {
                                $iPriceCal = ($sKm - 5) * 2000 / 0.5;
                            }
                        }
                        break;
                }
            }
            if (in_array($request->district_name, $ngoaithanh)) { // ngoai thanh
                switch ($request->district_name) {
                    case 'QUANGAY24H':
                    // tinh gia theo trong luong
                        $iWeightCal = $request->length * $request->width * $request->height / 5;
                        if ($iWeightCal <= 3000) {
                            $iPriceCal = 30000;
                        } else {
                            $iPriceCal = ($iWeightCal - 3000) * 2500 / 500;
                        }
                        break;
                    case 'QUANGAY6H':
                        // tinh gia theo so km
                        // lay toa do cua kho hang
                        $arrayStart = $this->getCoordinate($sStartAddress);
                        // lay toa do cua diem lay hang
                        $arrayEnd = $this->getCoordinate($sEndAddress);
                        // tinh khoang giua 2 toa do
                        if ($arrayStart && $arrayEnd) {
                            $sKm = $this->distance($arrayStart['latitude'], $arrayStart['longitude'], $arrayStart['latitude'], $arrayStart['longitude']);
                            if ($sKm <= 5) {
                                $iPriceCal = 35000;
                            } else {
                                $iPriceCal = ($sKm - 5) * 2500 / 0.5;
                            }
                        }
                        break;
                }
            }
            if (in_array($request->district_name, $ngoaithanh2)) { // ngoai thanh 2
                // tinh gia theo trong luong
                $iWeightCal = $request->length * $request->width * $request->height / 5;
                if ($iWeightCal <= 3000) {
                    $iPriceCal = 40000;
                } else {
                    $iPriceCal = ($iWeightCal - 3000) * 3000 / 500;
                }
            }
        } else { // chuyen phat ngoai thanh
            $iWeightCal = $request->length * $request->width * $request->height / 5; //: 5000 * 1000
            if ($request->service_id == 'lien_tinh') { // lien tinh
                $request->service_id = 2;
                if ($iWeightCal <= 500) {
                    $iPriceCal = 25000;
                } elseif ($iWeightCal > 500 && $iWeightCal <= 1000) {
                    $iPriceCal = 35000;
                } elseif ($iWeightCal > 1000 && $iWeightCal <= 1500) {
                    $iPriceCal = 45000;
                } elseif ($iWeightCal > 1500 && $iWeightCal <= 2000) {
                    $iPriceCal = 55000;
                } else {
                    $iPriceCal = ($iWeightCal - 2000) * 25000 / 500;
                }
            } else { // tiet kiem
                $request->service_id = 3;
                if ($iWeightCal <= 3000) {
                    $iPriceCal = 25000;
                } elseif ($iWeightCal > 3000 && $iWeightCal <= 30000) {
                    $iPriceCal = 35000;
                } elseif ($iWeightCal > 30000 && $iWeightCal <= 200000) {
                    $iPriceCal = 45000;
                } elseif ($iWeightCal > 200000 && $iWeightCal <= 500000) {
                    $iPriceCal = 55000;
                } else {
                    $iPriceCal = ($iWeightCal - 2000) * 25000 / 500;
                }
            }
        }

        $aOrder     = new Order();
        $iTemp      = DB::table('fs_order')->max('order_id');
        $iOrderCode = $iTemp ? $iTemp + 1 : 1;
        $aUser = User::find(Auth::id());

        $aOrder->user_id            = Auth::id();
        $aOrder->customer_code      = $aUser->customer_code;
        $aOrder->order_code         = 'VL' . date('d') . date('m') . date('Y') . $iOrderCode;
        $aOrder->receiver_name      = $request->receiver_name;
        $aOrder->phone              = $request->phone;
        $aOrder->address            = $request->address;
        $aOrder->province_id        = $request->province_id;
        $aOrder->district_id        = $request->district_id;
        $aOrder->ward_id            = $request->ward_id;
        $aOrder->product_name       = $request->product_name;
        $aOrder->weight             = $request->weight;
        $aOrder->length             = $request->length;
        $aOrder->width              = $request->width;
        $aOrder->height             = $request->height;
        $aOrder->cod                = $request->cod;
        $aOrder->note               = $request->note;
        $aOrder->transport_fee      = $iPriceCal;
        $aOrder->service_id         = $request->service_id;
        $aOrder->receiver_purchase_fee            = isset($request->receiver_purchase_fee) ? 1 : 0;
        $aOrder->status             = 1;
        $aOrder->physical_status    = 1;
        $aOrder->warehouse_id       = $request->warehouse_id;
        $aOrder->order_type       = 1;
        if ($aOrder->save()) {
            return redirect()->route('fs-order-index')->with('success', 'Thêm đơn hàng thành công');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aOrder     = Order::where('order_id', $id)->first();
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aDistricts = DB::table('fs_district')->where('province_id', '=', $aOrder->province_id)->get(['district_id as id', 'name as text']);
        $aWards     = DB::table('fs_ward')->where('district_id', '=', $aOrder->district_id)->get(['ward_id as id', 'name as text']);
        $aStatus    = DB::table('fs_order_status')->get(['status_id as id', 'name as text']);
        $aServices    = DB::table('fs_order_service')->get(['service_id as id', 'name as text']);
        $aWarehouses    = Warehouse::where('customer_id', Auth::id())->get();

        return view('fontend.order.add', ['aOrder' => $aOrder, 'aProvinces' => $aProvinces, 'aDistricts' => $aDistricts, 'aWards' => $aWards, 'aStatus' => $aStatus, 'aServices' => $aServices, 'aWarehouses' => $aWarehouses]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        $aUpdate = [
            'receiver_name'      => $request->receiver_name,
            'phone'              => $request->phone,
            'address'            => $request->address,
            'province_id'        => $request->province_id,
            'district_id'        => $request->district_id,
            'ward_id'            => $request->ward_id,
            'product_name'       => $request->product_name,
            'weight'             => $request->weight,
            'length'             => $request->length,
            'width'              => $request->width,
            'height'             => $request->height,
            'cod'                => $request->cod,
            'note'               => $request->note,
            'transport_fee'      => $request->transport_fee,
            'service_id'         => $request->service_id,
            'receiver_purchase_fee'            => isset($request->receiver_purchase_fee) ? 1 : 0,
            'status'             => $request->status,
            'warehouse_id'             => $request->warehouse_id,
        ];
        
        if (Order::where('order_id', '=', $id)->update($aUpdate)) {
            return redirect()->route('fs-order-index')->with('success', 'Cập nhật đơn hàng thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Order::where('order_id', $id)->delete()) {
            $sMessage = 'Xóa vận đơn thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa vận đơn thất bại';
            $sStatus  = 'error';
        }
        return redirect()->route('fs-order-index')->with($sStatus, $sMessage);
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file')) {
            $sPath = $request->file->getRealPath();
            $aRows = Excel::selectSheets('DanhSach')->skip(4)->load($sPath, function ($reader) {})->get()->toArray();
            if (!empty($aRows)) {
                $aUser    = User::where('status', '1')->where('user_group_id', '7')->where('id', Auth::id())->first();
                if (!$aUser) {
                    return redirect()->route('fs-order-index')->with('error', 'Người dùng này không thể tạo đơn hàng');
                }

                $iTemp      = DB::table('fs_order')->max('order_id');
                if (!$iTemp) {
                        $iOrderCode = 1;
                } else {
                    $iOrderCode = $iTemp;
                }

                foreach ($aRows[0] as $iKey => $aRow) {

                    // kiem tra neu 1 dong rong
                    if (empty($aRow['receiver_name']) && empty($aRow['phone']) && empty($aRow['address']) && empty($aRow['province_name']) && empty($aRow['district_name']) && empty($aRow['ward_name']) && empty($aRow['order_code']) && empty($aRow['order_type']) && empty($aRow['product_name']) && empty($aRow['cod']) && empty($aRow['weight']) && empty($aRow['service_id']) && empty($aRow['note']) && empty($aRow['receiver_purchase_fee'])) {
                        continue;
                    }
                    $sKey = (string)($iKey + 1);
                    // kiem tra tung field
                    if (!$aRow['receiver_name']) {dd('inside');
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập tên người nhận");
                    }
                    if (!$aRow['address']) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập địa chỉ");
                    }
                    if (!$aRow['order_type']) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng chọn loại đơn hàng");
                    }
                    if (!$aRow['product_name']) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập tên sản phẩm");
                    }
                    if ((int)$aRow['cod'] < 0) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, tiền thu hộ phải lớn hơn 0");
                    }
                    if ((int)$aRow['weight'] < 0) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT .Cân nặng phải lớn hơn 0");
                    }
                    if (!$aRow['service_id']) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng chọn dịch vụ");
                    }
                    if (!$aRow['note']) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng chọn ghi chú");
                    }
                    if (!$aRow['receiver_purchase_fee']) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng chọn người trả phí vận chuyển");
                    }
                    if (!$aRow['province_name'] || !$aRow['district_name'] || !$aRow['ward_name']) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập tên tỉnh/thành phố, quận/huyện, phường/xã");
                    }
                    $province_id = $this->findProvince($aRow['province_name']);
                    $district_id = $this->findDistrict($province_id, $aRow['district_name']);
                    $ward_id     = $this->findWard($province_id, $district_id, $aRow['ward_name']);
                    if (!$province_id || !$district_id || !$ward_id) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập đúng tên tỉnh/thành phố, quận/huyện, phường/xã");
                    }

                    $aInsert = [
                        'user_id'           => Auth::id(),
                        'customer_code'     => $aUser['customer_code'],
                        'order_code'        => isset($aRow['order_code']) ? $aRow['order_code'] : 'VL' . date('d') . date('m') . date('Y') . $iOrderCode,
                        'receiver_name'     => (string)$aRow['receiver_name'],
                        'phone'             => substr(trim($aRow['phone']), 0, 20),
                        'address'           => (string)$aRow['address'],
                        'province_id'       => (string)$province_id,
                        'district_id'       => (string)$district_id,
                        'ward_id'           => (string)$ward_id,
                        'product_name'      => (string)$aRow['product_name'],
                        'weight'            => isset($aRow['weight']) ? $aRow['weight'] : 0,
                        'cod'               => (int)$aRow['cod'],
                        'note'              => $aRow['note'] == 'XEM' ? 'Cho khách xem hàng' : ($aRow['note'] == 'XEM-THU' ? 'Cho khách xem hàng + thử' : 'Không cho khách xem hàng'),
                        'transport_fee'     => 20000,
                        'service_id'        => $aRow['service_id'] == 'Nhanh' ? 1 : ($aRow['service_id'] == 'TieuChuan' ? 2 : 3),
                        'receiver_purchase_fee'       => $aRow['receiver_purchase_fee'] == 'NguoiNhan' ? 1 : 0,
                        'order_type'        => $aRow['order_type'] == 'GIAOHANG' ? 1 : ($aRow['order_type'] == 'DOIHANG' ? 2 : 3),
                        'status'            => 1,
                        'physical_status'   => 1,
                        'is_import'         => 1,
                    ];
                    $iOrderCode++;
                    $aOrders[] = $aInsert;
                }
                if (!empty($aOrders)) {
                    foreach ($aOrders as $value) {
                        $aOrder = Order::create($value);
                    }
                    return redirect()->route('fs-order-index')->with('success', 'Tạo vận đơn thành công');
                }
            }
        } else {
            return redirect()->route('fs-order-index')->with('error', 'Vui lòng chọn tập tin để tải lên!');
        }
    }

    /* get province, district, ward */
    private function findProvince($sProvince)
    {
        $aRow = DB::table('fs_province')->where('name', 'like', "%" . $sProvince . "%")->first();
        if (!isset($aRow->province_id)) {
            return '';
        }
        return $aRow->province_id;
    }

    private function findDistrict($iProvinceId, $sDistrict)
    {
        $aRow = DB::table('fs_district')
            ->where('name', 'like', $sDistrict)
            ->where('province_id', 'like', $iProvinceId)->first();
        if (!isset($aRow->district_id)) {
            return '';
        }
        return $aRow->district_id;
    }

    private function findWard($iProvinceId, $iDistrictId, $sWard)
    {
        $aRow = DB::table('fs_ward')
            ->leftJoin('fs_district', 'fs_ward.district_id', 'like', 'fs_district.district_id')
            ->where('fs_ward.name', 'like', '%' . $sWard . '%')
            ->where('fs_district.province_id', 'like', $iProvinceId)
            ->where('fs_ward.district_id', 'like', $iDistrictId)
            ->first();
        if (!isset($aRow->ward_id)) {
            return '';
        }
        return $aRow->ward_id;
    }
}
