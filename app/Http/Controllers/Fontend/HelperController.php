<?php

namespace App\Http\Controllers\Fontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class HelperController extends Controller
{
    // public function getProvince ()
    // {
    //     $aRows = DB::table('fs_province')->get(['province_id as id', 'name as text'])->toJson();
    //     return $aRows;
    // }

    public function getDistrict ($id)
    {
        $aRows = DB::table('fs_district')->where('province_id' , '=', $id)->get(['district_id as id', 'name as text'])->toJson();
        return $aRows;
    }

    public function getWard ($id)
    {
        $aRows = DB::table('fs_ward')->where('district_id' , '=', $id)->get(['ward_id as id', 'name as text'])->toJson();
        return $aRows;
    }
}
