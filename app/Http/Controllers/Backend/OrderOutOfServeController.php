<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Customer;
use Illuminate\Support\Facades\DB;

class OrderOutOfServeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aSearch = $request->input('search');
        if ($aSearch) {
            $aWhere   = [];
            $aOrWhere = [];
            if (!empty($aSearch['customer_code'])) {
                $aWhere[] = ['fs_order.customer_code', 'like', "%" . $aSearch['customer_code'] . "%"];
            }
            if (!empty($aSearch['product_name'])) {
                $aWhere[] = ['fs_order.product_name', 'like', "%" . $aSearch['product_name'] . "%"];
            }
            if (!empty($aSearch['status'])) {
                $aWhere[] = ['fs_order.status', '=', $aSearch['status']];
            }
            if (!empty($aSearch['physical_status'])) {
                $aWhere[] = ['fs_order.physical_status', '=', $aSearch['physical_status']];
            }
            if (!empty($aSearch['province_id'])) {
                $aWhere[] = ['fs_order.province_id', '=', $aSearch['province_id']];
            }
            if (!empty($aSearch['district_id'])) {
                $aWhere[] = ['fs_order.district_id', '=', $aSearch['district_id']];
            }
            if (!empty($aSearch['ward_id'])) {
                $aWhere[] = ['fs_order.ward_id', '=', $aSearch['ward_id']];
            }
            if (!empty($aSearch['from_date'])) {
                $aArray     = explode('-', $aSearch['from_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $aOrWhere[] = ['fs_order.created_at', '>=', $iTime];
            }
            if (!empty($aSearch['to_date'])) {
                $aArray     = explode('-', $aSearch['to_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $aOrWhere[] = ['fs_order.created_at', '<=', $iTime];
            }
            $aOrders = Order::where($aWhere)->orWhere($aOrWhere)->get();
        } else {
            $aOrders = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_order_physical_status.name as physical_status_name, fs_user.id as user_id, fs_user.user_name, fs_province.name as province_name, fs_district.name as district_name, fs_ward.name as ward_name, fs_order_service.name as service_name'))
            ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
            ->join('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
            ->join('fs_user', 'fs_user.id', '=', 'fs_order.user_id')
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
            ->join('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
            ->join('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
            ->join('fs_order_service', 'fs_order_service.service_id', '=', 'fs_order.service_id')
            ->where('fs_order.inside_outside','=','outside')
            ->get();
        }

        /* get order status */
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aStatus    = DB::table('fs_order_status')->get(['status_id as id', 'name as text']);
        $aPhysicalStatus    = DB::table('fs_order_physical_status')->get(['status_id as id', 'name as text']);
        $aServices    = DB::table('fs_order_service')->get(['service_id as id', 'name as text']);
        $aCustomers    = Customer::where('status', '1')->get();
        
        return view('backend.module.Unloading.OrderOutOfServe.index', compact('aProvinces','aOrders'));
    }

    public function DeliveryOutOfServe()
    {
        return view('backend.module.Unloading.OrderOutOfServe.deliver');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
