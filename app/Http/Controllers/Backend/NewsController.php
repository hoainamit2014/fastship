<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aNews = News::orderBy('new_id')->paginate(10);
        if (count($aNews) == 0) {
            $aNews = array();
        }
        return view('backend.module.news.index', ['aNews' => $aNews]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.module.news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->input('title'))) {
            return redirect()->route('news-add')->with('error', 'Vui lòng điền đủ thông tin');
        } else {
            News::create([
                'title'       => $request->input('title'),
                'alias'       => '',
                'content'     => $request->input('content'),
                'description' => $request->input('description'),
                'keyword'     => $request->input('keyword'),
                'user_id'     => 0,
                'is_active'   => $request->input('is_active'),
            ]);
            return redirect()->route('news-index')->with('success', 'Thêm tin tức thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aNew = News::where('new_id', $id)->first();
        return view('backend.module.news.add', ['aNew' => $aNew]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aUpdate = [
            'title'       => $request->input('title'),
            'alias'       => '',
            'content'     => $request->input('content'),
            'description' => $request->input('description'),
            'keyword'     => $request->input('keyword'),
            'is_active'   => $request->input('is_active'),
        ];
        News::where('new_id', '=', $id)
            ->update($aUpdate);

        return redirect()->route('news-index')->with('success', 'Sửa tin tức thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (News::where('new_id', $id)->delete()) {
            $sMessage = 'Xóa tin tức thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa tin tức thất bại';
            $sStatus  = 'error';
        }
        return redirect()->route('news-index')->with($sStatus, $sMessage);
    }
}
