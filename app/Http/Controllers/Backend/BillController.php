<?php

namespace App\Http\Controllers\Backend;

use App\Bill;
use App\BillType;
use App\Branch;
use App\Order;
use DB;
use App\Customer;
use App\Http\Controllers\Controller;
use App\User;
use App\BillDetail;
use Helpers;
use Illuminate\Http\Request;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


    public function index()
    {
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::get();
        return view('backend.module.Bill.index', compact('aBranch', 'aCharge', 'aFilType', 'aFill'));
    }

    public function responseData(Request $request)
    {   

        $data           = array();
        $iDisplayStart  = $request->input('start');
        $iDisplayLength = $request->input('length');
        $iSearch        = $request->input('search');
        $sQuery         = Bill::select('*');
        $type = $request->input('type');

        if ($type) {
            $sQuery->where('bill_type_id', $type)->get();

        }

        if ($request->input('customer_id')) {
            $sQuery->where('customer_id', $request->input('customer_id'));
        }

        if ($request->input('columns')) {

            foreach ($request->input('columns') as $k => $aVal) {
                if ($aVal['data'] == 'bill_code' && !empty($aVal['search']['value'])) {
                    $sQuery->where('bill_code', 'like', "%{$aVal['search']['value']}%");
                }
                if ($aVal['data'] == 'order_number' && !empty($aVal['search']['value']) && $aVal['search']['value'] == 1) {
                    // Number : 1 => Show order_number = 0
                    // Number : 2 => Show All
                    $sQuery->where('order_number', '=', 0);
                }


                if ($aVal['data'] == 'bill_type_id' && !empty($aVal['search']['value'])) {
                    $sQuery->where('bill_type_id', 'like', "%{$aVal['search']['value']}%");
                }
                if ($aVal['data'] == 'responsible_person_id' && !empty($aVal['search']['value'])) {
                    $sQuery->where('responsible_person_id', '=', "%{$aVal['search']['value']}%");
                }

                if ($aVal['data'] == 'status' && !empty($aVal['search']['value'])) {
                    $sQuery->where('status', '=', $aVal['search']['value']);
                }
                if ($aVal['data'] == 'received_status' && !empty($aVal['search']['value'])) {
                    // $aVal = 1   => Show order_number = 0
                    // $aVal = 2   => Show All
                    $sQuery->where('received_status', '=', intval($aVal['search']['value']));
                }

                if ($aVal['data'] == 'create_date' && !empty($aVal['search']['value'])) {
                    $explodeDate = explode('-', $aVal['search']['value']);
                    $dateFrom    = strtotime($explodeDate[0] . ' 00:00:00');
                    $dateTo      = strtotime($explodeDate[1] . ' 23:59:59');
                    $sQuery->where('create_date', '>=', $dateFrom)->where('create_date', '<=', $dateTo);
                }

                if ($aVal['data'] == 'complete_date' && !empty($aVal['search']['value'])) {
                    $exComplete       = explode('-', $aVal['search']['value']);
                    $dateFromComplete = strtotime($exComplete[0] . ' 00:00:00');
                    $dateToComplete   = strtotime($exComplete[1] . ' 23:59:59');
                    $sQuery->where('complete_date', '>=', $dateFromComplete)->where('complete_date', '<=', $dateToComplete);
                }

            }
        }

        // Sort column
        $aColumns = array('id', 'bill_code', 'order_number', 'bill_type_id', 'responsible_person_id', 'status', 'received_status', 'author', 'create_date', 'complete_date');
        // echo $request->input('order')[0]['dir'];

        $sQuery->orderBy($aColumns[$request->input('order')[0]['column']], $request->input('order')[0]['dir']);
        
        $aTotalRecord = $sQuery->count();
        $aRows        = $sQuery->offset($iDisplayStart)->limit($iDisplayLength)->get();
        $aData        = array();
        foreach ($aRows as $k => $aVal) {
            $aItem = array(
                'id'                    => $aVal['id'],
                'bill_code'             => $aVal['bill_code'],
                'order_number'          => $aVal['order_number'],
                'bill_type_id'          => Helpers::getFillTypeName($aVal['bill_type_id']),
                'responsible_person_id' => !empty(Helpers::getAuthNameByID($aVal['responsible_person_id'])->full_name) ? Helpers::getAuthNameByID($aVal['responsible_person_id'])->full_name : '',
                'status'                => $aVal['status'],
                'received_status'       => $aVal['received_status'],
                'author'                => Helpers::getAuthNameByID($aVal['author'])->full_name,
                'create_date'           => date('d/m/Y h:i:s', $aVal['create_date']),
                'complete_date'         => date('d/m/Y h:i:s', $aVal['complete_date']),
            );
            array_push($aData, $aItem);
        }
        $results = array(
            "iTotalRecords"        => count($data),
            "iTotalDisplayRecords" => $aTotalRecord,
            "aaData"               => $aData,
        );

        return json_encode($results);
    }

    public function _getAuthor($id){
        return User::find($id);
    }

    public function AjaxDeployOrders(Request $request) {

        $sOrder = array();
        if (!empty($request->input('billCode'))) {
            $pillCode = $request->input('billCode');
            $sQuery = Bill::leftJoin('fs_bill_detail', 'fs_bill_detail.bill_code', '=', 'fs_bill.bill_code')
            ->where('fs_bill.bill_code', '=', $pillCode)
            ->first()->toArray();


            if ($sQuery) {
                $sOrder = Order::select(DB::raw('fs_order.* ,fs_user.full_name as customer_name , fs_order_physical_status.name as physical_status , fs_province.name as province_name ,fs_district.name as district_name ,fs_ward.name as ward_name , fs_warehouse.name as warehouse_name,fs_bill_type.name as bill_type_name'))
                ->leftJoin('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
                ->leftJoin('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
                ->leftJoin('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
                ->leftJoin('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
                ->leftJoin('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
                ->leftJoin('fs_user', 'fs_user.customer_code', '=', 'fs_order.customer_code')
                ->leftJoin('fs_warehouse', 'fs_warehouse.id', '=', 'fs_order.warehouse_id')
                ->leftJoin('fs_bill_type', 'fs_bill_type.id', '=', 'fs_order.order_type')


                ->whereIn('order_code', unserialize($sQuery['order_id']))
                ->get()
                ->toArray();
            }
        }
        
        if($sOrder){
            $numberOrderSuccess = 0;
            foreach($sOrder as $k => $aVal)
            {
                $sOrder[$k] = $aVal;
                $sOrder[$k]['gift_1'] = '';
                $sOrder[$k]['gift_2'] = '';
                if(json_decode($aVal['gift'])){
                    foreach(json_decode($aVal['gift']) as $kGift => $aGift)
                    {
                        $sOrder[$k]['gift_'.($kGift + 1)] = $aGift;

                    }
                }
                if($aVal['status'] == 7){
                    $numberOrderSuccess++;
                }
            }
        }
        if($sQuery['status'] == 2){
            $status_pickup = 'Đang xử lý';
        }elseif($sQuery['status'] == 3){
            $status_pickup = 'Đã xử lý';
        }else{
            $status_pickup = 'Tạo mới';
        }


        $aData = array(
            'bill_status' => !empty($sQuery['complete_date']) ? 'Đang xử lý' : 'Đã xử lý',
            'status_pickup' => $status_pickup,
            'received_status' => ($sQuery['received_status'] == 1) ? 'Đã nhận hàng' : 'Chưa nhận hàng',
            
            'data' => $sOrder, 
            'charge' => $this->_getAuthor($sQuery['responsible_person_id'])->full_name,
            'author' => $this->_getAuthor($sQuery['author'])->full_name,   
            'total' => count($sOrder),
            'orderSuccess' => $numberOrderSuccess
        );
        
        return json_encode($aData);

    }

    public function PrintBill(Request $request)
    {
        $billCode = $request->all();
        $bill = Bill::leftjoin('fs_bill_detail','fs_bill_detail.bill_code','=','fs_bill.bill_code')->where('fs_bill_detail.bill_code','=',$billCode)->first()->toArray();
        if(unserialize($bill['order_id'])){
            foreach(unserialize($bill['order_id']) as $k => $aVal){
                $orders[] = Order::where('order_code','=',$aVal)->first();
            }
        }
        $data = array(
            'bill' => $bill,
            'orders' => $orders
        );

        return view('backend.module.BIll.print-bill',['data' => $data]);
    }

    public function PrintBillRecevied(Request $request)
    {
        $billCode = $request->all();
        $bill = Bill::leftjoin('fs_bill_detail','fs_bill_detail.bill_code','=','fs_bill.bill_code')->where('fs_bill_detail.bill_code','=',$billCode)->first()->toArray();
        if(unserialize($bill['order_id'])){
            foreach(unserialize($bill['order_id']) as $k => $aVal){
                $order = Order::where('order_code','=',$aVal)->where('physical_status',3)->first();
                if($order){
                    $orders[] = $order;
                }
                
            }
        }
        $data = array(
            'bill' => $bill,
            'orders' => $orders
        );
        
        return view('backend.module.BIll.print-bill',['data' => $data]);
    }
    

    public function UpdateReciveGoodsSuccess(Request $request)
    {
        $billCode = $request->input('billCode'); 
        $updateBill = Bill::where('bill_code','=',$billCode)->update(['received_status' => 1]);

        if($updateBill){
            $billDetail = BillDetail::where('bill_code' , '=' , $billCode)->get()->first();
            if($billDetail){
                foreach(unserialize($billDetail['order_id']) as $k => $aVal)
                {
                    Order::where('order_code','=',$aVal)->update(['physical_status' => 3]);
                }
            }
            return redirect()->back()->with('success', 'Nhận hàng thành công');

        }else{
            return redirect()->back()->with('error', 'Hàng đã được nhận rồi.');

        }
    }

    public function Pickup()
    {
        $aBranch = Branch::get();
        $aCharge = User::select('id', 'full_name')->get();
        return view('backend.module.Unloading.Bill.Pickup.index', compact('aBranch', 'aCharge'));
    }

    public function PickupGoodFromCustomer(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        return view('backend.module.Bill.relative',[
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'aFilType' => $aFilType,
            'type' => 1,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Phiếu Pickup hàng từ đối tác'
        ]);
    }

    public function PickupGoodFromOrderRecover(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();

        return view('backend.module.Bill.relative',[
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'aFilType' => $aFilType,
            'type' => 2,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Phiếu Pickup đơn hàng thu hồi'

        ]);
    }
    public function BillReceiveGoodsFromPickupBill(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','3')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 3,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Phiếu nhận hàng từ phiếu pickup'

        ]);
    }
    public function BillReceiveGoodsFromPickupBills(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','4')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 4,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Phiếu nhận hàng từ nhiều phiếu pickup'

        ]);
    }
    public function LoadingToWarehouseFromCustomer(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','5')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 5,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Nhập hàng vào kho từ khách hàng'

        ]);
    }
    public function BillTransferToDCOther(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','6')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 6,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Phiếu chuyển hàng tới DC khác'

        ]);
    }
    public function BillLoadingToWareHouseFromDCOther(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','7')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 7,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Phiếu nhập hàng vào kho từ DC khác'

        ]);
    }
    public function BillDeployDelivery(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','8')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 8,
            'routeCurrent' => 'bill-pickup-good-from-customer',
            'aTypeName' => 'Phiếu triển khai giao hàng'

        ]);
    }
    public function BillDelivery(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','9')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 9,
            'aTypeName' => 'Phiếu giao hàng',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillReceiveGoodsFromDeliveryBill(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','10')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 10,
            'aTypeName' => 'Phiếu nhận hàng từ phiếu giao hàng',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillReceiveTakeMoneyFromDeliveryBill(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','11')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 11,
            'aTypeName' => 'Phiếu nhận tiền từ phiếu giao hàng',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillDeliveryOutOfServe(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','12')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 12,
            'aTypeName' => 'Phiếu giao hàng ngoại tuyến',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillReceiveGoodsOutOfServe(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','13')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 13,
            'aTypeName' => 'Phiếu nhận hàng đơn ngoại tuyến',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillReceiveTakeMoneyOutOfServe(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','14')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 14,
            'aTypeName' => 'Phiếu nhận tiền đơn ngoại tuyến',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillDeployToCustomer(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','15')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 15,
            'aTypeName' => 'Phiếu triển khai trả về đối tác',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillReturnGoodsForCustomer(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','16')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 16,
            'aTypeName' => 'Phiếu trả hàng về cho đối tác',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillReceiveGoodsFromReturnBill(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','17')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 17,
            'aTypeName' => 'Phiếu nhận hàng từ phiếu trả về',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }
    public function BillCheckWareHouse(Request $request){
        $aBranch  = Branch::get();
        $aCharge  = User::select('id', 'full_name')->get();
        $aFilType = BillType::get();
        $aFill    = Bill::where('id','=','18')->get();

        return view('backend.module.Bill.relative',[
            'aFill' => $aFill,
            'aBranch' => $aBranch,
            'aCharge' => $aCharge,
            'type' => 18,
            'aTypeName' => 'Phiếu kiểm kho',
            'routeCurrent' => 'bill-pickup-good-from-customer',
        ]);
    }

    

}
