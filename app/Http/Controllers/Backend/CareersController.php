<?php

namespace App\Http\Controllers\Backend;

use App\Careers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CareersController extends Controller
{
    public function __construct()
    {
        $this->prefix = 'fs_';
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aCareers = Careers::orderBy('career_id')->paginate(10);
        if (count($aCareers) == 0) {
            $aCareers = array();
        }
        return view('backend.module.careers.index', ['aCareers' => $aCareers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.module.careers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->input('title'))) {
            return redirect()->route('careers-add')->with('error', 'Vui lòng điền đủ thông tin');
        } else {
            Careers::create([
                'title'       => $request->input('title'),
                'alias'       => '',
                'content'     => $request->input('content'),
                'description' => $request->input('description'),
                'keyword'     => $request->input('keyword'),
                'expire_date' => $request->input('expire_date'),
                'user_id'     => null,
                'is_active'   => $request->input('is_active'),
            ]);
            return redirect()->route('careers-index')->with('success', 'Thêm tin tuyển dụng thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aCareer = Careers::where('career_id', $id)->first();
        return view('backend.module.careers.add', ['aCareer' => $aCareer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aUpdate = [
            'title'       => $request->input('title'),
            'alias'       => '',
            'content'     => $request->input('content'),
            'description' => $request->input('description'),
            'keyword'     => $request->input('keyword'),
            'expire_date' => $request->input('expire_date'),
            'user_id'     => null,
            'is_active'   => $request->input('is_active'),
        ];
        Careers::where('career_id', '=', $id)
            ->update($aUpdate);

        return redirect()->route('careers-index')->with('success', 'Sửa tin tuyển dụng thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Careers::where('career_id', $id)->delete()) {
            $sMessage = 'Xóa tin tuyển dụng thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa tin tuyển dụng thất bại';
            $sStatus  = 'error';
        }
        return redirect()->route('careers-index')->with($sStatus, $sMessage);
    }
}
