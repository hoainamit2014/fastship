<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Service;
use App\Branch;
use App\WareHouse;
use Excel;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aStatus    = DB::table('fs_order_status')->get(['status_id as id', 'name as text']);
        $aPhysicalStatus    = DB::table('fs_order_physical_status')->get(['status_id as id', 'name as text']);
        $aCustomers    = User::where('status', '1')->where('user_group_id', '7')->get();
        $aDC    = Branch::select(Db::raw('fs_branch.id, fs_province.name'))->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')->get();

        return view('backend.module.order.index', ['aStatus' => $aStatus, 'aProvinces' => $aProvinces,'aPhysicalStatus' => $aPhysicalStatus, 'aCustomers' => $aCustomers, 'aDC' => $aDC]);
    }

    public function search (Request $request)
    {
        $iDisplayStart  = $request->input('start');
        $iDisplayLength = $request->input('length');
        $iSearch        = $request->input('search');

        $sQuery = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_order_physical_status.name as physical_status_name, fs_user.id as user_id, fs_user.user_name, fs_province.name as province_name, fs_district.name as district_name, fs_ward.name as ward_name, fs_order_service.name as service_name, fs_order.gift, fs_order.save_to_dc_date'))
        ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
        ->join('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
        ->join('fs_user', 'fs_user.id', '=', 'fs_order.user_id')
        ->leftJoin('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
        ->leftJoin('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
        ->leftJoin('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
        ->join('fs_order_service', 'fs_order_service.service_id', '=', 'fs_order.service_id');
        
        if ($aSearch = $request->input('customsearch')) {
            if (!empty($aSearch['product_name'])) {
                $sQuery->where('fs_order.product_name', 'like', "%" . $aSearch['product_name'] . "%");
            }
            if (!empty($aSearch['customer_code'])) {
                $sQuery->where('fs_order.customer_code', 'like', "%" . $aSearch['customer_code'] . "%");
            }
            if (!empty($aSearch['status'])) {
                $sQuery->where('fs_order.status', '=', $aSearch['status']);
            }
            if (!empty($aSearch['physical_status'])) {
                $sQuery->where('fs_order.physical_status', '=', $aSearch['physical_status']);
            }
            if (!empty($aSearch['province_id'])) {
                $sQuery->where('fs_order.province_id', '=', $aSearch['province_id']);
            }
            if (!empty($aSearch['district_id'])) {
                $sQuery->where('fs_order.district_id', '=', $aSearch['district_id']);
            }
            if (!empty($aSearch['ward_id'])) {
                $sQuery->where('fs_order.ward_id', '=', $aSearch['ward_id']);
            }
            if (!empty($aSearch['from_date'])) {
                $aArray     = explode('-', $aSearch['from_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $sQuery->where('fs_order.created_at', '>=', $iTime);
            }
            if (!empty($aSearch['to_date'])) {
                $aArray     = explode('-', $aSearch['to_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $sQuery->where('fs_order.created_at', '<=', $iTime);
            }
            if (!empty($aSearch['delivery_date'])) {
                $aArray     = explode('-', $aSearch['delivery_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $sQuery->where('fs_order.delivery_date', '=', $aSearch['delivery_date']);
            }
            if (!empty($aSearch['save_to_dc_date'])) {
                $aArray     = explode('-', $aSearch['save_to_dc_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $sQuery->where('fs_order.save_to_dc_date', '=', $aSearch['save_to_dc_date']);
            }
            if (!empty($aSearch['delivery_success_date'])) {
                $aArray     = explode('-', $aSearch['delivery_success_date']);
                $sFrom      = strtotime($aArray[2] . '-' . $aArray[1] . '-' . $aArray[0]);
                $iTime      = date('Y-m-d', $sFrom);
                $sQuery->where('fs_order.delivery_success_date', '=', $aSearch['delivery_success_date']);
            }
            if (!empty($aSearch['flow_order'])) {
                $sQuery->where('fs_order.flow_order', '=', $aSearch['flow_order']);
            }
            if (!empty($aSearch['bigger_d'])) {
                $sQuery->where('fs_order.save_to_dc_date', '<=', date('Y-m-d', strtotime("-'".$aSearch['bigger_d']."' days")));
            }
            if (!empty($aSearch['smaller_d'])) {
                $sQuery->where('fs_order.save_to_dc_date', '>=', date('Y-m-d', strtotime("-'".$aSearch['smaller_d']."' days")));
            }
            if (!empty($aSearch['note'])) {
                if ($aSearch['note'] == 1) {// chua ghi chu
                    $sQuery->where('fs_order.note', '=', null);
                } elseif ($aSearch['note'] == 2) {// da ghi chu
                    $sQuery->where('fs_order.note', '!=', null);
                }
            }
            if (!empty($aSearch['weight'])) {
                if ($aSearch['weight'] == 1) {
                    $sQuery->where('fs_order.weight', '=', 0);
                } elseif ($aSearch['weight'] == 2) {
                    $sQuery->where('fs_order.weight', '!=', 0);
                }
            }
            if (!empty($aSearch['status_service'])) {
                if ($aSearch['status_service'] == 1) {
                    $sQuery->where('fs_order.service_id', '=', 0);
                } elseif ($aSearch['status_service'] == 2) {
                    $sQuery->where('fs_order.service_id', '!=', 0);
                }
            }
            if (!empty($aSearch['status_appointment'])) {
                if ($aSearch['status_appointment'] == 1) {
                    $sQuery->where('fs_order.appointment_delivery', '=', null);
                } elseif ($aSearch['status_appointment'] == 2) {
                    $sQuery->where('fs_order.appointment_delivery', '!=', null);
                }
            }
            if (!empty($aSearch['inside_outside'])) {
                $sQuery->where('fs_order.inside_outside', '=', $aSearch['inside_outside']);
            }
            if (!empty($aSearch['service_id'])) {
                $sQuery->where('fs_order.service_id', '=', $aSearch['service_id']);
            }
            if (!empty($aSearch['order_difficult_delivery'])) {
                $sQuery->where('fs_order.order_difficult_delivery', '=', 1);
            }
        }

        $aColumns = array('', 'fs_order.order_id', 'fs_order.order_code', 'fs_order.customer_order_code', 'province_name', 'district_name', 'ward_name', 'fs_order.address', 'fs_order.receiver_name', 'fs_order.phone', 'fs_order.product_name', 'fs_order.gift', 'fs_order.cod', 'fs_order.updated_at', 'fs_order.reason', 'status_name', 'physical_status_name', '', 'fs_order.note', '', '', 'user_name', '');
        if (!empty($aColumns[$request->input('order')[0]['column']])) {
            $sQuery->orderBy($aColumns[$request->input('order')[0]['column']], $request->input('order')[0]['dir']);
        }

        $aTotalRecord = $sQuery->count();
        $aRows        = $sQuery->offset($iDisplayStart)->limit($iDisplayLength)->get();

        foreach ($aRows as $iKey => $aValue) {
            $aRows[$iKey]['cod'] = number_format($aValue['cod']);
            $aRows[$iKey]['d+'] = '';
        }

        $results = array(
            "iTotalRecords"        => count($aRows),
            "iTotalDisplayRecords" => $aTotalRecord,
            "aaData"               => $aRows,
        );

        return json_encode($results);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aStatus    = DB::table('fs_order_status')->get(['status_id as id', 'name as text']);
        $aPhysicalStatus    = DB::table('fs_order_physical_status')->get(['status_id as id', 'name as text']);
        $aServices    = DB::table('fs_order_service')->get(['service_id as id', 'name as text']);
        $aCustomers    = User::where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.order.add', ['aProvinces' => $aProvinces, 'aStatus' => $aStatus, 'aPhysicalStatus' => $aPhysicalStatus, 'aServices' => $aServices, 'aCustomers' => $aCustomers]);
    }

    public function distance ($lat1, $lon1, $lat2, $lon2)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
      
        return  $miles * 1.609344;
    }

    public function getWarehouse ($iCustomerCode)
    {
        $aUser = WareHouse::select(Db::raw('fs_warehouse.address'))
            ->leftJoin('fs_user', 'fs_user.id', '=', 'fs_warehouse.user_id')->where('fs_user.customer_code', '=', $iCustomerCode)->first();
        return $aUser->address;
    }

    public function getCoordinate ($address)
    {
        $sGoogleApiKey = 'AIzaSyBVt1Fk48S7svJan6bjV_ztcXWbYCBWymQ';
        $sAddress = urlencode($address);
        $sAPi = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$sAddress.'&key='.$sGoogleApiKey;
        
        $sResult = file_get_contents($sAPi);dd($sResult);
        $sResult = json_decode($sResult, true);
        if ($sResult['status'] == 'OK' && $sResult['results'][0]) {
            $aData = array(
                'latitude' => $sResult['results'][0]['geometry']['location']['lat'],
                'longitude' => $sResult['results'][0]['geometry']['location']['lng'],
            );
            return $aData;
        }
        return false;
    }

    public function store(OrderRequest $request)
    {
        // tinh gia don hang
        $noithanh = [
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', 'Bình Thạnh', 'Gò Vấp', 'Phú Nhuận', 'Tân Bình', 'Tân Phú'
        ];
        $ngoaithanh = [
            '9', '12', 'Thủ Đức', 'Bình Tân'
        ];
        $ngoaithanh2 = [
            'Nhà Bè', 'Bình Chánh', 'Hóc Môn', 'Củ Chi'
        ];
        $sStartAddress = $this->getWarehouse($request->customer_code);
        $sEndAddress = $request->address;

        if ($request->province_id == 79) { // chuyen phat TPHCM
            $request->service_id = 1;
            $iPriceCal = 0;
            if (in_array($request->district_name, $noithanh)) { // noi thanh
                switch ($request->district_name) {
                    case 'QUANGAY24H':
                    // tinh gia theo trong luong
                        $iWeightCal = $request->length * $request->width * $request->height / 5;
                        if ($iWeightCal <= 3000) {
                            $iPriceCal = 20000;
                        } else {
                            $iPriceCal = ($iWeightCal - 3000) * 2000 / 500;
                        }
                        break;
                    case 'QUANGAY6H':
                        // tinh gia theo so km
                        // lay toa do cua kho hang
                        $arrayStart = $this->getCoordinate($sStartAddress);
                        // lay toa do cua diem lay hang
                        $arrayEnd = $this->getCoordinate($sEndAddress);
                        // tinh khoang giua 2 toa do
                        if ($arrayStart && $arrayEnd) {
                            $sKm = $this->distance($arrayStart['latitude'], $arrayStart['longitude'], $arrayStart['latitude'], $arrayStart['longitude']);
                            if ($sKm <= 5) {
                                $iPriceCal = 25000;
                            } else {
                                $iPriceCal = ($sKm - 5) * 2000 / 0.5;
                            }
                        }
                        break;
                }
            }
            if (in_array($request->district_name, $ngoaithanh)) { // ngoai thanh
                switch ($request->district_name) {
                    case 'QUANGAY24H':
                    // tinh gia theo trong luong
                        $iWeightCal = $request->length * $request->width * $request->height / 5;
                        if ($iWeightCal <= 3000) {
                            $iPriceCal = 30000;
                        } else {
                            $iPriceCal = ($iWeightCal - 3000) * 2500 / 500;
                        }
                        break;
                    case 'QUANGAY6H':
                        // tinh gia theo so km
                        // lay toa do cua kho hang
                        $arrayStart = $this->getCoordinate($sStartAddress);
                        // lay toa do cua diem lay hang
                        $arrayEnd = $this->getCoordinate($sEndAddress);
                        // tinh khoang giua 2 toa do
                        if ($arrayStart && $arrayEnd) {
                            $sKm = $this->distance($arrayStart['latitude'], $arrayStart['longitude'], $arrayStart['latitude'], $arrayStart['longitude']);
                            if ($sKm <= 5) {
                                $iPriceCal = 35000;
                            } else {
                                $iPriceCal = ($sKm - 5) * 2500 / 0.5;
                            }
                        }
                        break;
                }
            }
            if (in_array($request->district_name, $ngoaithanh2)) { // ngoai thanh 2
                // tinh gia theo trong luong
                $iWeightCal = $request->length * $request->width * $request->height / 5;
                if ($iWeightCal <= 3000) {
                    $iPriceCal = 40000;
                } else {
                    $iPriceCal = ($iWeightCal - 3000) * 3000 / 500;
                }
            }
        } else { // chuyen phat ngoai thanh
            $iWeightCal = $request->length * $request->width * $request->height / 5;
            if ($request->service_id == 'lien_tinh') { // lien tinh
                $request->service_id = 2;
                if ($iWeightCal <= 500) {
                    $iPriceCal = 25000;
                } elseif ($iWeightCal > 500 && $iWeightCal <= 1000) {
                    $iPriceCal = 35000;
                } elseif ($iWeightCal > 1000 && $iWeightCal <= 1500) {
                    $iPriceCal = 45000;
                } elseif ($iWeightCal > 1500 && $iWeightCal <= 2000) {
                    $iPriceCal = 55000;
                } else {
                    $iPriceCal = ($iWeightCal - 2000) * 25000 / 500;
                }
            } else { // tiet kiem
                $request->service_id = 3;
                if ($iWeightCal <= 3000) {
                    $iPriceCal = 25000;
                } elseif ($iWeightCal > 3000 && $iWeightCal <= 30000) {
                    $iPriceCal = 35000;
                } elseif ($iWeightCal > 30000 && $iWeightCal <= 200000) {
                    $iPriceCal = 45000;
                } elseif ($iWeightCal > 200000 && $iWeightCal <= 500000) {
                    $iPriceCal = 55000;
                } else {
                    $iPriceCal = ($iWeightCal - 2000) * 25000 / 500;
                }
            }
        }

        $aOrder     = new Order();
        $iTemp      = DB::table('fs_order')->max('order_id');
        $iOrderCode = $iTemp ? $iTemp + 1 : 1;

        $aOrder->user_id            = Auth::id();
        $aOrder->customer_code      = $request->customer_code;
        $aOrder->order_code         = 'VL' . date('d') . date('m') . date('Y') . $iOrderCode;
        $aOrder->receiver_name      = $request->receiver_name;
        $aOrder->phone              = $request->phone;
        $aOrder->address            = $request->address;
        $aOrder->province_id        = $request->province_id;
        $aOrder->district_id        = $request->district_id;
        $aOrder->ward_id            = $request->ward_id;
        $aOrder->product_name       = $request->product_name;
        $aOrder->weight             = $request->weight;
        $aOrder->length             = $request->length;
        $aOrder->width              = $request->width;
        $aOrder->height             = $request->height;
        $aOrder->cod                = $request->cod;
        $aOrder->note               = $request->note;
        $aOrder->transport_fee      = $iPriceCal;
        $aOrder->service_id         = $request->service_id;
        $aOrder->receiver_purchase_fee            = isset($request->receiver_purchase_fee) ? 1 : 0;
        $aOrder->status             = 1;
        $aOrder->physical_status    = 1;
        $aOrder->order_type    = 1;
        if ($aOrder->save()) {
            return redirect()->route('order-index')->with('success', 'Thêm đơn hàng thành công');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aOrder     = Order::where('order_id', $id)->first();
        $aProvinces = DB::table('fs_province')->get(['province_id as id', 'name as text']);
        $aDistricts = DB::table('fs_district')->where('province_id', '=', $aOrder->province_id)->get(['district_id as id', 'name as text']);
        $aWards     = DB::table('fs_ward')->where('district_id', '=', $aOrder->district_id)->get(['ward_id as id', 'name as text']);
        $aStatus    = DB::table('fs_order_status')->get(['status_id as id', 'name as text']);
        $aPhysicalStatus    = DB::table('fs_order_physical_status')->get(['status_id as id', 'name as text']);
        $aServices    = DB::table('fs_order_service')->get(['service_id as id', 'name as text']);
        $aCustomers    = User::where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.order.add', ['aOrder' => $aOrder, 'aProvinces' => $aProvinces, 'aDistricts' => $aDistricts, 'aWards' => $aWards, 'aStatus' => $aStatus, 'aPhysicalStatus' => $aPhysicalStatus, 'aServices' => $aServices, 'aCustomers' => $aCustomers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        $aUpdate = [
            'customer_code'      => $request->customer_code,
            'receiver_name'      => $request->receiver_name,
            'phone'              => $request->phone,
            'address'            => $request->address,
            'province_id'        => $request->province_id,
            'district_id'        => $request->district_id,
            'ward_id'            => $request->ward_id,
            'product_name'       => $request->product_name,
            'weight'             => $request->weight,
            'length'             => $request->length,
            'width'              => $request->width,
            'height'             => $request->height,
            'cod'                => $request->cod,
            'note'               => $request->note,
            'transport_fee'      => $request->transport_fee,
            'service_id'         => $request->service_id,
            'receiver_purchase_fee'            => isset($request->receiver_purchase_fee) ? 1 : 0,
            'status'             => $request->status,
            'physical_status'    => $request->physical_status
        ];
        
        if (Order::where('order_id', '=', $id)->update($aUpdate)) {
            return redirect()->route('order-index')->with('success', 'Cập nhật đơn hàng thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Order::where('order_id', $id)->delete()) {
            $sMessage = 'Xóa vận đơn thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa vận đơn thất bại';
            $sStatus  = 'error';
        }
        return redirect()->route('order-index')->with($sStatus, $sMessage);
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file')) {
            $sPath = $request->file->getRealPath();
            $aRows = Excel::skip(2)->load($sPath, function ($reader) {
                $reader->ignoreEmpty();
            })->get()->toArray();
            if (!empty($aRows)) {
                $iTemp      = DB::table('fs_order')->where(DB::raw("DATE(created_at) = '".date('Y-m-d')."'"))->max('order_id');
                if (!$iTemp) {
                        $iOrderCode = 1;
                } else {
                    $iOrderCode = $iTemp;
                }
                
                foreach ($aRows[0] as $iKey => $aRow) {
                    // kiem tra neu 1 dong rong
                    if (empty($aRow['receiver_name']) && empty($aRow['address']) && empty($aRow['province_name']) && empty($aRow['district_name']) && empty($aRow['product_name']) && empty($aRow['cod'])) {
                        continue;
                    }
                    $sKey = (string)($iKey + 1);
                    $aUser    = User::where('status', '1')->where('user_group_id', '7')->where('customer_code', $aRow['customer_code'])->first();
                    if (!$aUser) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, người dùng ngày không thể tạo đơn hàng");
                    }
                    // kiem tra tung field
                    if (!$aRow['receiver_name']) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập tên người nhận");
                    }
                    if (!$aRow['address']) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập địa chỉ");
                    }
                    if ((int)$aRow['order_type'] < 0 || (int)$aRow['order_type'] > 3) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng chọn loại đơn hàng");
                    }
                    if (!$aRow['product_name']) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập tên sản phẩm");
                    }
                    if ((int)$aRow['cod'] < 0) {
                        return redirect()->route('fs-order-index')->with('error', "Lỗi dòng STT {$sKey}, tiền thu hộ phải lớn hơn 0");
                    }
                    if ((int)$aRow['weight'] < 0) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT .Cân nặng phải lớn hơn 0");
                    }
                    if ((int)$aRow['receiver_purchase_fee'] < 0 || (int)$aRow['receiver_purchase_fee'] > 1) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng chọn người trả phí vận chuyển");
                    }
                    if (!$aRow['province_name'] || !$aRow['district_name']) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập tên tỉnh/thành phố, quận/huyện");
                    }
                    $province_id = $this->findProvince($aRow['province_name']);
                    $district_id = $this->findDistrict($province_id, $aRow['district_name']);
                    if (!$province_id || !$district_id) {
                        return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, vui lòng nhập đúng tên tỉnh/thành phố, quận/huyện");
                    }

                    if (!empty($aRow['customer_order_code'])) {
                        if ($this->checkOrderCodeExist($aRow['customer_order_code'])) {
                            return redirect()->route('order-index')->with('error', "Lỗi dòng STT {$sKey}, mã đơn hàng đã tồn tại");
                        }
                    }

                    $aInsert = [
                        'user_id'           => Auth::id(),
                        'customer_code'     => $aUser['customer_code'],
                        'order_code'        => 'U' . date('d') . date('m') . date('Y') . $iOrderCode,
                        'customer_order_code' => isset($aRow['customer_order_code']) ? $aRow['customer_order_code'] : null,
                        'receiver_name'     => (string)$aRow['receiver_name'],
                        'phone'             => substr(trim($aRow['phone']), 0, 20),
                        'address'           => (string)$aRow['address'],
                        'province_id'       => (string)$province_id,
                        'district_id'       => (string)$district_id,
                        'product_name'      => (string)$aRow['product_name'],
                        'gift'      => !empty($aRow['gif']) ? $aRow['gif'] : null,
                        'weight'            => isset($aRow['weight']) ? $aRow['weight'] : 0,
                        'length'            => isset($aRow['length']) ? $aRow['length'] : 0,
                        'width'            => isset($aRow['width']) ? $aRow['width'] : 0,
                        'height'            => isset($aRow['height']) ? $aRow['height'] : 0,
                        'cod'               => (int)$aRow['cod'],
                        'note'              => !empty($aRow['note']) ? $aRow['note'] : null,
                        'transport_fee'     => $aRow['transport_fee'],
                        'service_id'        => 1,
                        'receiver_purchase_fee'       => $aRow['receiver_purchase_fee'],
                        'status'            => 1,
                        'physical_status'   => 1,
                        'is_import'         => 1,
                    ];
                    $iOrderCode++;
                    $aOrders[] = $aInsert;
                }
                if (!empty($aOrders)) {
                    foreach ($aOrders as $value) {
                        $aOrder = Order::create($value);
                    }
                    return redirect()->route('order-index')->with('success', 'Tạo vận đơn thành công');
                }
            }
        } else {
            return redirect()->route('order-index')->with('error', 'Vui lòng chọn tập tin để tải lên!');
        }
    }

    public function export ()
    {
        if (isset($_GET['list_order_id'])) {
            $aArray = explode(',', $_GET['list_order_id']);
            $aArray = array_map(function($ele) {
                return trim($ele);
            }, $aArray);
            $aArray = array_unique($aArray);
            foreach ($aArray as $iKey => $value) {
                $aRow = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_order_physical_status.name as physical_status_name, fs_user.id as user_id, fs_user.user_name, fs_province.name as province_name, fs_district.name as district_name, fs_ward.name as ward_name, fs_order_service.name as service_name'))
                ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
                ->join('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
                ->join('fs_user', 'fs_user.id', '=', 'fs_order.user_id')
                ->join('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
                ->join('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
                ->join('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
                ->join('fs_order_service', 'fs_order_service.service_id', '=', 'fs_order.service_id')
                ->where('fs_order.order_code', 'like', $value)
                ->first();
                $aRow && $aLabels[] = $aRow;
            }
            if (isset($aLabels)) {
                $aLabels = $this->dataToExport($aLabels);
                Excel::create('orders', function($excel) use ($aLabels) {
                    $excel->sheet('sheet1', function($sheet) use ($aLabels) {
                        $sheet->fromArray($aLabels);
                    });

                })->download('xlsx');
            } else {
                return redirect()->route('order-export')->with('error', 'Vui lòng nhập đúng mã đơn hàng');
            }
        }
        return view('backend.module.order.export');
    }

    /* show order detail */
    public function show ()
    {
        if (isset($_GET['order_code'])) {
            $aRow = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_order_physical_status.name as physical_status_name, fs_user.id as user_id, fs_user.user_name, fs_province.name as province_name, fs_district.name as district_name, fs_ward.name as ward_name, fs_order_service.name as service_name'))
            ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
            ->join('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
            ->join('fs_user', 'fs_user.id', '=', 'fs_order.user_id')
            ->leftJoin('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
            ->leftJoin('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
            ->leftJoin('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
            ->join('fs_order_service', 'fs_order_service.service_id', '=', 'fs_order.service_id')
            ->where('fs_order.order_code', 'like', $_GET['order_code'])
            ->orWhere('fs_order.customer_order_code', 'like', $_GET['order_code'])
            ->first();
            
            if ($aRow) {
                return view('backend.module.order.show', ['aRow' => $aRow]);
            } else {
                return redirect()->route('order-show')->with('error', 'Vui lòng nhập đúng mã đơn hàng');
            }
        }
        return view('backend.module.order.show');
    }

    /* print label */
    public function printlabel ()
    {
        return view('backend.module.order.printlabel');
    }

    public function label ()
    {
        if (isset($_GET['order_code'])) {
            $aArray = explode(',', $_GET['order_code']);
            $aArray = array_map(function($ele) {
                return trim($ele);
            }, $aArray);
            $aArray = array_unique($aArray);
            foreach ($aArray as $iKey => $value) {
                $aRow = Order::select(DB::raw('fs_order.*, fs_user.user_name'))
                ->where('order_code', 'like', $value)
                ->join('fs_user', 'fs_user.id', '=', 'fs_order.user_id')
                ->first();
                $aRow && $aLabels[] = $aRow;
            }
            if (isset($aLabels)) {
                if (isset($_GET['print_label'])) {
                    return view('backend.module.order.label', ['aLabels' => $aLabels, 'bLabel' => true]);
                } else {
                    return view('backend.module.order.label', ['aLabels' => $aLabels]);
                }
            } else {
                return redirect()->route('order-print-label')->with('error', 'Vui lòng nhập đúng mã đơn hàng');
            }
        }
    }

    /* print order */
    public function print ($id)
    {
        $aRow = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_order_physical_status.name as physical_status_name, fs_user.id as user_id, fs_user.user_name, fs_province.name as province_name, fs_district.name as district_name, fs_ward.name as ward_name, fs_order_service.name as service_name'))
            ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
            ->join('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
            ->join('fs_user', 'fs_user.id', '=', 'fs_order.user_id')
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
            ->join('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
            ->join('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
            ->join('fs_order_service', 'fs_order_service.service_id', '=', 'fs_order.service_id')
            ->where('fs_order.order_id', '=', $id)
            ->first();
        return view('backend.module.order.print', ['aRow' => $aRow]);
    }

    /* config order price and */
    public function setting()
    {
        $aRows = Service::get();
        return view('backend.module.order.setting', ['aRows' => $aRows]);
    }

    public function storeService(Request $request)
    {
        $aInsert = [
            'name'     => $request->name,
            'number'   => (int)$request->number
        ];
        $aOrder = Service::create($aInsert);
        if ($aOrder) {
            return redirect()->route('order-setting')->with('success', 'Tạo dịch vụ thành công');
        }
        return redirect()->route('order-setting')->with('error', 'Tạo dịch vụ thất bại');
    }

    public function destroyService($id)
    {
        if (DB::table('fs_order_service')->where('service_id', $id)->delete()) {
            $sMessage = 'Xóa gói dịch vụ thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa gói dịch vụ thất bại';
            $sStatus  = 'error';
        }
        return redirect()->route('order-setting')->with($sStatus, $sMessage);
    }

    /* get province, district, ward */
    private function findProvince($sProvince)
    {
        $aRow = DB::table('fs_province')->where('name', 'like', "%" . $sProvince . "%")->first();
        if (!isset($aRow->province_id)) {
            return '';
        }
        return $aRow->province_id;
    }

    private function findDistrict($iProvinceId, $sDistrict)
    {
        $aRow = DB::table('fs_district')
            ->where('name', 'like', $sDistrict)
            ->where('province_id', '=', $iProvinceId)->first();
        if (!isset($aRow->district_id)) {
            return '';
        }
        return $aRow->district_id;
    }

    private function findWard($iProvinceId, $iDistrictId, $sWard)
    {
        $aRow = DB::table('fs_ward')
            ->leftJoin('fs_district', 'fs_ward.district_id', '=', 'fs_district.district_id')
            ->where('fs_ward.name', 'like', '%' . $sWard . '%')
            ->where('fs_district.province_id', '=', $iProvinceId)
            ->where('fs_ward.district_id', '=', $iDistrictId)
            ->first();
        if (!isset($aRow->ward_id)) {
            return '';
        }
        return $aRow->ward_id;
    }

    /* change data to export */
    private function dataToExport($aRows)
    {
        foreach ($aRows as $iKey => $aRow) {
            $aLabels[] = [
                'STT' => $iKey + 1,
                'Mã đối tác' => $aRow->customer_code,
                'Mã đơn hàng' => $aRow->order_code,
                'Người nhận' => $aRow->receiver_name,
                'SĐT' => $aRow->phone,
                'Địa chỉ' => $aRow->address,
                'Tỉnh/Thành phố' => $aRow->province_name,
                'Quận/huyện' => $aRow->district_name,
                'Phường/xã' => $aRow->ward_name,
                'Tên sản phẩm' => $aRow->product_name,
                'Trọng lượng' => $aRow->weight . ' kg',
                'Chiều dài' => $aRow->length . ' mm',
                'Chiều cao' => $aRow->height . ' mm',
                'Chiều rộng' => $aRow->width . ' mm',
                'Phí giao hàng' => $aRow->transport_fee . ' đ',
                'COD' => $aRow->cod . ' đ',
                'Dịch vụ' => $aRow->service_name,
                'Người thanh toán phí' => $aRow->receiver_purchase_fee ? 'Người nhận' : 'Người gửi',
                'Trạng thái' => $aRow->status_name,
                'TT vật lý' => $aRow->physical_status_name,
                'Ghi chú' => $aRow->note,
            ];
        }
        return $aLabels;
    }

    private function checkOrderCodeExist ($sCustomerOrderCode)
    {
        $aOrder = Order::where('customer_order_code', '=', $sCustomerOrderCode)->first();
        return !empty($aOrder) ? true : false;
    }

    // cap nhat don hang
    public function orderUpdateOutside(Request $request)
    {
        if (isset($_GET['list_order_id'])) {
            $aArray = explode(',', $_GET['list_order_id']);
            $aArray = array_map(function($ele) {
                return trim($ele);
            }, $aArray);
            $aArray = array_unique($aArray);
            if (!empty($aArray)) {
                foreach ($aArray as $iKey => $value) {
                    Order::where('order_code', '=', $value)->update(array('inside_outside' => 'outside'));
                }
                return redirect()->route('order-update-outside')->with('success', 'Cập nhật đơn hàng thành công');
            } else {
                return redirect()->route('order-update-outside')->with('error', 'Vui lòng nhập đúng mã đơn hàng');
            }
        }
        return view('backend.module.order.updateOutSide');
    }

    // cap nhat don hang
    public function orderUpdateStatus(Request $request)
    {
        if (isset($_GET['list_order_id'])) {
            $aArray = explode(',', $_GET['list_order_id']);
            $aArray = array_map(function($ele) {
                return trim($ele);
            }, $aArray);
            $aArray = array_unique($aArray);
            if (!empty($aArray)) {
                foreach ($aArray as $iKey => $value) {
                    $aUpdate = [
                        'status' => $_GET['status_id']
                    ];
                    if ($_GET['status_id'] == 7) {
                        $aUpdate['delivery_success_date'] = date('Y-m-d');
                    }
                    Order::where('order_code', '=', $value)->update($aUpdate);
                }
                return redirect()->route('order-update-status')->with('success', 'Cập nhật đơn hàng thành công');
            } else {
                return redirect()->route('order-update-status')->with('error', 'Vui lòng nhập đúng mã đơn hàng');
            }
        }
        $aStatus    = DB::table('fs_order_status')->get();
        return view('backend.module.order.updateStatus', compact('aStatus'));
    }

    // cap nhat don hang
    public function orderUpdateReturn(Request $request)
    {
        if (isset($_GET['list_order_id'])) {
            $aArray = explode(',', $_GET['list_order_id']);
            $aArray = array_map(function($ele) {
                return trim($ele);
            }, $aArray);
            $aArray = array_unique($aArray);
            if (!empty($aArray)) {
                foreach ($aArray as $iKey => $value) {
                    Order::where('order_code', '=', $value)->update(array('flow_order' => 'tra_ve'));
                }
                return redirect()->route('order-update-return')->with('success', 'Cập nhật đơn hàng thành công');
            } else {
                return redirect()->route('order-update-return')->with('error', 'Vui lòng nhập đúng mã đơn hàng');
            }
        }
        return view('backend.module.order.updateReturn');
    }
}
