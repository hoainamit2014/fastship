<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iCount = DB::table('fs_options')->count();
        if ($iCount == 0) {
            $aArray = [
                'website_name',
                'logo_website',
                'address',
                'email',
                'phone',
                'facebook',
                'instagram',
                'twitter',
                'linkedin',
            ];

            foreach ($aArray as $value) {

                Setting::create([
                    'option_name' => $value,
                ]);
            }
        } else {
            $aWebsiteName = Setting::where('option_name', 'website_name')->first();
            $aLogoWebsite = Setting::where('option_name', 'logo_website')->first();
            $aAddress     = Setting::where('option_name', 'address')->first();
            $aEmail       = Setting::where('option_name', 'email')->first();
            $aPhone       = Setting::where('option_name', 'phone')->first();
            $aFacebook    = Setting::where('option_name', 'facebook')->first();
            $aInstagram   = Setting::where('option_name', 'instagram')->first();
            $aTwitter     = Setting::where('option_name', 'twitter')->first();
            $aLinkedin    = Setting::where('option_name', 'linkedin')->first();

            return view('backend.module.settings.index', [
                'aWebsiteName' => $aWebsiteName,
                'aLogoWebsite' => $aLogoWebsite,
                'aAddress'     => $aAddress,
                'aEmail'       => $aEmail,
                'aPhone'       => $aPhone,
                'aFacebook'    => $aFacebook,
                'aInstagram'   => $aInstagram,
                'aTwitter'     => $aTwitter,
                'aLinkedin'    => $aLinkedin,
            ]);
        }
        return view('backend.module.settings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $image               = $request->file('image');
            $input['image_name'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath     = public_path('/images');
            $input['image_url']  = $image->move($destinationPath, $input['image_name']);

            if (!empty($input['image_url'])) {
                Setting::where('option_name', 'logo_website')
                    ->update([
                        'option_value' => url('/public/images') . '/' . $input['image_name'],
                    ]);
            }
        }
        Setting::where('option_name', 'website_name')
            ->update([
                'option_value' => $request->input('website_name'),
            ]);
        Setting::where('option_name', 'address')
            ->update([
                'option_value' => $request->input('address'),
            ]);
        Setting::where('option_name', 'email')
            ->update([
                'option_value' => $request->input('email'),
            ]);
        Setting::where('option_name', 'phone')
            ->update([
                'option_value' => $request->input('phone'),
            ]);
        Setting::where('option_name', 'facebook')
            ->update([
                'option_value' => $request->input('facebook'),
            ]);
        Setting::where('option_name', 'instagram')
            ->update([
                'option_value' => $request->input('instagram'),
            ]);
        Setting::where('option_name', 'twitter')
            ->update([
                'option_value' => $request->input('twitter'),
            ]);
        Setting::where('option_name', 'linkedin')
            ->update([
                'option_value' => $request->input('linkedin'),
            ]);
        return redirect()->route('settings-index')->with('success', 'Cập nhật cài đặt thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
