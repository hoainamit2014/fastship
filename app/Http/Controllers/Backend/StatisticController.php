<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Store;
use App\Order;
use App\Branch;
use App\User;

class StatisticController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function convertDate ($date)
    {
        $date = explode('-', $date);
        $date = "{$date[2]}-{$date[1]}-{$date[0]}";
        return $date;
    }

    public function inDc(Request $request)
    {
        // search
        $aSearch = $request->input('search');$aWhere = array();
        if (!empty($aSearch['from_date'])) {
            $aSearch['from_date_convert'] = $this->convertDate($aSearch['from_date']);
        } else {
            $aSearch['from_date_convert'] = date('Y-m-d');
            $aSearch['from_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['to_date'])) {
            $aSearch['to_date_convert'] = $this->convertDate($aSearch['to_date']);
        } else {
            $aSearch['to_date_convert'] = date('Y-m-d');
            $aSearch['to_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['customer_id'])) {
            $aWhere[] = ['fs_bill.customer_id', '=', $aSearch['customer_id']];
        }

        // find branch_id
        $aBranchs = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->get()->toArray();
        if (!empty($aSearch['branch_id'])) {
            $aRows = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->where('fs_branch.id', $aSearch['branch_id'])
            ->get()->toArray();
        } else {
            $aRows = $aBranchs;
        }

        $aArray = array(); $D1 = $D2 = $D3 = $D4 = $cancel = 0;

        // find order_id in branch_id
        foreach ($aRows as $iKey => $value) {
            $aTemp = Store::select(Db::raw('fs_store.bill_code, fs_store.order_id'))
                ->join('fs_bill', 'fs_bill.bill_code', '=', 'fs_store.bill_code')
                ->where('fs_store.branch_id', $value['id'])
                ->where($aWhere)
                ->get()->toArray();
            $aOrders = array();
            foreach ($aTemp as $value) {
                $aOrders = array_merge(unserialize($value['order_id']));
            }
            $sOrders = implode(',', $aOrders);
            $aTemp = Order::select(Db::raw('fs_order.order_id, fs_order.status, fs_order.updated_at, fs_order.created_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->get()->toArray();

            // find statistics
            foreach ($aTemp as $aItem) {

                if ($aItem['status'] == 7) {
                    $updated_at = strtotime($aItem['updated_at']);
                    $created_at = strtotime($aItem['created_at']);
                    if ($updated_at - $created_at < 24*60*60) {
                        $D1++;
                    }
                    if ($updated_at - $created_at < 2*24*60*60 && $updated_at - $created_at >= 24*60*60) {
                        $D2++;
                    }
                    if ($updated_at - $created_at < 3*24*60*60 && $updated_at - $created_at >= 2*24*60*60) {
                        $D3++;
                    }
                    if ($updated_at - $created_at >= 3*24*60*60) {
                        $D4++;
                    }
                } elseif ($aItem['status'] == 2) {
                    $cancel++;
                }
            }
            $count = count($aTemp);
            $aRows[$iKey]['D1'] = ($count > 0 ? round($D1 * 100 / $count, 2) : 0) . "% ({$D1}/{$count})";
            $aRows[$iKey]['D2'] = ($count > 0 ? round($D2 * 100 / $count, 2) : 0) . "% ({$D2}/{$count})";
            $aRows[$iKey]['D3'] = ($count > 0 ? round($D3 * 100 / $count, 2) : 0) . "% ({$D3}/{$count})";
            $aRows[$iKey]['D4'] = ($count > 0 ? round($D4 * 100 / $count, 2) : 0) . "% ({$D4}/{$count})";
            $aRows[$iKey]['cancel'] = ($count > 0 ? round($cancel * 100 / $count, 2) : 0) . "% ({$cancel}/{$count})";
            $D1 = $D2 = $D3 = $D4 = $cancel = 0;
        }
        $aCustomers    = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.statistic.inDc', ['aRows' => $aRows, 'aBranchs' => $aBranchs, 'aCustomers' => $aCustomers, 'aSearch' => $aSearch]);
    }

    public function createNew(Request $request)
    {
        $aSearch = $request->input('search');$aWhere = array();
        if (!empty($aSearch['from_date'])) {
            $aSearch['from_date_convert'] = $this->convertDate($aSearch['from_date']);
        } else {
            $aSearch['from_date_convert'] = date('Y-m-d');
            $aSearch['from_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['to_date'])) {
            $aSearch['to_date_convert'] = $this->convertDate($aSearch['to_date']);
        } else {
            $aSearch['to_date_convert'] = date('Y-m-d');
            $aSearch['to_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['customer_id'])) {
            $aWhere[] = ['fs_bill.customer_id', '=', $aSearch['customer_id']];
        }

        // find branch_id
        $aBranchs = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->get()->toArray();
        if (!empty($aSearch['branch_id'])) {
            $aRows = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->where('fs_branch.id', $aSearch['branch_id'])
            ->get()->toArray();
            $aSelectBranchs = $aRows;
        } else {
            $aSelectBranchs = $aRows = $aBranchs;
        }

        // find order_id by branch_id
        foreach ($aRows as $iKey => $value) {
            // find bill
            $aTemp = Store::select(Db::raw('fs_store.bill_code, fs_store.order_id'))
                ->join('fs_bill', 'fs_bill.bill_code', '=', 'fs_store.bill_code')
                ->where('fs_store.branch_id', $value['id'])
                ->where($aWhere)
                ->get()->toArray();
            $aOrders = array();
            foreach ($aTemp as $value) {
                $aOrders = array_merge(unserialize($value['order_id']));
            }
            $sOrders = implode(',', $aOrders);

            
            // find order_id
            $aTemp = Order::select(Db::raw('fs_order.order_id, fs_order.updated_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 1)
                ->get()->toArray();
            $aTemp2 = Order::select(Db::raw('count(fs_order.order_id) as count, fs_order.updated_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 1)
                ->groupBy('fs_order.updated_at')
                ->get()->toArray();
            $aRows[$iKey]['list_order'] = $aTemp;
            $aRows[$iKey]['group_order'] = $aTemp2;
        }

        // sort by date
        $from_date_convert = strtotime($aSearch['from_date_convert']);
        $to_date_convert = strtotime($aSearch['to_date_convert']);
        $aResults = array(); $Key = 0;
        while ($from_date_convert <= $to_date_convert) {
            $aResults[$Key][] = date('d-m-Y', $from_date_convert);
            foreach ($aRows as $iKey => $value) {
                $aResults[$Key][] = $this->countOrder($value['list_order'], $from_date_convert);
            }
            $from_date_convert += 24*60*60;
            $Key++;
        }

        // find sum and average
        $aSum = $aAverage = array();
        foreach ($aRows as $aRow) {
            $count = count($aRow['list_order']);
            $aSum[] = $count;
            $array = array_map(function ($a) {
                return $a['count'];
            }, $aRow['group_order']);
            $aAverage[] = array_sum($array) / $Key;
        }

        $aCustomers    = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.statistic.createNew', ['aRows' => $aResults, 'aBranchs' => $aBranchs, 'aCustomers' => $aCustomers, 'aSearch' => $aSearch, 'aSelectBranchs' => $aSelectBranchs, 'aSum' => $aSum, 'aAverage' => $aAverage]);
    }

    // tinh tong so order thoa ngay thang
    private function countOrder ($aRows, $sDate)
    {
        $count = 0;
        foreach ($aRows as $aRow) {
            if ($sDate == strtotime($aRow['updated_at'])) {
                $count++;
            }
        }
        return $count;
    }

    public function receiveNew (Request $request)
    {
        $aSearch = $request->input('search');$aWhere = array();
        if (!empty($aSearch['from_date'])) {
            $aSearch['from_date_convert'] = $this->convertDate($aSearch['from_date']);
        } else {
            $aSearch['from_date_convert'] = date('Y-m-d');
            $aSearch['from_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['to_date'])) {
            $aSearch['to_date_convert'] = $this->convertDate($aSearch['to_date']);
        } else {
            $aSearch['to_date_convert'] = date('Y-m-d');
            $aSearch['to_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['customer_id'])) {
            $aWhere[] = ['fs_bill.customer_id', '=', $aSearch['customer_id']];
        }

        // find branch_id
        $aBranchs = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->get()->toArray();
        if (!empty($aSearch['branch_id'])) {
            $aRows = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->where('fs_branch.id', $aSearch['branch_id'])
            ->get()->toArray();
            $aSelectBranchs = $aRows;
        } else {
            $aSelectBranchs = $aRows = $aBranchs;
        }

        // find order_id by branch_id
        foreach ($aRows as $iKey => $value) {
            // find bill
            $aTemp = Store::select(Db::raw('fs_store.bill_code, fs_store.order_id'))
                ->join('fs_bill', 'fs_bill.bill_code', '=', 'fs_store.bill_code')
                ->where('fs_store.branch_id', $value['id'])
                ->where($aWhere)
                ->get()->toArray();
            $aOrders = array();
            foreach ($aTemp as $value) {
                $aOrders = array_merge(unserialize($value['order_id']));
            }
            $sOrders = implode(',', $aOrders);

            
            // find order_id
            $aTemp = Order::select(Db::raw('fs_order.order_id, fs_order.updated_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 4)
                ->get()->toArray();
            $aTemp2 = Order::select(Db::raw('count(fs_order.order_id) as count, fs_order.updated_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 4)
                ->groupBy('fs_order.updated_at')
                ->get()->toArray();
            $aRows[$iKey]['list_order'] = $aTemp;
            $aRows[$iKey]['group_order'] = $aTemp2;
        }

        // sort by date
        $from_date_convert = strtotime($aSearch['from_date_convert']);
        $to_date_convert = strtotime($aSearch['to_date_convert']);
        $aResults = array(); $Key = 0;
        while ($from_date_convert <= $to_date_convert) {
            $aResults[$Key][] = date('d-m-Y', $from_date_convert);
            foreach ($aRows as $iKey => $value) {
                $aResults[$Key][] = $this->countOrder($value['list_order'], $from_date_convert);
            }
            $from_date_convert += 24*60*60;
            $Key++;
        }

        // find sum and average
        $aSum = $aAverage = array();
        foreach ($aRows as $aRow) {
            $count = count($aRow['list_order']);
            $aSum[] = $count;
            $array = array_map(function ($a) {
                return $a['count'];
            }, $aRow['group_order']);
            $aAverage[] = array_sum($array) / $Key;
        }

        $aCustomers    = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.statistic.receiveNew', ['aRows' => $aResults, 'aBranchs' => $aBranchs, 'aCustomers' => $aCustomers, 'aSearch' => $aSearch, 'aSelectBranchs' => $aSelectBranchs, 'aSum' => $aSum, 'aAverage' => $aAverage]);
    }

    public function inventoryInDc (Request $request)
    {
        $aSearch = $request->input('search');$aWhere = array();
        if (!empty($aSearch['customer_id'])) {
            $aWhere[] = ['fs_bill.customer_id', '=', $aSearch['customer_id']];
        }

        // find branch_id
        $aBranchs = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->get()->toArray();
        if (!empty($aSearch['branch_id'])) {
            $aRows = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->where('fs_branch.id', $aSearch['branch_id'])
            ->get()->toArray();
        } else {
            $aRows = $aBranchs;
        }

        // find order_id by branch_id
        foreach ($aRows as $iKey => $value) {
            // find bill
            $aTemp = Store::select(Db::raw('fs_store.bill_code, fs_store.order_id'))
                ->join('fs_bill', 'fs_bill.bill_code', '=', 'fs_store.bill_code')
                ->where('fs_store.branch_id', $value['id'])
                ->where($aWhere)
                ->get()->toArray();
            $aOrders = array();
            foreach ($aTemp as $value) {
                $aOrders = array_merge(unserialize($value['order_id']));
            }
            $sOrders = implode(',', $aOrders);

            $aRows[$iKey]['count'] = Order::select(Db::raw('fs_order.order_id, fs_order.updated_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.status', 2)
                ->get()->count();
        }

        $aCustomers    = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.statistic.inventoryInDc', ['aRows' => $aRows, 'aBranchs' => $aBranchs, 'aCustomers' => $aCustomers, 'aSearch' => $aSearch]);
    }

    public function byCustomer (Request $request)
    {
        $aSearch = $request->input('search');$aWhere =  $aWhere2 = array();
        if (!empty($aSearch['from_date'])) {
            $aSearch['from_date_convert'] = $this->convertDate($aSearch['from_date']);
        } else {
            $aSearch['from_date_convert'] = date('Y-m-d');
            $aSearch['from_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['to_date'])) {
            $aSearch['to_date_convert'] = $this->convertDate($aSearch['to_date']);
        } else {
            $aSearch['to_date_convert'] = date('Y-m-d');
            $aSearch['to_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['branch_id'])) {
            $aWhere[] = ['fs_store.branch_id', '=', $aSearch['branch_id']];   
        }
        if (!empty($aSearch['customer_id'])) {
            $aWhere2[] = ['id', '=', $aSearch['customer_id']];
        }
        $aSelectCustomers = $aRows = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->where($aWhere2)->get();

        // find order_id by branch_id
        foreach ($aRows as $iKey => $value) {
            // find bill
            $aTemp = Store::select(Db::raw('fs_store.order_id'))
                ->join('fs_bill', 'fs_bill.bill_code', '=', 'fs_store.bill_code')
                ->where($aWhere)
                ->get()->toArray();
            $aOrders = array();
            foreach ($aTemp as $value) {
                $aOrders = array_merge(unserialize($value['order_id']));
            }
            $sOrders = implode(',', $aOrders);

            // find order_id
            $aTemp = Order::select(Db::raw('fs_order.order_id, fs_order.updated_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 4)
                ->get()->toArray();
            $aTemp2 = Order::select(Db::raw('count(fs_order.order_id) as count, fs_order.updated_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 4)
                ->groupBy('fs_order.updated_at')
                ->get()->toArray();
            $aRows[$iKey]['list_order'] = $aTemp;
            $aRows[$iKey]['group_order'] = $aTemp2;
        }

        // sort by date
        $from_date_convert = strtotime($aSearch['from_date_convert']);
        $to_date_convert = strtotime($aSearch['to_date_convert']);
        $aResults = array(); $Key = 0;
        while ($from_date_convert <= $to_date_convert) {
            $aResults[$Key][] = date('d-m-Y', $from_date_convert);
            foreach ($aRows as $iKey => $value) {
                $aResults[$Key][] = $this->countOrder($value['list_order'], $from_date_convert);
            }
            $from_date_convert += 24*60*60;
            $Key++;
        }

        // find sum and average
        $aSum = $aAverage = array();
        foreach ($aRows as $aRow) {
            $count = count($aRow['list_order']);
            $aSum[] = $count;
            $array = array_map(function ($a) {
                return $a['count'];
            }, $aRow['group_order']);
            $aAverage[] = array_sum($array) / $Key;
        }

        $aBranchs = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->get()->toArray();

        $aCustomers    = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.statistic.byCustomer', ['aRows' => $aResults, 'aBranchs' => $aBranchs, 'aCustomers' => $aCustomers, 'aSearch' => $aSearch, 'aSum' => $aSum, 'aAverage' => $aAverage, 'aSelectCustomers' => $aSelectCustomers]);
    }

    public function leadTimeSuccess (Request $request)
    {
        $aSearch = $request->input('search');$aWhere = array();
        if (!empty($aSearch['from_date'])) {
            $aSearch['from_date_convert'] = $this->convertDate($aSearch['from_date']);
        } else {
            $aSearch['from_date_convert'] = date('Y-m-d');
            $aSearch['from_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['to_date'])) {
            $aSearch['to_date_convert'] = $this->convertDate($aSearch['to_date']);
        } else {
            $aSearch['to_date_convert'] = date('Y-m-d');
            $aSearch['to_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['customer_id'])) {
            $aWhere[] = ['fs_bill.customer_id', '=', $aSearch['customer_id']];
        }

        // find branch_id
        $aBranchs = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->get()->toArray();
        if (!empty($aSearch['branch_id'])) {
            $aRows = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->where('fs_branch.id', $aSearch['branch_id'])
            ->get()->toArray();
        } else {
            $aRows = $aBranchs;
        }

        $aArray = array(); $D1 = $D2 = $D3 = $D4 = $cancel = 0;

        // find order_id in branch_id
        foreach ($aRows as $iKey => $value) {
            $aTemp = Store::select(Db::raw('fs_store.bill_code, fs_store.order_id'))
                ->join('fs_bill', 'fs_bill.bill_code', '=', 'fs_store.bill_code')
                ->where('fs_store.branch_id', $value['id'])
                ->where($aWhere)
                ->get()->toArray();
            $aOrders = array();
            foreach ($aTemp as $value) {
                $aOrders = array_merge(unserialize($value['order_id']));
            }
            $sOrders = implode(',', $aOrders);
            $aTemp = Order::select(Db::raw('fs_order.order_id, fs_order.status, fs_order.updated_at, fs_order.created_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 7)
                ->get()->toArray();

            // find statistics
            foreach ($aTemp as $aItem) {
                $updated_at = strtotime($aItem['updated_at']);
                $created_at = strtotime($aItem['created_at']);
                if ($updated_at - $created_at < 24*60*60) {
                    $D1++;
                }
                if ($updated_at - $created_at < 2*24*60*60 && $updated_at - $created_at >= 24*60*60) {
                    $D2++;
                }
                if ($updated_at - $created_at < 3*24*60*60 && $updated_at - $created_at >= 2*24*60*60) {
                    $D3++;
                }
                if ($updated_at - $created_at >= 3*24*60*60) {
                    $D4++;
                }
            }
            $count = count($aTemp);
            $aRows[$iKey]['D1'] = ($count > 0 ? round($D1 * 100 / $count, 2) : 0) . "% ({$D1}/{$count})";
            $aRows[$iKey]['D2'] = ($count > 0 ? round($D2 * 100 / $count, 2) : 0) . "% ({$D2}/{$count})";
            $aRows[$iKey]['D3'] = ($count > 0 ? round($D3 * 100 / $count, 2) : 0) . "% ({$D3}/{$count})";
            $aRows[$iKey]['D4'] = ($count > 0 ? round($D4 * 100 / $count, 2) : 0) . "% ({$D4}/{$count})";
            $aRows[$iKey]['count'] = count($aTemp);
            $D1 = $D2 = $D3 = $D4 = $cancel = 0;
        }
        $aCustomers    = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.statistic.leadTimeSuccess', ['aRows' => $aRows, 'aBranchs' => $aBranchs, 'aCustomers' => $aCustomers, 'aSearch' => $aSearch]);
    }

    public function leadTimeStart (Request $request)
    {
        $aSearch = $request->input('search');$aWhere = array();
        if (!empty($aSearch['from_date'])) {
            $aSearch['from_date_convert'] = $this->convertDate($aSearch['from_date']);
        } else {
            $aSearch['from_date_convert'] = date('Y-m-d');
            $aSearch['from_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['to_date'])) {
            $aSearch['to_date_convert'] = $this->convertDate($aSearch['to_date']);
        } else {
            $aSearch['to_date_convert'] = date('Y-m-d');
            $aSearch['to_date'] = date('d-m-Y');
        }
        if (!empty($aSearch['customer_id'])) {
            $aWhere[] = ['fs_bill.customer_id', '=', $aSearch['customer_id']];
        }

        // find branch_id
        $aBranchs = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->get()->toArray();
        if (!empty($aSearch['branch_id'])) {
            $aRows = Branch::select(Db::raw('fs_branch.id, fs_province.name'))
            ->join('fs_province', 'fs_province.province_id', '=', 'fs_branch.province_id')
            ->where('fs_branch.id', $aSearch['branch_id'])
            ->get()->toArray();
        } else {
            $aRows = $aBranchs;
        }

        $aArray = array(); $D1 = $D2 = $D3 = $D4 = $cancel = 0;

        // find order_id in branch_id
        foreach ($aRows as $iKey => $value) {
            $aTemp = Store::select(Db::raw('fs_store.bill_code, fs_store.order_id'))
                ->join('fs_bill', 'fs_bill.bill_code', '=', 'fs_store.bill_code')
                ->where('fs_store.branch_id', $value['id'])
                ->where($aWhere)
                ->get()->toArray();
            $aOrders = array();
            foreach ($aTemp as $value) {
                $aOrders = array_merge(unserialize($value['order_id']));
            }
            $sOrders = implode(',', $aOrders);
            $aTemp = Order::select(Db::raw('fs_order.order_id, fs_order.status, fs_order.updated_at, fs_order.created_at'))
                ->whereIn('fs_order.order_id', $aOrders)
                ->where('fs_order.updated_at', '>=', $aSearch['from_date_convert'])
                ->where('fs_order.updated_at', '<=', $aSearch['to_date_convert'])
                ->where('fs_order.status', 4)
                ->get()->toArray();

            // find statistics
            foreach ($aTemp as $aItem) {
                $updated_at = strtotime($aItem['updated_at']);
                $created_at = strtotime($aItem['created_at']);
                if ($updated_at - $created_at < 24*60*60) {
                    $D1++;
                }
                if ($updated_at - $created_at < 2*24*60*60 && $updated_at - $created_at >= 24*60*60) {
                    $D2++;
                }
                if ($updated_at - $created_at < 3*24*60*60 && $updated_at - $created_at >= 2*24*60*60) {
                    $D3++;
                }
                if ($updated_at - $created_at >= 3*24*60*60) {
                    $D4++;
                }
            }
            $count = count($aTemp);
            $aRows[$iKey]['D1'] = ($count > 0 ? round($D1 * 100 / $count, 2) : 0) . "% ({$D1}/{$count})";
            $aRows[$iKey]['D2'] = ($count > 0 ? round($D2 * 100 / $count, 2) : 0) . "% ({$D2}/{$count})";
            $aRows[$iKey]['D3'] = ($count > 0 ? round($D3 * 100 / $count, 2) : 0) . "% ({$D3}/{$count})";
            $aRows[$iKey]['D4'] = ($count > 0 ? round($D4 * 100 / $count, 2) : 0) . "% ({$D4}/{$count})";
            $aRows[$iKey]['count'] = count($aTemp);
            $D1 = $D2 = $D3 = $D4 = $cancel = 0;
        }
        $aCustomers    = User::select(Db::raw('id, customer_code'))->where('status', '1')->where('user_group_id', '7')->get();

        return view('backend.module.statistic.leadTimeStart', ['aRows' => $aRows, 'aBranchs' => $aBranchs, 'aCustomers' => $aCustomers, 'aSearch' => $aSearch]);
    }

}
