<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class UnloadingFromDcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.module.Unloading.DC.index');
    }
}
