<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Position_Nav;
use App\Term;
use App\Term_Taxonomy;
use Request;

class MenuController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->prefix = 'fs_';
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $rq = $request::all();
        if (isset($rq['action']) && $rq['action'] == 'delete') {
            $term = Term::leftJoin($this->prefix . 'term_taxonomy', $this->prefix . 'term.term_id', '=', $this->prefix . 'term_taxonomy.term_id')
                ->where($this->prefix . 'term_taxonomy.taxonomy', '=', 'sub-menu')
                ->orWhere($this->prefix . 'term_taxonomy.taxonomy', '=', 'navs-menu')
                ->get()
                ->toArray();

            $aTerm   = $this->cursive($term);
            $aTermID = array();
            if ($aTerm) {
                foreach ($aTerm as $key => $value) {
                    if ($value['term_id'] == $rq['menu']) {
                        $aTermID[] = $value['term_id'];
                        if (isset($value['sub-menu']) && !empty($value['sub-menu'])) {
                            foreach ($value['sub-menu'] as $k => $v) {
                                $aTermID[] = $v['term_id'];
                                if (isset($v['sub-menu']) && !empty($v['sub-menu'])) {
                                    foreach ($v['sub-menu'] as $subK => $subV) {
                                        $aTermID[] = $subV['term_id'];
                                        if (isset($subV['sub-menu']) && !empty($subV['sub-menu'])) {
                                            foreach ($subV['sub-menu'] as $subK2 => $subV2) {
                                                $aTermID[] = $subV2['term_id'];
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            $term          = Term::whereIn('term_id', $aTermID)->delete();
            $term_taxonomy = Term_Taxonomy::whereIn('term_id', $aTermID)->delete();
            if ($term && $term_taxonomy) {
                return redirect()->route('menu-index')->with('success', 'Xóa menu thành công');
            }
        } else {
            $navs_menu         = Term::leftJoin($this->prefix . 'term_taxonomy', $this->prefix . 'term.term_id', '=', $this->prefix . 'term_taxonomy.term_id')->where($this->prefix . 'term_taxonomy.taxonomy', '=', 'navs-menu')->where($this->prefix . 'term_taxonomy.parent', '=', '0')->get()->toArray();
            $position          = Position_Nav::get()->toArray();
            $data['navs_menu'] = $navs_menu;
            if (!isset($_GET['action']) && !empty($navs_menu)) {
                return redirect(Request::fullUrl() . '?action=edit&menu=' . $navs_menu[0]['term_id']);
            }

            $data['menu_list'] = Term::leftJoin($this->prefix . 'term_taxonomy', $this->prefix . 'term.term_id', '=', $this->prefix . 'term_taxonomy.term_id')->where($this->prefix . 'term_taxonomy.taxonomy', '=', 'sub-menu')->get()->toArray();
            $data['position']  = $position;

            return view('backend.module.menu.index', compact('data'));
        }

    }

    private function cursive($menu, $parent = 0)
    {
        $aReturn = array();
        foreach ($menu as $key => $val) {
            if ($val['parent'] == $parent) {
                $aReturn[$key] = $val;
                if ($this->cursive($menu, $val['term_id'])) {
                    $aReturn[$key]['sub-menu'] = $this->cursive($menu, $val['term_id']);
                }
            }
        }
        return $aReturn;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rq               = $request::all();
        $term             = new Term();
        $term->name       = $rq['name'];
        $term->slug       = str_slug($rq['name']);
        $term->term_group = 0;
        $term->save();
        $term_taxonomy              = new Term_Taxonomy();
        $term_taxonomy->term_id     = $term->id;
        $term_taxonomy->count       = 0;
        $term_taxonomy->description = '';

        if (isset($rq['sub'])) {
            $term_taxonomy->taxonomy = 'sub-menu';
            if (!empty($rq['parent'])) {
                $term_taxonomy->parent = $rq['parent'];
            } else {
                $term_taxonomy->parent = $rq['parent_menu'];
            }
            $term_taxonomy->save();
        } else {
            $term_taxonomy->taxonomy = 'navs-menu';
            $term_taxonomy->parent   = 0;
            $term_taxonomy->save();
        }
        return back()->with('success', 'Tạo menu thành công');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $term = Term::leftJoin($this->prefix . 'term_taxonomy', $this->prefix . 'term.term_id', '=', $this->prefix . 'term_taxonomy.term_id')->where($this->prefix . 'term_taxonomy.taxonomy', '=', 'sub-menu')->where($this->prefix . 'term.term_id', $id)->get()->toArray();
        return response()->json($term[0]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rq            = $request::all();
        $aTerm         = array('name' => $rq['name']);
        $aTermTaxonomy = array('parent' => $rq['parent']);
        $term          = Term::where('term_id', $id)->update($aTerm);
        $term_taxonomy = Term_Taxonomy::where('term_id', $id)->update($aTermTaxonomy);
        return back()->with('success', 'Cập nhật menu thành công');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $rq               = $request::all();
        $checkExistsChild = Term_Taxonomy::where('parent', $rq['id'])->get()->toArray();
        if (!empty($checkExistsChild)) {
            return response()->json(
                [
                    'code'    => '403',
                    'message' => 'Vui lòng di chuyển các menu con sang danh mục khác và thử lại',
                ]
            );
        } else {
            $term          = Term::where('term_id', $rq['id'])->delete();
            $term_taxonomy = Term_Taxonomy::where('term_id', $rq['id'])->delete();
            return response()->json(
                [
                    'code'    => '201',
                    'message' => 'Xóa menu thành công',
                ]
            );
        }

    }
}
