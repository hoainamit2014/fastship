<?php

namespace App\Http\Controllers\Backend;

use App\Bill;
use App\BillDetail;
use App\Http\Controllers\Controller;
use App\Order;
use App\WareHouse;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DeliveryController extends Controller {
	public function __construct() {
		$this->prefix = 'fs_';
		$this->auth = $this->middleware('auth');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$auth = Auth::user();
		$aUser = User::where('id','!=',$auth->id)->get()->toArray();
		// Customer group id = 7
		$customer = User::where('user_group_id','=',7)->get()->toArray();
		$aDM = User::where('user_group_id','=',2)->get()->toArray();

		$aProvince = Order::select(Db::raw('fs_province.* , count(*) as count'))
			->leftJoin('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
			->groupBy('fs_province.province_id')
			->get()
			->toArray();
		$aDistrict = Order::select(Db::raw('fs_district.* , count(*) as count'))
			->leftJoin('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
			->groupBy('fs_district.district_id')
			->get()
			->toArray();
		$aWard = Order::select(Db::raw('fs_ward.* , count(*) as count'))
			->leftJoin('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
			->groupBy('fs_ward.ward_id')
			->get()
			->toArray();
		$aTotalRows = array();
		if (!empty($aProvince)) {
			foreach ($aProvince as $k => $aVal) {
				$aVal = array(
					'id' => $aVal['province_id'],
					'name' => $aVal['name'],
					'count' => $aVal['count'],
					'type' => 1,
					'parent' => 0,
				);
				array_push($aTotalRows, $aVal);
			}
		}
		if (!empty($aDistrict)) {
			foreach ($aDistrict as $k => $aVal) {
				$aVal = array(
					'id' => $aVal['district_id'],
					'name' => $aVal['name'],
					'count' => $aVal['count'],
					'type' => 2,
					'parent' => $aVal['province_id'],
				);
				array_push($aTotalRows, $aVal);
			}
		}
		if (!empty($aWard)) {
			foreach ($aWard as $k => $aVal) {
				$aVal = array(
					'id' => $aVal['ward_id'],
					'name' => $aVal['name'],
					'count' => $aVal['count'],
					'type' => 3,
					'parent' => $aVal['district_id'],
				);
				array_push($aTotalRows, $aVal);
			}
		}

		return view('backend.module.Unloading.OrderProcess.Deploy.index', compact('aUser', 'aTotalRows','customer','aDM'));
	}

	public function AjaxDeployPickup(Request $request) {
		$sOrder = array();
		if (!empty($request->input('billCode'))) {
			$pillCode = $request->input('billCode');
			$sQuery = Bill::leftJoin('fs_bill_detail', 'fs_bill_detail.bill_code', '=', 'fs_bill.bill_code')
				->where('fs_bill.bill_code', '=', $pillCode)
				->first()->toArray();

			if ($sQuery) {
				$sOrder = Order::select(DB::raw('fs_order.* ,fs_user.full_name as customer_name , fs_order_physical_status.name as physical_status , fs_province.name as province_name ,fs_district.name as district_name ,fs_ward.name as ward_name'))
					->leftJoin('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
					->leftJoin('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
					->leftJoin('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
					->leftJoin('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
					->leftJoin('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
					->leftJoin('fs_user', 'fs_user.customer_code', '=', 'fs_order.customer_code')
					->whereIn('order_code', unserialize($sQuery['order_id']))
					->get()
					->toArray();
			}
		}
		if($sOrder){

			foreach($sOrder as $k => $aVal)
			{
				$sOrder[$k] = $aVal;
				$sOrder[$k]['gift_1'] = '';
				$sOrder[$k]['gift_2'] = '';
				if(json_decode($aVal['gift'])){
					foreach(json_decode($aVal['gift']) as $kGift => $aGift)
					{
						$sOrder[$k]['gift_'.($kGift + 1)] = $aGift;
					}
				}
			}
		}
		$aData = array(
			'bill_status' => !empty($sQuery['complete_date']) ? 'Đang xử lý' : 'Đã xử lý',
			'data' => $sOrder, 
			'charge' => $this->_getAuthor($sQuery['responsible_person_id'])->full_name,
			'author' => $this->_getAuthor($sQuery['author'])->full_name,		
		);

		return $aData;
	}

	public function _getAuthor($id){
		return User::find($id);
	}

	public function getDataRowsTable(Request $request) {
		$aProvince = array();
		$aDistrict = array();
		$aWard = array();
		$aReturn = array();
		if (!empty($request['data'])) {
			foreach ($request['data'] as $k => $aVal) {
				switch ($aVal['type']) {
				case 1:
					$aProvince[] = $aVal['id'];
					break;
				case 2:
					$aDistrict[] = $aVal['id'];
					break;
				case 3:
					$aWard[] = $aVal['id'];
					break;
				default:
					break;
				}
			}

			$aWhere = Order::select(DB::raw('fs_order.*,fs_province.name as province_name, fs_district.name as district_name , fs_ward.name as ward_name , fs_order_status.name as status_name ,fs_order'))
				->leftJoin('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
				->leftJoin('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
				->leftJoin('fs_ward', 'fs_ward.ward_id', '=', 'fs_order.ward_id')
				->leftJoin('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.physical_status');
			if (!empty($aProvince)) {
				$aWhere->WhereIn('fs_order.province_id', $aProvince);
			}

			if (!empty($aDistrict)) {
				$aWhere->WhereIn('fs_order.district_id', $aDistrict);
			}

			if (!empty($aWard)) {
				$aWhere->WhereIn('fs_order.ward_id', $aWard);
			}

			$aReturn = $aWhere->get()->toArray();

		}

		echo json_encode($aReturn);

	}

	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function CreateOrderProcess(Request $request) {

		$auth = Auth::user();
		// 1: Phiếu Pickup hàng từ đối tác
		// 2: Phiếu Pickup đơn hàng thu hồi
		// 3: Phiếu nhận hàng từ phiếu pickup
		// 4: Phiếu nhận hàng từ nhiều phiếu pickup
		// 5: Nhập hàng vào kho từ khách hàng
		// 6: Phiếu chuyển hàng tới DC khác
		// 7: Phiếu nhập hàng vào kho từ DC khác
		// 8: Phiếu triển khai giao hàng
		// 9: Phiếu giao hàng
		// 10: Phiếu nhận hàng từ phiếu giao hàng
		// 11: Phiếu nhận tiền từ phiếu giao hàng
		// 12: Phiếu giao hàng ngoại tuyến
		// 13: Phiếu nhận hàng đơn ngoại tuyến
		// 14: Phiếu nhận tiền đơn ngoại tuyến
		// 15: Phiếu triển khai trả về đối tác
		// 16: Phiếu trả hàng về cho đối tác
		// 17: Phiếu nhận hàng từ phiếu trả về
		// 18: Phiếu kiểm kho
		if (!empty($request['list']['order'])) {
			$aListOrder = explode(',', $request['list']['order']);
			$Order_ID = strtoupper('VL' . date('dmY') . $this->generateRandomString(5));
			$bill = new Bill();
			$bill->bill_code = $Order_ID;
			$bill->order_number = count($aListOrder);
			$bill->bill_type_id = 8;
			$bill->responsible_person_id = $request['charge_dm'];
			$bill->status = 1;
			$bill->received_status = 2;
			$bill->author = $auth->id;
			$bill->create_date = time();
			$bill->save();

			$billDetail = new BillDetail();
			$billDetail->bill_code = $Order_ID;
			$billDetail->order_id = serialize($aListOrder);
			$billDetail->create_date = time();
			$billDetail->save();
		}

		return redirect()->route('bill-index')->with('success', 'Tạo phiếu thành công');
	}

	public function getWareHouse(Request $request)
	{
		$id = $request->input('_id');
		$wareHouse = WareHouse::where('user_id','=',$id)->get();
		return $wareHouse;
	}
}
