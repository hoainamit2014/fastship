<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use App\UserGroup;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->prefix = 'fs_';
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aReturn = json_encode(User::where('user_group_id', '!=', 7)->get()->toArray());
        return view('backend.module.user.index', compact('aReturn'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uGroup = UserGroup::get()->toArray();
        return view('backend.module.user.add', compact('uGroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user        = new User();
        $checkExists = $this->checkExists($request->username);
        if (!empty($checkExists)) {
            return back()->with('error', __('Tài khoản đã được tồn tại \n vui lòng thử lại'));
        } else {
            $user->user_name            = $request->username;
            $user->full_name            = $request->fullname;
            $user->password             = Hash::make($request->password);
            $user->email                = $request->email;
            $user->image_path           = $request->image;
            $user->address              = $request->address;
            $user->birthday             = $request->birthday;
            $user->sex                  = $request->sex;
            $user->phone                = $request->phone;
            $user->position             = $request->position;
            $user->brand                = $request->brand;
            $user->department           = $request->department;
            $user->start_work_date      = $request->start_work_date;
            $user->contract_expiry_date = $request->exprire_date;
            $user->status               = $request->status;
            $user->sponsor              = $request->sponsor;
            $user->account_number       = $request->account_number;
            $user->account_name         = $request->owner;
            $user->bank_branch          = $request->bank_branch;
            $user->bank_name            = $request->bank_name;
            $user->identity_card        = $request->identity_card;
            $user->release_date         = $request->release_date;
            $user->register_place       = $request->register_place;
            $user->insurrance           = $request->insurrance;
            $user->save();
            if ($user) {
                return redirect()->route('users-index')->with('success', __('Tạo tài khoản thành công'));
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    private function checkExists($username)
    {
        $uName   = strip_tags($username);
        $results = User::where('user_name', $username)->first();
        return $results;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if (intval($request->id)) {
            $uGroup = UserGroup::get()->toArray();
            $aEdit  = User::where('id', intval($request->id))->first();
            return view('backend.module.user.add', compact('aEdit', 'uGroup'));
        } else {
            return redirect()->route('users-index')->with('error', __('Có lỗi xảy ra , vui lòng thử lại'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user                       = User::find($id);
        $user->full_name            = $request->fullname;
        $user->email                = $request->email;
        $user->image_path           = $request->image;
        $user->address              = $request->address;
        $user->birthday             = $request->birthday;
        $user->sex                  = $request->sex;
        $user->phone                = $request->phone;
        $user->position             = $request->position;
        $user->brand                = $request->brand;
        $user->department           = $request->department;
        $user->start_work_date      = $request->start_work_date;
        $user->contract_expiry_date = $request->exprire_date;
        $user->status               = $request->status;
        $user->sponsor              = $request->sponsor;
        $user->account_number       = $request->account_number;
        $user->account_name         = $request->owner;
        $user->bank_branch          = $request->bank_branch;
        $user->bank_name            = $request->bank_name;
        $user->identity_card        = $request->identity_card;
        $user->release_date         = $request->release_date;
        $user->register_place       = $request->register_place;
        $user->insurrance           = $request->insurrance;
        if ($user->save()) {
            return back()->with('success', __('Cập nhật thông tin thành công'));
        }
    }

    public function delete(Request $request)
    {
        User::destroy(intval($request->id));
        return back()->with('success', __('Xóa dữ liệu thành công'));
    }

    public function lock(Request $request)
    {
        $user                 = User::find($request->id);
        $user->profile_status = (($user->profile_status == 1) ? 2 : 1);
        if ($user->save()) {
            return back()->with('success', __('Khóa tài khoản thành công'));
        }

    }
    public function generate(Request $request)
    {
        $id             = intval($request->id);
        $pw             = strip_tags($request->password);
        $user           = User::find($id);
        $user->password = Hash::make($pw);
        if ($user->save()) {
            return response()->json([
                'status'  => '201',
                'message' => 'Reset mật khẩu thành công',
            ]);
        }
    }

}
