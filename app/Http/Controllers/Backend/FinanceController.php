<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Finance;
use App\Order;
use App\User;
use App\Bill;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrderRequest;
use Illuminate\Support\Facades\Auth;

class FinanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $aUsers = User::where('status', '1')->where('user_group_id', '5')->get();
        return view('backend.module.finance.index', compact('aUsers'));
    }

    public function order(Request $request)
    {
        return view('backend.module.finance.order');
    }

    public function orderSalary(Request $request)
    {
        $aUsers = User::where('status', '1')->where('user_group_id', '2')->get();
        return view('backend.module.finance.order-salary', compact('aUsers'));
    }

    public function transferToAccountant(Request $request)
    {
        if (isset($request->submit)) {
            $iDmId = $request->dm_id;
            if (empty($iDmId)) {
                return redirect()->route('finance-transfer-to-accountant')->with('error', "Vui lòng chọn kế toán nhận tiền");
            }

            $aOrders = Order::select(Db::raw('fs_order.order_id, fs_order.cod, fs_order.transport_fee'))
                ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
                ->join('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
                ->join('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
                ->where('fs_order.delivery_success_date', date('Y-m-d'))
                ->where('fs_order.is_transfer_to_accountant', 0)
                ->get()->toArray();

            if (!empty($aOrders)) {
                $aTemps = array_map(function($a) {
                    return $a['order_id'];
                }, $aOrders);

                $total_money = 0;
     
                foreach ($aOrders as $value) {
                    $total_money += $value['cod'] + $value['transport_fee'];
                }

                $aFinance = new Finance();
                $aFinance->count = count($aOrders);
                $aFinance->user_id = Auth::id();
                $aFinance->accountant_id = $iDmId;
                $aFinance->list_order = serialize($aTemps);
                $aFinance->finance_type = 'Phiếu chuyển tiền';
                $aFinance->total_money = $total_money;
                $aFinance->save();

                // update is_transfer_to_accountant
                foreach ($aOrders as $aOrder) {
                    Order::where('order_id', '=', $aOrder['order_id'])->update(array('is_transfer_to_accountant' => 1));
                }

                return redirect('fs/cpanel/finance/detail/' . $aFinance->id)->with('success', "Tạo phiếu thành công");
            } else {
                return redirect()->route('finance-transfer-to-accountant')->with('error', 'Không tìm thấy đơn hàng nào');
            }
        }
        $aUsers = User::where('status', '1')->where('user_group_id', '5')->get();
        $aData = Order::select(DB::raw('count(*) as count_order, sum(cod) as count_cod, sum(transport_fee) as count_transport_fee'))->where('delivery_success_date', date('Y-m-d'))->where('fs_order.is_transfer_to_accountant', 0)->first();

        return view('backend.module.finance.transfer-to-accountant', compact('aUsers', 'aData'));
    }

    public function transferToAccountantSearch(Request $request)
    {
        $iDisplayStart  = $request->input('start');
        $iDisplayLength = $request->input('length');

        $sQuery = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_province.name as province_name, fs_district.name as district_name'))
                ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
                ->join('fs_province', 'fs_province.province_id', '=', 'fs_order.province_id')
                ->join('fs_district', 'fs_district.district_id', '=', 'fs_order.district_id')
                ->where('fs_order.delivery_success_date', date('Y-m-d'))
                ->where('fs_order.is_transfer_to_accountant', 0);

        $aTotalRecord = $sQuery->count();
        $aRows        = $sQuery->offset($iDisplayStart)->limit($iDisplayLength)->get();

        foreach ($aRows as $iKey => $aValue) {
            $aRows[$iKey]['sum'] = number_format($aValue['cod'] + $aValue['transport_fee']);
            $aRows[$iKey]['cod'] = number_format($aValue['cod']);
            $aRows[$iKey]['transport_fee'] = number_format($aValue['transport_fee']);
            $aRows[$iKey]['stt'] = $iKey + 1;
        }

        $results = array(
            "iTotalRecords"        => count($aRows),
            "iTotalDisplayRecords" => $aTotalRecord,
            "aaData"               => $aRows,
        );

        return json_encode($results);
    }

    public function detail (Request $request, $id)
    {
        $aFinance = Finance::select(Db::raw('fs_finance.*, fs_user1.user_name as creater_name, fs_user2.user_name as accountant_name'))
            ->join('fs_user AS fs_user1', 'fs_user1.id', '=', 'fs_finance.user_id')
            ->join('fs_user AS fs_user2', 'fs_user2.id', '=', 'fs_finance.accountant_id')
            ->where('finance_id', $id)
            ->first();
        $temp = unserialize($aFinance->list_order);
        $aOrders = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name'))
                ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
                ->whereIn('fs_order.order_id', $temp)
                ->get();
        return view('backend.module.finance.detail', compact('aOrders', 'aFinance'));
    }

    public function indexSearch(Request $request)
    {
        $iDisplayStart  = $request->input('start');
        $iDisplayLength = $request->input('length');

        $sQuery = Finance::select(Db::raw('fs_finance.*, fs_user1.user_name as creater_name, fs_user2.user_name as accountant_name'))
            ->join('fs_user AS fs_user1', 'fs_user1.id', '=', 'fs_finance.user_id')
            ->join('fs_user AS fs_user2', 'fs_user2.id', '=', 'fs_finance.accountant_id');

        if ($aSearch = $request->input('customsearch')) {
            if (!empty($aSearch['created_date'])) {
                $temp = explode(' - ', $aSearch['created_date']);
                $sQuery->whereDate('created_at', '>=', date('Y-m-d', strtotime($temp[0])).' 00:00:00');
                $sQuery->whereDate('created_at', '<=', date('Y-m-d', strtotime($temp[1])).' 00:00:00');
            }
            if (!empty($aSearch['complete_date'])) {
                $temp = explode(' - ', $aSearch['complete_date']);
                $sQuery->whereDate('updated_at', '>=', date('Y-m-d', strtotime($temp[0])).' 00:00:00');
                $sQuery->whereDate('updated_at', '<=', date('Y-m-d', strtotime($temp[1])).' 00:00:00');
            }
            if (!empty($aSearch['dm_id'])) {
                $sQuery->where('fs_finance.accountant_id', '=', $aSearch['dm_id']);
            }
            if (!empty($aSearch['finance_status'])) {
                $sQuery->where('fs_finance.status', '=', $aSearch['finance_status']);
            }
        }

        $aTotalRecord = $sQuery->count();
        $aRows        = $sQuery->offset($iDisplayStart)->limit($iDisplayLength)->get();

        foreach ($aRows as $iKey => $aValue) {
            $aRows[$iKey]['created_date'] = date("d-m-Y h:i:s", strtotime($aValue['created_at']));
            if ($aValue['status']) {
                $aRows[$iKey]['updated_date'] = date("d-m-Y h:i:s", strtotime($aValue['updated_at']));
            } else {
                $aRows[$iKey]['updated_date'] = '---';
            }
        }

        $results = array(
            "iTotalRecords"        => count($aRows),
            "iTotalDisplayRecords" => $aTotalRecord,
            "aaData"               => $aRows,
        );

        return json_encode($results);
    }

    public function updateStatusFinance (Request $request, $id)
    {
        Finance::where('finance_id', '=', $id)->update(array('status' => 1));

        return json_encode(array('success' => true));
    }

    // tim kiem dach sach don hang tinh luong
    public function orderSalarySearch(Request $request)
    {
        $iDisplayStart  = $request->input('start');
        $iDisplayLength = $request->input('length');

        $sQuery = Order::select(Db::raw('fs_order.*, fs_order_status.name as status_name, fs_order_physical_status.name as physical_status_name, fs_user.user_name as dm_name'))
            ->join('fs_order_status', 'fs_order_status.status_id', '=', 'fs_order.status')
            ->join('fs_order_physical_status', 'fs_order_physical_status.status_id', '=', 'fs_order.physical_status')
            ->join('fs_user', 'fs_user.id', '=', 'fs_order.responsible_person_id')
            ->where('fs_order.status', '=', 7);

        if ($aSearch = $request->input('customsearch')) {
            if (!empty($aSearch['key_word'])) {
                $sQuery->where(function ($query) use ($aSearch) {
                    $query->where('fs_order.order_code', '=', $aSearch['key_word'])
                          ->orWhere('fs_order.customer_order_code', '=', $aSearch['key_word']);
                });
            }
            if (!empty($aSearch['complete_date'])) {
                $temp = explode(' - ', $aSearch['complete_date']);
                $temp1 = explode('/', $temp[0]);
                $temp2 = explode('/', $temp[1]);
                $sQuery->whereDate('fs_order.delivery_success_date', '>=', "{$temp1[2]}-$temp1[1]-$temp1[0]");
                $sQuery->whereDate('fs_order.delivery_success_date', '<=', "{$temp2[2]}-$temp2[1]-$temp2[0]");
            }
            if (!empty($aSearch['dm_id'])) {
                $sQuery->where('fs_order.responsible_person_id', '=', $aSearch['dm_id']);
            }
            if (!empty($aSearch['order_type'])) {
                $sQuery->where('fs_order.order_type', '=', $aSearch['order_type']);
            }
            if (!empty($aSearch['flow_order'])) {
                $sQuery->where('fs_order.flow_order', '=', $aSearch['flow_order']);
            }
        }
        
        $aTotalRecord = $sQuery->count();
        $aRows        = $sQuery->offset($iDisplayStart)->limit($iDisplayLength)->get();

        $iTotalSalary = 0;
        foreach ($aRows as $iKey => $aValue) {
            $aRows[$iKey]['stt'] = $iKey + 1;
            $aRows[$iKey]['delivery_success_date'] = date("d-m-Y", strtotime($aValue['delivery_success_date']));
            $aRows[$iKey]['dc_number'] = 1;
            $aRows[$iKey]['customer_number'] = 1;
            $aRows[$iKey]['district_number'] = 1;

            // trong luong theo kich thuoc: dai x rong x cao / 5000
            $iWeightCal = $aValue['length'] * $aValue['width'] * $aValue['height'] / 5000;
            if ($iWeightCal < 3) {
                $aRows[$iKey]['weight_number'] = 0.6;
            } elseif ($iWeightCal >= 3 && $iWeightCal < 13) {
                $aRows[$iKey]['weight_number'] = 1;
            } elseif ($iWeightCal >= 13 && $iWeightCal < 25) {
                $aRows[$iKey]['weight_number'] = 2;
            } elseif ($iWeightCal >= 25 && $iWeightCal < 70) {
                $aRows[$iKey]['weight_number'] = 3;
            } elseif ($iWeightCal >= 70 && $iWeightCal < 100) {
                $aRows[$iKey]['weight_number'] = 4;
            } else {
                $aRows[$iKey]['weight_number'] = 5;
            }
            $iMoney = $aRows[$iKey]['customer_number'] * $aRows[$iKey]['district_number'] 
            * $aRows[$iKey]['weight_number'] * $aRows[$iKey]['transport_fee'];
            $aRows[$iKey]['salary'] = number_format($iMoney, 2);
            $aRows[$iKey]['transport_fee'] = number_format($aValue['transport_fee'], 2);
            $iTotalSalary += $iMoney;
        }

        $results = array(
            "iTotalRecords"        => count($aRows),
            "iTotalDisplayRecords" => $aTotalRecord,
            "aaData"               => $aRows,
            "iTotalSalary"         => number_format($iTotalSalary, 2),
        );

        return json_encode($results);
    }
}
