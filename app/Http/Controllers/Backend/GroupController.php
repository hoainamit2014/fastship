<?php

namespace App\Http\Controllers\Backend;

use App\Group;
use App\Http\Controllers\Controller;
use App\UserGroupCustom;
use App\UserGroupSetting;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aRows = Group::get();
        if (count($aRows) == 0) {
            $aRows = array();
        }
        return view('backend.module.group.index', ['aRows' => $aRows]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.module.group.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->input('title'))) {
            return redirect()->route('group-add')->with('error', 'Vui lòng điền đủ thông tin');
        } else {
            Group::create([
                'title'     => $request->input('title'),
                'is_active' => $request->input('is_active'),
            ]);
            return redirect()->route('group-index')->with('success', 'Thêm nhóm người dùng thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setting($id)
    {
        $aRow      = Group::where('user_group_id', $id)->first();
        $aSettings = UserGroupSetting::get();
        $aTemps    = UserGroupCustom::where('user_group_id', $id)->get()->toArray();
        if (!empty($aTemps)) {
            foreach ($aTemps as $aTemp) {
                $aCustoms[$aTemp['setting_id']] = $aTemp['value'];
            }
        }
        return view('backend.module.group.setting', ['aRow' => $aRow, 'aSettings' => $aSettings, 'aCustoms' => isset($aCustoms) ? $aCustoms : array()]);
    }

    public function updateSetting(Request $request, $id)
    {
        $aRequest = $request->input('val');
        if (!empty($aRequest)) {
            foreach ($aRequest as $iKey => $value) {
                if ($value == '') {
                    continue;
                }
                UserGroupCustom::where('setting_id', $iKey)->where('user_group_id', '=', (int) $id)->delete();
                $iSettingId = UserGroupCustom::create([
                    'setting_id'    => $iKey,
                    'user_group_id' => (int) $id,
                    'value'         => $value,
                ]);
            }
            return redirect(url('fs/cpanel/group/setting/' . $id))->with('success', 'Cập nhật cài đặt nhóm người dùng thành công');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aRow = Group::where('user_group_id', $id)->first();
        return view('backend.module.group.add', ['aRow' => $aRow]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aUpdate = [
            'title'     => $request->input('title'),
            'is_active' => $request->input('is_active'),
        ];
        Group::where('user_group_id', '=', $id)
            ->update($aUpdate);

        return redirect()->route('group-index')->with('success', 'Sửa nhóm người dùng thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Group::where('user_group_id', $id)->delete()) {
            $sMessage = 'Xóa nhóm người dùng thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa nhóm người dùng thất bại';
            $sStatus  = 'error';
        }
        return redirect()->route('group-index')->with($sStatus, $sMessage);
    }
}
