<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\User;
use Hash;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->prefix = 'fs_';
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aReturn = json_encode(User::where('user_group_id', '=', 7)->get()->toArray());
        return view('backend.module.customer.index', compact('aReturn'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.module.customer.add');
    }

    public function postAdd(CustomerRequest $request)
    {
        if (User::where('user_name', '=', $request->username)->exists()) {
            return back()->with('error', __('Tài khoản đã được tồn tại'));
        } else {
            // User group customer = 7
            $cus                    = new User();
            $cus->user_name         = $request->username;
            $cus->full_name         = $request->fullname;
            $cus->password          = Hash::make($request->password);
            $cus->email             = $request->email;
            $cus->address           = $request->address;
            $cus->phone             = $request->phone;
            $cus->status            = $request->status;
            $cus->apply_default_fee = $request->apply_fee_default;
            $cus->customer_code     = $request->customer_code;
            $cus->user_group_id     = 7;
            $cus->save();
            return redirect()->route('customer-index')->with('success', __('Tạo tài khoản thành công'));

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $aEdit = User::find(intval($request->id));
        return view('backend.module.customer.edit', compact('aEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $cus                    = User::find($request->id);
        $cus->user_name         = $request->user_name;
        $cus->full_name         = $request->fullname;
        $cus->email             = $request->email;
        $cus->address           = $request->address;
        $cus->phone             = $request->phone;
        $cus->status            = $request->status;
        $cus->apply_default_fee = !empty($request->apply_fee_default) ? 1 : 0;
        $cus->customer_code     = $request->customer_code;
        $cus->save();
        return redirect()->route('customer-index')->with('success', __('Cập nhật thông tin thành công'));
    }

    public function delete(Request $request)
    {
        User::destroy($request->id);
        return redirect()->route('customer-index')->with('success', __('Xóa thành công'));
    }

    public function generate(Request $request)
    {
        $id            = intval($request->id);
        $pw            = $request->password;
        $cus           = User::find($id);
        $cus->password = Hash::make($pw);
        if ($cus->save()) {
            return response()->json([
                'status'  => '201',
                'message' => 'Reset mật khẩu thành công',
            ]);
        }
    }
    public function priceDefault(Request $request)
    {
        $cus                    = User::find($request->id);
        $cus->apply_default_fee = $request->apply_fee_default;
        $cus->save();
        return redirect()->route('customer-index')->with('success', __('Cài đặt giá mặc định thành công'));
    }
}
