<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ExpressDelivery;
use App\ExpressDeliveryAddon;
use App\DeliveryOfSavingsSettings;

use App\ExpressDeliveryInterprovincial;
use App\ExpressDeliveryInterprovincialAddon;



class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $aExpressDelivery = ExpressDelivery::get()->toArray();
        $aExpressDeliveryAddon = ExpressDeliveryAddon::get()->toArray();
        $ExpressDeliveryInterprovincial = ExpressDeliveryInterprovincial::get()->toArray();
        $ExpressDeliveryInterprovincialAddon = ExpressDeliveryInterprovincialAddon::get()->toArray();
        $aDeliveryOfSavingsSettings = DeliveryOfSavingsSettings::get()->toArray();

        
        return view('backend.module.order.settings-area',compact('aExpressDelivery','aExpressDeliveryAddon','ExpressDeliveryInterprovincial','ExpressDeliveryInterprovincialAddon','aDeliveryOfSavingsSettings'));
    }

    public function postExpressDelivery(Request $request)
    {
        try {
            $checkExitst = ExpressDelivery::where('package_code','=',$request->input('services_code'))->first();
            if(!$checkExitst){
                $express = new ExpressDelivery();

                if( !empty($request->input('services_name')) ){
                    $express->package_name = $request->input('services_name');
                }
                if( !empty($request->input('services_name')) ){
                    $express->package_code = $request->input('services_code');
                }

                
                $express->kg = $request->input('kg') ? $request->input('kg') : 0;
                $express->internal = !empty($request->input('internal_price')) ? $request->input('internal_price') : 0;
                $express->external = !empty($request->input('external_price')) ? $request->input('external_price') : 0;
                $express->external2 = !empty($request->input('external_price_2')) ? $request->input('external_price_2') : 0;

                $express->save();
                return back()->with('success', __('Thêm dịch vụ thành công'));
            }else{
                return back()->with('error', __('Dịch vụ này đã tồn tại trong hệ thống.'));
            }
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    public function getExpressDelivery(Request $request)
    {
        try {
            $checkExitst = ExpressDelivery::where('id','=',$request->input('id'))->first();
            return json_encode($checkExitst);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateExpressDelivery(Request $request)
    {
        try {
            $id = $request->input('id_services');
            $express = ExpressDelivery::find($id);

            if( !empty($request->input('services_name')) ){
                $express->package_name = $request->input('services_name');
            }
            if( !empty($request->input('services_name')) ){
                $express->package_code = $request->input('services_code');
            }

            $express->kg = $request->input('kg') ? $request->input('kg') : 0;
            $express->internal = !empty($request->input('internal_price')) ? $request->input('internal_price') : 0;
            $express->external = !empty($request->input('external_price')) ? $request->input('external_price') : 0;
            $express->external2 = !empty($request->input('external_price_2')) ? $request->input('external_price_2') : 0;

            $express->save();
            return back()->with('success', __('Cập nhật dịch vụ thành công'));            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteExpressDelivery(Request $request)
    {
        try {
            $id = $request->input('id');
            $express = ExpressDelivery::find($id)->delete();

            return back()->with('success', __('Xóa thành công'));            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    
    public function getExpressDeliveryAddon(Request $request)
    {
        try {
            $id = $request->input('id');
            $expressAddon = ExpressDeliveryAddon::find($id);
            if(unserialize($expressAddon['services_price'])){
                foreach(unserialize($expressAddon['services_price']) as $k => $aVal)
                {
                    $expressAddon->$k = $aVal;
                }
            }

            return $expressAddon;
        } catch (Exception $e) {
            return $e->getMessage();
            
        }
    }

    public function postExpressDeliveryAddon(Request $request)
    {
        try {
            $expressAddon = new ExpressDeliveryAddon();
            if(!empty($request->input('services_name'))){
                $expressAddon->services_name = $request->input('services_name');
            }
            if(!empty($request->input('services_code'))){
                $expressAddon->services_code = $request->input('services_code');
            }
            $aServicesPrice = array(
                'condition' => $request->input('condition'),
                'services_price' => $request->input('services_price'), // services_price = 0 ? 'free' : services_price
                'percent' => $request->input('percent_declaration_value')
            ); 
            
            if(!empty($request->input('services_name'))){
                $expressAddon->services_price = serialize($aServicesPrice);
            }

            if(!empty($request->input('note'))){
                $expressAddon->note = $request->input('note');
            }
            
            $expressAddon = $expressAddon->save();

            if($expressAddon){
                return back()->with('success', __('Thêm dịch vụ cộng thêm thành công'));
            }else{
                return back()->with('error', __('Có lỗi xảy ra'));
            }
        } catch (Exception $e) {
            return $e->getMessage();
            
        }
    }

    public function updateExpressDeliveryAddon(Request $request)
    {
        try {
            $id = $request->input('id_services_addon');
            $expressAddon = ExpressDeliveryAddon::find($id);

            if( !empty($request->input('services_name')) ){
                $expressAddon->services_name = $request->input('services_name');
            }
            if( !empty($request->input('services_name')) ){
                $expressAddon->services_code = $request->input('services_code');
            }

            $aServicesPrice = array(
                'condition' => $request->input('condition'),
                'services_price' => $request->input('services_price'), // services_price = 0 ? 'free' : services_price
                'percent' => $request->input('percent_declaration_value')
            ); 
            
            if(!empty($request->input('services_name'))){
                $expressAddon->services_price = serialize($aServicesPrice);
            }

            if(!empty($request->input('note'))){
                $expressAddon->note = $request->input('note');
            }
            
            $expressAddon = $expressAddon->save();

            return back()->with('success', __('Cập nhật dịch vụ cộng thêm thành công'));            
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }
    public function deleteExpressDeliveryAddon(Request $request)
    {
        try {
            $id = $request->input('id');
            $express = ExpressDeliveryAddon::find($id)->delete();
            return back()->with('success', __('Xóa thành công'));     
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }
    
    public function postExpressDeliveryInterprovincial(Request $request)
    {
        try {
            $expressDeliveryInterprovincial = new ExpressDeliveryInterprovincial();
            $internal = array(
                'center' => $request->input('internal_center_1_2'),
                'ward' => $request->input('internal_ward_2_3'),
            );
            $external = array(
                'center' => $request->input('external_center_2_3'),
                'ward' => $request->input('external_ward_3_4')
            );

            $special = array(
                'center' => $request->input('special_center_1'),
                'ward' => $request->input('special_center_1_dot_5')
            );
            
            if(!empty( $request->input('kg') )){
                $expressDeliveryInterprovincial->kg = $request->input('kg');
            }
            
            $expressDeliveryInterprovincial->internal_area = serialize($internal);
            $expressDeliveryInterprovincial->external_area = serialize($external);
            $expressDeliveryInterprovincial->special_area = serialize($special);
            $express = $expressDeliveryInterprovincial->save();
            if($express){
                return back()->with('success', __('Thêm khu vực thành công'));            
            }else{
                return back()->with('success', __('Có lỗi xảy ra , vui lòng thử lại'));            

            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getExpressDeliveryInterprovincial(Request $request)
    {
        $id = $request->input('id');
        $aExpressDeliveryInterprovincial = ExpressDeliveryInterprovincial::find($id);
        $aReturn = array(
            'id' => $aExpressDeliveryInterprovincial['id'],
            'kg' => $aExpressDeliveryInterprovincial['kg'],
            'internal_area' => unserialize($aExpressDeliveryInterprovincial['internal_area']),
            'external_area' => unserialize($aExpressDeliveryInterprovincial['external_area']),
            'special_area' => unserialize($aExpressDeliveryInterprovincial['special_area'])
        );

        return json_encode($aReturn);
    }

    public function updateExpressDeliveryInterprovincial(Request $request)
    {
        $id = $request->input('id');
        $expressDeliveryInterprovincial = ExpressDeliveryInterprovincial::find($id);
        $internal = array(
            'center' => $request->input('internal_center_1_2'),
            'ward' => $request->input('internal_ward_2_3'),
        );
        $external = array(
            'center' => $request->input('external_center_2_3'),
            'ward' => $request->input('external_ward_3_4')
        );

        $special = array(
            'center' => $request->input('special_center_1'),
            'ward' => $request->input('special_center_1_dot_5')
        );
        
        if(!empty( $request->input('kg') )){
            $expressDeliveryInterprovincial->kg = $request->input('kg');
        }
        
        $expressDeliveryInterprovincial->internal_area = serialize($internal);
        $expressDeliveryInterprovincial->external_area = serialize($external);
        $expressDeliveryInterprovincial->special_area = serialize($special);
        $express = $expressDeliveryInterprovincial->save();
        if($express){
            return back()->with('success', __('Cập nhật thành công'));            
        }else{
            return back()->with('error', __('Có lỗi xảy ra , vui lòng thử lại'));            

        }
    }
    
    public function deleteExpressDeliveryInterprovincial(Request $request)
    {
        try {
            $id = $request->input('id');
            $express = ExpressDeliveryInterprovincial::find($id)->delete();
            return back()->with('success', __('Xóa thành công'));     
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function postExpressDeliveryInterprovincialAddon(Request $request)
    {
        try {
            $aExpressDeliveryInterprovincialAddon = new ExpressDeliveryInterprovincialAddon();
            $aExpressDeliveryInterprovincialAddon->services_name = $request->input('services_name');
            $aExpressDeliveryInterprovincialAddon->services_code = $request->input('services_code');
            $price = array(
                'condition' => $request->input('condition'),
                'services_price' => $request->input('services_price'),
                'percent_declaration_value' => $request->input('percent_declaration_value'),
                'postage' => $request->input('postage'),
                'note' => $request->input('note')
            );

            $aExpressDeliveryInterprovincialAddon->price = serialize( $price );
            $aExpressDeliveryInterprovincials = $aExpressDeliveryInterprovincialAddon->save();
            if($aExpressDeliveryInterprovincials){
                return back()->with('success', __('Thêm dịch vụ cộng thêm thành công'));     
            }else{
                return back()->with('error', __('Có lỗi xảy ra vui lòng thử lại'));     
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getExpressDeliveryInterprovincialAddon(Request $request)
    {
        try {
            $id = $request->input('id');
            $aExpressDeliveryInterprovincialAddon = ExpressDeliveryInterprovincialAddon::find($id);
            $aExpressDeliveryInterprovincialAddon['price'] = unserialize($aExpressDeliveryInterprovincialAddon['price']);
            return json_encode($aExpressDeliveryInterprovincialAddon);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateExpressDeliveryInterprovincialAddon(Request $request)
    {
        try {
            $id = $request->input('id');
            $aExpressDeliveryInterprovincialAddon = ExpressDeliveryInterprovincialAddon::find($id);

            $aExpressDeliveryInterprovincialAddon->services_name = $request->input('services_name');
            $aExpressDeliveryInterprovincialAddon->services_code = $request->input('services_code');
            $price = array(
                'condition' => $request->input('condition'),
                'services_price' => $request->input('services_price'),
                'percent_declaration_value' => $request->input('percent_declaration_value'),
                'postage' => $request->input('postage'),
                'note' => $request->input('note')
            );

            $aExpressDeliveryInterprovincialAddon->price = serialize( $price );
            $aExpressDeliveryInterprovincials = $aExpressDeliveryInterprovincialAddon->save();
            if($aExpressDeliveryInterprovincials){
                return back()->with('success', __('Cập nhật cộng thêm thành công'));     
            }else{
                return back()->with('error', __('Có lỗi xảy ra vui lòng thử lại'));     
            }
            return json_encode($aExpressDeliveryInterprovincialAddon);
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteExpressDeliveryInterprovincialAddon(Request $request)
    {
        try {
            $id = $request->input('id');
            $express = ExpressDeliveryInterprovincialAddon::find($id)->delete();
            return back()->with('success', __('Xóa thành công'));     
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function postDeliveryOfSavings(Request $request)
    {
        try {
            $aDeliveryOfSavingsSettings = new DeliveryOfSavingsSettings();
            $internal = array(
                'center' => $request->input('internal_center'),
                'ward' => $request->input('internal_ward'),
            );
            $external = array(
                'center' => $request->input('external_center'),
                'ward' => $request->input('external_ward')
            );

            $special = array(
                'center' => $request->input('special_center'),
                'ward' => $request->input('special_ward')
            );
            
            if(!empty( $request->input('kg') )){
                $aDeliveryOfSavingsSettings->kg = $request->input('kg');
            }
            
            $aDeliveryOfSavingsSettings->internal_area = serialize($internal);
            $aDeliveryOfSavingsSettings->external_area = serialize($external);
            $aDeliveryOfSavingsSettings->special_area = serialize($special);
            $express = $aDeliveryOfSavingsSettings->save();
            if($express){
                return back()->with('success', __('Thêm thành công'));     
            }else{
                return back()->with('error', __('Có lỗi xảy ra'));     

            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getDeliveryOfSavings(Request $request)
    {
        try {
            $id = $request->input('id');
            $aDeliveryOfSavingsSettings = DeliveryOfSavingsSettings::find($id);
            $aDeliveryOfSavingsSettings['internal_area'] = unserialize($aDeliveryOfSavingsSettings['internal_area']);
            $aDeliveryOfSavingsSettings['external_area'] = unserialize($aDeliveryOfSavingsSettings['external_area']);

            $aDeliveryOfSavingsSettings['special_area'] = unserialize($aDeliveryOfSavingsSettings['special_area']);

            return json_encode($aDeliveryOfSavingsSettings);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateDeliveryOfSavings(Request $request)
    {
        $id = $request->input('id');
        $aDeliveryOfSavingsSettings = DeliveryOfSavingsSettings::find($id);
        $internal = array(
            'center' => $request->input('internal_center'),
            'ward' => $request->input('internal_ward'),
        );
        $external = array(
            'center' => $request->input('external_center'),
            'ward' => $request->input('external_ward')
        );

        $special = array(
            'center' => $request->input('special_center'),
            'ward' => $request->input('special_ward')
        );
        
        if(!empty( $request->input('kg') )){
            $aDeliveryOfSavingsSettings->kg = $request->input('kg');
        }
        
        $aDeliveryOfSavingsSettings->internal_area = serialize($internal);
        $aDeliveryOfSavingsSettings->external_area = serialize($external);
        $aDeliveryOfSavingsSettings->special_area = serialize($special);
        $express = $aDeliveryOfSavingsSettings->save();
        if($express){
            return back()->with('success', __('Cập nhật thành công'));     
        }else{
            return back()->with('error', __('Có lỗi xảy ra'));     

        }
    }

    public function deleteDeliveryOfSavings(Request $request)
    {
        try {
            $id = $request->input('id');
            $express = DeliveryOfSavingsSettings::find($id)->delete();
            return back()->with('success', __('Xóa thành công'));     
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}
