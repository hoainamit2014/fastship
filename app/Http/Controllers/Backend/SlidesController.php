<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Slide;
use Illuminate\Http\Request;

class SlidesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aSlides = Slide::get();
        if (count($aSlides) == 0) {
            $aSlides = array();
        }
        return view('backend.module.slides.index', ['aSlides' => $aSlides]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.module.slides.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $image               = $request->file('image');
            $input['image_name'] = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath     = public_path('/images');
            $sImagePath          = $image->move($destinationPath, $input['image_name']);
        }
        if (!empty($request->input('slide_id'))) {
            // update
            $aUpdate = [
                'title'     => $request->input('title'),
                'subtitle'  => $request->input('subtitle'),
                'link'      => $request->input('link'),
                'is_active' => $request->input('is_active'),
            ];
            if (isset($sImagePath)) {
                $aUpdate['image_path'] = url('/public/images') . '/' . $input['image_name'];
            }
            Slide::where('slide_id', '=', $request->input('slide_id'))
                ->update($aUpdate);
            $sMessage = 'Cập nhật slide thành công';
        } else {
            // insert
            if (empty($request->input('title')) && empty($request->input('subtitle'))
                && empty($request->input('link')) && empty($sImagePath)) {
                return redirect()->route('slides-add')->with('error', 'Thêm slide thất bại');
            } else {
                Slide::create([
                    'title'      => $request->input('title'),
                    'subtitle'   => $request->input('subtitle'),
                    'link'       => $request->input('link'),
                    'image_path' => isset($sImagePath) ? url('/public/images') . '/' . $input['image_name'] : null,
                    'is_active'  => $request->input('is_active'),
                ]);
                $sMessage = 'Thêm slide thành công';
            }
        }
        return redirect()->route('slides-index')->with('success', $sMessage);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aSlide = Slide::where('slide_id', $id)->first();
        return view('backend.module.slides.add', ['aSlide' => $aSlide]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Slide::where('slide_id', $id)->delete()) {
            $sMessage = 'Xóa slide thành công';
            $sStatus  = 'success';
        } else {
            $sMessage = 'Xóa slide thất bại';
            $sStatus  = 'error';
        }
        return redirect()->route('slides-index')->with($sStatus, $sMessage);
    }
}
