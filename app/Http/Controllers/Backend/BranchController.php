<?php

namespace App\Http\Controllers\Backend;

use App\Branch;
use App\Districts;
use App\Http\Controllers\Controller;
use App\Http\Requests\BranchRequest;
use App\Province;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->prefix = 'fs_';
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch = Branch::get()->toArray();
        if ($branch) {
            foreach ($branch as $k => $val) {
                $branch[$k]['province_id'] = $this->getDistrictName($val['district_id']);
            }
        }

        $aReturn = json_encode($branch);

        return view('backend.module.branch.index', compact('aReturn'));
    }

    public function getDistrictName($id)
    {
        $district = Districts::where('district_id', $id)->first();
        return 'DC ' . $district->name;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data     = array();
        @$id      = (isset($_GET['_id']) && !empty($_GET['_id'])) ? ($_GET['_id']) : '';
        $province = $this->getProvince();

        if ($id) {
            $data['district'] = $this->getDistrict($id ? $id : 01);
            die(json_encode($data));
        }

        return view('backend.module.branch.add', compact('province', 'district', 'data'));
    }
    public function postAdd(BranchRequest $request)
    {
        $branch                       = new Branch();
        $branch->province_id          = $request['province'];
        $branch->district_id          = $request['district'];
        $branch->address              = $request['address'];
        $branch->coefficient          = $request['coefficient'];
        $branch->parent_id            = $request['parent_id'];
        $branch->sort                 = $request['sort'];
        $branch->area_responsible     = $request['area_responsible'];
        $branch->deploy_order_loading = $request['deploy_order_loading'];
        $branch->center               = $request['center_branch'];
        $branch->other_dc_interface   = $request['delivery_of_other_address'];
        $branch->save();
        return redirect()->route('branch-index')->with('success', 'Tạo DC thành công');
    }

    public function getProvince()
    {
        $province = Province::get()->toArray();
        return $province;
    }

    public function getDistrict($id)
    {
        $district = Districts::where('province_id', $id)->get()->toArray();
        return $district;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $data      = array();
        @$_id      = (isset($_GET['_id']) && !empty($request->_id)) ? ($request->_id) : '';
        $branch_id = intval($request->id);

        $province = $this->getProvince();

        if ($_id) {
            $data['district'] = $this->getDistrict($_id ? $_id : 01);
            die(json_encode($data));
        }

        $aEdit = Branch::find($branch_id);

        if ($branch_id) {
            $district = $this->getDistrict($aEdit->province_id);
        }
        // Get all branch
        $branch = Branch::get()->toArray();

        return view('backend.module.branch.edit', compact('province', 'district', 'data', 'aEdit', 'branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $branch                       = Branch::find($request->id);
        $branch->province_id          = $request['province'];
        $branch->district_id          = $request['district'];
        $branch->address              = $request['address'];
        $branch->coefficient          = $request['coefficient'];
        $branch->parent_id            = $request['parent_id'];
        $branch->sort                 = $request['sort'];
        $branch->area_responsible     = $request['area_responsible'];
        $branch->deploy_order_loading = $request['deploy_order_loading'];
        $branch->center               = $request['center_branch'];
        $branch->other_dc_interface   = $request['delivery_of_other_address'];
        $branch->save();
        return redirect()->route('branch-index')->with('success', 'Cập nhật DC thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        Branch::destroy($request->id);
        return redirect()->route('branch-index')->with('success', 'Xóa DC thành công');
    }
}
