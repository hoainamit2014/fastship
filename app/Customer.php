<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table    = 'fs_customer';
    public $timestamps  = false;
    protected $guard    = 'customer';
    protected $fillable = [
        'username', 'name', 'email', 'phone', 'customer_code', 'apply_default_fee',
    ];
    protected $hidden = [
        'password',
    ];
}
