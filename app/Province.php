<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table    = 'fs_province';
    protected $fillable = [
        'province_name', 'status',
    ];
}
