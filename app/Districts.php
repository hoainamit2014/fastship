<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    protected $table    = 'fs_district';
    protected $fillable = [
        'name', 'province_id', 'type', 'location',
    ];
}
