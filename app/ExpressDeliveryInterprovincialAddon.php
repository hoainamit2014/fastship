<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpressDeliveryInterprovincialAddon extends Model {
	protected $table = 'fs_express_delivery_interprovincial_area_addon';
	public $timestamps = false;
	protected $fillable = [
		'services_name', 'services_code', 'price'
	];
}
