<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'fs_user_group';
	public $timestamps = false;
    protected $fillable = [
        'title', 'is_active'
    ];
}
