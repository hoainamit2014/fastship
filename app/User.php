<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'fs_user';

    public $timestamps = false;

    protected $fillable = [
        'user_name', 'full_name', 'password', 'email', 'image_path', 'address', 'birthday', 'sex', 'phone', 'position', 'department', 'area', 'contract_expiry_date', 'sponsor', 'account_number', 'account_name', 'bank_branch', 'bank_name', 'identity_card', 'release_date', 'register_place', 'insurrance', 'brand', 'start_work_date', 'user_group_id', 'customer_code', 'apply_default_fee', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}
